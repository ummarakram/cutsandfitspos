      $(document).ready(function() {
        new WOW().init();

    if ($(".sortable").length > 0) {

       /* $('ol.sortable').on('click',function(){
            alert('ssss');
            }
        );*/
        $('ol.sortable').nestedSortable({
            disableNesting: 'no-nest',
            forcePlaceholderSize: true,
			disableParentChange : true,
            handle: '.icon-move',
            helper: 'clone',
            items: 'li',
            maxLevels: 6,
            opacity: .6,
            placeholder: 'placeholder',
            revert: 250,
            tabSize: 25,
            protectRoot: true,
            tolerance: 'pointer',
            toleranceElement: '> div',
            update : function () {

                $.ajax({
                    type: 'post',
                    url: 'megamenu/reorder',
                    data: {
                        order : $(this).nestedSortable('serialize'),
                        csrf_ci_auth: $.cookie('ci_auth_csrf_ci_auth')
                        }
                });
            }
        });

        $(".delete").click(function(){
            var data_href = $(this).data('href');
            var data_name = $(this).data('name');
            $('#confirmmodal').find('.modal-footer').html('<a href="' + data_href + '" class="btn red white-text waves-effect waves-light"><i class="icon-trash icon-white"></i> Delete</a>');
            $('#confirmmodal').modal('show');
            return false;
        });

        $('.sortable li div').on('mouseenter', function(){
            $('span', this).fadeIn(100);
        }).on('mouseleave', function(){
            $('span', this).fadeOut(100);
        });
    }

});
      /*$(document).ready(function(){

          $('.sortable').nestedSortable({
              handle: 'div.icon-move',
              items: 'li',
              toleranceElement: '> div'
          });

      });
*/

function search_user() {
  // Declare variables
  var input, filter, table, tr, td, i;
  input = $("#search_user");
  filter = input.val().toUpperCase();
  if(filter){
    $('#clean-icon').show();
  } else {
  	$('#clean-icon').hide();
  }
  table = document.getElementById("user_table");
  tr = table.getElementsByTagName("tr");

  // Loop through all table rows, and hide those who don't match the search query
  for (i = 0; i < tr.length; i++) {
    firstname = tr[i].getElementsByTagName("td")[0];
    lastname = tr[i].getElementsByTagName("td")[1];
    email = tr[i].getElementsByTagName("td")[2];
    if (firstname || lastname || email) {
      if (firstname.innerHTML.toUpperCase().indexOf(filter) > -1 || lastname.innerHTML.toUpperCase().indexOf(filter) > -1 || email.innerHTML.toUpperCase().indexOf(filter) > -1) {
        tr[i].style.display = "";
      } else {
        tr[i].style.display = "none";
      }
    }
  }
}