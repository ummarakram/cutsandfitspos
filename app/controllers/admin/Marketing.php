<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Marketing extends MY_Controller
{


    function __construct()
    {
        parent::__construct();
        if (!$this->loggedIn) {
            $this->session->set_userdata('requested_page', $this->uri->uri_string());
            $this->sma->md('login');
        }
        $this->data_slug= array();
        $this->lang->admin_load('products', $this->Settings->user_language);
        $this->load->library('form_validation');
        $this->load->admin_model('products_model');
        $this->load->admin_model('auth_model');
        $this->lang->admin_load('settings', $this->Settings->user_language);
        $this->digital_upload_path = 'files/';
        $this->upload_path = 'assets/uploads/';
        $this->thumbs_path = 'assets/uploads/thumbs/';
        $this->image_types = 'gif|jpg|jpeg|png|tif';
        $this->digital_file_types = 'zip|psd|ai|rar|pdf|doc|docx|xls|xlsx|ppt|pptx|gif|jpg|jpeg|png|tif|txt';
        $this->allowed_file_size = '1024';
    }

    function index($warehouse_id = NULL)
    {
        $this->sma->checkPermissions();

        $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');
        if ($this->Owner || $this->Admin || !$this->session->userdata('warehouse_id')) {
            $this->data['warehouses'] = $this->site->getAllWarehouses();
            $this->data['warehouse_id'] = $warehouse_id;
            $this->data['warehouse'] = $warehouse_id ? $this->site->getWarehouseByID($warehouse_id) : NULL;
        } else {
            $this->data['warehouses'] = NULL;
            $this->data['warehouse_id'] = $this->session->userdata('warehouse_id');
            $this->data['warehouse'] = $this->session->userdata('warehouse_id') ? $this->site->getWarehouseByID($this->session->userdata('warehouse_id')) : NULL;
        }

        $this->data['supplier'] = $this->input->get('supplier') ? $this->site->getCompanyByID($this->input->get('supplier')) : NULL;
        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('products')));
        $meta = array('page_title' => lang('products'), 'bc' => $bc);
        $this->page_construct('marketing/index', $meta, $this->data);
    }

    function product_actions($wh = NULL)
    {
        $this->sma->checkPermissions();
        if (!$this->Owner && !$this->GP['bulk_actions']) {
            $this->session->set_flashdata('warning', lang('access_denied'));
            redirect($_SERVER["HTTP_REFERER"]);
        }

        $this->form_validation->set_rules('form_action', lang("form_action"), 'required');

        if ($this->form_validation->run() == true) {

            if (!empty($_POST['val'])) {
                if($this->input->post('form_action') == 'export_google_feed'){

                    //SET SHOP VARIABLES
                    $shop_name = $this->Settings->site_name;
                    $shop_link = base_url();
                    $default_currency_code = $this->Settings->default_currency;

                    $feed_products = [];
                    //LOOP THROUGH PRODUCTS
                    foreach ($_POST['val'] as $id){
                        $product = $this->products_model->getProductDetail($id);
                        $option_price = 0.00;
                        if ($variants = $this->products_model->getProductOptions($id)) {
                            $option_price = $variants[0]->price;
                        }

                        //$category_name = $this->site->getCategoryByID($product->category_id);
                        //CREATE EMPTY ARRAY FOR GOOGLE-FRIENDLY INFO
                        $gf_product = [];

                        //FLAGS FOR LATER
                        $gf_product['is_clothing'] = False; //set True or False, depending on whether product is clothing
                        $gf_product['is_on_sale'] = False; //set True or False depending on whether product is on sale

                        //feed attributes
                        $gf_product['g:id'] = $product->id;
                        $gf_product['g:sku'] = $product->code;
                        $gf_product['g:title'] = $product->name;
                        $gf_product['g:description'] = $product->product_details;
                        $gf_product['g:link'] = base_url('product/').$product->slug;
                        $gf_product['g:image_link'] = base_url('assets/uploads/').$product->image;
                        $gf_product['g:availability'] = 'in stock';
                        $gf_product['g:price'] = $this->sma->formatDecimal($product->price + $option_price).' '.$default_currency_code;
                        $gf_product['g:google_product_category'] = 536;
                        $gf_product['g:brand'] = 'Enso';
                        $gf_product['g:gtin'] = '';
                        $gf_product['g:mpn'] = '';

                        if (($gf_product['g:gtin'] == "") && ($gf_product['g:mpn'] == "")) { $gf_product['g:identifier_exists'] = "no"; };
                        $gf_product['g:condition'] = 'NEW'; //must be NEW or USED
                        //remove this IF block if you don't sell any clothing
                        if ($gf_product['is_clothing']) {
                            $gf_product['g:age_group'] = ''; //newborn/infant/toddle/kids/adult
                            $gf_product['g:color'] = '';
                            $gf_product['g:gender'] ='';
                            $gf_product['g:size'] = '';
                        }
                        if ($gf_product['is_on_sale']) {
                            $gf_product['g:sale_price'] = '';
                            $gf_product['g:sale_price_effective_date'] = ''." ".'';
                        }

                        $feed_products[] = $gf_product;
                    }
                    //CREATE XML

                    $doc = new DOMDocument('1.0', 'UTF-8');

                    $xmlRoot = $doc->createElement("rss");
                    $xmlRoot = $doc->appendChild($xmlRoot);
                    $xmlRoot->setAttribute('version', '2.0');
                    $xmlRoot->setAttributeNS('http://www.w3.org/2000/xmlns/', 'xmlns:g', "http://base.google.com/ns/1.0");

                    $channelNode = $xmlRoot->appendChild($doc->createElement('channel'));
                    $channelNode->appendChild($doc->createElement('title', $shop_name));
                    $channelNode->appendChild($doc->createElement('link', $shop_link));

                    foreach ($feed_products as $product) {
                        $itemNode = $channelNode->appendChild($doc->createElement('item'));
                        foreach($product as $key=>$value) {
                            if ($value != "") {
                                if (is_array($product[$key])) {
                                    $subItemNode = $itemNode->appendChild($doc->createElement($key));
                                    foreach($product[$key] as $key2=>$value2){
                                        $subItemNode->appendChild($doc->createElement($key2))->appendChild($doc->createTextNode($value2));
                                    }
                                } else {
                                    $itemNode->appendChild($doc->createElement($key))->appendChild($doc->createTextNode($value));
                                }

                            } else {

                                $itemNode->appendChild($doc->createElement($key));
                            }

                        }
                    }
                    //To view out put in the browser
                    //  $doc->formatOutput = TRUE;
                    // echo $doc->saveXML();
                    //Save XML as a file
                    $filename = 'google_feed_' . date('Y_m_d_H_i_s').'.xml';
                    //$doc->save('xml/'.$filename.'.xml');

                    header('Content-type: text/xml');
                    header('Content-Disposition: attachment; filename="'.$filename.'"');
                    echo $doc->saveXML();
                    exit();


                }
            } else {
                $this->session->set_flashdata('error', $this->lang->line("no_product_selected"));
                redirect($_SERVER["HTTP_REFERER"]);
            }
        } else {
            $this->session->set_flashdata('error', validation_errors());
            redirect(isset($_SERVER["HTTP_REFERER"]) ? $_SERVER["HTTP_REFERER"] : 'marketing/');
        }
    }

    function g_analytics($warehouse_id = NULL)
    {

        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('products')));
        $meta = array('page_title' => lang('products'), 'bc' => $bc);
        $this->page_construct('marketing/analytics', $meta, $this->data);

    }

    function seo_settings($warehouse_id = NULL)
    {
       /* $record_num = end($this->uri->segment_array());
        echo $record_num;
        die;*/

        /*$page_slugs1 = $this->products_model->getMetaTitleBySlug('home');
        echo $page_slugs1;
        exit('d');*/

        $slug_array = array();
        $slug_array_data = array();
        $slug_array[]='default';
        $slug_array[]='cart';
        $slug_array[]='checkout';
        $slug_array[]='wishlist';
        $slug_array[]='login';
        $slug_array[]='register_form';
        $slug_array[]='profile';
        $slug_array[]='orders';
        $slug_array[]='quotes';
        $slug_array[]='downloads';
        $slug_array[]='addresses';
        $slug_array[]='products';
        $slug_array[]='product';
        $page_slugs = $this->products_model->getAllPagesSlug();
        $products_slugs= $this->products_model->getAllProductsSlug();
        /*echo "<pre>";
        print_r($products_slugs);
        exit('asd');*/
        $slug_array =array_merge($slug_array,$page_slugs);

        $this->getCategoryTree();
        $cat_slugs =$this->data_slug;
        $slug_array =array_merge($slug_array,$cat_slugs);
        $slug_array =array_merge($slug_array,$products_slugs);



        foreach ($slug_array as $key => $slug){
            $slug_array_data[$key]['id'] = $key + 1;
            $slug_array_data[$key]['slug'] =$slug;
            $slug_array_data[$key]['meta_title'] = $this->products_model->getMetaTitleBySlug($slug);
            $slug_array_data[$key]['meta_description'] =  $this->products_model->getMetaDescriptionBySlug($slug);

        }

       /* echo "<pre>";
        print_r($slug_array_data);
        exit('sd');*/

        $this->data['slug_array_data'] = $slug_array_data;
        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('products')));
        $meta = array('page_title' => lang('products'), 'bc' => $bc);
        $this->page_construct('marketing/seo_settings', $meta, $this->data);

    }

    function update_meta_settings(){

        if ($this->input->post()) {
            if($_POST['slug'] !=''){
                $this->db->delete('seo_settings', array('slug' => $_POST['slug']));
                $setting_data = array(
                    'slug' => $_POST['slug'],
                    'meta_title' => $_POST['meta_title'],
                    'meta_description' => $_POST['meta_description'],

                );
                $this->db->insert('seo_settings', $setting_data);

            }
            $this->session->set_flashdata('message', 'Settings Updated Successfully');
            admin_redirect("marketing/seo_settings");

        }else{
            $this->session->set_flashdata('message', 'Settings Not Updated Successfully');
            admin_redirect("marketing/seo_settings");
        }

    }

    function open_graph_settings()
    {


        $slug_array = array();
        $slug_array_data = array();
        $slug_array[]='default';
        $slug_array[]='cart';
        $slug_array[]='checkout';
        $slug_array[]='wishlist';
        $slug_array[]='login';
        $slug_array[]='register_form';
        $slug_array[]='profile';
        $slug_array[]='orders';
        $slug_array[]='quotes';
        $slug_array[]='downloads';
        $slug_array[]='addresses';
        $slug_array[]='products';
        $slug_array[]='product';
        $page_slugs = $this->products_model->getAllPagesSlug();
        $slug_array =array_merge($slug_array,$page_slugs);
        $products_slugs= $this->products_model->getAllProductsSlug();

        $this->getCategoryTree();
        $cat_slugs =$this->data_slug;
        $slug_array =array_merge($slug_array,$cat_slugs);
        $slug_array =array_merge($slug_array,$products_slugs);



        foreach ($slug_array as $key => $slug){
            $op_data = $this->products_model->getOpenGraphDataBySlug($slug);
            $op_data_seo = $this->products_model->getSeoSettingsDataBySlug($slug);
            $image = $this->products_model->getProductImageBySlug($slug);
            $slug_array_data[$key]['id'] = $key + 1;
            $slug_array_data[$key]['slug'] =$slug;
            $slug_array_data[$key]['opn_title'] = $op_data->opn_title;
            $slug_array_data[$key]['opn_description'] = $op_data->opn_description;
            // $slug_array_data[$key]['link'] = $op_data->link;
            $slug_array_data[$key]['image'] = $op_data->image;
            $slug_array_data[$key]['seo_title'] = $op_data_seo->meta_title;
            $slug_array_data[$key]['seo_description'] = $op_data_seo->meta_description;
            $slug_array_data[$key]['main_image'] = $image;

        }


        $this->data['slug_array_data'] = $slug_array_data;
        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('products')));
        $meta = array('page_title' => lang('products'), 'bc' => $bc);
        $this->page_construct('marketing/open_graph_settings', $meta, $this->data);

    }

    function update_open_graph_settings(){

        if ($this->input->post()) {
            if($_POST['slug'] !=''){

                    $this->db->delete('opengraph_settings', array('slug' => $_POST['slug']));
                    $setting_data = array(
                        'slug' => $_POST['slug'],
                        'image' => $_POST['image'],
                        'image_alt' => $_POST['image_alt'],
                        'opn_title' => $_POST['opn_title'],
                        'opn_description' => $_POST['opn_description'],
                    );
                    $this->db->insert('opengraph_settings', $setting_data);

            }
            $this->session->set_flashdata('message', 'Settings Updated Successfully');
            admin_redirect("marketing/open_graph_settings");

        }else{
            $this->session->set_flashdata('message', 'Settings Not Updated Successfully');
            admin_redirect("marketing/open_graph_settings");
        }

    }


    protected function getCategoryTree($level = 0, $prefix = '') {
        $cate_array=array();
        $rows = $this->db
            ->select('id,parent_id,name,slug')
            ->where('parent_id', $level)
            ->order_by('id','asc')
            ->get('categories')
            ->result();

        $category = '';
        if (count($rows) > 0) {
            foreach ($rows as $row) {
                $cate_array[]= $row->slug;
                $this->data_slug[] = $row->slug;
                $category .= $prefix . $row->name . "\n";
                // Append subcategories
                $category .= $this->getCategoryTree($row->id, $prefix . '-');
            }
        }

        return $category;
    }

    public function printCategoryTree() {
        echo $this->getCategoryTree();
    }





    public function savegooglecode()
    {
        // $db = get_instance()->db->conn_id;
        //  $val = mysqli_real_escape_string($db, $this->input->post('email'));
        $this->db->where('setting_id', 1);
        if ($this->db->update('settings', array('google_analytic_script' => $this->input->post('gdata')))) {
        }
        $this->session->set_flashdata('message', lang('setting_updated'));
        return true;
    }

    public function savegooglecode_head()
    {


        $this->db->where('setting_id', 1);
        if ($this->db->update('settings', array('google_analytic_script_head' => $this->input->post('gdata_head')))) {
        }
        $this->session->set_flashdata('message', lang('setting_updated'));
        return true;
    }


    function genrate_sitemap(){

        //To fetch all the active products URL
        $products = $this->products_model->getAllProductsSlug();
        foreach($products as $product_url){
            $products_url[] =  base_url('product/').$product_url;
        }

        //To fetch all the Pages URL
        $pages= $this->products_model->getAllPagesSlug();
        foreach($pages as $page_url){
            $pages_url[] = base_url('page/').$page_url;
        }

        //To fetch all the Categories URL
        $categories = $this->products_model->getAllCategoriesSlug();
        foreach($categories as $category_url){
            $categories_url[] = base_url('category/').$category_url;
        }

        $site_map = '';

        //CREATE XML
        header('Content-type: text/xml');
        header('Pragma: public');
        header('Cache-control: private');
        header('Expires: -1');

        $site_map .= '<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd">';

        $site_map .= '<url>';
        $site_map .= '<loc>'.base_url().'</loc>';
        $site_map .= '<lastmod>' . date('Y-m-d').'T'. date('H:i:s').'+00:00' . '</lastmod>' . '<priority>' .'1.00'. '</priority>';
        $site_map .= '</url>';

        //All the pages URL
        foreach($pages_url as $rec){
            $site_map .= '<url>';
            $site_map .= '<loc>'.$rec.'</loc>';
            $site_map .= '<lastmod>' . date('Y-m-d').'T'. date('H:i:s').'+00:00' . '</lastmod>' . '<priority>' .'0.80'. '</priority>';
            $site_map .= '</url>';
        }

        //All the Category URL
        foreach($categories_url as $rec){
            $site_map .= '<url>';
            $site_map .= '<loc>'.$rec.'</loc>';
            $site_map .= '<lastmod>' . date('Y-m-d').'T'. date('H:i:s').'+00:00' . '</lastmod>' . '<priority>' .'0.80'. '</priority>';
            $site_map .= '</url>';
        }

        //All the Active Products URL
        foreach($products_url as $rec){
            $site_map .= '<url>';
            $site_map .= '<loc>'.$rec.'</loc>';
            $site_map .= '<lastmod>' . date('Y-m-d').'T'. date('H:i:s').'+00:00' . '</lastmod>' . '<priority>' .'0.80'. '</priority>';
            $site_map .= '</url>';
        }
        $site_map .= '</urlset>';

        //To save the file on the root directory
        $fp = fopen("sitemap.xml","wb");
        fwrite($fp,$site_map);
        fclose($fp);

        $message = 'Sitemap genreated successfully.<br>  '.base_url().'sitemap.xml';
        $this->session->set_flashdata('message', $message);
        redirect($_SERVER["HTTP_REFERER"]);
    }

    function subscribers()
    {

        $this->data['error'] = validation_errors() ? validation_errors() : $this->session->flashdata('error');

        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => admin_url('marketing/subscribers'), 'page' => lang('marketing')), array('link' => '#', 'page' => 'Subscribers'));
        $meta = array('page_title' => 'Subscribers', 'bc' => $bc);
        $this->page_construct('marketing/subscribers', $meta, $this->data);
    }
    function add_subscriber()
    {

        $this->form_validation->set_rules('email', 'Email', 'trim|is_unique[subscribers.email]|required');

        if ($this->form_validation->run() == true) {
            $data = array(
                'email' => $this->input->post('email'),
                'status' => $this->input->post('status'),
            );
        } elseif ($this->input->post('add_subscriber')) {
            $this->session->set_flashdata('error', validation_errors());
            admin_redirect("marketing/subscribers");
        }

        if ($this->form_validation->run() == true && $this->auth_model->addSubscriber($data)) { //check to see if we are creating the customer
            $this->session->set_flashdata('message', 'Subscriber Added Successfully');
            admin_redirect("marketing/subscribers");
        } else {
            $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
            $this->data['modal_js'] = $this->site->modal_js();
            $this->data['page_title'] = 'New Subscriber';
            $this->load->view($this->theme . 'marketing/add_subscriber', $this->data);
        }
    }
    function getSubscribers()
    {
        $this->load->library('datatables');
        $this->datatables
            ->select("id, email, IF(status>0,'Enable','Disable') ")
            ->from("subscribers")
            ->add_column("Actions", "<div class=\"text-center\"><a href='" . admin_url('marketing/edit_subscriber/$1') . "' class='tip' title='" . 'Edit Subscriber' . "' data-toggle='modal' data-target='#myModal'><i class=\"fa fa-edit\"></i></a> <a href='#' class='tip po' title='<b>" . 'Delete Subscriber' . "</b>' data-content=\"<p>" . lang('r_u_sure') . "</p><a class='btn btn-danger po-delete' href='" . admin_url('marketing/delete_subscriber/$1') . "'>" . lang('i_m_sure') . "</a> <button class='btn po-close'>" . lang('no') . "</button>\"  rel='popover'><i class=\"fa fa-trash-o\"></i></a></div>", "id");
        //->unset_column('id');

        echo $this->datatables->generate();
    }
    function edit_subscriber($id = NULL)
    {
        $cur_details = $this->auth_model->getSubscriberByID($id);
        if ($this->input->post('email') != $cur_details->email) {
            $this->form_validation->set_rules('email','email', 'required|is_unique[subscribers.email]');
        }
        $this->form_validation->set_rules('status', 'status', 'required');

        if ($this->form_validation->run() == true) {

            $data = array(
                'email' => $this->input->post('email'),
                'status' => $this->input->post('status'),
            );
        } elseif ($this->input->post('edit_subscriber')) {
            $this->session->set_flashdata('error', validation_errors());
            admin_redirect("marketing/subscribers");
        }

        if ($this->form_validation->run() == true && $this->auth_model->updateSubscriber($id, $data)) { //check to see if we are updateing the customer
            $this->session->set_flashdata('message','Subscriber Updated Successfully');
            admin_redirect("marketing/subscribers");
        } else {
            $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
            $this->data['subscriber'] = $this->auth_model->getSubscriberByID($id);
            $this->data['id'] = $id;
            $this->data['modal_js'] = $this->site->modal_js();
            $this->load->view($this->theme . 'marketing/edit_subscriber', $this->data);
        }
    }
    function delete_subscriber($id = NULL)
    {

        if ($this->auth_model->deleteSubscriber($id)) {
            $this->sma->send_json(array('error' => 0, 'msg' => 'Subscriber Deleted Successfully'));
        }
    }
    function subscriber_actions()
    {

        $this->form_validation->set_rules('form_action', lang("form_action"), 'required');

        if ($this->form_validation->run() == true) {

            if (!empty($_POST['val'])) {
                if ($this->input->post('form_action') == 'delete') {
                    foreach ($_POST['val'] as $id) {
                        $this->auth_model->deleteSubscriber($id);
                    }
                    $this->session->set_flashdata('message','Subscribers are deleted successfully');
                    redirect($_SERVER["HTTP_REFERER"]);
                }
            } else {
                $this->session->set_flashdata('error', lang("no_record_selected"));
                redirect($_SERVER["HTTP_REFERER"]);
            }
        } else {
            $this->session->set_flashdata('error', validation_errors());
            redirect($_SERVER["HTTP_REFERER"]);
        }
    }

}