<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Shop extends MY_Shop_Controller
{

    function __construct() {
        parent::__construct();
        if ($this->Settings->mmode) { redirect('notify/offline'); }
        $this->load->library('form_validation');
        $this->load->admin_model('products_model');
        $this->load->library('encrypt');
    }

    // Display Page
    function page($slug) {
        $page = $this->shop_model->getPageBySlug($slug);
        $this->data['page'] = $page;
        $this->data['page_title'] = $page->title;
        $this->data['page_desc'] = $page->description;

            $this->load->helper('captcha');
            $vals = array(
                'img_path' => './assets/captcha/',
                'img_url' => base_url('assets/captcha/'),
                'img_width' => 210,
                'img_height' => 34,
                'word_length' => 5,
                'colors' => array('background' => array(255, 255, 255), 'border' => array(204, 204, 204), 'text' => array(102, 102, 102), 'grid' => array(204, 204, 204))
            );
            $cap = create_captcha($vals);
            $capdata = array(
                'captcha_time' => $cap['time'],
                'ip_address' => $this->input->ip_address(),
                'word' => $cap['word']
            );

            $query = $this->db->insert_string('captcha', $capdata);
            $this->db->query($query);
        $this->data['image'] = $cap['image'];
        $this->data['captcha'] = array('name' => 'captcha',
                'id' => 'captcha',
                'type' => 'text',
                'class' => 'form-control',
                'required' => 'required',
                'placeholder' => lang('type_captcha')
            );


        $this->page_construct('pages/page', $this->data);
    }

    function contact_us(){
        $this->form_validation->set_rules('name', 'Name', 'trim|required');
        $this->form_validation->set_rules('email', 'Email', 'trim|required');
        $this->form_validation->set_rules('enquiry', 'Enquiry', 'trim|required');

        if ($this->form_validation->run() == true) {

            if ($address = $this->shop_model->getAddressByID($this->input->post('address'))) {
                $customer = $this->site->getCompanyByID($this->session->userdata('company_id'));
                $biller = $this->site->getCompanyByID($this->shop_settings->biller);
                $note = $this->db->escape_str($this->input->post('comment'));
            }
        } else {
            $this->session->set_flashdata('error', validation_errors());
            redirect('page/contact-us');
        }


    }


    // Display Page
    function product($slug) {
        $promotion =0;
        $how_much_off=0;
        $how_much_off_cart_notify=0;
        $how_much_off_cart_promotion=0;
        $how_much_off_cart_percentage=0;




        $product = $this->shop_model->getProductBySlug($slug);
        if (!$slug || !$product) {
            $this->session->set_flashdata('error', lang('product_not_found'));
            $this->sma->md('/');
        }
        $this->data['barcode'] = "<img src='" . admin_url('products/gen_barcode/' . $product->code . '/' . $product->barcode_symbology . '/40/0') . "' alt='" . $product->code . "' class='pull-left' />";
        if ($product->type == 'combo') {
            $this->data['combo_items'] = $this->shop_model->getProductComboItems($product->id);
        }
        $this->shop_model->updateProductViews($product->id, $product->views);
        $this->data['product'] = $product;
        $this->data['unit'] = $this->site->getUnitByID($product->unit);
        $this->data['brand'] = $this->site->getBrandByID($product->brand);
        $this->data['images'] = $this->shop_model->getProductPhotos($product->id);
        $this->data['category'] = $this->site->getCategoryByID($product->category_id);
        $this->data['subcategory'] = $product->subcategory_id ? $this->site->getCategoryByID($product->subcategory_id) : NULL;
        $this->data['tax_rate'] = $product->tax_rate ? $this->site->getTaxRateByID($product->tax_rate) : NULL;
       // $this->data['warehouse'] = $this->shop_model->getAllWarehouseWithPQ($product->id);
        //$this->data['options'] = $this->shop_model->getProductOptionsWithWH($product->id);
        $this->data['variants'] = $this->shop_model->getProductOptions($product->id);
        $this->data['saved_variants'] = $this->products_model->getAllSavedVariants($product->id);

        $variantsshop = $this->shop_model->getProductOptions($product->id);
        $this->data['variableimages']= $this->products_model->getVariableImages($product->id);

        $saved_variants = $this->products_model->getAllSavedVariants($product->id);
        $variantp = $this->products_model->getAllVariants();

        $saved_combinations = array();
        $saved_variableofcombinations = array();
        $saved_combinations_with_price = array();
        $saved_variants_names = array();
        $product_variants = array();
        $product_variants =  $this->products_model->getProductOptionsForWebsite($product->id);
        $reserved_variants = array();
        if($this->Settings->country_price_group == 0) {


            if ($this->Settings->country_vat == 1) {
        $tax_rate1 = $this->site->getTaxRateByID($product->tax_rate);
        $ctax1 = $this->site->calculateTax($product, $tax_rate1, $product->price);
        $tax1 = $this->sma->formatDecimal($ctax1['amount']);
        $price1 = $this->sma->formatDecimal($product->price);
        $product->price = $this->sma->formatDecimal($product->tax_method ? $price1+$tax1 : $price1);
            }

            foreach ($product_variants as $key => $value) {
                $variant = $this->products_model->VariantSavedGetByNamePid($value->name, $value->product_id);
                $variant_selected = $this->products_model->getReserveVariant($variant, $value->product_id);
                if ($variant_selected) {
                    $reserved_variants[] = $variant_selected;
                    $product_variants[$key]->del_check = 0;
                } else {
                    $product_variants[$key]->del_check = 1;
                }
                if ((int)$value->quantity > 0) {
                    $saved_combinations[] = $value->name;
                    $saved_combinations_with_price[$key]['name'] = $value->name;
                    if ($this->Settings->country_vat == 1) {

                        $tax_rate = $this->site->getTaxRateByID($product->tax_rate);
                        $ctax = $this->site->calculateTax($product, $tax_rate, $value->price);
                        $tax = $this->sma->formatDecimal($ctax['amount']);
                        $price = $this->sma->formatDecimal($value->price);
                        $unit_price = $this->sma->formatDecimal($product->tax_method ? $price + $tax : $price);
                        $pricenew = $product->price + $unit_price;
                    } else {

                        $pricenew = $product->price + $value->price;
                    }

                    $account_POS_settings = $this->site->getPosSettings();
                    if ($account_POS_settings->rounding != 0) {
                        $pricenew = $this->sma->roundNumber($pricenew, $account_POS_settings->rounding);
                    }
                    $saved_combinations_with_price[$key]['price'] = $this->sma->convertMoney($pricenew);

                    // $saved_combinations_with_price[$key]['price']= number_format($pricenew,2);
                    $saved_comb_values = explode(' / ', $value->name);
                    foreach ($saved_comb_values as $val) {
                        $val = str_replace("/", "", $val);
                        $saved_variableofcombinations[] = $val;
                    }
                }


            }
        }else{

            $product_group_price_data= $this->site->getProductGroupPrice($product->id,$this->Settings->country_price_group);
            if( $product_group_price_data->price !='') {
                $product->price = $product_group_price_data->price;
            }else{

            }

            if ($this->Settings->country_vat == 1) {
                $tax_rate1 = $this->site->getTaxRateByID($product->tax_rate);
                $ctax1 = $this->site->calculateTax($product, $tax_rate1, $product->price);
                $tax1 = $this->sma->formatDecimal($ctax1['amount']);
                $price1 = $this->sma->formatDecimal($product->price);
                $product->price = $this->sma->formatDecimal($product->tax_method ? $price1 + $tax1 : $price1);
            }

        //Calucalte if this product(category) on promotion with custom module
        $category_id = $this->site->getCategoryIdByProductID($product->id);
        $how_much_off_new = $this->site->getPromotionPercentage('buyCategoryWithPercentage',$product->id, $category_id);
        //Calucalte if this multiple products on promotion with custom module
        $how_much_off = $this->site->getPromotionPercentage('buyProductsWithPercentage',$product->id, $category_id);

        foreach ($product_variants as $key => $value){
            $variant = $this->products_model->VariantSavedGetByNamePid($value->name,$value->product_id);
            $variant_selected = $this->products_model->getReserveVariant($variant,$value->product_id);
            if($variant_selected){
                $reserved_variants[]= $variant_selected;
                $product_variants[$key]->del_check = 0;
            }else{
                $product_variants[$key]->del_check = 1;
            }
            if( (int)$value->quantity > 0){
                $saved_combinations[]=$value->name;
                $saved_combinations_with_price[$key]['name']= $value->name;

                    $product_group_price_variant_data= $this->site->getProductGroupPrice($product->id,$this->Settings->country_price_group,$value->id);
                    if( $product_group_price_variant_data->price !='') {
                        $value->price = $product_group_price_variant_data->price;
                    }
                   // $product->price= $product_group_price_data->price;

                    if ($this->Settings->country_vat == 1) {

                $tax_rate = $this->site->getTaxRateByID($product->tax_rate);

                $ctax = $this->site->calculateTax($product, $tax_rate, $value->price);
                $tax = $this->sma->formatDecimal($ctax['amount']);
                $price = $this->sma->formatDecimal($value->price);
                $unit_price = $this->sma->formatDecimal($product->tax_method ? $price+$tax : $price);
                        if( $product_group_price_variant_data->price !='') {
                            $pricenew =  $unit_price;
                        }else{
                $pricenew = $product->price + $unit_price;
                        }
                    } else {
                        if( $product_group_price_variant_data->price !='') {
                            $pricenew =  $value->price;
                        }else{
                            $pricenew = $product->price + $value->price;
                        }
                    }

                $account_POS_settings = $this->site->getPosSettings();
                if($account_POS_settings->rounding != 0) {
                    $pricenew = $this->sma->roundNumber($pricenew,$account_POS_settings->rounding);
                }

                if($pricenew >= 1000){
                    $price_format =str_pad($pricenew, 4, '0', STR_PAD_LEFT);
                  }else{
                $price_format = number_format($pricenew,2);
                }


                $saved_combinations_with_price[$key]['price'] = $price_format;
                    $saved_combinations_with_price[$key]['price'] = $this->sma->convertMoney($saved_combinations_with_price[$key]['price']);

                    $price_format =  $this->sma->convertMoney($price_format);
                    $price_format =  str_replace($this->Settings->current_currency_symbol,"",$price_format);
                    $saved_combinations_with_price[$key]['price_format'] = $price_format;



                    if($how_much_off_new){
                         $promo_price = $this->site->calCulateProductPriceWithPromotionPercentagePos(str_replace(',','',$price_format) , $how_much_off_new);

                        $saved_combinations_with_price[$key]['promo'] = $promo_price;
                        $saved_combinations_with_price[$key]['price_save'] = str_replace(',','',$price_format) - $promo_price;
                        $saved_combinations_with_price[$key]['currency_symbol'] = $this->Settings->current_currency_symbol ;
                    }else if($how_much_off){
                        $promo_price = $this->site->calCulateProductPriceWithPromotionPercentagePos(str_replace(',','',$price_format) , $how_much_off);
                        $saved_combinations_with_price[$key]['promo'] = $promo_price;
                        $saved_combinations_with_price[$key]['price_save'] = str_replace(',','',$price_format) - $promo_price;
                        $saved_combinations_with_price[$key]['currency_symbol'] = $this->Settings->current_currency_symbol ;
                    } else{
                        $saved_combinations_with_price[$key]['promo'] = 0;
                    }


               // $saved_combinations_with_price[$key]['price']= number_format($pricenew,2);
                $saved_comb_values= explode(' / ',$value->name);
                foreach ($saved_comb_values as $val){
                    $val =str_replace("/","",$val);
                    $saved_variableofcombinations[] = $val;
                }
            }


        }
        }

        $savedvariables = array();
        foreach ($saved_variants as $keys => $values){
            if($saved_variants[$keys]['variant_value'] !=''){
                $savedvariables =  explode(',',$saved_variants[$keys]['variant_value']);

            }
            if(!empty($savedvariables)){
                //   $saved_variants_names[] =  explode(' / ',$savedvariables);
                $saved_variants_names[]= $savedvariables;
            }


        }

        $how_much_off_cart = $this->site->getPromotionPercentage('cartvaluerangeFree');

        if(!$how_much_off_cart) {

        if($how_much_off) {
            $promotion =1;
        }

        if($how_much_off_new) {
            $promotion =1;
            $how_much_off = $how_much_off_new;
        }
        }else{
            $how_much_off_cart_notify = 1;
            $how_much_off_cart_percentage = $how_much_off_cart;
            $how_much_off_cart_promotion = $this->site->getPromotionCartRange('cartvaluerangeFree');
        }



        $this->data['saved_variableofcombinations'] = $saved_variableofcombinations;
        $this->data['product_variants'] = $product_variants;
        $this->data['saved_combinations_with_price'] = $saved_combinations_with_price;
        $this->data['saved_variants'] = $saved_variants;
        $this->data['saved_combinations'] = $saved_combinations;
        $this->data['saved_variants_names'] = $saved_variants_names;
        $this->data['promotion'] = $promotion;
        $this->data['how_much_off'] = $how_much_off;
        $this->data['how_much_off_cart_promotion'] = $how_much_off_cart_promotion;
        $this->data['how_much_off_cart_notify'] = $how_much_off_cart_notify;
        $this->data['how_much_off_cart_percentage'] = $how_much_off_cart_percentage;

        $this->load->helper('text');
        $this->data['page_title'] = $product->code.' - '.$product->name;
        $this->data['page_desc'] = character_limiter(strip_tags($product->product_details), 200);
        $this->page_construct('pages/view_product', $this->data);
    }

    function product_size_guide($size_id){
        $guide_data = $this->site->getSizingGuideDetailByID($size_id);
        $guide_data->heading = $guide_data->title;

            $guide_data->tab_heading = '<ul class="nav nav-tabs" role="tablist">
                            <li role="presentation" class="active">
                                <a href="#1chart" aria-controls="sizecChart" role="tab"
                                   data-toggle="tab"> '.$guide_data->tab1_label.' </a>
                            </li>
                            <li role="presentation" class=" '.$guide_data->tab2_label.' == \'\' ? \'hide\' : \'\' ">
                                <a href="#2chart" aria-controls="sizecChart" role="tab"
                                   data-toggle="tab">'.$guide_data->tab2_label.'</a>
                            </li>
                            <li role="presentation" class=" '.$guide_data->tab3_label.' == \'\' ? \'hide\' : \'\' ?>">
                                <a href="#3chart" aria-controls="sizecChart" role="tab"
                                   data-toggle="tab">'.$guide_data->tab3_label.'</a>
                            </li>
                            <li role="presentation" class=" '.$guide_data->tab4_label.' == \'\' ? \'hide\' : \'\' ?>">
                                <a href="#4chart" aria-controls="sizecChart" role="tab"
                                   data-toggle="tab">'.$guide_data->tab4_label.'</a>
                            </li></ul>';

        $guide_data->tab_description = '<div class="tab-content">
                            <div role="tabpanel" class="tab-pane active" id="1chart"><br/>
                                '.$guide_data->tab1_description.'
                            </div>
                            <div role="tabpanel" class="tab-pane " id="2chart"><br/>
                                '.$guide_data->tab2_description.'
                            </div>
                            <div role="tabpanel" class="tab-pane " id="3chart"><br/>
                                '.$guide_data->tab3_description.'
                            </div>
                            <div role="tabpanel" class="tab-pane " id="4chart"><br/>
                                '.$guide_data->tab4_description.'
                            </div>
                        </div>';

        echo json_encode($guide_data);
        exit();
    }


    function product_quickview($product_id){
        $product_data = $this->shop_model->getProductByIDForQuickView($product_id);
        if($product_data->show_variable_price_on_shop){
            $product_variable_prices = $this->site->getProductVariableLowestAndHeighestPrice($product_data->id);
            $product_data->price = str_replace('.0000','',$product_variable_prices->lowest) .'-'. str_replace('.0000','',$product_variable_prices->highest) . ' AED';
        }else{
        $product_data->price = $this->sma->convertMoney($product_data->price) . ' AED';
        }

        if($product_data->promotion > 0){
            $product_data->promo_price =  $this->sma->convertMoney($product_data->promo_price) . ' AED';
            $product_data->save_amount = $this->sma->convertMoney($product_data->price - $product_data->promo_price). ' AED';
        }
        $product_data->details = strlen($product_data->details) > 150 ? substr($product_data->details, 0 , 150). '...' : $product_data->details ;
        $product_data->quantity = str_replace('.0000','', $product_data->quantity);
        $variants = $this->shop_model->getProductVariants($product_id);
        if ($variants) {
            foreach ($variants as $variant) {
                $opts[$variant['id']] = $variant['name'].($variant['price'] > 0 ? ' (+'.$this->sma->convertMoney($variant['price'], TRUE, FALSE).')' : ($variant['price'] == 0 ? '' : ' (+'.$this->sma->convertMoney($variant['price'], TRUE, FALSE).')'));
            }
            $product_data->variants = form_dropdown('option', $opts, '', 'class="form-control" required="required"');
        }

        //Calucalte if this product(category) on promotion with custom module
        $category_id = $this->site->getCategoryIdByProductID($product_id);
        $how_much_off = $this->site->getPromotionPercentage('buyCategoryWithPercentage',$product_id, $category_id);
        if($how_much_off){
            $product_data->promotion =  1;
            $product_data->promo_price =  $this->site->calCulateProductPriceWithPromotionPercentage($product_data->price, $how_much_off);
            $product_data->save_amount = $this->sma->convertMoney($product_data->price - $product_data->promo_price). ' AED';
        }

        //Calucalte if this multiple products on promotion with custom module
        $how_much_off = $this->site->getPromotionPercentage('buyProductsWithPercentage',$product_id, $category_id);
        if($how_much_off){
            $product_data->promotion =  1;
            $product_data->promo_price =  $this->site->calCulateProductPriceWithPromotionPercentage($product_data->price, $how_much_off);
            $product_data->save_amount = $this->sma->convertMoney($product_data->price - $product_data->promo_price). ' AED';
        }

       // print_r($product_data); die;
        echo json_encode($product_data);
        exit();
    }


    //For the facebook shop request which comes from the wordpres folder.
    function fb_product_request($product_code){
        if($product_code){
            $product = $this->shop_model->getProductByCode($product_code);
            if($product){
                $id = $this->Settings->item_addition ? md5($product->id) : md5(microtime());
                $price = $product->promotion ? $product->promo_price : $product->price;
                $options = $this->shop_model->getProductVariants($product->id);
                $option = FALSE;
                if (!empty($options)) {
                    if ($this->input->post('option')) {
                        foreach ($options as $op) {
                            if ($op['id'] == $this->input->post('option')) {
                                $option = $op;
                            }
                        }
                    } else {
                        $option = array_values($options)[0];
                    }
                    $price = $option['price']+$price;
                }

                $selected = $option ? $option['id'] : FALSE;
                $tax_rate = $this->site->getTaxRateByID($product->tax_rate);
                $ctax = $this->site->calculateTax($product, $tax_rate, $price);
                $tax = $this->sma->formatDecimal($ctax['amount']);
                $data = array(
                    'id'            => $id,
                    'product_id'    => $product->id,
                    'qty'           => 1,
                    'name'          => $product->name,
                    'code'          => $product->code,
                    'price'         => $product->price,
                    'tax'           => $tax,
                    'image'         => $product->image,
                    'option'        => $selected,
                    'options'       => !empty($options) ? $options : NULL
                );
                if ($this->cart->insert($data)) {
                    $this->session->set_flashdata('message', 'Product added to cart successfully');
                    redirect(site_url('/cart'));
                }
            }else{
                $this->session->set_flashdata('warning', 'Product not validate.!');
                redirect('/');
            }
        }else{
            $this->session->set_flashdata('error', 'Something went wrong.!');
            redirect('/');
        }
    }


    // Products,  categories and brands page
    function products($category_slug = NULL, $subcategory_slug = NULL, $brand_slug = NULL) {
        $this->session->set_userdata('requested_page', $this->uri->uri_string());
        if ($this->input->get('category')) { $category_slug = $this->input->get('category', TRUE); }
        if ($this->input->get('brand')) { $brand_slug = $this->input->get('brand', TRUE); }
        $reset = $category_slug || $subcategory_slug || $brand_slug ? TRUE : FALSE;

        $category_slug_on_sale = array();

        $promotion =0;
        $how_much_off=0;
        $how_much_off_cart_notify=0;
        $how_much_off_cart_promotion=0;
        $how_much_off_cart_percentage=0;



        //promotion start
        if($category_slug){
            $category_data =  $this->shop_model->getCategoryBySlug($category_slug);
            $how_much_off_cart = $this->site->getPromotionPercentage('cartvaluerangeFree');

            if(!$how_much_off_cart) {
                $how_much_off_new = $this->site->getPromotionPercentage('buyCategoryWithPercentage','', $category_data->id);
                if($how_much_off_new) {
                    $promotion =1;
                    $how_much_off = $how_much_off_new;
                }
            }else{
                $how_much_off_cart_notify = 1;
                $how_much_off_cart_percentage = $how_much_off_cart;
                $how_much_off_cart_promotion = $this->site->getPromotionCartRange('cartvaluerangeFree');
            }
        }
        //promotion end

        if($category_slug == 'on-sale'){
            $how_much_off_cart_on_sale = $this->site->getPromotionPercentage('cartvaluerangeFree');

            if(!$how_much_off_cart_on_sale) {
                $buyCategoryWithPercentage_onsale = $this->site->getPromotionPercentage_category_check('buyCategoryWithPercentage');
                $buyProductsWithPercentage_onsale = $this->site->getPromotionPercentage_product_check('buyProductsWithPercentage');
                $category_slug_on_sale['name'] = 'On-Sale';
                $category_slug_on_sale['slug'] = 'on_sale';
                if($buyCategoryWithPercentage_onsale) {
                    $category_slug_on_sale['type'] = 'category';
                    $category_slug_on_sale['ids'] = $this->site->getPromotionPercentage_category_onsale('buyCategoryWithPercentage');
                    $category_slug = $category_slug_on_sale;
                }else if ($buyProductsWithPercentage_onsale){
                    $category_slug_on_sale['type'] = 'product';
                    $category_slug_on_sale['ids'] = $this->site->getPromotionPercentage_product_onsale('buyProductsWithPercentage');
                    $category_slug = $category_slug_on_sale;
                }
            }
        }else{
            $category_slug = $category_slug ? $this->shop_model->getCategoryBySlug($category_slug) : NULL;
        }

        $filters = array(
            'query' => $this->input->get('query'),
            'category' => $category_slug,
            'subcategory' => $subcategory_slug ? $this->shop_model->getCategoryBySlug($subcategory_slug) : NULL,
            'brand' => $brand_slug ? $this->shop_model->getBrandBySlug($brand_slug) : NULL,
            'sorting' => $reset ? NULL : $this->input->get('sorting'),
            'min_price' => $reset ? NULL : $this->input->get('min_price'),
            'max_price' => $reset ? NULL : $this->input->get('max_price'),
            'in_stock' => $reset ? NULL : $this->input->get('in_stock'),
            'page' => $this->input->get('page') ? $this->input->get('page', TRUE) : 1,
        );

        $this->data['filters'] = $filters;
        $this->data['breadcrumb'] = $this->generateBreadcrumb();
        $this->data['category_slug'] = $category_slug;
        //promotion start
        $this->data['promotion'] = $promotion;
        $this->data['how_much_off'] = $how_much_off;
        $this->data['how_much_off_cart_promotion'] = $how_much_off_cart_promotion;
        $this->data['how_much_off_cart_notify'] = $how_much_off_cart_notify;
        $this->data['how_much_off_cart_percentage'] = $how_much_off_cart_percentage;
        //promotion end


        $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');
        $this->data['page_title'] = lang('products'). ' - ' .$this->shop_settings->shop_name;
        $this->data['page_desc'] = 'This is sma shop products page description';
        $this->page_construct('pages/products', $this->data);
    }

    function generateBreadcrumb(){
        $ci=&get_instance();
        $i=2;
        $uri = $ci->uri->segment($i);
        $link='';

        while($uri != ''){
            $prep_link = '';
            for($j=1; $j<=$i; $j++){
                $prep_link.=$ci->uri->segment($j).'/';
            }

            if($ci->uri->segment($i+1)== ''){
                $link.='<li class="active"><a href="'.site_url($prep_link).'">';
                $link.=$ci->uri->segment($i).'</a></li>';
            }else{
                $link.='<li><a href="'.site_url($prep_link).'">';
                $link.=$ci->uri->segment($i).'</a><span class="divider"></span></li>';
            }

            $i++;
            $uri = $ci->uri->segment($i);
        }
        return $link;
    }

    // Search products page - ajax
    function search() {
        $filters = $this->input->post('filters') ? $this->input->post('filters', TRUE) : FALSE;
        $limit = 15;
        if(isset($filters['sorting']) || isset($filters['min_price']) || isset($filters['max_price'])) {
            $total_rows = $this->shop_model->getProductsCount_filter($filters);
        }else{
        $total_rows = $this->shop_model->getProductsCount($filters);
        }
        $filters['limit'] = $limit;
        $filters['offset'] = isset($filters['page']) && !empty($filters['page']) && ($filters['page'] > 1) ? (($filters['page']*$limit)-$limit) : NULL;
        if(isset($filters['sorting']) || isset($filters['min_price']) || isset($filters['max_price'])) {
            $products = $this->shop_model->getProducts_filter($filters);
        }else{
            $products = $this->shop_model->getProducts($filters);
        }
        if($products){
            $this->load->helper(array('text', 'pagination'));
            foreach($products as &$value) {
                $promotext_2 = $this->site->getPromotionPercentage_promotext($value['id']);
                $promotext = $promotext_2 ? 'Promo' : 'New';
                if($promotext_2){
                    $promotext_2 ='<div class="box"><div class="ribbon"><span>'.$promotext_2.'</span></div></div>';
                }else{
                    $promotext_2 ='';
                }


                $value['buy1get1_offer'] = $this->shop_model->findOfferedProducts('buyOneGetOne',$value['id']);
                $value['buy1get1_other_offer'] = $this->shop_model->findOfferedProducts('buyOneGetOtherProductFree',$value['id']);
                $value['buy1get1_cat_offer'] = $this->shop_model->findOfferedProducts('buyOneGetCategoryFree',$value['id'],$value['category_id']);
                $value['popular_offer'] = $this->shop_model->findOfferedProducts('onAddingProduct',$value['id']);
                $value['veriants'] = $this->shop_model->getProductVariants($value['id']);
                $value['details'] = character_limiter(strip_tags($value['details']), 200);
                $value['details_n'] = character_limiter(strip_tags($value['details']), 200);
                $value['promo_status'] = $value['promotion'];
                $value['promo_price'] = $this->sma->convertMoney($value['promo_price']);
                $value['currency_symbol'] =  $this->Settings->current_currency_symbol ;
                $value['promo_text'] =  $promotext ;
                $value['promotext_2'] =  $promotext_2 ;
 
                if($value['show_variable_price_on_shop']){
                    $product_variable_prices = $this->site->getProductVariableLowestAndHeighestPrice($value['id']);
                    /*$value['price'] = str_replace('.0000','',$product_variable_prices->lowest) .'-'. str_replace('.0000','',$product_variable_prices->highest);*/
                    $value['price'] = $this->sma->convertMoney($product_variable_prices->lowest) .'-'. $this->sma->convertMoney($product_variable_prices->highest);
                }else{
                    $value['price'] = $this->sma->convertMoney($value['price']);
                   /* $value['price'] = $value['price'];*/
                }

                $value['buyProductsWithPercentage'] = $this->shop_model->findOfferedProducts('buyProductsWithPercentage',$value['id'], $value['category_id']);
                if(!empty($value['buyProductsWithPercentage'])){
                    $how_much_discount = $this->site->getPromotionPercentage('buyProductsWithPercentage',$value['id'], $value['category_id']);
                    $value['calcluated_different_product_promotion_price'] =  $this->site->calCulateProductPriceWithPromotionPercentage($value['price'], $how_much_discount);
                }else{
                    $value['calcluated_different_product_promotion_price'] = '';
                }

                $value['buyCategoryWithPercentage'] = $this->shop_model->findOfferedProducts('buyCategoryWithPercentage',$value['id'], $value['category_id']);
                if(!empty($value['buyCategoryWithPercentage'])){
                    $how_much_off = $this->site->getPromotionPercentage('buyCategoryWithPercentage',$value['id'], $value['category_id']);
                    $value['calcluated_promotion_price'] =  $this->site->calCulateProductPriceWithPromotionPercentage($value['price'], $how_much_off);
                }else{
                    $value['calcluated_promotion_price'] = '';
                }

            }



            if(!empty($value['veriants'])){
                foreach ($value['veriants'] as $key => $arr) {
                    $opts[$arr["id"]] =  $arr["name"].( $arr["price"] > 0 ? ' (+'.$this->sma->convertMoney($arr["price"], TRUE, FALSE).')' : ($arr["price"] == 0 ? '' : ' (+'.$this->sma->convertMoney($arr["price"], TRUE, FALSE).')'));
                }
                $value['veriants'] = form_dropdown('option', $opts, '', 'class="form-control selectpicker" required="required"');
            }else{
                $value['veriants'] = '';
            }

            $pagination = pagination('shop/products', $total_rows, $limit);
            $info = array('page' => (isset($filters['page']) && !empty($filters['page']) ? $filters['page'] : 1), 'total' => ceil($total_rows/$limit));

            $this->sma->send_json(array('filters' => $filters, 'products' => $products, 'pagination' => $pagination, 'info' => $info));
        } else {
            $this->sma->send_json(array('filters' => $filters, 'products' => FALSE, 'pagination' => FALSE, 'info' => FALSE));
        }
    }

    // Customer quotations
    function quotes($id = NULL, $hash = NULL) {
        if (!$this->loggedIn && !$hash) { redirect('login'); }
        if ($this->Staff) { admin_redirect('quotes'); }
        if ($id) {
            if ($order = $this->shop_model->getQuote(['id' => $id, 'hash' => $hash])) {
                $this->data['inv'] = $order;
                $this->data['rows'] = $this->shop_model->getQuoteItems($id);
                $this->data['customer'] = $this->site->getCompanyByID($order->customer_id);
                $this->data['biller'] = $this->site->getCompanyByID($order->biller_id);
                $this->data['created_by'] = $this->site->getUser($order->created_by);
                $this->data['updated_by'] = $this->site->getUser($order->updated_by);
                $this->data['page_title'] = lang('view_quote');
                $this->data['page_desc'] = '';
                $this->page_construct('pages/view_quote', $this->data);
            } else {
                $this->session->set_flashdata('error', lang('access_denied'));
                redirect('/');
            }
        } else {
            if ($this->input->get('download')) {
                $id = $this->input->get('download', TRUE);
                $order = $this->shop_model->getQuote(['id' => $id]);
                $this->data['inv'] = $order;
                $this->data['rows'] = $this->shop_model->getQuoteItems($id);
                $this->data['customer'] = $this->site->getCompanyByID($order->customer_id);
                $this->data['biller'] = $this->site->getCompanyByID($order->biller_id);
                // $this->data['created_by'] = $this->site->getUser($order->created_by);
                // $this->data['updated_by'] = $this->site->getUser($order->updated_by);
                $this->data['Settings'] = $this->Settings;
                $html = $this->load->view($this->Settings->theme.'/shop/views/pages/pdf_quote', $this->data, TRUE);
                if ($this->input->get('view')) {
                    echo $html;
                    exit;
                } else {
                    $name = lang("quote") . "_" . str_replace('/', '_', $order->reference_no) . ".pdf";
                    $this->sma->generate_pdf($html, $name);
                }
            }
            $page = $this->input->get('page') ? $this->input->get('page', TRUE) : 1;
            $limit = 10;
            $offset = ($page*$limit)-$limit;
            $this->load->helper('pagination');
            $total_rows = $this->shop_model->getQuotesCount();
            $this->session->set_userdata('requested_page', $this->uri->uri_string());
            $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');
            $this->data['orders'] = $this->shop_model->getQuotes($limit, $offset);
            $this->data['pagination'] = pagination('shop/quotes', $total_rows, $limit);
            $this->data['page_info'] = array('page' => $page, 'total' => ceil($total_rows/$limit));
            $this->data['page_title'] = lang('my_orders');
            $this->data['page_desc'] = '';
            $this->page_construct('pages/quotes', $this->data);
        }
    }

    // Customer order/orders page
    function orders($id = NULL, $hash = NULL, $pdf = NULL, $buffer_save = NULL) {


        if($this->session->userdata('custom_guest_login')){
        }else{
        if (!$this->loggedIn && !$hash) { redirect('login'); }
        }

        if ($this->Staff) { admin_redirect('sales'); }
        if ($id && !$pdf) {
            if ($order = $this->shop_model->getOrder(['id' => $id, 'hash' => $hash])) {
                $this->data['inv'] = $order;
                $this->data['rows'] = $this->shop_model->getOrderItems($id);
                $this->data['customer'] = $this->site->getCompanyByID($order->customer_id);
                $this->data['biller'] = $this->site->getCompanyByID($order->biller_id);
                $this->data['address'] = $this->shop_model->getAddressByID($order->address_id);
                $this->data['return_sale'] = $order->return_id ? $this->shop_model->getOrder(['id' => $id]) : NULL;
                $this->data['return_rows'] = $order->return_id ? $this->shop_model->getOrderItems($order->return_id) : NULL;
                $this->data['paypal'] = $this->shop_model->getPaypalSettings();
                $this->data['skrill'] = $this->shop_model->getSkrillSettings();
                $this->data['page_title'] = lang('view_order');
                $this->data['page_desc'] = '';
                $this->page_construct('pages/view_order', $this->data);
            } else {
                $this->session->set_flashdata('error', lang('access_denied'));
                redirect('/');
            }
        } elseif ($pdf || $this->input->get('download')) {
            $id = $pdf ? $id : $this->input->get('download', TRUE);

            if($this->session->userdata('custom_guest_login')){
                $order = $this->shop_model->getOrder(['id' => $id, 'hash' => $hash]);
            }else{
                $order = $this->shop_model->getOrder(['id' => $id]);
            }

                $this->session->unset_userdata('custom_guest_login');
                $this->data['inv'] = $order;
                $this->data['rows'] = $this->shop_model->getOrderItems($id);
                $this->data['customer'] = $this->site->getCompanyByID($order->customer_id);
                $this->data['biller'] = $this->site->getCompanyByID($order->biller_id);
                $this->data['address'] = $this->shop_model->getAddressByID($order->address_id);
                $this->data['return_sale'] = $order->return_id ? $this->shop_model->getOrder(['id' => $id]) : NULL;
                $this->data['return_rows'] = $order->return_id ? $this->shop_model->getOrderItems($order->return_id) : NULL;
                $this->data['Settings'] = $this->Settings;
                $this->data['invoice_footer'] = $this->data['biller']->invoice_footer;

                $html = $this->load->view($this->Settings->theme.'/shop/views/pages/pdf_invoice', $this->data, TRUE);
                if ($this->input->get('view')) {
                    echo $html;
                    exit;
                } else {
                    $name = lang("invoice") . "_" . str_replace('/', '_', $order->reference_no) . ".pdf";
                if ($buffer_save) {
                    //return $this->sma->generate_pdf($html, $name, $buffer_save, $this->data['biller']->invoice_footer);
                    return $this->sma->generate_pdf($html, $name, $buffer_save);
                } else {
                    $this->sma->generate_pdf($html, $name, false);
                }
            }
        } elseif (!$id) {
            $page = $this->input->get('page') ? $this->input->get('page', TRUE) : 1;
            $limit = 10;
            $offset = ($page*$limit)-$limit;
            $this->load->helper('pagination');
            $total_rows = $this->shop_model->getOrdersCount();
            $this->session->set_userdata('requested_page', $this->uri->uri_string());
            $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');
            $this->data['orders'] = $this->shop_model->getOrders($limit, $offset);
            $this->data['pagination'] = pagination('shop/orders', $total_rows, $limit);
            $this->data['page_info'] = array('page' => $page, 'total' => ceil($total_rows/$limit));
            $this->data['page_title'] = lang('my_orders');
            $this->data['page_desc'] = '';
            $this->page_construct('pages/orders', $this->data);
        }
    }

    //Validate if the active user guest checkout then return false
    function validateUserByEmail($email){
        if($email){
            $is_user_active = $this->shop_model->getActiveUserByEmail($this->input->post('email'));
            if($is_user_active){
                return false;
            }else{
                return true;
            }
        }else{
            return false;
        }
    }

    //Validate user address  return true false
    function validat_address($postal_code_val, $address_city_val, $country_code){
        $iso_country_code = $this->site->getCountryIOS($country_code);

        if($iso_country_code){
            $is_address_validate = $this->site->postal_code_validationByRequest($postal_code_val,$address_city_val,$iso_country_code);

            if($is_address_validate){
                return $is_address_validate;
            }
        }else{
            return false;
        }
    }

    function address_validate_message(){
        return false;
    }


    //To save the guest user information to process the order
    function guest_register(){
        $guest_checkout = $this->input->post('guest_checkout');
        if (!$guest_checkout && !$this->loggedIn) { redirect('login'); }
        /* if (!$this->loggedIn) { redirect('login'); }*/
        $this->form_validation->set_rules('address', lang("address"), 'trim|required');
        $this->form_validation->set_rules('note', lang("comment"), 'trim');

        if ($guest_checkout) {
            $this->form_validation->set_rules('name', lang("name"), 'trim|required');
            $this->form_validation->set_rules('email', lang("email"), 'trim|required|valid_email|callback_validateUserByEmail');
            $this->form_validation->set_rules('phone', lang("phone"), 'trim|required');
            $this->form_validation->set_rules('billing_line1', lang('billing_address').' '.lang("line1"), 'trim|required');
            $this->form_validation->set_rules('billing_city', lang('billing_address').' '.lang("city"), 'trim|required');
            $this->form_validation->set_rules('billing_country', lang('billing_address').' '.lang("country"), 'trim|required');
            $this->form_validation->set_rules('shipping_line1', lang('shipping_address').' '.lang("line1"), 'trim|required');
            $this->form_validation->set_rules('shipping_city', lang('shipping_address').' '.lang("city"), 'trim|required');
            $this->form_validation->set_rules('shipping_country', lang('shipping_address').' '.lang("country"), 'trim|required');
            $this->form_validation->set_rules('shipping_phone', lang('shipping_address').' '.lang("phone"), 'trim|required');
            $this->form_validation->set_rules('billing_state', lang('billing_address').' '.lang("state"), 'trim|required');
            $this->form_validation->set_rules('shipping_state', lang('shipping_address').' '.lang("state"), 'trim|required');

            $this->form_validation->set_message('validateUserByEmail','Please login/forgot password with this email address!');

        }else{
            shop_redirect('cart/checkout');
        }


        //Address validate
        $custom_billing_address_validation = '';
        $custom_shipping_address_validation = '';

        if($this->input->post('billing_country')){
            $custom_billing_address_validation = $this->validat_address($this->input->post('billing_postal_code') , $this->input->post('billing_city') , $this->input->post('billing_country'));
            if($custom_billing_address_validation == 1){

                if($this->input->post('shipping_country')){
                    $custom_shipping_address_validation =  $this->validat_address($this->input->post('shipping_postal_code') , $this->input->post('shipping_city') , $this->input->post('shipping_country'));
                    if($custom_shipping_address_validation == 1){
                    }else{
                        $this->form_validation->set_rules('shipping_country', 'shipping_country' , 'callback_address_validate_message');
                        $this->form_validation->set_message('address_validate_message', $custom_shipping_address_validation);

                        $custom_shipping_address_validation = 0;
                    }
                }

            }else{
                $this->form_validation->set_rules('billing_country', 'billing_country' , 'callback_address_validate_message');
                $this->form_validation->set_message('address_validate_message', $custom_billing_address_validation);

                $custom_billing_address_validation = 0;
            }
        }

        if ($this->form_validation->run() == true  && $custom_billing_address_validation == 1 && $custom_shipping_address_validation == 1) {

            if ($guest_checkout || $address = $this->shop_model->getAddressByID($this->input->post('address'))) {

                $new_customer = false;
                if ($guest_checkout) {
                    $address = [
                        'phone' => $this->input->post('shipping_phone'),
                        'line1' => $this->input->post('shipping_line1'),
                        'line2' => $this->input->post('shipping_line2'),
                        'city' => $this->input->post('shipping_city'),
                        'state' => $this->input->post('shipping_state'),
                        'postal_code' => $this->input->post('shipping_postal_code'),
                        'country' => $this->input->post('shipping_country'),
                    ];
                }

                // $customer = $this->site->getCompanyByID($this->session->userdata('company_id'));
                if ($this->input->post('address') != 'new') {
                    $customer = $this->site->getCompanyByID($this->session->userdata('company_id'));
                } else {

                    if (!($customer = $this->shop_model->getCompanyByEmail($this->input->post('email')))) {

                        $customer = new stdClass();
                        $customer->name = $this->input->post('name');
                        $customer->company = $this->input->post('company');
                        $customer->phone = $this->input->post('phone');
                        $customer->email = $this->input->post('email');
                        $customer->address = $this->input->post('billing_line1').'<br>'.$this->input->post('billing_line2');
                        $customer->city = $this->input->post('billing_city');
                        $customer->state = $this->input->post('billing_state');
                        $customer->postal_code = $this->input->post('billing_postal_code');
                        $customer->country = $this->input->post('billing_country');
                        $customer_group = $this->shop_model->getCustomerGroup($this->Settings->customer_group);
                        $price_group = $this->shop_model->getPriceGroup($this->Settings->price_group);
                        $customer->customer_group_id = (!empty($customer_group)) ? $customer_group->id : NULL;
                        $customer->customer_group_name = (!empty($customer_group)) ? $customer_group->name : NULL;
                        $customer->price_group_id = (!empty($price_group)) ? $price_group->id : NULL;
                        $customer->price_group_name = (!empty($price_group)) ? $price_group->name : NULL;
                        $customer->group_id = 3;
                        $customer->group_name = 'customer';
                        $new_customer = true;
                    }
                }

                if ($new_customer) {
                    $customer = (array) $customer;
                }

                //Create new company
                if (is_array($customer) && !empty($customer)) {
                    $this->db->insert('companies', $customer);
                    $company_id = $this->db->insert_id();
                }else{
                    $company_id = $customer->id;
                }


                $split_str = explode(' ', $this->input->post('name'), 2);
                $first_name = $split_str[0];
                $last_name = $split_str[1];

                $additional_data = [
                    'username' => $this->input->post('email'),
                    'email' => $this->input->post('email'),
                    'active' => 0,
                    'first_name' => $first_name,
                    'last_name' => $last_name,
                    'phone' => $this->input->post('phone'),
                    'company' => $this->input->post('company'),
                    'gender' => 'male',
                    'company_id' => $company_id,
                    'activation_code' => time(),
                    'group_id' => 3
                ];
                //Add the guest as a new customer
                $user_already_exist = $this->shop_model->getUserByEmail($this->input->post('email'));

                if(!$user_already_exist){
                    if (is_array($additional_data) && !empty($additional_data)) {
                        $this->db->insert('users', $additional_data);
                        $customer_id = $this->db->insert_id();
                    }
                }else if($user_already_exist->active == 0){
                    $up_data = array(
                        'activation_code' => time(),
                    );
                    $this->db->where('id', $user_already_exist->id);
                    $this->db->update('users', $up_data);
                }else{
                    $customer_id = $user_already_exist->id;
                }

                //Add the customer address
                if (is_array($address) && !empty($address)) {
                    $address['company_id'] = $company_id;
                    $this->db->insert('addresses', $address);
                    $address_id = $this->db->insert_id();
                }

                $this->session->set_userdata('custom_guest_checkout',1);
                $this->session->set_userdata('custom_guest_checkout_user_id',$customer_id);
                $this->session->set_userdata('user_id',$customer_id);
                $this->session->set_userdata('custom_guest_checkout_company_id',$company_id);
                $this->session->set_userdata('company_id',$company_id);

                $this->session->set_userdata('custom_guest_login', 1);
                $this->session->set_userdata('guest_checkout',1);
                $this->session->set_userdata('name',$this->input->post('name'));
                $this->session->set_userdata('company',$this->input->post('company'));
                $this->session->set_userdata('email',$this->input->post('email'));
                $this->session->set_userdata('phone',$this->input->post('phone'));
                $this->session->set_userdata('address','old');


                //send Account activation email
                $email=  $this->input->post('email');
                $this->load->library('ion_auth');
                $data = $this->ion_auth->verifyUserActivation($email);
                $parse_data = array(
                    'user_name' => $data->first_name . ' ' . $data->last_name,
                    'site_link' => site_url(),
                    'site_name' => $this->Settings->site_name,
                    'email' => $email,
                    'activation_link' => anchor('activate/' . $data->id . '/' . $data->activation_code, lang('email_activate_link')),
                    'logo' => '<img src="' . base_url() . 'assets/uploads/logos/' . $this->Settings->logo . '" alt="' . $this->Settings->site_name . '"/>'
                );

                $subject = $this->lang->line('email_activation_subject') . ' - ' . $this->Settings->site_name;
                $msg = file_get_contents('./themes/' . $this->Settings->theme . '/admin/views/email_templates/activate_email.html');
                $message = $this->parser->parse_string($msg, $parse_data);

                if ($this->sma->send_email($email, $subject, $message)) {
                    $this->auth_model->trigger_events(array('activation_email_successful'));
                }

                redirect('cart/checkout');


            } else {
                $this->session->set_flashdata('error', lang('address_x_found'));
                redirect(isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : 'cart/checkout');
            }
        } else {
            //$this->session->set_flashdata('error', validation_errors());
            $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
            $this->data['product'] =  NULL;
            $this->page_construct('pages/checkout', $this->data);
            //redirect('cart/checkout');
            /* $this->session->set_flashdata('error', validation_errors());
             redirect('cart/checkout');*/
        }

    }

    // Add new Order form shop
    function order() {

            $promotion_discount = 0;
            if ($this->session->userdata('promotion_discount') > 0) {
                $promotion_discount = $this->session->userdata('promotion_discount');
            }

        $post_merchant_id= $this->input->post('pt_merchant_id');
        $post_secret_key = $this->input->post('pt_secret_key');
        if($post_merchant_id) {
            $cart_names = array();
            foreach ($this->cart->contents() as $item) {
                $cart_names[] = $item['name'];
            }
            $this->session->set_userdata('products_names',$cart_names);
            $this->session->set_userdata('total',$this->cart->total());
        }


        $guest_checkout = $this->input->post('guest_checkout');
        if (!$guest_checkout && !$this->loggedIn) { redirect('login'); }
        /* if (!$this->loggedIn) { redirect('login'); }*/
        $this->form_validation->set_rules('address', lang("address"), 'trim|required');
        $this->form_validation->set_rules('note', lang("comment"), 'trim');


        if ($guest_checkout) {
            $this->form_validation->set_rules('payment_by', 'Payment Method', 'required');
            $this->form_validation->set_rules('delivery_by', 'Delivery Method', 'required');
            $this->form_validation->set_rules('name', lang("name"), 'trim|required');
            $this->form_validation->set_rules('email', lang("email"), 'trim|required|valid_email');
            $this->form_validation->set_rules('phone', lang("phone"), 'trim|required');
           // $this->form_validation->set_rules('payment_method', lang("payment_method"), 'required');
            //$this->form_validation->set_rules('billing_line1', lang('billing_address').' '.lang("line1"), 'trim|required');
            //$this->form_validation->set_rules('billing_city', lang('billing_address').' '.lang("city"), 'trim|required');
            //$this->form_validation->set_rules('billing_country', lang('billing_address').' '.lang("country"), 'trim|required');
            //$this->form_validation->set_rules('shipping_line1', lang('shipping_address').' '.lang("line1"), 'trim|required');
            //$this->form_validation->set_rules('shipping_city', lang('shipping_address').' '.lang("city"), 'trim|required');
            //$this->form_validation->set_rules('shipping_country', lang('shipping_address').' '.lang("country"), 'trim|required');
            //$this->form_validation->set_rules('shipping_phone', lang('shipping_address').' '.lang("phone"), 'trim|required');
        }else{
            $this->form_validation->set_rules('payment_by', 'Payment Method', 'required');
            $this->form_validation->set_rules('delivery_by', 'Delivery Method', 'required');
        }
        if ($guest_checkout && $this->Settings->indian_gst) {
            $this->form_validation->set_rules('billing_state', lang('billing_address').' '.lang("state"), 'trim|required');
            $this->form_validation->set_rules('shipping_state', lang('shipping_address').' '.lang("state"), 'trim|required');
        }

        if ($this->form_validation->run() == true) {


            if ($guest_checkout || $address = $this->shop_model->getAddressByID($this->input->post('address'))) {

                $new_customer = false;
                if ($guest_checkout && $new_customer) {
                    $address = [
                        'phone' => $this->input->post('shipping_phone'),
                        'line1' => $this->input->post('shipping_line1'),
                        'line2' => $this->input->post('shipping_line2'),
                        'city' => $this->input->post('shipping_city'),
                        'state' => $this->input->post('shipping_state'),
                        'postal_code' => $this->input->post('shipping_postal_code'),
                        'country' => $this->input->post('shipping_country'),
                    ];
                }


                // $customer = $this->site->getCompanyByID($this->session->userdata('company_id'));
                if ($this->input->post('address') != 'new') {
                    $customer = $this->site->getCompanyByID($this->session->userdata('company_id'));
                } else {

                    if (!($customer = $this->shop_model->getCompanyByEmail($this->input->post('email')))) {

                        $customer = new stdClass();
                        $customer->name = $this->input->post('name');
                        $customer->company = $this->input->post('company');
                        $customer->phone = $this->input->post('phone');
                        $customer->email = $this->input->post('email');
                        $customer->address = $this->input->post('billing_line1').'<br>'.$this->input->post('billing_line2');
                        $customer->city = $this->input->post('billing_city');
                        $customer->state = $this->input->post('billing_state');
                        $customer->postal_code = $this->input->post('billing_postal_code');
                        $customer->country = $this->input->post('billing_country');
                        $customer_group = $this->shop_model->getCustomerGroup($this->Settings->customer_group);
                        $price_group = $this->shop_model->getPriceGroup($this->Settings->price_group);
                        $customer->customer_group_id = (!empty($customer_group)) ? $customer_group->id : NULL;
                        $customer->customer_group_name = (!empty($customer_group)) ? $customer_group->name : NULL;
                        $customer->price_group_id = (!empty($price_group)) ? $price_group->id : NULL;
                        $customer->price_group_name = (!empty($price_group)) ? $price_group->name : NULL;
                        $new_customer = true;
                    }
                }
                $biller = $this->site->getCompanyByID($this->shop_settings->biller);
                $note = $this->db->escape_str($this->input->post('comment'));
                $product_tax = 0; $total = 0;
                $gst_data = [];
                $total_cgst = $total_sgst = $total_igst = 0;
                foreach ($this->cart->contents() as $item) {

                    $item_option = NULL;
                    if ($product_details = $this->site->getProductByID($item['product_id'])) {

                        $product_group_price_data= $this->site->getProductGroupPrice($item['product_id'],$this->Settings->country_price_group);
                        if( $product_group_price_data->price !='') {
                            $price = $product_group_price_data->price;
                        }else {

                        $price = $product_details->promotion ? $product_details->promo_price : $product_details->price;
                        }

                      //  $price = $product_details->promotion ? $product_details->promo_price : $product_details->price;

                        if ($item['option']) {
                            if ($product_variant = $this->shop_model->getProductVariantByID($item['option'])) {
                                $item_option = $product_variant->id;
                                $product_group_price_variant_data= $this->site->getProductGroupPrice($item['product_id'],$this->Settings->country_price_group,$item_option);
                                if( $product_group_price_variant_data->price !='') {
                                    $price = $product_group_price_variant_data->price;
                                }else {
                                $price = $product_variant->price+$price;
                            }
                                //$price = $product_variant->price+$price;
                            }
                        }

                        $item_net_price = $unit_price = $price;
                        $item_quantity = $item_unit_quantity = $item['qty'];
                        $pr_item_tax = $item_tax = 0;
                        $tax = "";
                        if ($this->Settings->country_vat == 1) {
                        if (!empty($product_details->tax_rate)) {

                            $tax_details = $this->site->getTaxRateByID($product_details->tax_rate);
                            $ctax = $this->site->calculateTax($product_details, $tax_details, $unit_price);
                            $item_tax = $ctax['amount'];
                            $tax = $ctax['tax'];
                            if ($product_details->tax_method != 1) {
                                $item_net_price = $unit_price - $item_tax;
                            }
                            $pr_item_tax = $this->sma->formatDecimal(($item_tax * $item_unit_quantity), 4);
                            if ($this->Settings->indian_gst && $gst_data = $this->gst->calculteIndianGST($pr_item_tax, ($biller->state == $customer->state), $tax_details)) {
                                $total_cgst += $gst_data['cgst'];
                                $total_sgst += $gst_data['sgst'];
                                $total_igst += $gst_data['igst'];
                            }
                        }
                        }

                        $product_tax += $pr_item_tax;
                        $subtotal = (($item_net_price * $item_unit_quantity) + $pr_item_tax);

                        $unit = $this->site->getUnitByID($product_details->unit);

                        $product = [
                            'product_id' => $product_details->id,
                            'product_code' => $product_details->code,
                            'product_name' => $product_details->name,
                            'product_type' => $product_details->type,
                            'option_id' => $item_option,
                            'net_unit_price' => $item_net_price,
                            'unit_price' => $this->sma->formatDecimal($item_net_price + $item_tax),
                            'quantity' => $item_quantity,
                            'product_unit_id' => $unit ? $unit->id : NULL,
                            'product_unit_code' => $unit ? $unit->code : NULL,
                            'unit_quantity' => $item_unit_quantity,
                            'warehouse_id' => $this->shop_settings->warehouse,
                            'item_tax' => $pr_item_tax,
                            'tax_rate_id' => $product_details->tax_rate,
                            'tax' => $tax,
                            'discount' => NULL,
                            'item_discount' => 0,
                            'subtotal' => $this->sma->formatDecimal($subtotal),
                            'serial_no' => NULL,
                            'real_unit_price' => $price,
                        ];

                        $products[] = ($product + $gst_data);
                        $total += $this->sma->formatDecimal(($item_net_price * $item_unit_quantity), 4);

                    } else {
                        $this->session->set_flashdata('error', lang('product_x_found').' ('.$item['name'].')');
                        redirect(isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : 'cart');
                    }
                }

                $shipping = $this->session->userdata('shipping_amount');
                $order_tax = $this->site->calculateOrderTax($this->Settings->default_tax_rate2, ($total + $product_tax));
                $total_tax = $this->sma->formatDecimal(($product_tax + $order_tax), 4);

                $grand_total = $this->sma->formatDecimal(($total + round($total_tax) + $shipping), 4);

                if($this->session->userdata('gift_cart_amount')){
                    $card_number = $this->session->userdata('gift_cart_no');
                    $update_balance=0;
                    $card_amonut = $this->session->userdata('gift_cart_amount');
                    $new_grand_total = $grand_total - $card_amonut;
                    if($new_grand_total < 0){
                        $update_balance =   $card_amonut - $grand_total;
                        $new_grand_total =0.00;
                        $card_amonut = $grand_total;
                    }
                    $grand_total = $new_grand_total;
                    $this->shop_model->updateGiftCartBalance($card_number, $update_balance);
                }

                $data = [
                    'date' => date('Y-m-d H:i:s'),
                    'reference_no' => $this->site->getReference('so'),
                    'customer_id' => $customer->id,
                    'customer' => ($customer->company && $customer->company != '-' ? $customer->company : $customer->name),
                    'biller_id' => $biller->id,
                    'biller' => ($biller->company && $biller->company != '-' ? $biller->company : $biller->name),
                    'warehouse_id' => $this->shop_settings->warehouse,
                    'note' => $note,
                    'staff_note' => NULL,
                    'total' => $total,
                    'product_discount' => 0,
                    'order_discount_id' => $promotion_discount,
                    'order_discount' => $promotion_discount,
                    'total_discount' => $promotion_discount,
                    'product_tax' => $product_tax,
                    'order_tax_id' => $this->Settings->default_tax_rate2,
                    'order_tax' => $order_tax,
                    'total_tax' => $total_tax,
                    'shipping' => $shipping,
                    'grand_total' => $grand_total,
                    'total_items' => $this->cart->total_items(),
                    'sale_status' => 'pending',
                    'payment_status' => 'pending',
                    'payment_by' => $this->input->post('payment_by'),
                    'delivery_by' => $this->input->post('delivery_by'),
                    'redeem_points' => $this->input->post('redeemd_point'),
                    'payment_term' => NULL,
                    'due_date' => NULL,
                    'paid' => 0,
                    'created_by' => $this->session->userdata('user_id'),
                    'shop' => 1,
                    'address_id' => $address->id,
                    'hash' => hash('sha256', microtime() . mt_rand()),
                    'card_amonut' => $card_amonut,
                    'card_number' => $card_number,
                    'promotion_discount' => $promotion_discount,
                ];
                if ($this->Settings->invoice_view == 2) {
                    $data['cgst'] = $total_cgst;
                    $data['sgst'] = $total_sgst;
                    $data['igst'] = $total_igst;
                }
                if ($new_customer) {
                    $customer = (array) $customer;
                }


                // $this->sma->print_arrays($data, $products , $customer , $address);
                // $this->sma->print_arrays($data, $products, $customer, $address);

                if ($sale_id = $this->shop_model->addSale($data, $products,$customer, $address)) {
                    if($post_merchant_id){
                        $this->session->set_userdata('post_merchant_id',$post_merchant_id);
                        $this->session->set_userdata('post_secret_key',$post_secret_key);
                        $this->session->set_userdata('sale_id',$sale_id);
                        shop_redirect('order_payment');
                    }
                    $this->cart->destroy();
                    $this->session->unset_userdata('gift_cart_amount');
                    $this->session->unset_userdata('gift_cart_no');
                    $this->session->unset_userdata('shipping_amount');

                    if($this->session->userdata('custom_guest_checkout')){
                        $this->order_received($sale_id,$data['hash']);
                    }else{
                    $this->order_received($sale_id);
                    }

                    if($this->session->unset_userdata('custom_guest_checkout')){
                        //Guest checkout session destroy
                        $this->session->unset_userdata('custom_guest_checkout');
                        $this->session->unset_userdata('custom_guest_checkout_user_id');
                        $this->session->unset_userdata('custom_guest_checkout_company_id');

                        //$this->session->unset_userdata('company_id');

                        //$this->session->unset_userdata('guest_checkout');
                        //$this->session->unset_userdata('name');
                        //$this->session->unset_userdata('company');
                       // $this->session->unset_userdata('email');
                        //$this->session->unset_userdata('phone');
                        //$this->session->unset_userdata('address');
                    }


                    $this->session->set_flashdata('info', lang('order_added_make_payment'));
                    shop_redirect('orders/'.$sale_id.'/'.($this->loggedIn ? '' : $data['hash']));
                }
            } else {
                $this->session->set_flashdata('error', lang('address_x_found'));
                redirect(isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : 'cart/checkout');
            }
        } else {
            $this->session->set_flashdata('error', validation_errors());
            redirect('cart/checkout'.($guest_checkout ? '#guest' : ''));
            /* $this->session->set_flashdata('error', validation_errors());
             redirect('cart/checkout');*/
        }
    }
    function order_check() {

        $customer = $this->site->getCompanyByID($this->session->userdata('company_id'));
        $biller = $this->site->getCompanyByID($this->shop_settings->biller);
        /*$note = $this->db->escape_str($this->input->post('comment'));*/
        $product_tax = 0; $total = 0;
        $gst_data = [];
        $total_cgst = $total_sgst = $total_igst = 0;

        foreach ($this->cart->contents() as $item) {

            $item_option = NULL;
            if ($product_details = $this->site->getProductByID($item['product_id'])) {
                $price = $product_details->promotion ? $product_details->promo_price : $product_details->price;

                if ($item['option']) {
                    if ($product_variant = $this->shop_model->getProductVariantByID($item['option'])) {
                        $item_option = $product_variant->id;
                        $price = $product_variant->price+$price;
                    }
                }

                $item_net_price = $unit_price = $price;
                $item_quantity = $item_unit_quantity = $item['qty'];
                $pr_item_tax = $item_tax = 0;
                $tax = "";

                if (!empty($product_details->tax_rate)) {

                    $tax_details = $this->site->getTaxRateByID($product_details->tax_rate);
                    $ctax = $this->site->calculateTax($product_details, $tax_details, $unit_price);
                    $item_tax = $ctax['amount'];
                    $tax = $ctax['tax'];
                    if ($product_details->tax_method != 1) {
                        $item_net_price = $unit_price - $item_tax;
                    }
                    $pr_item_tax = $this->sma->formatDecimal(($item_tax * $item_unit_quantity), 4);
                    if ($this->Settings->indian_gst && $gst_data = $this->gst->calculteIndianGST($pr_item_tax, ($biller->state == $customer->state), $tax_details)) {
                        $total_cgst += $gst_data['cgst'];
                        $total_sgst += $gst_data['sgst'];
                        $total_igst += $gst_data['igst'];
                    }
                }

                $product_tax += $pr_item_tax;
                $subtotal = (($item_net_price * $item_unit_quantity) + $pr_item_tax);

                $unit = $this->site->getUnitByID($product_details->unit);

                $product = [
                    'product_id' => $product_details->id,
                    'product_code' => $product_details->code,
                    'product_name' => $product_details->name,
                    'product_type' => $product_details->type,
                    'option_id' => $item_option,
                    'net_unit_price' => $item_net_price,
                    'unit_price' => $this->sma->formatDecimal($item_net_price + $item_tax),
                    'quantity' => $item_quantity,
                    'product_unit_id' => $unit ? $unit->id : NULL,
                    'product_unit_code' => $unit ? $unit->code : NULL,
                    'unit_quantity' => $item_unit_quantity,
                    'warehouse_id' => $this->shop_settings->warehouse,
                    'item_tax' => $pr_item_tax,
                    'tax_rate_id' => $product_details->tax_rate,
                    'tax' => $tax,
                    'discount' => NULL,
                    'item_discount' => 0,
                    'subtotal' => $this->sma->formatDecimal($subtotal),
                    'serial_no' => NULL,
                    'real_unit_price' => $price,
                ];

                $products[] = ($product + $gst_data);
                $total += $this->sma->formatDecimal(($item_net_price * $item_unit_quantity), 4);

            } else {
                $this->session->set_flashdata('error', lang('product_x_found').' ('.$item['name'].')');
                redirect(isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : 'cart');
            }
        }
        $shipping = $this->shop_settings->shipping;
        $order_tax = $this->site->calculateOrderTax($this->Settings->default_tax_rate2, ($total + $product_tax));
        $total_tax = $this->sma->formatDecimal(($product_tax + $order_tax), 4);
        $grand_total = $this->sma->formatDecimal(($total + $total_tax + $shipping), 4);
        if($this->session->userdata('gift_cart_amount')){
            $card_number = $this->session->userdata('gift_cart_no');
            $update_balance=0;
            $card_amonut = $this->session->userdata('gift_cart_amount');
            $new_grand_total = $grand_total - $card_amonut;
            if($new_grand_total < 0){
                $update_balance =   $card_amonut - $grand_total;
                $new_grand_total =0.00;
                $card_amonut = $grand_total;
            }
            $grand_total = $new_grand_total;
            $this->shop_model->updateGiftCartBalance($card_number, $update_balance);
        }


        $data = [
            'date' => date('Y-m-d H:i:s'),
            'reference_no' => $this->site->getReference('so'),
            'customer_id' => $customer->id,
            'customer' => ($customer->company && $customer->company != '-' ? $customer->company : $customer->name),
            'biller_id' => $biller->id,
            'biller' => ($biller->company && $biller->company != '-' ? $biller->company : $biller->name),
            'warehouse_id' => $this->shop_settings->warehouse,
            /* 'note' => $note,*/
            'staff_note' => NULL,
            'total' => $total,
            'product_discount' => 0,
            'order_discount_id' => NULL,
            'order_discount' => 0,
            'total_discount' => 0,
            'product_tax' => $product_tax,
            'order_tax_id' => $this->Settings->default_tax_rate2,
            'order_tax' => $order_tax,
            'total_tax' => $total_tax,
            'shipping' => $shipping,
            'grand_total' => $grand_total,
            'total_items' => $this->cart->total_items(),
            'sale_status' => 'pending',
            'payment_status' => 'pending',
            'payment_by' => $this->input->post('payment_by'),
            'redeem_points' => $this->input->post('redeemd_point'),
            'payment_term' => NULL,
            'due_date' => NULL,
            'paid' => 0,
            'created_by' => $this->session->userdata('user_id'),
            'shop' => 1,
            /* 'address_id' => $address->id,*/
            'hash' => hash('sha256', microtime() . mt_rand()),
            'card_amonut' => $card_amonut,
            'card_number' => $card_number,
        ];
        if ($this->Settings->invoice_view == 2) {
            $data['cgst'] = $total_cgst;
            $data['sgst'] = $total_sgst;
            $data['igst'] = $total_igst;
        }

        // $this->sma->print_arrays($data, $products);

        if ($sale_id = $this->shop_model->addSale_check($data, $products)) {
           redirect('cart/checkout');
        }

    }

    // Customer address list
    function addresses() {
        if (!$this->loggedIn) { redirect('login'); }
        if ($this->Staff) { admin_redirect('customers'); }
        $this->session->set_userdata('new_address_added',0);
        $this->session->set_userdata('requested_page', $this->uri->uri_string());
        $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');
        $this->data['addresses'] = $this->shop_model->getAddresses();
        $this->data['page_title'] = lang('my_addresses');
        $this->data['page_desc'] = '';
        $this->page_construct('pages/addresses', $this->data);
    }

    // Add/edit customer address
    function address($id = NULL) {
        $custom_checker_flag = $this->session->userdata('custom_guest_checkout');

        if (!$this->loggedIn) {
            if($custom_checker_flag){
            }else{
                $this->sma->send_json(array('status' => 'error', 'message' => lang('please_login')));
            }
        }
        
        $this->form_validation->set_rules('line1', lang("line1"), 'trim|required');
        $this->form_validation->set_rules('line2', lang("line2"), 'trim|required');
        $this->form_validation->set_rules('city', lang("city"), 'trim|required');
        $this->form_validation->set_rules('state', lang("state"), 'trim|required');
        $this->form_validation->set_rules('postal_code', lang("postal_code"), 'trim|required');
        $this->form_validation->set_rules('country', lang("country"), 'trim|required');
        $this->form_validation->set_rules('phone', lang("phone"), 'trim|required');

        if ($this->form_validation->run() == true) {

            $user_addresses = $this->shop_model->getAddresses();
            if (count($user_addresses) >= 6) {
                $this->sma->send_json(array('status' => 'error', 'message' => lang('already_have_max_addresses'), 'level' => 'error'));
            }
           $added_country = $this->site->countryCode($this->input->post('country'));
           // echo  $this->Settings->selected_country;
            $newCountryAddress = 0;
            if($added_country != $this->Settings->selected_country){
                $newCountryAddress =1;
                $this->session->set_userdata('different_country_code',1);
            }

            $data = ['line1' => $this->input->post('line1'),
                'line2' => $this->input->post('line2'),
                'phone' => $this->input->post('phone'),
                'city' => $this->input->post('city'),
                'state' => $this->input->post('state'),
                'postal_code' => $this->input->post('postal_code'),
                'country' => $this->input->post('country'),
                'company_id' => $this->session->userdata('company_id')];

            if ($id) {
                $this->db->update('addresses', $data, ['id' => $id]);
                $this->session->set_flashdata('message', lang('address_updated'));
                $this->sma->send_json(array('redirect' => $_SERVER['HTTP_REFERER']));
            } else {
                $this->db->insert('addresses', $data);
                $address_id = $this->db->insert_id();
                if($this->session->userdata('new_address_added') == 1){
                   // shop_redirect(base_url().'cart/checkout?adr='.$address_id);
                    $old_address_id = $this->input->post('old_address_id');
                    if($old_address_id == 0){
                        $old_address_id = $address_id;
                    }
                    if($old_address_id !=0 && $newCountryAddress == 1){
                        $this->session->set_userdata('new_address_id',$address_id);
                        $this->sma->send_json(array('redirect' => base_url().'cart/checkout?adr='.$old_address_id.'&new_adr='.$address_id));
                      //  $new_address_id = $address_id;
                    }
                    $this->sma->send_json(array('redirect' => base_url().'cart/checkout?adr='.$address_id));
                }
                $this->session->set_flashdata('message', lang('address_added'));
                $this->sma->send_json(array('redirect' => $_SERVER['HTTP_REFERER']));
            }

        } elseif ($this->input->is_ajax_request()) {
            $this->sma->send_json(array('status' => 'error', 'message' => validation_errors()));
        } else {
            $this->session->set_userdata('new_address_added',0);
            shop_redirect('shop/addresses');
        }
    }

    // Customer wishlist page
    function wishlist() {
        if (!$this->loggedIn) { redirect('login'); }
        $this->session->set_userdata('requested_page', $this->uri->uri_string());
        $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');
        $total = $this->shop_model->getWishlist(TRUE);
        $products = $this->shop_model->getWishlist();
        $this->load->helper('text');
        foreach($products as $product) {
            $item = $this->shop_model->getProductByID($product->product_id);
            $item->details = character_limiter(strip_tags($item->details), 140);
            $items[] = $item;
        }
        $this->data['items'] = $products ? $items : NULL;
        $this->data['page_title'] = lang('wishlist');
        $this->data['page_desc'] = '';
        $this->page_construct('pages/wishlist', $this->data);
    }

    // Send us email
    function send_message() {

        $this->form_validation->set_rules('name', lang("name"), 'required');
        $this->form_validation->set_rules('email', lang("email"), 'required|valid_email');
        //$this->form_validation->set_rules('subject', lang("subject"), 'required');
        $this->form_validation->set_rules('enquiry', 'Enquiry', 'required');

        if ($this->form_validation->run() == true) {
            try {
                if ($this->sma->send_email($this->shop_settings->email, $this->input->post('subject'), $this->input->post('enquiry'), $this->input->post('email'), $this->input->post('name'))) {
                    redirect('page/contact-success');
                }
                $this->sma->send_json(array('status' => 'error', 'message' => lang('action_failed')));
            } catch (Exception $e) {
                $this->sma->send_json(array('status' => 'error', 'message' => $e->getMessage()));
            }

        } elseif ($this->input->is_ajax_request()) {
            $this->sma->send_json(array('status' => 'Error!', 'message' => validation_errors(), 'level' => 'error'));
        } else {
            //$this->session->set_flashdata('warning', 'Please try to send message from contact page!');
            $this->session->set_flashdata('error', validation_errors());
            redirect('page/contact-us');
        }
    }

    function send_custome_order() {

        $this->form_validation->set_rules('name', lang("name"), 'required');
        $this->form_validation->set_rules('email', lang("email"), 'required|valid_email');
        //$this->form_validation->set_rules('subject', lang("subject"), 'required');
        $this->form_validation->set_rules('enquiry', 'Enquiry', 'required');

        if ($this->form_validation->run() == true) {
            try {
                unset($_POST['captcha']);
                if ($this->db->insert('custom_order', $_POST)) {
                    //To check that if payment is successfull then change its status to paid.
                    if ($this->sma->send_email($this->shop_settings->email, $this->input->post('subject'), $this->input->post('enquiry'), $this->input->post('email'), $this->input->post('name'))) {
                        redirect('page/contact-success');
                    }
                }
                $this->sma->send_json(array('status' => 'error', 'message' => lang('action_failed')));
            } catch (Exception $e) {
                $this->sma->send_json(array('status' => 'error', 'message' => $e->getMessage()));
            }

        } elseif ($this->input->is_ajax_request()) {
            $this->sma->send_json(array('status' => 'Error!', 'message' => validation_errors(), 'level' => 'error'));
        } else {
            //$this->session->set_flashdata('warning', 'Please try to send message from contact page!');
            $this->session->set_flashdata('error', validation_errors());
            redirect('page/custom-order');
        }
    }

    // Add attachment to sale on manual payment
    function manual_payment($order_id) {
        if ($_FILES['payment_receipt']['size'] > 0) {
            $this->load->library('upload');
            $config['upload_path'] = 'files/';
            $config['allowed_types'] = 'zip|rar|pdf|doc|docx|xls|xlsx|ppt|pptx|gif|jpg|jpeg|png|tif|txt';
            $config['max_size'] = 2048;
            $config['overwrite'] = FALSE;
            $config['max_filename'] = 25;
            $config['encrypt_name'] = TRUE;
            $this->upload->initialize($config);
            if (!$this->upload->do_upload('payment_receipt')) {
                $error = $this->upload->display_errors();
                $this->session->set_flashdata('error', $error);
                redirect($_SERVER["HTTP_REFERER"]);
            }
            $manual_payment = $this->upload->file_name;
            $this->db->update('sales', ['attachment' => $manual_payment], ['id' => $order_id]);
            $this->session->set_flashdata('message', lang('file_submitted'));
            redirect(isset($_SERVER["HTTP_REFERER"]) ? $_SERVER["HTTP_REFERER"] : '/shop/orders');
        }
    }

    // Digital products download
    function downloads($id = NULL, $hash = NULL) {
        if (!$this->loggedIn) { redirect('login'); }
        if ($this->Staff) { admin_redirect(); }
        if ($id && $hash && md5($id) == $hash) {
            $sale = $this->shop_model->getDownloads(1, 0, $id);
            if (!empty($sale)) {
                $product = $this->site->getProductByID($id);
                if (file_exists('./files/'.$product->file)) {
                    $this->load->helper('download');
                    force_download('./files/'.$product->file, NULL);
                    exit;
                } else {
                    header('Content-Description: File Transfer');
                    header('Content-Type: application/octet-stream');
                    header("Content-Transfer-Encoding: Binary");
                    header("Content-disposition: attachment; filename=\"" . basename($product->file) . "\"");
                    // header('Content-Length: ' . filesize($product->file));
                    readfile($product->file);
                }
            }
            $this->session->set_flashdata('error', lang('file_x_exist'));
            redirect($_SERVER["HTTP_REFERER"]);
        } else {
            $page = $this->input->get('page') ? $this->input->get('page', TRUE) : 1;
            $limit = 10;
            $offset = ($page*$limit)-$limit;
            $this->load->helper('pagination');
            $total_rows = $this->shop_model->getDownloadsCount();
            $this->session->set_userdata('requested_page', $this->uri->uri_string());
            $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');
            $this->data['downloads'] = $this->shop_model->getDownloads($limit, $offset);
            $this->data['pagination'] = pagination('shop/download', $total_rows, $limit);
            $this->data['page_info'] = array('page' => $page, 'total' => ceil($total_rows/$limit));
            $this->data['page_title'] = lang('my_downloads');
            $this->data['page_desc'] = '';
            $this->page_construct('pages/downloads', $this->data);
        }
    }

    public function order_received($id = null, $hash = null)
    {
        if ($inv = $this->shop_model->getOrder(['id' => $id, 'hash' => $hash])) {

            $user = $this->site->getUser($inv->created_by);
            $customer = $this->site->getCompanyByID($inv->customer_id);
            $biller = $this->site->getCompanyByID($inv->biller_id);

            $this->load->library('parser');
            $parse_data = array(
                'reference_number' => $inv->reference_no,
                'contact_person' => $customer->name,
                'company' => $customer->company && $customer->company != '-' ? '('.$customer->company.')' : '',
                'site_link' => base_url(),
                'site_name' => $this->Settings->site_name,
                'logo' => '<img src="' . base_url() . 'assets/uploads/logos/' . $biller->logo . '" alt="' . ($biller->company != '-' ? $biller->company : $biller->name) . '"/>',
            );

            $msg = file_get_contents('./themes/' . $this->Settings->theme . '/admin/views/email_templates/sale.html');
            $message = $this->parser->parse_string($msg, $parse_data);
            $this->load->model('pay_model');
            $paypal = $this->pay_model->getPaypalSettings();
            $skrill = $this->pay_model->getSkrillSettings();
            $btn_code = '<div id="payment_buttons" class="text-center margin010">';
            if ($paypal->active == "1" && $inv->grand_total != "0.00") {
                if (trim(strtolower($customer->country)) == $biller->country) {
                    $paypal_fee = $paypal->fixed_charges + ($inv->grand_total * $paypal->extra_charges_my / 100);
                } else {
                    $paypal_fee = $paypal->fixed_charges + ($inv->grand_total * $paypal->extra_charges_other / 100);
                }
                $btn_code .= '<a href="https://www.paypal.com/cgi-bin/webscr?cmd=_xclick&business=' . $paypal->account_email . '&item_name=' . $inv->reference_no . '&item_number=' . $inv->id . '&image_url=' . base_url() . 'assets/uploads/logos/' . $this->Settings->logo . '&amount=' . (($inv->grand_total - $inv->paid) + $paypal_fee) . '&no_shipping=1&no_note=1&currency_code=' . $this->default_currency->code . '&bn=FC-BuyNow&rm=2&return=' . admin_url('sales/view/' . $inv->id) . '&cancel_return=' . admin_url('sales/view/' . $inv->id) . '&notify_url=' . admin_url('payments/paypalipn') . '&custom=' . $inv->reference_no . '__' . ($inv->grand_total - $inv->paid) . '__' . $paypal_fee . '"><img src="' . base_url('assets/images/btn-paypal.png') . '" alt="Pay by PayPal"></a> ';
            }

            if ($skrill->active == "1" && $inv->grand_total != "0.00") {
                if (trim(strtolower($customer->country)) == $biller->country) {
                    $skrill_fee = $skrill->fixed_charges + ($inv->grand_total * $skrill->extra_charges_my / 100);
                } else {
                    $skrill_fee = $skrill->fixed_charges + ($inv->grand_total * $skrill->extra_charges_other / 100);
                }
                $btn_code .= ' <a href="https://www.moneybookers.com/app/payment.pl?method=get&pay_to_email=' . $skrill->account_email . '&language=EN&merchant_fields=item_name,item_number&item_name=' . $inv->reference_no . '&item_number=' . $inv->id . '&logo_url=' . base_url() . 'assets/uploads/logos/' . $this->Settings->logo . '&amount=' . (($inv->grand_total - $inv->paid) + $skrill_fee) . '&return_url=' . admin_url('sales/view/' . $inv->id) . '&cancel_url=' . admin_url('sales/view/' . $inv->id) . '&detail1_description=' . $inv->reference_no . '&detail1_text=Payment for the sale invoice ' . $inv->reference_no . ': ' . $inv->grand_total . '(+ fee: ' . $skrill_fee . ') = ' . $this->sma->formatMoney($inv->grand_total + $skrill_fee) . '&currency=' . $this->default_currency->code . '&status_url=' . admin_url('payments/skrillipn') . '"><img src="' . base_url('assets/images/btn-skrill.png') . '" alt="Pay by Skrill"></a>';
            }

            $btn_code .= '<div class="clearfix"></div></div>';
            $message = $message . $btn_code;


            if($this->session->userdata('custom_guest_login')){
                $attachment = $this->orders($id, $hash, true, 'S');
            }else{
            $attachment = $this->orders($id, null, true, 'S');
            }
            $subject = lang('new_order_received');
            $sent = $error = $bcc = false;

            try {

                if ($this->sma->send_email($user->email, $subject, $message, null, null, $attachment, $bcc)) {
                    $subject_admin = 'New order Received';
                    $msg_admin = file_get_contents('./themes/' . $this->Settings->theme . '/admin/views/email_templates/sale_admin.html');
                    $message_admin = $this->parser->parse_string($msg_admin, $parse_data);

                    $allAdmin = $this->site->getAllAdmins();
                    foreach ($allAdmin as $key=> $admin){
                        $this->sma->send_email($admin->email, $subject_admin, $message_admin, null, null, null, $bcc);
                    }

                    delete_files($attachment);
                    $sent = true;
                }
            } catch (Exception $e) {
                $error = $e->getMessage();
            }
            return ['sent' => $sent, 'error' => $error];
        }
    }
    //public function order_payment($id = null, $merchant_id = null, $secret_key = null)
    public function order_payment()
    {
     $is_canceled_pt = $this->input->get('is_canceled_pt');
     if($is_canceled_pt == 1){
         $custom_sale_id = $this->session->userdata('sale_id');
         admin_redirect('sales/deleteAjax/'.urlencode($this->encrypt->encode($custom_sale_id)));
     }

        $this->data['page_title'] = 'Payment';
        $this->data['page_desc'] = '';
        $this->page_construct('pages/payment', $this->data);
    }
    public function order_payment_status()
    {
        $sale_id = $_POST['order_id'];
        $response_message = $_POST['response_message'];

        $this->cart->destroy();
        $this->session->unset_userdata('gift_cart_amount');
        $this->session->unset_userdata('gift_cart_no');
        $this->session->unset_userdata('post_merchant_id');
        $this->session->unset_userdata('post_secret_key');
        $this->session->unset_userdata('sale_id');
        $this->session->unset_userdata('products_names');
        $this->session->unset_userdata('total');

        unset($_POST['pt_customer_email']);
        unset($_POST['pt_customer_password']);
        unset($_POST['pt_token']);

        //To save the paytabs user information
        if ($this->db->insert('paytabs_information', $_POST)) {
            //To check that if payment is successfull then change its status to paid.

            $this->order_received($sale_id);
            if($response_message == 'Approved'){
                admin_redirect('sales/add_paymentAjax/'.urlencode($this->encrypt->encode($sale_id)));
            }
            $this->session->set_flashdata('info', lang('order_added_make_payment'));
            shop_redirect('orders/'.$sale_id);
        }
        return false;
    }
}