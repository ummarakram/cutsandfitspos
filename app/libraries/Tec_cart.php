<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Tec_cart
{

    public $product_id_rules = '\.a-z0-9_-';
    public $product_name_rules = '\s\S'; // '\w \-\.\:';
    public $product_name_safe = TRUE;
    public $cart_id = FALSE;
    protected $_cart_contents = array();

    public function __construct($params = array()) {
        $this->load->helper('cookie');
        if ($cart_id = get_cookie('cart_id', TRUE)) {
            $this->cart_id = $cart_id;
            $result = $this->db->get_where('cart', array('id' => $this->cart_id))->row();
            $this->_cart_contents = $result ? json_decode($result->data, true) : NULL;
        } else {
            $this->_setup();
        }
        if (empty($this->_cart_contents)) {
            $this->_empty();
        }
    }

    public function __get($var) {
        return get_instance()->$var;
    }

    private function _setup() {
        $this->load->helper('string');
        $val = md5(random_string('alnum', 16).microtime());
        set_cookie('cart_id', $val, 2592000);
        return $this->cart_id = $val;
    }

    private function _empty() {
        $this->_cart_contents = array('cart_total' => 0, 'total_item_tax' => 0, 'total_items' => 0, 'total_unique_items' => 0);
    }

    public function cart_id() {
        return $this->cart_id;
    }

    public function insert($items = array()) {

        if (!is_array($items) OR count($items) === 0) {
            return FALSE;
        }

        $save_cart = FALSE;
        if (isset($items['id'])) {
            if (($rowid = $this->_insert($items))) {
                $save_cart = TRUE;
            }
        } else {
            foreach ($items as $val) {
                if (is_array($val) && isset($val['id'])) {
                    if ($this->_insert($val)) {
                        $save_cart = TRUE;
                    }
                }
            }
        }

        if ($save_cart === TRUE) {
            $this->_save_cart();
            return isset($rowid) ? $rowid : TRUE;
        }

        return FALSE;
    }

    protected function _insert($items = array()) {
        if (!is_array($items) OR count($items) === 0) {
            return FALSE;
        }

        $items['name'] = htmlentities($items['name']);

        if (!isset($items['id'], $items['qty'], $items['name'])) {
            return FALSE;
        }

        $items['qty'] = (float) $items['qty'];

        if ($items['qty'] == 0) {
            return FALSE;
        }

        if (!preg_match('/^[' . $this->product_id_rules . ']+$/i', $items['id'])) {
            return FALSE;
        }

        if ($this->product_name_safe && !preg_match('/^[' . $this->product_name_rules . ']+$/i' . (UTF8_ENABLED ? 'u' : ''), $items['name'])) {
            return FALSE;
        }

        $items['price'] = (float) $items['price'];

        if (isset($items['options']) && count($items['options']) > 0) {
            $rowid = md5($items['id'] . serialize((array) $items['options']));
        } else {
            $rowid = md5($items['id']);
        }

        $old_quantity = isset($this->_cart_contents[$rowid]['qty']) ? (int) $this->_cart_contents[$rowid]['qty'] : 0;

        $items['rowid'] = $rowid;
        $items['qty'] += $old_quantity;
        $this->_cart_contents[$rowid] = $items;

        return $rowid;
    }

    public function update($items = array()) {
        if (!is_array($items) OR count($items) === 0) {
            return FALSE;
        }

        $save_cart = FALSE;
        if (isset($items['rowid'])) {
            if ($this->_update($items) === TRUE) {
                $save_cart = TRUE;
            }
        } else {
            foreach ($items as $val) {
                if (is_array($val) && isset($val['rowid'])) {
                    if ($this->_update($val) === TRUE) {
                        $save_cart = TRUE;
                    }
                }
            }
        }

        if ($save_cart === TRUE) {
            $this->_save_cart();
            return TRUE;
        }

        return FALSE;
    }

    protected function _update($items = array()) {
        if (!isset($items['rowid'], $this->_cart_contents[$items['rowid']])) {
            return FALSE;
        }

        if (isset($items['qty'])) {
            $items['qty'] = (float) $items['qty'];
            if ($items['qty'] == 0) {
                unset($this->_cart_contents[$items['rowid']]);
                return TRUE;
            }
        }
        $tax=0;

        $keys = array_intersect(array_keys($this->_cart_contents[$items['rowid']]), array_keys($items));

        $this->load->admin_model('products_model');
        $this->load->admin_model('shop_model');

        $op_id = $items['option'];

        $product = $this->site->getProductByID($items['product_id']);

        $options = $this->shop_model->getProductVariants($product->id);
        $price = $product->promotion ? $product->promo_price : $product->price;
        $product_group_price_data= $this->site->getProductGroupPrice($product->id,$this->session->userdata('country_price_group'));
        if( $product_group_price_data->price !='') {
            $price = $product_group_price_data->price;
        }
        if (!empty($options)) {
            foreach ($options as $op) {
                if ($op['id'] == $op_id) {
                    $option = $op;
                }
            }
            $product_group_price_variant_data= $this->site->getProductGroupPrice($product->id,$this->session->userdata('country_price_group'),$op_id);
            if( $product_group_price_variant_data->price !='') {
                $price = $product_group_price_variant_data->price;
            }else{
                $price = $option['price']+$price;
            }
            // $price = $option['price']+$price;
        }

        if($this->session->userdata('country_vat') == 1) {
            $tax_rate = $this->site->getTaxRateByID($product->tax_rate);
            $ctax = $this->site->calculateTax($product, $tax_rate, $price);
            $tax = $this->sma->formatDecimal($ctax['amount']);
            $price = $this->sma->formatDecimal($price);
            $unit_price = $this->sma->formatDecimal($product->tax_method ? $price+$tax : $price);
        }else{
            $unit_price = $price;
        }
        $product_id= $items['product_id'];

        //if promotion with percentage is activated then we need to change the price of products.
        $how_much_off=0;
        $how_much_off_cart = $this->site->getPromotionPercentage('cartvaluerangeFree');
        if(!$how_much_off_cart) {
        $category_id = $this->site->getCategoryIdByProductID($product_id);
        $how_much_off_cat = $this->site->getPromotionPercentage('buyCategoryWithPercentage',$product_id, $category_id);
        $how_much_off_pro = $this->site->getPromotionPercentage('buyProductsWithPercentage',$product_id, $category_id);
        if($how_much_off_cat){
            $unit_price =  $this->site->calCulateProductPriceWithPromotionPercentagePos($unit_price, $how_much_off_cat);
        }else if($how_much_off_pro){
            $unit_price =  $this->site->calCulateProductPriceWithPromotionPercentagePos($unit_price, $how_much_off_pro);
        }
        }

        $account_POS_settings = $this->site->getPosSettings();
        if($account_POS_settings->rounding != 0) {
            $unit_price  = $this->sma->roundNumber($unit_price, $account_POS_settings->rounding);
        }



        if (isset($items['price'])) {
            $items['price'] = (float)$unit_price;
            $items['tax'] = (float)$tax;
        }

        foreach (array_diff($keys, array('id', 'name')) as $key) {
            $this->_cart_contents[$items['rowid']][$key] = $items[$key];
        }

        return TRUE;
    }

    protected function _save_cart() {
        $this->_cart_contents['cart_total'] = 0;
        $this->_cart_contents['total_items'] = 0;
        $this->_cart_contents['total_item_tax'] = 0;
        $this->_cart_contents['total_unique_items'] = 0;
        foreach ($this->_cart_contents as $key => $val) {
            if (!is_array($val) OR !isset($val['price'], $val['qty'])) {
                continue;
            }

            $this->_cart_contents['total_unique_items'] += 1;
            $this->_cart_contents['total_items'] += $val['qty'];
            $this->_cart_contents['cart_total'] += $this->sma->formatDecimal(($val['price'] * $val['qty']), 4);
            $this->_cart_contents['total_item_tax'] += $this->sma->formatDecimal(($val['tax'] * $val['qty']), 4);
            $this->_cart_contents[$key]['row_tax'] = $this->sma->formatDecimal(($this->_cart_contents[$key]['tax'] * $this->_cart_contents[$key]['qty']), 4);
            $this->_cart_contents[$key]['subtotal'] = $this->sma->formatDecimal(($this->_cart_contents[$key]['price'] * $this->_cart_contents[$key]['qty']), 4);
        }

        if (count($this->_cart_contents) <= 4) {
            $this->db->delete('cart', array('id' => $this->cart_id));
            return FALSE;
        }

        if ($this->db->get_where('cart', array('id' => $this->cart_id))->num_rows() > 0) {
            return $this->db->update('cart', array('time' => time(), 'user_id' => $this->session->userdata('user_id'), 'data' => json_encode($this->_cart_contents)), array('id' => $this->cart_id));
        } else {
            return $this->db->insert('cart', array('id' => $this->cart_id, 'time' => time(), 'user_id' => $this->session->userdata('user_id'), 'data' => json_encode($this->_cart_contents)));
        }

    }

    public function total() {
        return $this->sma->formatDecimal($this->_cart_contents['cart_total'], 4);
    }

    public function shipping() {

        return $this->sma->formatDecimal($this->shop_settings->shipping, 4);
    }

    public function order_tax() {
        if (!empty($this->Settings->tax2)) {
            if ($order_tax_details = $this->site->getTaxRateByID($this->Settings->default_tax_rate2)) {
                if ($order_tax_details->type == 2 || $order_tax_details->rate == 0) {
                    $order_tax = $this->sma->formatDecimal($order_tax_details->rate, 4);
                } elseif ($order_tax_details->type == 1) {
                    $order_tax = $this->sma->formatDecimal(((($this->total()) * $order_tax_details->rate) / 100), 4);
                }
                return $order_tax;
            }
        }
        return 0;
    }

    public function total_item_tax() {
        return $this->sma->formatDecimal($this->_cart_contents['total_item_tax'], 4);
    }

    public function remove($rowid) {
        unset($this->_cart_contents[$rowid]);
        $this->_save_cart();
        return TRUE;
    }

    public function total_items($unique = FALSE) {
        return $unique ? $this->_cart_contents['total_unique_items'] : $this->_cart_contents['total_items'];
    }

    public function contents($newest_first = FALSE) {
        $cart = ($newest_first) ? array_reverse($this->_cart_contents) : $this->_cart_contents;
        unset($cart['total_items']);
        unset($cart['total_item_tax']);
        unset($cart['total_unique_items']);
        unset($cart['cart_total']);
        return $cart;
    }

    public function get_item($row_id) {
        return (in_array($row_id, array('total_items', 'cart_total'), TRUE) OR !isset($this->_cart_contents[$row_id]))
        ? FALSE
        : $this->_cart_contents[$row_id];
    }

    public function has_options($row_id = '') {
        return (isset($this->_cart_contents[$row_id]['options']) && count($this->_cart_contents[$row_id]['options']) !== 0);
    }

    public function product_options($row_id = '') {
        return isset($this->_cart_contents[$row_id]['options']) ? $this->_cart_contents[$row_id]['options'] : array();
    }

    public function destroy() {
        $this->_empty();
        return $this->db->delete('cart', array('id' => $this->cart_id));
    }

    // Get cart with currency conversion
    function cart_data($re = false) {
        $citems = $this->contents();
        foreach($citems as &$value) {
            $value['price'] = $this->sma->convertMoney($value['price']);
            $value['subtotal'] = $this->sma->convertMoney($value['subtotal']);
            if ($this->has_options($value['rowid'])) {
                $value['options'] = $this->product_options($value['rowid']);
                foreach($value['options'] as &$opt_value) {
                    $opt_value['price'] = $this->sma->convertMoney($opt_value['price']);
                }
            }
        }
        $total = $this->sma->convertMoney($this->total(), FALSE, FALSE);
        $total_withooutconvert = $this->total();
        $shipping = $this->sma->convertMoney($this->shipping(), FALSE, FALSE);
        $order_tax = $this->sma->convertMoney($this->order_tax(), FALSE, FALSE);

        // product promotion done! message
        if($this->session->userdata('product_on_promotion_ping')) {
            $show_alert = 'you can select another product which has same value or less for free (Products With the same Mark)';
            $this->session->unset_userdata('product_on_promotion_ping');
        }else if($this->session->userdata('product_on_promotion_get')){
            $show_alert = 'Congrts.! You get free product.';
            $this->session->unset_userdata('product_on_promotion_get');
        }
        $total_old=0;
        $cartPromotionPercentage=0;
        $tax_permotion_total=0;
        $tax_permotion_discount=0;
        $permotion_discount=0;

        $total_rep = $this->sma->convertMoney($this->total()-$this->total_item_tax());
        //cart promotion
        $new_sb = $this->total()-$this->total_item_tax();
        $how_much_off = $this->site->getPromotionPercentage('cartvaluerangeFree');
        $price_new_total = 0;

        $range = $this->site->getPromotionCartRange('cartvaluerangeFree');
        if($how_much_off){
            if($range >0 && $total >=  $range) {
                $total_old = $total;
                $total_withooutconvert_old = $total_withooutconvert;
                $cartPromotionPercentage = $how_much_off;
                $total = $this->site->calCulateProductPriceWithPromotionPercentagePos($total, $how_much_off);
                $total_withooutconvert = $this->site->calCulateProductPriceWithPromotionPercentagePos($total_withooutconvert, $how_much_off);

                foreach ($this->_cart_contents as $key => $val) {
                    if (!is_array($val) OR !isset($val['price'], $val['qty'])) {
                        continue;
                    }
                    $price = $val['price'] - $val['tax'];
                    $price_new = $this->site->calCulateProductPriceWithPromotionPercentagePos($price, $how_much_off);
                    $product = $this->site->getProductByID($val['product_id']);
                    if ($this->session->userdata('country_vat') == 1) {
                        $tax_rate = $this->site->getTaxRateByID($product->tax_rate);
                        $ctax = $this->site->calculateTax($product, $tax_rate, ($price_new));
                        $tax_permotion = $this->sma->formatDecimal($ctax['amount']);
                    } else {
                        $tax_permotion = 0;
                    }
                    $tax_permotion_total += $this->sma->formatDecimal(($tax_permotion * $val['qty']), 4);
                    $price_new_total += $price_new;
                }
                $overall_permotion_discount = $total_old - $total;
                $overall_permotion_discount_mini = $total_withooutconvert_old - $total_withooutconvert;
                $total_dis = $this->site->calCulateProductPriceWithPromotionPercentagePos($this->total(), $how_much_off);
                $tax_permotion_discount = $total - $this->sma->convertMoney($tax_permotion_total);
                $tax_permotion_discount_1= $total_dis - $tax_permotion_total;
                $permotion_discount = ($this->total() - $this->total_item_tax()) - $tax_permotion_discount_1;
                $this->session->set_userdata('promotion_discount',$overall_permotion_discount_mini);
            }else{
                $this->session->set_userdata('promotion_discount',0);
            }
        }else{
            $this->session->set_userdata('promotion_discount',0);
        }



        $cart = array(
            'total_items' => $this->total_items(),
            'total_unique_items' => $this->total_items(TRUE),
            'contents' => $citems,
            'total_item_tax' => $this->sma->convertMoney($this->total_item_tax()),
            'subtotal' => $this->sma->convertMoney($this->total()- round($this->total_item_tax())),
           // 'subtotal' => $this->sma->convertMoney($total_sub),
            'total' => $this->sma->formatMoney($total, $this->selected_currency->symbol),
            'total_real' => $this->sma->formatMoney($this->total(), $this->selected_currency->symbol),
            //'total' => $this->sma->formatMoney($total, $this->selected_currency->symbol),
            'total_old' => $this->sma->formatMoney($total_old, $this->selected_currency->symbol),
            'cartPromotionPercentage' => $cartPromotionPercentage,
            'tax_permotion_total' => $this->sma->convertMoney($tax_permotion_total),
            'tax_permotion_discount' => $this->sma->formatMoney($tax_permotion_discount),
            'overall_permotion_discount' => $this->sma->formatMoney($overall_permotion_discount),
            'permotion_discount' => $this->sma->convertMoney($permotion_discount),
            'shipping' => $this->sma->formatMoney($shipping, $this->selected_currency->symbol),
            'order_tax' => $this->sma->formatMoney($order_tax, $this->selected_currency->symbol),
            'grand_total' => $this->sma->formatMoney(($this->sma->formatDecimal($total)+$this->sma->formatDecimal($order_tax)+$this->sma->formatDecimal($shipping)), $this->selected_currency->symbol),
            'cat_on_promotion' => $show_alert,
            );

        if ($re) {
            return $cart;
        }

        $this->sma->send_json($cart);
    }

}