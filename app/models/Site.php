<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Site extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    public function get_total_qty_alerts()
    {
        $this->db->where('quantity < alert_quantity', NULL, FALSE)->where('track_quantity', 1);
        return $this->db->count_all_results('products');
    }

    public function get_expiring_qty_alerts()
    {
        $date = date('Y-m-d', strtotime('+3 months'));
        $this->db->select('SUM(quantity_balance) as alert_num')
            ->where('expiry !=', NULL)->where('expiry !=', '0000-00-00')
            ->where('expiry <', $date);
        $q = $this->db->get('purchase_items');
        if ($q->num_rows() > 0) {
            $res = $q->row();
            return (INT)$res->alert_num;
        }
        return FALSE;
    }

    public function get_shop_sale_alerts()
    {
        $this->db->join('deliveries', 'deliveries.sale_id=sales.id', 'left')
            ->where('sales.shop', 1)->where('sales.sale_status', 'completed')->where('sales.payment_status', 'paid')
            ->group_start()->where('deliveries.status !=', 'delivered')->or_where('deliveries.status IS NULL', NULL)->group_end();
        return $this->db->count_all_results('sales');
    }

    public function get_shop_payment_alerts()
    {
        $this->db->where('shop', 1)->where('attachment !=', NULL)->where('payment_status !=', 'paid');
        return $this->db->count_all_results('sales');
    }

    public function get_setting()
    {
        $q = $this->db->get('settings');
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getDateFormat($id)
    {
        $q = $this->db->get_where('date_format', array('id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getAllCompanies($group_name)
    {
        $q = $this->db->get_where('companies', array('group_name' => $group_name));
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getCompanyByID($id)
    {
        $q = $this->db->get_where('companies', array('id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getAwardPointsOfCustomer($email)
    {
        $q = $this->db->get_where('companies', array('email' => $email), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }


    public function getCustomerGroupByID($id)
    {
        $q = $this->db->get_where('customer_groups', array('id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getUser($id = NULL)
    {
        if (!$id) {
            $id = $this->session->userdata('user_id');
        }
        $q = $this->db->get_where('users', array('id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getProductByID($id)
    {
        $q = $this->db->get_where('products', array('id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getCusomerID($email)
    {
        $q = $this->db->get_where('companies', array('email' => $email), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getProductOptions($pid)
    {
        $q = $this->db->get_where('product_variants', array('product_id' => $pid));
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getAllCurrencies()
    {
        $q = $this->db->get('currencies');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getCurrencyByCode($code)
    {
        $q = $this->db->get_where('currencies', array('code' => $code), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getAllTaxRates()
    {
        $q = $this->db->get('tax_rates');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getTaxRateByID($id)
    {
        $q = $this->db->get_where('tax_rates', array('id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getAllWarehouses()
    {
        $q = $this->db->get('warehouses');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getAllWarehousesForEcommerce()
    {
        $q = $this->db->get('settings');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data = $row->ecommerce_shop;
            }
            return (explode(",", $data));
        }
        return FALSE;
    }

    public function getAllWarehousesForEcommerceShop()
    {
        $q = $this->db->get('shop_settings');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data = $row->warehouse;
            }
            return (explode(",", $data));
        }
        return FALSE;
    }

    public function getWarehouseByID($id)
    {
        $q = $this->db->get_where('warehouses', array('id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getAllCategoriesWithSubCategories()
    {
        $q = $this->db->get("categories");
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }


    public function getAllCategories()
    {
        $this->db->where('parent_id', NULL)->or_where('parent_id', 0)->order_by('name');
        $q = $this->db->get("categories");
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getSubCategories($parent_id)
    {
        $this->db->where('parent_id', $parent_id)->order_by('name');
        $q = $this->db->get("categories");
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getCategoryByID($id)
    {
        $q = $this->db->get_where('categories', array('id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getGiftCardByID($id)
    {
        $q = $this->db->get_where('gift_cards', array('id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getGiftCardByNO($no)
    {
        $q = $this->db->get_where('gift_cards', array('card_no' => $no), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function updateInvoiceStatus()
    {
        $date = date('Y-m-d');
        $q = $this->db->get_where('invoices', array('status' => 'unpaid'));
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                if ($row->due_date < $date) {
                    $this->db->update('invoices', array('status' => 'due'), array('id' => $row->id));
                }
            }
            $this->db->update('settings', array('update' => $date), array('setting_id' => '1'));
            return true;
        }
    }

    public function modal_js()
    {
        return '<script type="text/javascript">' . file_get_contents($this->data['assets'] . 'js/modal.js') . '</script>';
    }

    public function getReference($field)
    {
        $q = $this->db->get_where('order_ref', array('ref_id' => '1'), 1);
        if ($q->num_rows() > 0) {
            $ref = $q->row();
            switch ($field) {
                case 'so':
                    $prefix = $this->Settings->sales_prefix;
                    break;
                case 'pos':
                    $prefix = isset($this->Settings->sales_prefix) ? $this->Settings->sales_prefix . '/POS' : '';
                    break;
                case 'qu':
                    $prefix = $this->Settings->quote_prefix;
                    break;
                case 'po':
                    $prefix = $this->Settings->purchase_prefix;
                    break;
                case 'to':
                    $prefix = $this->Settings->transfer_prefix;
                    break;
                case 'do':
                    $prefix = $this->Settings->delivery_prefix;
                    break;
                case 'pay':
                    $prefix = $this->Settings->payment_prefix;
                    break;
                case 'ppay':
                    $prefix = $this->Settings->ppayment_prefix;
                    break;
                case 'ex':
                    $prefix = $this->Settings->expense_prefix;
                    break;
                case 're':
                    $prefix = $this->Settings->return_prefix;
                    break;
                case 'rep':
                    $prefix = $this->Settings->returnp_prefix;
                    break;
                case 'qa':
                    $prefix = $this->Settings->returnp_prefix;
                    break;
                default:
                    $prefix = '';
            }

            // $ref_no = (!empty($prefix)) ? $prefix . '/' : '';
            $ref_no = $prefix;

            if ($this->Settings->reference_format == 1) {
                $ref_no .= date('Y') . "/" . sprintf("%04s", $ref->{$field});
            } elseif ($this->Settings->reference_format == 2) {
                $ref_no .= date('Y') . "/" . date('m') . "/" . sprintf("%04s", $ref->{$field});
            } elseif ($this->Settings->reference_format == 3) {
                $ref_no .= sprintf("%04s", $ref->{$field});
            } else {
                $ref_no .= $this->getRandomReference();
            }

            return $ref_no;
        }
        return FALSE;
    }

    public function getRandomReference($len = 12)
    {
        $result = '';
        for ($i = 0; $i < $len; $i++) {
            $result .= mt_rand(0, 9);
        }

        if ($this->getSaleByReference($result)) {
            $this->getRandomReference();
        }

        return $result;
    }

    public function getSaleByReference($ref)
    {
        $this->db->like('reference_no', $ref, 'before');
        $q = $this->db->get('sales', 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function updateReference($field)
    {
        $q = $this->db->get_where('order_ref', array('ref_id' => '1'), 1);
        if ($q->num_rows() > 0) {
            $ref = $q->row();
            $this->db->update('order_ref', array($field => $ref->{$field} + 1), array('ref_id' => '1'));
            return TRUE;
        }
        return FALSE;
    }

    public function checkPermissions()
    {
        $q = $this->db->get_where('permissions', array('group_id' => $this->session->userdata('group_id')), 1);
        if ($q->num_rows() > 0) {
            return $q->result_array();
        }
        return FALSE;
    }

    public function getNotifications()
    {
        $date = date('Y-m-d H:i:s', time());
        $this->db->where("from_date <=", $date);
        $this->db->where("till_date >=", $date);
        if (!$this->Owner) {
            if ($this->Supplier) {
                $this->db->where('scope', 4);
            } elseif ($this->Customer) {
                $this->db->where('scope', 1)->or_where('scope', 3);
            } elseif (!$this->Customer && !$this->Supplier) {
                $this->db->where('scope', 2)->or_where('scope', 3);
            }
        }
        $q = $this->db->get("notifications");
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
    }

    public function getUpcomingEvents()
    {
        $dt = date('Y-m-d');
        $this->db->where('start >=', $dt)->order_by('start')->limit(5);
        if ($this->Settings->restrict_calendar) {
            $this->db->where('user_id', $this->session->userdata('user_id'));
        }

        $q = $this->db->get('calendar');

        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getUserGroup($user_id = false)
    {
        if (!$user_id) {
            $user_id = $this->session->userdata('user_id');
        }
        $group_id = $this->getUserGroupID($user_id);
        $q = $this->db->get_where('groups', array('id' => $group_id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getUserGroupID($user_id = false)
    {
        $user = $this->getUser($user_id);
        return $user->group_id;
    }

    public function getWarehouseProductsVariants($option_id, $warehouse_id = NULL)
    {
        if ($warehouse_id) {
            $this->db->where('warehouse_id', $warehouse_id);
        }
        $q = $this->db->get_where('warehouses_products_variants', array('option_id' => $option_id));
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getPurchasedItem($clause)
    {
        $orderby = ($this->Settings->accounting_method == 1) ? 'asc' : 'desc';
        $this->db->order_by('date', $orderby);
        $this->db->order_by('purchase_id', $orderby);
        if (!isset($clause['option_id']) || empty($clause['option_id'])) {
            $this->db->group_start()->where('option_id', NULL)->or_where('option_id', 0)->group_end();
        }
        $q = $this->db->get_where('purchase_items', $clause);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function setPurchaseItem($clause, $qty)
    {
        if ($product = $this->getProductByID($clause['product_id'])) {
            if ($pi = $this->getPurchasedItem($clause)) {
                $quantity_balance = $pi->quantity_balance + $qty;
                return $this->db->update('purchase_items', array('quantity_balance' => $quantity_balance), array('id' => $pi->id));
            } else {
                $unit = $this->getUnitByID($product->unit);
                $clause['product_unit_id'] = $product->unit;
                $clause['product_unit_code'] = $unit->code;
                $clause['product_code'] = $product->code;
                $clause['product_name'] = $product->name;
                $clause['purchase_id'] = $clause['transfer_id'] = $clause['item_tax'] = NULL;
                $clause['net_unit_cost'] = $clause['real_unit_cost'] = $clause['unit_cost'] = $product->cost;
                $clause['quantity_balance'] = $clause['quantity'] = $clause['unit_quantity'] = $clause['quantity_received'] = $qty;
                $clause['subtotal'] = ($product->cost * $qty);
                if (isset($product->tax_rate) && $product->tax_rate != 0) {
                    $tax_details = $this->site->getTaxRateByID($product->tax_rate);
                    $ctax = $this->calculateTax($product, $tax_details, $product->cost);
                    $item_tax = $clause['item_tax'] = $ctax['amount'];
                    $tax = $clause['tax'] = $ctax['tax'];
                    $clause['tax_rate_id'] = $tax_details->id;
                    if ($product->tax_method != 1) {
                        $clause['net_unit_cost'] = $product->cost - $item_tax;
                        $clause['unit_cost'] = $product->cost;
                    } else {
                        $clause['net_unit_cost'] = $product->cost;
                        $clause['unit_cost'] = $product->cost + $item_tax;
                    }
                    $pr_item_tax = $this->sma->formatDecimal($item_tax * $clause['unit_quantity'], 4);
                    if ($this->Settings->indian_gst && $gst_data = $this->gst->calculteIndianGST($pr_item_tax, ($this->Settings->state == $supplier_details->state), $tax_details)) {
                        $clause['gst'] = $gst_data['gst'];
                        $clause['cgst'] = $gst_data['cgst'];
                        $clause['sgst'] = $gst_data['sgst'];
                        $clause['igst'] = $gst_data['igst'];
                    }
                    $clause['subtotal'] = (($clause['net_unit_cost'] * $clause['unit_quantity']) + $pr_item_tax);
                }
                $clause['status'] = 'received';
                $clause['date'] = date('Y-m-d');
                $clause['option_id'] = !empty($clause['option_id']) && is_numeric($clause['option_id']) ? $clause['option_id'] : NULL;
                return $this->db->insert('purchase_items', $clause);
            }
        }
        return FALSE;
    }

    public function syncVariantQty($variant_id, $warehouse_id, $product_id = NULL)
    {
        $balance_qty = $this->getBalanceVariantQuantity($variant_id);
        $wh_balance_qty = $this->getBalanceVariantQuantity($variant_id, $warehouse_id);
        if ($this->db->update('product_variants', array('quantity' => $balance_qty), array('id' => $variant_id))) {
            if ($this->getWarehouseProductsVariants($variant_id, $warehouse_id)) {
                $this->db->update('warehouses_products_variants', array('quantity' => $wh_balance_qty), array('option_id' => $variant_id, 'warehouse_id' => $warehouse_id));
            } else {
                if ($wh_balance_qty) {
                    $this->db->insert('warehouses_products_variants', array('quantity' => $wh_balance_qty, 'option_id' => $variant_id, 'warehouse_id' => $warehouse_id, 'product_id' => $product_id));
                }
            }
            return TRUE;
        }
        return FALSE;
    }

    public function getWarehouseProducts($product_id, $warehouse_id = NULL)
    {
        if ($warehouse_id) {
            $this->db->where('warehouse_id', $warehouse_id);
        }
        $q = $this->db->get_where('warehouses_products', array('product_id' => $product_id));
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function syncProductQty($product_id, $warehouse_id)
    {
        $balance_qty = $this->getBalanceQuantity($product_id);
        $wh_balance_qty = $this->getBalanceQuantity($product_id, $warehouse_id);
        if ($this->db->update('products', array('quantity' => $balance_qty), array('id' => $product_id))) {
            if ($this->getWarehouseProducts($product_id, $warehouse_id)) {
                $this->db->update('warehouses_products', array('quantity' => $wh_balance_qty), array('product_id' => $product_id, 'warehouse_id' => $warehouse_id));
            } else {
                if (!$wh_balance_qty) {
                    $wh_balance_qty = 0;
                }
                $product = $this->site->getProductByID($product_id);
                $this->db->insert('warehouses_products', array('quantity' => $wh_balance_qty, 'product_id' => $product_id, 'warehouse_id' => $warehouse_id, 'avg_cost' => $product->cost));
            }
            return TRUE;
        }
        return FALSE;
    }

    public function getSaleByID($id)
    {
        $q = $this->db->get_where('sales', array('id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getSalePayments($sale_id)
    {
        $q = $this->db->get_where('payments', array('sale_id' => $sale_id));
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function syncSalePayments($id)
    {
        $sale = $this->getSaleByID($id);
        if ($payments = $this->getSalePayments($id)) {
            $paid = 0;
            $grand_total = $sale->grand_total + $sale->rounding;
            foreach ($payments as $payment) {
                $paid += $payment->amount;
            }

            $payment_status = $paid == 0 ? 'pending' : $sale->payment_status;
            if ($this->sma->formatDecimal($grand_total) == $this->sma->formatDecimal($paid)) {
                $payment_status = 'paid';
            } elseif ($sale->due_date <= date('Y-m-d') && !$sale->sale_id) {
                $payment_status = 'due';
            } elseif ($paid != 0) {
                $payment_status = 'partial';
            }

            if ($this->db->update('sales', array('paid' => $paid, 'payment_status' => $payment_status), array('id' => $id))) {
                return true;
            }
        } else {
            $payment_status = ($sale->due_date <= date('Y-m-d')) ? 'due' : 'pending';
            if ($this->db->update('sales', array('paid' => 0, 'payment_status' => $payment_status), array('id' => $id))) {
                return true;
            }
        }

        return FALSE;
    }

    public function getPurchaseByID($id)
    {
        $q = $this->db->get_where('purchases', array('id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getPurchasePayments($purchase_id)
    {
        $q = $this->db->get_where('payments', array('purchase_id' => $purchase_id));
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function syncPurchasePayments($id)
    {
        $purchase = $this->getPurchaseByID($id);
        $paid = 0;
        if ($payments = $this->getPurchasePayments($id)) {
            foreach ($payments as $payment) {
                $paid += $payment->amount;
            }
        }

        $payment_status = $paid <= 0 ? 'pending' : $purchase->payment_status;
        if ($this->sma->formatDecimal($purchase->grand_total) > $this->sma->formatDecimal($paid) && $paid > 0) {
            $payment_status = 'partial';
        } elseif ($this->sma->formatDecimal($purchase->grand_total) <= $this->sma->formatDecimal($paid)) {
            $payment_status = 'paid';
        }

        if ($this->db->update('purchases', array('paid' => $paid, 'payment_status' => $payment_status), array('id' => $id))) {
            return true;
        }

        return FALSE;
    }

    private function getBalanceQuantity($product_id, $warehouse_id = NULL)
    {
        $this->db->select('SUM(COALESCE(quantity_balance, 0)) as stock', False);
        $this->db->where('product_id', $product_id)->where('quantity_balance !=', 0);
        if ($warehouse_id) {
            $this->db->where('warehouse_id', $warehouse_id);
        }
        $this->db->group_start()->where('status', 'received')->or_where('status', 'partial')->group_end();
        $q = $this->db->get('purchase_items');
        if ($q->num_rows() > 0) {
            $data = $q->row();
            return $data->stock;
        }
        return 0;
    }

    private function getBalanceVariantQuantity($variant_id, $warehouse_id = NULL)
    {
        $this->db->select('SUM(COALESCE(quantity_balance, 0)) as stock', False);
        $this->db->where('option_id', $variant_id)->where('quantity_balance !=', 0);
        if ($warehouse_id) {
            $this->db->where('warehouse_id', $warehouse_id);
        }
        $this->db->group_start()->where('status', 'received')->or_where('status', 'partial')->group_end();
        $q = $this->db->get('purchase_items');
        if ($q->num_rows() > 0) {
            $data = $q->row();
            return $data->stock;
        }
        return 0;
    }

    public function calculateAVCost($product_id, $warehouse_id, $net_unit_price, $unit_price, $quantity, $product_name, $option_id, $item_quantity)
    {

        $product = $this->getProductByID($product_id);
        $real_item_qty = $quantity;
        $wp_details = $this->getWarehouseProduct($warehouse_id, $product_id);
        $con = $wp_details ? $wp_details->avg_cost : $product->cost;
        $tax_rate = $this->getTaxRateByID($product->tax_rate);
        $ctax = $this->calculateTax($product, $tax_rate, $con);
        if ($product->tax_method) {
            $avg_net_unit_cost = $con;
            $avg_unit_cost = ($con + $ctax['amount']);
        } else {
            $avg_unit_cost = $con;
            $avg_net_unit_cost = ($con - $ctax['amount']);
        }

        if ($pis = $this->getPurchasedItems($product_id, $warehouse_id, $option_id)) {
            $cost_row = array();
            $quantity = $item_quantity;
            $balance_qty = $quantity;
            foreach ($pis as $pi) {
                if (!empty($pi) && $balance_qty <= $quantity && $quantity > 0) {

                    if ($pi->quantity_balance >= $quantity && $quantity > 0) {
                        $balance_qty = $pi->quantity_balance - $quantity;
                        $cost_row = array('date' => date('Y-m-d'), 'product_id' => $product_id, 'sale_item_id' => 'sale_items.id', 'purchase_item_id' => $pi->id, 'quantity' => $quantity, 'purchase_net_unit_cost' => $avg_net_unit_cost, 'purchase_unit_cost' => $avg_unit_cost, 'sale_net_unit_price' => $net_unit_price, 'sale_unit_price' => $unit_price, 'quantity_balance' => $balance_qty, 'inventory' => 1, 'option_id' => $option_id);
                        $quantity = 0;
                    } elseif ($quantity > 0) {
                        $quantity = $quantity - $pi->quantity_balance;
                        $balance_qty = $quantity;
                        $cost_row = array('date' => date('Y-m-d'), 'product_id' => $product_id, 'sale_item_id' => 'sale_items.id', 'purchase_item_id' => $pi->id, 'quantity' => $pi->quantity_balance, 'purchase_net_unit_cost' => $avg_net_unit_cost, 'purchase_unit_cost' => $avg_unit_cost, 'sale_net_unit_price' => $net_unit_price, 'sale_unit_price' => $unit_price, 'quantity_balance' => 0, 'inventory' => 1, 'option_id' => $option_id);
                    }
                }
                if (empty($cost_row)) {
                    break;
                }
                $cost[] = $cost_row;
                if ($quantity == 0) {
                    break;
                }
            }
        }
        if ($quantity > 0 && !$this->Settings->overselling) {
            $this->session->set_flashdata('error', sprintf(lang("quantity_out_of_stock_for_%s"), ($pi->product_name ? $pi->product_name : $product_name)));
            redirect($_SERVER["HTTP_REFERER"]);
        } elseif ($quantity > 0) {
            $cost[] = array('date' => date('Y-m-d'), 'product_id' => $product_id, 'sale_item_id' => 'sale_items.id', 'purchase_item_id' => NULL, 'quantity' => $real_item_qty, 'purchase_net_unit_cost' => $avg_net_unit_cost, 'purchase_unit_cost' => $avg_unit_cost, 'sale_net_unit_price' => $net_unit_price, 'sale_unit_price' => $unit_price, 'quantity_balance' => NULL, 'overselling' => 1, 'inventory' => 1);
            $cost[] = array('pi_overselling' => 1, 'product_id' => $product_id, 'quantity_balance' => (0 - $quantity), 'warehouse_id' => $warehouse_id, 'option_id' => $option_id);
        }
        return $cost;
    }

    public function calculateCost($product_id, $warehouse_id, $net_unit_price, $unit_price, $quantity, $product_name, $option_id, $item_quantity)
    {
        $pis = $this->getPurchasedItems($product_id, $warehouse_id, $option_id);
        $real_item_qty = $quantity;
        $quantity = $item_quantity;
        $balance_qty = $quantity;
        foreach ($pis as $pi) {
            $cost_row = NULL;
            if (!empty($pi) && $balance_qty <= $quantity && $quantity > 0) {
                $purchase_unit_cost = $pi->unit_cost ? $pi->unit_cost : ($pi->net_unit_cost + ($pi->item_tax / $pi->quantity));
                if ($pi->quantity_balance >= $quantity && $quantity > 0) {
                    $balance_qty = $pi->quantity_balance - $quantity;
                    $cost_row = array('date' => date('Y-m-d'), 'product_id' => $product_id, 'sale_item_id' => 'sale_items.id', 'purchase_item_id' => $pi->id, 'quantity' => $quantity, 'purchase_net_unit_cost' => $pi->net_unit_cost, 'purchase_unit_cost' => $purchase_unit_cost, 'sale_net_unit_price' => $net_unit_price, 'sale_unit_price' => $unit_price, 'quantity_balance' => $balance_qty, 'inventory' => 1, 'option_id' => $option_id);
                    $quantity = 0;
                } elseif ($quantity > 0) {
                    $quantity = $quantity - $pi->quantity_balance;
                    $balance_qty = $quantity;
                    $cost_row = array('date' => date('Y-m-d'), 'product_id' => $product_id, 'sale_item_id' => 'sale_items.id', 'purchase_item_id' => $pi->id, 'quantity' => $pi->quantity_balance, 'purchase_net_unit_cost' => $pi->net_unit_cost, 'purchase_unit_cost' => $purchase_unit_cost, 'sale_net_unit_price' => $net_unit_price, 'sale_unit_price' => $unit_price, 'quantity_balance' => 0, 'inventory' => 1, 'option_id' => $option_id);
                }
            }
            $cost[] = $cost_row;
            if ($quantity == 0) {
                break;
            }
        }
        if ($quantity > 0) {
            $this->session->set_flashdata('error', sprintf(lang("quantity_out_of_stock_for_%s"), (isset($pi->product_name) ? $pi->product_name : $product_name)));
            redirect($_SERVER["HTTP_REFERER"]);
        }
        return $cost;
    }

    public function getPurchasedItems($product_id, $warehouse_id, $option_id = NULL)
    {
        $orderby = ($this->Settings->accounting_method == 1) ? 'asc' : 'desc';
        $this->db->select('id, quantity, quantity_balance, net_unit_cost, unit_cost, item_tax');
        $this->db->where('product_id', $product_id)->where('warehouse_id', $warehouse_id)->where('quantity_balance !=', 0);
        if (!isset($option_id) || empty($option_id)) {
            $this->db->group_start()->where('option_id', NULL)->or_where('option_id', 0)->group_end();
        } else {
            $this->db->where('option_id', $option_id);
        }
        $this->db->group_start()->where('status', 'received')->or_where('status', 'partial')->group_end();
        $this->db->group_by('id');
        $this->db->order_by('date', $orderby);
        $this->db->order_by('purchase_id', $orderby);
        $q = $this->db->get('purchase_items');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getProductComboItems($pid, $warehouse_id = NULL)
    {
        $this->db->select('products.id as id, combo_items.item_code as code, combo_items.quantity as qty, products.name as name, products.type as type, combo_items.unit_price as unit_price, warehouses_products.quantity as quantity')
            ->join('products', 'products.code=combo_items.item_code', 'left')
            ->join('warehouses_products', 'warehouses_products.product_id=products.id', 'left')
            ->group_by('combo_items.id');
        if ($warehouse_id) {
            $this->db->where('warehouses_products.warehouse_id', $warehouse_id);
        }
        $q = $this->db->get_where('combo_items', array('combo_items.product_id' => $pid));
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }

            return $data;
        }
        return FALSE;
    }

    public function item_costing($item, $pi = NULL)
    {

        $item_quantity = $pi ? $item['aquantity'] : $item['quantity'];
        if (!isset($item['option_id']) || empty($item['option_id']) || $item['option_id'] == 'null') {
            $item['option_id'] = NULL;
        }

        if ($this->Settings->accounting_method != 2 && !$this->Settings->overselling) {

            if ($this->getProductByID($item['product_id'])) {
                if ($item['product_type'] == 'standard') {
                    $unit = $this->getUnitByID($item['product_unit_id']);
                    $item['net_unit_price'] = $this->convertToBase($unit, $item['net_unit_price']);
                    $item['unit_price'] = $this->convertToBase($unit, $item['unit_price']);
                    $cost = $this->calculateCost($item['product_id'], $item['warehouse_id'], $item['net_unit_price'], $item['unit_price'], $item['quantity'], $item['product_name'], $item['option_id'], $item_quantity);
                } elseif ($item['product_type'] == 'combo') {
                    $combo_items = $this->getProductComboItems($item['product_id'], $item['warehouse_id']);
                    foreach ($combo_items as $combo_item) {
                        $pr = $this->getProductByCode($combo_item->code);
                        if ($pr->tax_rate) {
                            $pr_tax = $this->getTaxRateByID($pr->tax_rate);
                            if ($pr->tax_method) {
                                $item_tax = $this->sma->formatDecimal((($combo_item->unit_price) * $pr_tax->rate) / (100 + $pr_tax->rate));
                                $net_unit_price = $combo_item->unit_price - $item_tax;
                                $unit_price = $combo_item->unit_price;
                            } else {
                                $item_tax = $this->sma->formatDecimal((($combo_item->unit_price) * $pr_tax->rate) / 100);
                                $net_unit_price = $combo_item->unit_price;
                                $unit_price = $combo_item->unit_price + $item_tax;
                            }
                        } else {
                            $net_unit_price = $combo_item->unit_price;
                            $unit_price = $combo_item->unit_price;
                        }
                        if ($pr->type == 'standard') {
                            $cost[] = $this->calculateCost($pr->id, $item['warehouse_id'], $net_unit_price, $unit_price, ($combo_item->qty * $item['quantity']), $pr->name, NULL, $item_quantity);
                        } else {
                            $cost[] = array(array('date' => date('Y-m-d'), 'product_id' => $pr->id, 'sale_item_id' => 'sale_items.id', 'purchase_item_id' => NULL, 'quantity' => ($combo_item->qty * $item['quantity']), 'purchase_net_unit_cost' => 0, 'purchase_unit_cost' => 0, 'sale_net_unit_price' => $combo_item->unit_price, 'sale_unit_price' => $combo_item->unit_price, 'quantity_balance' => NULL, 'inventory' => NULL));
                        }
                    }
                } else {
                    $cost = array(array('date' => date('Y-m-d'), 'product_id' => $item['product_id'], 'sale_item_id' => 'sale_items.id', 'purchase_item_id' => NULL, 'quantity' => $item['quantity'], 'purchase_net_unit_cost' => 0, 'purchase_unit_cost' => 0, 'sale_net_unit_price' => $item['net_unit_price'], 'sale_unit_price' => $item['unit_price'], 'quantity_balance' => NULL, 'inventory' => NULL));
                }
            } elseif ($item['product_type'] == 'manual') {
                $cost = array(array('date' => date('Y-m-d'), 'product_id' => $item['product_id'], 'sale_item_id' => 'sale_items.id', 'purchase_item_id' => NULL, 'quantity' => $item['quantity'], 'purchase_net_unit_cost' => 0, 'purchase_unit_cost' => 0, 'sale_net_unit_price' => $item['net_unit_price'], 'sale_unit_price' => $item['unit_price'], 'quantity_balance' => NULL, 'inventory' => NULL));
            }

        } else {

            if ($this->getProductByID($item['product_id'])) {
                if ($item['product_type'] == 'standard') {
                    $cost = $this->calculateAVCost($item['product_id'], $item['warehouse_id'], $item['net_unit_price'], $item['unit_price'], $item['quantity'], $item['product_name'], $item['option_id'], $item_quantity);
                } elseif ($item['product_type'] == 'combo') {
                    $combo_items = $this->getProductComboItems($item['product_id'], $item['warehouse_id']);
                    foreach ($combo_items as $combo_item) {
                        $pr = $this->getProductByCode($combo_item->code);
                        if ($pr->tax_rate) {
                            $pr_tax = $this->getTaxRateByID($pr->tax_rate);
                            if ($pr->tax_method) {
                                $item_tax = $this->sma->formatDecimal((($combo_item->unit_price) * $pr_tax->rate) / (100 + $pr_tax->rate));
                                $net_unit_price = $combo_item->unit_price - $item_tax;
                                $unit_price = $combo_item->unit_price;
                            } else {
                                $item_tax = $this->sma->formatDecimal((($combo_item->unit_price) * $pr_tax->rate) / 100);
                                $net_unit_price = $combo_item->unit_price;
                                $unit_price = $combo_item->unit_price + $item_tax;
                            }
                        } else {
                            $net_unit_price = $combo_item->unit_price;
                            $unit_price = $combo_item->unit_price;
                        }
                        $cost[] = $this->calculateAVCost($combo_item->id, $item['warehouse_id'], $net_unit_price, $unit_price, ($combo_item->qty * $item['quantity']), $item['product_name'], $item['option_id'], $item_quantity);
                    }
                } else {
                    $cost = array(array('date' => date('Y-m-d'), 'product_id' => $item['product_id'], 'sale_item_id' => 'sale_items.id', 'purchase_item_id' => NULL, 'quantity' => $item['quantity'], 'purchase_net_unit_cost' => 0, 'purchase_unit_cost' => 0, 'sale_net_unit_price' => $item['net_unit_price'], 'sale_unit_price' => $item['unit_price'], 'quantity_balance' => NULL, 'inventory' => NULL));
                }
            } elseif ($item['product_type'] == 'manual') {
                $cost = array(array('date' => date('Y-m-d'), 'product_id' => $item['product_id'], 'sale_item_id' => 'sale_items.id', 'purchase_item_id' => NULL, 'quantity' => $item['quantity'], 'purchase_net_unit_cost' => 0, 'purchase_unit_cost' => 0, 'sale_net_unit_price' => $item['net_unit_price'], 'sale_unit_price' => $item['unit_price'], 'quantity_balance' => NULL, 'inventory' => NULL));
            }

        }
        return $cost;
    }

    public function item_costing_check($item, $pi = NULL)
    {
        $item_quantity = $pi ? $item['aquantity'] : $item['quantity'];
        if (!isset($item['option_id']) || empty($item['option_id']) || $item['option_id'] == 'null') {
            $item['option_id'] = NULL;
        }

        if ($this->Settings->accounting_method != 2 && !$this->Settings->overselling) {

            if ($this->getProductByID($item['product_id'])) {
                if ($item['product_type'] == 'standard') {
                    $unit = $this->getUnitByID($item['product_unit_id']);
                    $item['net_unit_price'] = $this->convertToBase($unit, $item['net_unit_price']);
                    $item['unit_price'] = $this->convertToBase($unit, $item['unit_price']);
                    $cost = $this->calculateCost($item['product_id'], $item['warehouse_id'], $item['net_unit_price'], $item['unit_price'], $item['quantity'], $item['product_name'], $item['option_id'], $item_quantity);
                } elseif ($item['product_type'] == 'combo') {
                    $combo_items = $this->getProductComboItems($item['product_id'], $item['warehouse_id']);
                    foreach ($combo_items as $combo_item) {
                        $pr = $this->getProductByCode($combo_item->code);
                        if ($pr->tax_rate) {
                            $pr_tax = $this->getTaxRateByID($pr->tax_rate);
                            if ($pr->tax_method) {
                                $item_tax = $this->sma->formatDecimal((($combo_item->unit_price) * $pr_tax->rate) / (100 + $pr_tax->rate));
                                $net_unit_price = $combo_item->unit_price - $item_tax;
                                $unit_price = $combo_item->unit_price;
                            } else {
                                $item_tax = $this->sma->formatDecimal((($combo_item->unit_price) * $pr_tax->rate) / 100);
                                $net_unit_price = $combo_item->unit_price;
                                $unit_price = $combo_item->unit_price + $item_tax;
                            }
                        } else {
                            $net_unit_price = $combo_item->unit_price;
                            $unit_price = $combo_item->unit_price;
                        }
                        if ($pr->type == 'standard') {
                            $cost[] = $this->calculateCost($pr->id, $item['warehouse_id'], $net_unit_price, $unit_price, ($combo_item->qty * $item['quantity']), $pr->name, NULL, $item_quantity);
                        } else {
                            $cost[] = array(array('date' => date('Y-m-d'), 'product_id' => $pr->id, 'sale_item_id' => 'sale_items.id', 'purchase_item_id' => NULL, 'quantity' => ($combo_item->qty * $item['quantity']), 'purchase_net_unit_cost' => 0, 'purchase_unit_cost' => 0, 'sale_net_unit_price' => $combo_item->unit_price, 'sale_unit_price' => $combo_item->unit_price, 'quantity_balance' => NULL, 'inventory' => NULL));
                        }
                    }
                } else {
                    $cost = array(array('date' => date('Y-m-d'), 'product_id' => $item['product_id'], 'sale_item_id' => 'sale_items.id', 'purchase_item_id' => NULL, 'quantity' => $item['quantity'], 'purchase_net_unit_cost' => 0, 'purchase_unit_cost' => 0, 'sale_net_unit_price' => $item['net_unit_price'], 'sale_unit_price' => $item['unit_price'], 'quantity_balance' => NULL, 'inventory' => NULL));
                }
            } elseif ($item['product_type'] == 'manual') {
                $cost = array(array('date' => date('Y-m-d'), 'product_id' => $item['product_id'], 'sale_item_id' => 'sale_items.id', 'purchase_item_id' => NULL, 'quantity' => $item['quantity'], 'purchase_net_unit_cost' => 0, 'purchase_unit_cost' => 0, 'sale_net_unit_price' => $item['net_unit_price'], 'sale_unit_price' => $item['unit_price'], 'quantity_balance' => NULL, 'inventory' => NULL));
            }

        } else {

            if ($this->getProductByID($item['product_id'])) {
                if ($item['product_type'] == 'standard') {
                    $cost = $this->calculateAVCost($item['product_id'], $item['warehouse_id'], $item['net_unit_price'], $item['unit_price'], $item['quantity'], $item['product_name'], $item['option_id'], $item_quantity);
                } elseif ($item['product_type'] == 'combo') {
                    $combo_items = $this->getProductComboItems($item['product_id'], $item['warehouse_id']);
                    foreach ($combo_items as $combo_item) {
                        $pr = $this->getProductByCode($combo_item->code);
                        if ($pr->tax_rate) {
                            $pr_tax = $this->getTaxRateByID($pr->tax_rate);
                            if ($pr->tax_method) {
                                $item_tax = $this->sma->formatDecimal((($combo_item->unit_price) * $pr_tax->rate) / (100 + $pr_tax->rate));
                                $net_unit_price = $combo_item->unit_price - $item_tax;
                                $unit_price = $combo_item->unit_price;
                            } else {
                                $item_tax = $this->sma->formatDecimal((($combo_item->unit_price) * $pr_tax->rate) / 100);
                                $net_unit_price = $combo_item->unit_price;
                                $unit_price = $combo_item->unit_price + $item_tax;
                            }
                        } else {
                            $net_unit_price = $combo_item->unit_price;
                            $unit_price = $combo_item->unit_price;
                        }
                        $cost[] = $this->calculateAVCost($combo_item->id, $item['warehouse_id'], $net_unit_price, $unit_price, ($combo_item->qty * $item['quantity']), $item['product_name'], $item['option_id'], $item_quantity);
                    }
                } else {
                    $cost = array(array('date' => date('Y-m-d'), 'product_id' => $item['product_id'], 'sale_item_id' => 'sale_items.id', 'purchase_item_id' => NULL, 'quantity' => $item['quantity'], 'purchase_net_unit_cost' => 0, 'purchase_unit_cost' => 0, 'sale_net_unit_price' => $item['net_unit_price'], 'sale_unit_price' => $item['unit_price'], 'quantity_balance' => NULL, 'inventory' => NULL));
                }
            } elseif ($item['product_type'] == 'manual') {
                $cost = array(array('date' => date('Y-m-d'), 'product_id' => $item['product_id'], 'sale_item_id' => 'sale_items.id', 'purchase_item_id' => NULL, 'quantity' => $item['quantity'], 'purchase_net_unit_cost' => 0, 'purchase_unit_cost' => 0, 'sale_net_unit_price' => $item['net_unit_price'], 'sale_unit_price' => $item['unit_price'], 'quantity_balance' => NULL, 'inventory' => NULL));
            }

        }
        return $cost;
    }

    public function costing($items)
    {

        $citems = array();
        foreach ($items as $item) {
            $option = (isset($item['option_id']) && !empty($item['option_id']) && $item['option_id'] != 'null' && $item['option_id'] != 'false') ? $item['option_id'] : '';
            $pr = $this->getProductByID($item['product_id']);
            $item['option_id'] = $option;
            if ($pr && $pr->type == 'standard') {
                if (isset($citems['p' . $item['product_id'] . 'o' . $item['option_id']])) {
                    $citems['p' . $item['product_id'] . 'o' . $item['option_id']]['aquantity'] += $item['quantity'];
                } else {
                    $citems['p' . $item['product_id'] . 'o' . $item['option_id']] = $item;
                    $citems['p' . $item['product_id'] . 'o' . $item['option_id']]['aquantity'] = $item['quantity'];
                }
            } elseif ($pr && $pr->type == 'combo') {
                $wh = $this->Settings->overselling ? NULL : $item['warehouse_id'];
                $combo_items = $this->getProductComboItems($item['product_id'], $wh);
                foreach ($combo_items as $combo_item) {
                    if ($combo_item->type == 'standard') {
                        if (isset($citems['p' . $combo_item->id . 'o' . $item['option_id']])) {
                            $citems['p' . $combo_item->id . 'o' . $item['option_id']]['aquantity'] += ($combo_item->qty * $item['quantity']);
                        } else {
                            $cpr = $this->getProductByID($combo_item->id);
                            if ($cpr->tax_rate) {
                                $cpr_tax = $this->getTaxRateByID($cpr->tax_rate);
                                if ($cpr->tax_method) {
                                    $item_tax = $this->sma->formatDecimal((($combo_item->unit_price) * $cpr_tax->rate) / (100 + $cpr_tax->rate));
                                    $net_unit_price = $combo_item->unit_price - $item_tax;
                                    $unit_price = $combo_item->unit_price;
                                } else {
                                    $item_tax = $this->sma->formatDecimal((($combo_item->unit_price) * $cpr_tax->rate) / 100);
                                    $net_unit_price = $combo_item->unit_price;
                                    $unit_price = $combo_item->unit_price + $item_tax;
                                }
                            } else {
                                $net_unit_price = $combo_item->unit_price;
                                $unit_price = $combo_item->unit_price;
                            }
                            $cproduct = array('product_id' => $combo_item->id, 'product_name' => $cpr->name, 'product_type' => $combo_item->type, 'quantity' => ($combo_item->qty * $item['quantity']), 'net_unit_price' => $net_unit_price, 'unit_price' => $unit_price, 'warehouse_id' => $item['warehouse_id'], 'item_tax' => $item_tax, 'tax_rate_id' => $cpr->tax_rate, 'tax' => ($cpr_tax->type == 1 ? $cpr_tax->rate . '%' : $cpr_tax->rate), 'option_id' => NULL, 'product_unit_id' => $cpr->unit);
                            $citems['p' . $combo_item->id . 'o' . $item['option_id']] = $cproduct;
                            $citems['p' . $combo_item->id . 'o' . $item['option_id']]['aquantity'] = ($combo_item->qty * $item['quantity']);
                        }
                    }
                }
            }
        }
        //$this->sma->print_arrays($combo_items, $citems);
        $cost = array();
        foreach ($citems as $item) {
            $item['aquantity'] = $citems['p' . $item['product_id'] . 'o' . $item['option_id']]['aquantity'];
            $cost[] = $this->item_costing($item, TRUE);
        }

        return $cost;
    }

    public function costing_check($items)
    {
        $citems = array();
        foreach ($items as $item) {
            $option = (isset($item['option_id']) && !empty($item['option_id']) && $item['option_id'] != 'null' && $item['option_id'] != 'false') ? $item['option_id'] : '';
            $pr = $this->getProductByID($item['product_id']);
            $item['option_id'] = $option;
            if ($pr && $pr->type == 'standard') {
                if (isset($citems['p' . $item['product_id'] . 'o' . $item['option_id']])) {
                    $citems['p' . $item['product_id'] . 'o' . $item['option_id']]['aquantity'] += $item['quantity'];
                } else {
                    $citems['p' . $item['product_id'] . 'o' . $item['option_id']] = $item;
                    $citems['p' . $item['product_id'] . 'o' . $item['option_id']]['aquantity'] = $item['quantity'];
                }
            } elseif ($pr && $pr->type == 'combo') {
                $wh = $this->Settings->overselling ? NULL : $item['warehouse_id'];
                $combo_items = $this->getProductComboItems($item['product_id'], $wh);
                foreach ($combo_items as $combo_item) {
                    if ($combo_item->type == 'standard') {
                        if (isset($citems['p' . $combo_item->id . 'o' . $item['option_id']])) {
                            $citems['p' . $combo_item->id . 'o' . $item['option_id']]['aquantity'] += ($combo_item->qty * $item['quantity']);
                        } else {
                            $cpr = $this->getProductByID($combo_item->id);
                            if ($cpr->tax_rate) {
                                $cpr_tax = $this->getTaxRateByID($cpr->tax_rate);
                                if ($cpr->tax_method) {
                                    $item_tax = $this->sma->formatDecimal((($combo_item->unit_price) * $cpr_tax->rate) / (100 + $cpr_tax->rate));
                                    $net_unit_price = $combo_item->unit_price - $item_tax;
                                    $unit_price = $combo_item->unit_price;
                                } else {
                                    $item_tax = $this->sma->formatDecimal((($combo_item->unit_price) * $cpr_tax->rate) / 100);
                                    $net_unit_price = $combo_item->unit_price;
                                    $unit_price = $combo_item->unit_price + $item_tax;
                                }
                            } else {
                                $net_unit_price = $combo_item->unit_price;
                                $unit_price = $combo_item->unit_price;
                            }
                            $cproduct = array('product_id' => $combo_item->id, 'product_name' => $cpr->name, 'product_type' => $combo_item->type, 'quantity' => ($combo_item->qty * $item['quantity']), 'net_unit_price' => $net_unit_price, 'unit_price' => $unit_price, 'warehouse_id' => $item['warehouse_id'], 'item_tax' => $item_tax, 'tax_rate_id' => $cpr->tax_rate, 'tax' => ($cpr_tax->type == 1 ? $cpr_tax->rate . '%' : $cpr_tax->rate), 'option_id' => NULL, 'product_unit_id' => $cpr->unit);
                            $citems['p' . $combo_item->id . 'o' . $item['option_id']] = $cproduct;
                            $citems['p' . $combo_item->id . 'o' . $item['option_id']]['aquantity'] = ($combo_item->qty * $item['quantity']);
                        }
                    }
                }
            }
        }
        // $this->sma->print_arrays($combo_items, $citems);
        $cost = array();
        foreach ($citems as $item) {
            $item['aquantity'] = $citems['p' . $item['product_id'] . 'o' . $item['option_id']]['aquantity'];
            $cost[] = $this->item_costing_check($item, TRUE);
        }
        return $cost;
    }

    public function syncQuantity($sale_id = NULL, $purchase_id = NULL, $oitems = NULL, $product_id = NULL)
    {
        if ($sale_id) {

            $sale_items = $this->getAllSaleItems($sale_id);
            foreach ($sale_items as $item) {
                if ($item->product_type == 'standard') {
                    $this->syncProductQty($item->product_id, $item->warehouse_id);
                    if (isset($item->option_id) && !empty($item->option_id)) {
                        $this->syncVariantQty($item->option_id, $item->warehouse_id, $item->product_id);
                    }
                } elseif ($item->product_type == 'combo') {
                    $wh = $this->Settings->overselling ? NULL : $item->warehouse_id;
                    $combo_items = $this->getProductComboItems($item->product_id, $wh);
                    foreach ($combo_items as $combo_item) {
                        if ($combo_item->type == 'standard') {
                            $this->syncProductQty($combo_item->id, $item->warehouse_id);
                        }
                    }
                }
            }

        } elseif ($purchase_id) {

            $purchase_items = $this->getAllPurchaseItems($purchase_id);
            foreach ($purchase_items as $item) {
                $this->syncProductQty($item->product_id, $item->warehouse_id);
                if (isset($item->option_id) && !empty($item->option_id)) {
                    $this->syncVariantQty($item->option_id, $item->warehouse_id, $item->product_id);
                }
            }

        } elseif ($oitems) {

            foreach ($oitems as $item) {
                if (isset($item->product_type)) {
                    if ($item->product_type == 'standard') {
                        $this->syncProductQty($item->product_id, $item->warehouse_id);
                        if (isset($item->option_id) && !empty($item->option_id)) {
                            $this->syncVariantQty($item->option_id, $item->warehouse_id, $item->product_id);
                        }
                    } elseif ($item->product_type == 'combo') {
                        $combo_items = $this->getProductComboItems($item->product_id, $item->warehouse_id);
                        foreach ($combo_items as $combo_item) {
                            if ($combo_item->type == 'standard') {
                                $this->syncProductQty($combo_item->id, $item->warehouse_id);
                            }
                        }
                    }
                } else {
                    $this->syncProductQty($item->product_id, $item->warehouse_id);
                    if (isset($item->option_id) && !empty($item->option_id)) {
                        $this->syncVariantQty($item->option_id, $item->warehouse_id, $item->product_id);
                    }
                }
            }

        } elseif ($product_id) {
            $warehouses = $this->getAllWarehouses();
            foreach ($warehouses as $warehouse) {
                $this->syncProductQty($product_id, $warehouse->id);
                if ($product_variants = $this->getProductVariants($product_id)) {
                    foreach ($product_variants as $pv) {
                        $this->syncVariantQty($pv->id, $warehouse->id, $product_id);
                    }
                }
            }
        }
    }


    public function getAllVariantsGroup()
    {
        $q = $this->db->get('variants');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }


    public function getProductVariants($product_id)
    {
        $q = $this->db->get_where('product_variants', array('product_id' => $product_id));
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getAllSaleItems($sale_id)
    {
        $q = $this->db->get_where('sale_items', array('sale_id' => $sale_id));
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getAllPurchaseItems($purchase_id)
    {
        $q = $this->db->get_where('purchase_items', array('purchase_id' => $purchase_id));
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function syncPurchaseItems($data = array())
    {
        if (!empty($data)) {
            foreach ($data as $items) {
                foreach ($items as $item) {
                    if (isset($item['pi_overselling'])) {
                        unset($item['pi_overselling']);
                        $option_id = (isset($item['option_id']) && !empty($item['option_id'])) ? $item['option_id'] : NULL;
                        $clause = array('purchase_id' => NULL, 'transfer_id' => NULL, 'product_id' => $item['product_id'], 'warehouse_id' => $item['warehouse_id'], 'option_id' => $option_id);
                        if ($pi = $this->getPurchasedItem($clause)) {
                            $quantity_balance = $pi->quantity_balance + $item['quantity_balance'];
                            $this->db->update('purchase_items', array('quantity_balance' => $quantity_balance), array('id' => $pi->id));
                        } else {
                            $clause['quantity'] = 0;
                            $clause['item_tax'] = 0;
                            $clause['quantity_balance'] = $item['quantity_balance'];
                            $clause['status'] = 'received';
                            $clause['option_id'] = !empty($clause['option_id']) && is_numeric($clause['option_id']) ? $clause['option_id'] : NULL;
                            $this->db->insert('purchase_items', $clause);
                        }
                    } else {
                        if ($item['inventory']) {
                            $this->db->update('purchase_items', array('quantity_balance' => $item['quantity_balance']), array('id' => $item['purchase_item_id']));
                        }
                    }
                }
            }
            return TRUE;
        }
        return FALSE;
    }

    public function getProductByCode($code)
    {
        $q = $this->db->get_where('products', array('code' => $code), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function check_customer_deposit($customer_id, $amount)
    {
        $customer = $this->getCompanyByID($customer_id);
        return $customer->deposit_amount >= $amount;
    }

    public function getCategoryIdByProductID($product_id)
    {
        $q = $this->db->get_where('products', array('id' => $product_id), 1);
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                return $row->category_id;
            }
        }
        return FALSE;
    }


    public function getWarehouseProduct($warehouse_id, $product_id)
    {
        $q = $this->db->get_where('warehouses_products', array('product_id' => $product_id, 'warehouse_id' => $warehouse_id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    //Get all active promotions
    public function getPromotions()
    {
        $this->db->where('status', 'active');
        $this->db->where('endDate >=', date('Y-m-d'));
        $q = $this->db->get('promotions');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }


    public function getAllBaseUnits()
    {
        $q = $this->db->get_where("units", array('base_unit' => NULL));
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getUnitsByBUID($base_unit)
    {
        $this->db->where('id', $base_unit)->or_where('base_unit', $base_unit)
            ->group_by('id')->order_by('id asc');
        $q = $this->db->get("units");
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getUnitByID($id)
    {
        $q = $this->db->get_where("units", array('id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getPriceGroupByID($id)
    {
        $q = $this->db->get_where('price_groups', array('id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getProductGroupPrice($product_id, $group_id, $option_id = null)
    {
        if ($option_id === NULL) {
            $option_id = 0;
        }

        $q = $this->db->get_where('product_prices', array('price_group_id' => $group_id, 'product_id' => $product_id, 'option_id' => $option_id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getAllBrands()
    {
        $q = $this->db->get("brands");
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getAllAuthors()
    {
        $q = $this->db->get("authors");
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getAllPublishers()
    {
        $q = $this->db->get("publishers");
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getBrandByID($id)
    {
        $q = $this->db->get_where('brands', array('id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getAuthorByID($id)
    {
        $q = $this->db->get_where('authors', array('id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getPublisherByID($id)
    {
        $q = $this->db->get_where('publishers', array('id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function convertToBase($unit, $value)
    {
        switch ($unit->operator) {
            case '*':
                return $value / $unit->operation_value;
                break;
            case '/':
                return $value * $unit->operation_value;
                break;
            case '+':
                return $value - $unit->operation_value;
                break;
            case '-':
                return $value + $unit->operation_value;
                break;
            default:
                return $value;
        }
    }

    function calculateTax($product_details = NULL, $tax_details, $custom_value = NULL, $c_on = NULL)
    {
        $value = $custom_value ? $custom_value : (($c_on == 'cost') ? $product_details->cost : $product_details->price);
        $tax_amount = 0;
        $tax = 0;
        if ($tax_details && $tax_details->type == 1 && $tax_details->rate != 0) {
            if ($product_details && $product_details->tax_method == 1) {
                $tax_amount = $this->sma->formatDecimal((($value) * $tax_details->rate) / 100, 4);
                $tax = $this->sma->formatDecimal($tax_details->rate, 0) . "%";
            } else {
                $tax_amount = $this->sma->formatDecimal((($value) * $tax_details->rate) / (100 + $tax_details->rate), 4);
                $tax = $this->sma->formatDecimal($tax_details->rate, 0) . "%";
            }
        } elseif ($tax_details && $tax_details->type == 2) {
            $tax_amount = $this->sma->formatDecimal($tax_details->rate);
            $tax = $this->sma->formatDecimal($tax_details->rate, 0);
        }
        return array('id' => $tax_details->id, 'tax' => $tax, 'amount' => $tax_amount);
    }

    public function getAddressByID($id)
    {
        return $this->db->get_where('addresses', ['id' => $id], 1)->row();
    }

    public function getAddressByCompanyID($company_id)
    {
        $this->db->select('*');
        $this->db->where('company_id', $company_id);
        $q = $this->db->get("addresses");
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getAllAdmins()
    {
        $this->db->select('*');
        $this->db->where('group_id', 1);
        $this->db->where('active', 1);
        $q = $this->db->get("users");
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function checkSlug($slug, $type = NULL)
    {
        if (!$type) {
            return $this->db->get_where('products', ['slug' => $slug], 1)->row();
        } elseif ($type == 'category') {
            return $this->db->get_where('categories', ['slug' => $slug], 1)->row();
        } elseif ($type == 'brand') {
            return $this->db->get_where('brands', ['slug' => $slug], 1)->row();
        }
        return FALSE;
    }

    public function calculateDiscount($discount = NULL, $amount)
    {
        if ($discount && $this->Settings->product_discount) {
            $dpos = strpos($discount, '%');
            if ($dpos !== false) {
                $pds = explode("%", $discount);
                return $this->sma->formatDecimal(((($this->sma->formatDecimal($amount)) * (Float)($pds[0])) / 100), 4);
            } else {
                return $this->sma->formatDecimal($discount, 4);
            }
        }
        return 0;
    }

    public function calculateOrderTax($order_tax_id = NULL, $amount)
    {
        if ($this->Settings->tax2 != 0 && $order_tax_id) {
            if ($order_tax_details = $this->site->getTaxRateByID($order_tax_id)) {
                if ($order_tax_details->type == 1) {
                    return $this->sma->formatDecimal((($amount * $order_tax_details->rate) / 100), 4);
                } else {
                    return $this->sma->formatDecimal($order_tax_details->rate, 4);
                }
            }
        }
        return 0;
    }

    //This function to get selected authors during editing product
    public function selectedAuthors($id)
    {
        $this->db->select('author_id');
        $this->db->where('product_id', $id);
        $q = $this->db->get("product_authors");
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row->author_id;
            }
            return $data;
        }
        return FALSE;
    }


    //Promotion module....
    public function selectedPromotionalWarehouses($id)
    {
        $this->db->distinct('warehouse_id');
        $this->db->select('warehouse_id');
        $this->db->where('upselling_id', $id);
        $q = $this->db->get("promotion_items");
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row->warehouse_id;
            }
            return $data;
        }
        return FALSE;
    }

    public function selectedPromotionalCategories($id)
    {
        $this->db->distinct('category_id');
        $this->db->select('category_id');
        $this->db->where('upselling_id', $id);
        $q = $this->db->get("promotion_items");
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row->category_id;
            }
            return $data;
        }
        return FALSE;
    }


    public function selectedPromotionalItems($id)
    {
        $this->db->distinct('item_id');
        $this->db->select('item_id');
        $this->db->where('upselling_id', $id);
        $q = $this->db->get("promotion_items");
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row->item_id;
            }
            return $data;
        }
        return FALSE;
    }

    //Get Active payment methods
    public function getActivePaymentMethod($name, $table_name)
    {
        $this->db->select('id');
        $this->db->where('active', 1);
        $this->db->where('name', $name);
        //echo $this->db->get_compiled_select(); die;
        $q = $this->db->get($table_name);
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row->id;
            }
            return $data;
        }
        return FALSE;
    }

    //Get Active payment methods All data
    public function getActivePaymentMethodData($name, $table_name)
    {
        $this->db->select('*');
        $this->db->where('active', 1);
        $this->db->where('name', $name);
        //echo $this->db->get_compiled_select(); die;
        $q = $this->db->get($table_name);
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    //Mulitple products promotion
    public function checkBuyOneGetOtherOne($product_id, $pos_req = Null)
    {
        $is_promotion = $this->promotionAddToCartOther($product_id);
        if (!empty($is_promotion)) {
            foreach ($is_promotion as $row) {
                $data_how_buy = $row->how_many_buy;
                $data_how_free = $row->how_many_free;
                $promotion_id = $row->upselling_id;

                if ($pos_req >= 0 && $pos_req != NULL) {
                    if ($pos_req == 0) {
                        $pr = array('msg' => 'you can select another product for free.!');
                        $this->sma->send_json($pr);
                    } elseif ($data_how_buy == $pos_req) {
                        $pr = array('msg_cnfrm' => 'you get free product!');
                        $this->sma->send_json($pr);
                    } else {

                    }
                } else {
                    $how_many_this_product_incart = $this->specific_item_count_other($product_id, $promotion_id);
                }

                if ($how_many_this_product_incart == $data_how_buy) {
                    $this->session->set_userdata('product_on_promotion_ping', $product_id);
                    return true;
                } else if ($how_many_this_product_incart == $data_how_buy + 1) {
                    //Prepare product for promotion - other products
                    if ($this->cart->total_items() > 0) {
                        $getPromotionDetails = $this->getPromotionDetails($promotion_id);
                        foreach ($this->cart->contents() AS $item) {
                            if (in_array($item['product_id'], $getPromotionDetails)) {

                                $this->cart->update(array(
                                    'rowid' => $item['rowid'],
                                    'price' => 0,
                                ));
                                $this->session->set_userdata('product_on_promotion_get', $product_id);
                                return true;

                            }
                        }
                    }

                }
            }
        }
    }

    public function promotionAddToCartOther($product_id)
    {
        $this->db->select('promotions.*, promotion_items.*')
            ->from('promotions')
            ->join('promotion_items', 'promotions.id = promotion_items.upselling_id')
            ->where('promotions.status', 'active')
            ->where('promotions.type', 'buyOneGetOtherProductFree')
            ->where('promotions.endDate >=', date('Y-m-d'))
            ->where('promotion_items.item_id', $product_id)
            ->order_by('promotions.id desc');
        //echo $this->db->get_compiled_select(); die;
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            foreach (($query->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
    }

    public function specific_item_count_other($product_id, $promotion_id)
    {
        $total = 0;
        if ($this->cart->total_items() > 0) {
            $getPromotionDetails = $this->getPromotionDetails($promotion_id);
            foreach ($this->cart->contents() AS $item) {
                if (in_array($item['product_id'], $getPromotionDetails)) {
                    $total++;
                }
            }
        }
        return $total;
    }

    public function getPromotionDetails($promotion_id)
    {
        //GEt the details from the prmomotion_items table
        $this->db->select('*')
            ->from('promotion_items')
            ->where('upselling_id', $promotion_id);
        $this->db->group_by('item_id');
        //echo $this->db->get_compiled_select(); die;
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            foreach (($query->result()) as $row) {
                $data[] = $row->item_id;
            }
            return $data;
        }

    }
    //End of Mulitple products promotion


    //start of Category funtion --Promotion
    //Category promotion ---To check that product category on promotion or not?
    public function checkBuyOneOnCategoryFree($product_id)
    {
        $product_category_id = $this->getCategoryIdByProductID($product_id);
        $is_category_on_promtion = $this->checkCategoryOnPromotion($product_category_id, $product_id);
        if (!empty($is_category_on_promtion)) {
            //get all warehouse id
            foreach ($is_category_on_promtion as $rec) {
                $ware_houses[] = $rec->warehouse_id;
            }
            foreach ($is_category_on_promtion as $row) {
                $data_how_buy = $row->how_many_buy;
                $data_how_free = $row->how_many_free;
                $how_many_this_product_incart = $this->specific_item_category_count($product_category_id, $product_id, $ware_houses, $row->upselling_id);

                if ($how_many_this_product_incart == $data_how_buy) {
                    $this->session->set_userdata('product_on_promotion_ping', $product_category_id);
                    return true;
                } elseif ($how_many_this_product_incart == $data_how_buy + 1) {
                    $this->ready_category_product_for_promtion($product_category_id, $product_id, $ware_houses);
                    $this->session->set_userdata('product_on_promotion_get', $product_category_id);
                    return true;
                }
            }
        }
    }

    //To check that this selected category product on promotion or not.?
    public function checkCategoryOnPromotion($category_id, $product_id)
    {
        $this->db->select('promotions.*, promotion_items.*')
            ->from('promotions')
            ->join('promotion_items', 'promotions.id = promotion_items.upselling_id')
            ->where('promotions.status', 'active')
            ->where('promotions.type', 'buyOneGetCategoryFree')
            ->where('promotions.endDate >=', date('Y-m-d'))
            ->where('promotion_items.category_id', $category_id)
            ->order_by('promotions.id desc');
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            foreach (($query->result()) as $row) {
                // $this_product_exist_wh[] = $this->site->getWarehouseProduct($row->warehouse_id , $product_id);
                $data[] = $row;
            }
            //if(array_filter($this_product_exist_wh)){
            return $data;
        } else {
            return false;
        }
    }

    public function specific_item_category_count($product_category_id, $product_id, $warehouse_id, $promotion_id)
    {
        $total = 0;
        $category_on_promtion = $this->get_all_category_on_promotion($promotion_id);
        if ($this->cart->total_items() > 0) {
            foreach ($this->cart->contents() AS $item) {
                if ($item['product_id']) {
                    $category_id = $this->getCategoryIdByProductID($item['product_id']);
                    //$selection_product = $this->checkProductExistInWarehouse($item['product_id'], $warehouse_id);
                    if (in_array($category_id, $category_on_promtion)) {
                        $total++;
                    }
                }
            }
        }
        return $total;
    }

    public function get_all_category_on_promotion($promotion_id)
    {
        $this->db->distinct();
        $this->db->select('category_id')
            ->from('promotion_items')
            ->where('upselling_id', $promotion_id);
        //echo $this->db->get_compiled_select(); die;
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            foreach (($query->result()) as $row) {
                $data[] = $row->category_id;
            }
            return $data;
        }
    }

    //prepare product for promotion - Category
    public function ready_category_product_for_promtion($product_category_id, $product_id, $warehouse_id)
    {
        if ($this->cart->total_items() > 0) {
            foreach ($this->cart->contents() AS $item) {
                if ($item['product_id']) {
                    $category_id = $this->getCategoryIdByProductID($item['product_id']);
                    ///$selection_product = $this->checkProductExistInWarehouse($item['product_id'], $warehouse_id);
                    if ($category_id == $product_category_id) {
                        $com_price[] = $item['price'];
                    }
                }
            }
            //To get the lowest value from price
            foreach ($com_price as $rec) {
                $price_cus[] = $rec;
            }
            //Find the lowest value in array.
            $lowest_price = min($price_cus);
            foreach ($this->cart->contents() AS $item) {
                if ($item['product_id']) {
                    $category_id = $this->getCategoryIdByProductID($item['product_id']);
                    if ($category_id == $product_category_id) {
                        if ($lowest_price == $item['price']) {
                            $this->cart->update(array(
                                'rowid' => $item['rowid'],
                                'price' => 0,
                            ));
                            return true;
                        }
                    }
                }
            }
        }
    }

    //end of Category funtion


    //Buy one get one same product

    //Find the promotion on same product
    public function checkBuyOneGetOne($product_id)
    {
        $is_promotion = $this->promotionAddToCart($product_id);
        if (!empty($is_promotion)) {
            foreach ($is_promotion as $row) {
                $data_how_buy = $row->how_many_buy;
                $data_how_free = $row->how_many_free;
                $how_many_this_product_incart = $this->specific_item_count($product_id);
                if ($how_many_this_product_incart == $data_how_buy) {

                    $this->session->set_userdata('product_on_promotion_get', $product_id);

                    $product = $this->getProductByID($product_id);
                    $price = 0;
                    $id = $this->Settings->item_addition ? md5($product->id) : md5(microtime());
                    $tax_rate = $this->getTaxRateByID($product->tax_rate);
                    $ctax = $this->calculateTax($product, $tax_rate, $price);
                    $tax = $this->sma->formatDecimal($ctax['amount']);
                    $unit_price = $this->sma->formatDecimal($product->tax_method ? $price + $tax : $price);

                    $option = FALSE;
                    if (!empty($options)) {
                        if ($this->input->post('option')) {
                            foreach ($options as $op) {
                                if ($op['id'] == $this->input->post('option')) {
                                    $option = $op;
                                }
                            }
                        } else {
                            $option = array_values($options)[0];
                        }
                        $price = $option['price'] + $price;
                    }
                    $selected = $option ? $option['id'] : FALSE;

                    $data = array(
                        'id' => $id,
                        'product_id' => $product->id,
                        'qty' => $data_how_free,
                        'name' => $product->name,
                        'code' => $product->code,
                        'price' => $unit_price,
                        'tax' => $tax,
                        'image' => $product->image,
                        'option' => $selected,
                        'options' => !empty($options) ? $options : NULL
                    );


                    if ($this->cart->insert($data)) {
                        //Check item on promotion or not
                        $this->checkBuyOneGetOne($product->id);
                        if ($this->input->post('quantity')) {
                            $this->session->set_flashdata('message', lang('item_added_to_cart'));
                            redirect($_SERVER['HTTP_REFERER']);
                        } else {
                            $this->cart->cart_data();
                        }
                    }
                }
            }
        }
    }

    public function specific_item_count($product_id)
    {
        $total = 0;
        if ($this->cart->total_items() > 0) {
            foreach ($this->cart->contents() AS $item) {
                if ($product_id == $item['product_id']) {
                    $total++;
                }
            }
        }
        return $total;
    }

    public function promotionAddToCart($product_id)
    {
        $this->db->select('promotions.*, promotion_items.*')
            ->from('promotions')
            ->join('promotion_items', 'promotions.id = promotion_items.upselling_id')
            ->where('promotions.status', 'active')
            ->where('promotions.type', 'buyOneGetOne')
            //->where('promotions.startDate', date('Y-m-d'))
            ->where('promotions.endDate >=', date('Y-m-d'))
            ->where('promotion_items.item_id', $product_id)
            ->order_by('promotions.id desc')
            ->limit(1);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            foreach (($query->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
    }

    public function specific_promotional_item_delete($product_id)
    {
        if ($this->cart->total_items() > 0) {
            foreach ($this->cart->contents() AS $item) {
                if ($product_id == $item['product_id'] && $item['price'] == 0) {
                    $this->cart->remove($item['rowid']);
                }
            }
        }
        return true;
    }


    //Reduce Image sizes
    public function compress($source, $destination, $quality)
    {

        $info = getimagesize($source);

        if ($info['mime'] == 'image/jpeg')
            $image = imagecreatefromjpeg($source);

        elseif ($info['mime'] == 'image/gif')
            $image = imagecreatefromgif($source);

        elseif ($info['mime'] == 'image/png')
            $image = imagecreatefrompng($source);

        imagejpeg($image, $destination, $quality);

        return $destination;
    }


    public function getAllProducts()
    {
        $this->db->limit(20, 10);
        $q = $this->db->get('products');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row->image;
            }
            return $data;
        }
        return FALSE;
    }

    public function getAllSizeGuide()
    {
        $this->db->select('id,title');
        $q = $this->db->get('sizing_info');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getSizeGuideWithID($id)
    {
        $this->db->where('id', $id);
        $this->db->select('*');
        $q = $this->db->get('sizing_info');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getPromotionPercentage_category_onsale($type, $product_id = NULL, $category_id = NULL)
    {
        $cat_ids = array();
        if ($type == 'buyCategoryWithPercentage') {
            $this->db->select('promotions.*')
                ->from('promotions')
               // ->join('promotion_items', 'promotions.id = promotion_items.upselling_id')
                ->where('promotions.status', 'active')
                ->where('promotions.type', $type)
                ->where('promotions.startDate <=', date('Y-m-d'))
                ->where('promotions.endDate >=', date('Y-m-d'))
                ->order_by('promotions.id desc');
                //->limit(1);
            $query = $this->db->get();
            if ($query->num_rows() > 0) {
                foreach (($query->result()) as $row) {
                    $upselling_id = $row->id;
                    $this->db->select('category_id')
                        ->from('promotion_items')
                        ->where('upselling_id', $upselling_id);
                       $query_new  = $this->db->get();
                    if ($query_new->num_rows() > 0) {
                        foreach (($query_new->result()) as $row) {
                            $cat_ids[]= $row->category_id;
                          }
                        }
                }
                return $cat_ids;
            } else {
                return $cat_ids;
            }
        }
    }
    public function getPromotionPercentage_product_onsale($type, $product_id = NULL, $category_id = NULL)
    {
        $cat_ids = array();

        if ($type == 'buyProductsWithPercentage') {
            $this->db->select('promotions.*')
                ->from('promotions')
             //   ->join('promotion_items', 'promotions.id = promotion_items.upselling_id')
                ->where('promotions.status', 'active')
                ->where('promotions.type', $type)
                ->where('promotions.startDate <=', date('Y-m-d'))
                ->where('promotions.endDate >=', date('Y-m-d'))
                ->order_by('promotions.id desc');
                //->limit(1);
            $query = $this->db->get();
            if ($query->num_rows() > 0) {
                foreach (($query->result()) as $row) {
                    $title = $row->title;
                     $upselling_id = $row->id;

                    $this->db->select('item_id')
                        ->from('promotion_items')
                        ->where('upselling_id', $upselling_id);
                    $query_new  = $this->db->get();
                    if ($query_new->num_rows() > 0) {
                        foreach (($query_new->result()) as $row) {
                            $cat_ids[]= $row->item_id;
                        }
                    }
                }
                return $cat_ids;
            } else {
                return $cat_ids;
            }
        }


    }
    public function getPromotionPercentage_product_check($type, $product_id = NULL, $category_id = NULL)
    {
        $cat_ids = array();

        if ($type == 'buyProductsWithPercentage') {
            $this->db->select('promotions.*, promotion_items.*')
                ->from('promotions')
                ->join('promotion_items', 'promotions.id = promotion_items.upselling_id')
                ->where('promotions.status', 'active')
                ->where('promotions.type', $type)
                ->where('promotions.startDate <=', date('Y-m-d'))
                ->where('promotions.endDate >=', date('Y-m-d'))
                ->order_by('promotions.id desc')
                ->limit(1);
            $query = $this->db->get();
            if ($query->num_rows() > 0) {
                foreach (($query->result()) as $row) {
                    $title = $row->title;
                    $data_how_much_percentage = $row->how_much_percentage;
                }
                   return $data_how_much_percentage;
            } else {
                //  return '';
            }
        }
    }
    public function getPromotionPercentage_category_check($type, $product_id = NULL, $category_id = NULL)
    {
        $cat_ids = array();

        if ($type == 'buyCategoryWithPercentage') {
            $this->db->select('promotions.*, promotion_items.*')
                ->from('promotions')
                ->join('promotion_items', 'promotions.id = promotion_items.upselling_id')
                ->where('promotions.status', 'active')
                ->where('promotions.type', $type)
                ->where('promotions.startDate <=', date('Y-m-d'))
                ->where('promotions.endDate >=', date('Y-m-d'))
                ->order_by('promotions.id desc')
                ->limit(1);
            $query = $this->db->get();
            if ($query->num_rows() > 0) {
                foreach (($query->result()) as $row) {
                    $title = $row->title;
                    $data_how_much_percentage = $row->how_much_percentage;
                }
                   return $data_how_much_percentage;
            } else {
                //  return '';
            }
        }
    }

    //Promotion with percentage calcluatoin
    public function getPromotionPercentage($type, $product_id = NULL, $category_id = NULL,$where_to_apply=NULL,$title_request=NULL)
    {
        $title = '';
        $data_how_much_percentage = '';
        if($where_to_apply == NULL){
            $where_to_apply = 'ecommerce';
        }

        if ($type == 'buyCategoryWithPercentage') {
            $this->db->select('promotions.*, promotion_items.*')
                ->from('promotions')
                ->join('promotion_items', 'promotions.id = promotion_items.upselling_id')
                ->where('promotions.status', 'active')
                ->where('promotions.type', $type)
                ->where('promotions.startDate <=', date('Y-m-d'))
                ->where('promotions.endDate >=', date('Y-m-d'))
                ->where('promotion_items.category_id', $category_id)
                ->order_by('promotions.id desc')
                ->limit(1);
            $query = $this->db->get();
            if ($query->num_rows() > 0) {
                foreach (($query->result()) as $row) {
                    if($where_to_apply  == $row->where_to_apply){
                        $title = $row->name;
                        $data_how_much_percentage = $row->how_much_percentage;
                    }else if($row->where_to_apply == 'both'){
                        $title = $row->name;
                    $data_how_much_percentage = $row->how_much_percentage;
                }
                }
                if($title_request){
                    return $title;
                }
                return $data_how_much_percentage;
            } else {
                return '';
            }
        } else if ($type == 'buyProductsWithPercentage') {
            $this->db->select('promotions.*, promotion_items.*')
                ->from('promotions')
                ->join('promotion_items', 'promotions.id = promotion_items.upselling_id')
                ->where('promotions.status', 'active')
                ->where('promotions.type', $type)
                ->where('promotions.startDate <=', date('Y-m-d'))
                ->where('promotions.endDate >=', date('Y-m-d'))
                ->where('promotion_items.item_id', $product_id)
                ->order_by('promotions.id desc')
                ->limit(1);
            $query = $this->db->get();
            if ($query->num_rows() > 0) {
                foreach (($query->result()) as $row) {
                    if($where_to_apply  == $row->where_to_apply){
                        $title = $row->name;
                        $data_how_much_percentage = $row->how_much_percentage;
                    }else if($row->where_to_apply == 'both'){
                        $title = $row->name;
                    $data_how_much_percentage = $row->how_much_percentage;
                }
                }
                if($title_request){
                    return $title;
                }
                return $data_how_much_percentage;
            } else {
                return '';
            }
        }
        else if ($type == 'cartvaluerangeFree') {

            $this->db->select('promotions.*, promotion_items.*')
                ->from('promotions')
                ->join('promotion_items', 'promotions.id = promotion_items.upselling_id','left')
                ->where('promotions.status', 'active')
                ->where('promotions.type', $type)
                ->where('promotions.startDate <=', date('Y-m-d'))
                ->where('promotions.endDate >=', date('Y-m-d'))
                ->order_by('promotions.id DESC')
                ->limit(1);
            $query = $this->db->get();
            if ($query->num_rows() > 0) {
                foreach (($query->result()) as $row) {
                    if($where_to_apply  == $row->where_to_apply){
                        $title = $row->name;
                        $data_how_much_percentage = $row->how_much_percentage;
                    }else if($row->where_to_apply == 'both'){
                        $title = $row->name;
                    $data_how_much_percentage = $row->how_much_percentage;
                }

                }
                if($title_request){
                return $title;
            }
                return $data_how_much_percentage;
            } else {
                return '';
            }
        }
    }


    public function getPromotionPercentage_promotext($product_id = NULL)
    {

        $promo_text = '';
        $promo_title_cart = $this->getPromotionPercentage('cartvaluerangeFree',NULL,NULL,NULL,TRUE);
        if(!$promo_title_cart) {

            //Calucalte if this product(category) on promotion with custom module
            $category_id = $this->getCategoryIdByProductID($product_id);
            //Calucalte if this multiple products on promotion with custom module
            $promo_title_off = $this->getPromotionPercentage('buyProductsWithPercentage',$product_id, $category_id,NULL,TRUE);
            if($promo_title_off) {
                $promo_text = $promo_title_off;
            }
            $promo_title_off_category = $this->getPromotionPercentage('buyCategoryWithPercentage',$product_id, $category_id,NULL,TRUE);
            if($promo_title_off_category) {
                $promo_text = $promo_title_off_category;
            }
        }else{
            // $promo_text = $promo_title_cart;
        }
        return substr($promo_text,0,12);
    }



    public function getPromotionCartRange($type)
    {
        if ($type == 'cartvaluerangeFree') {
            $this->db->select('promotions.*')
                ->from('promotions')
                ->where('promotions.status', 'active')
                ->where('promotions.type', $type)
                ->where('promotions.startDate <=', date('Y-m-d'))
                ->where('promotions.endDate >=', date('Y-m-d'))
                ->order_by('promotions.id desc');
            $query = $this->db->get();
            if ($query->num_rows() > 0) {
                foreach (($query->result()) as $row) {
                    $title = $row->title;
                    $data_how_much_percentage = $row->cart_amount_limit;
                }
                return $data_how_much_percentage;
            } else {
                return 0;
            }
        }
    }

    //Promotion with percentage calcluatoin
    public function calCulateProductPriceWithPromotionPercentage($actual_price, $percentage)
    {
        $get_per = $actual_price * $percentage;
        $get_val = $get_per / 100;
        $get_price = $actual_price - $get_val;
        return $this->sma->convertMoney($get_price);
    }

    public function calCulateProductPriceWithPromotionPercentagePos($actual_price, $percentage)
    {
        $get_per = $actual_price * $percentage;
        $get_val = $get_per / 100;
        return $get_price = $actual_price - $get_val;
    }

    public function getCustomerPaymentMethodForSale($sale_id)
    {
        $this->db->select('payment_by,redeem_points')
            ->from('sales')
            ->where('id', $sale_id);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            foreach (($query->result()) as $row) {
                $payment_by = $row->payment_by;
                $redeem_points = $row->redeem_points;
            }
            if ($redeem_points) {
                return $payment_by . '+ REDEEM POINTS';
            } else {
                return $payment_by;
            }
        } else {
            return '';
        }
    }

    //For chart section
    function today_sale($date = NULL, $warehouse_id = NULL, $end_date = NULL)
    {
        if ($end_date) {
            $cus = $this->reports_model->getNoSalesByDate($date, $end_date, $warehouse_id);
        }

        if (!$date) {
            $date = date('Y-m-d');
        }
        $cost = $this->getCosting($date, $warehouse_id);
        $this->data['discount'] = $this->getOrderDiscount($date, $warehouse_id);
        $this->data['expenses'] = $this->getExpenses($date, $warehouse_id);
        $this->data['returns'] = $this->getReturns($date, $warehouse_id);
        $this->data['swh'] = $warehouse_id;
        $this->data['date'] = $date;

        if ($cus) {
            return $this->sma->formatMoney($cus->total);
        }
        return $this->sma->formatMoney($cost->sales);
    }

    public function getNumberOfItemsAgainstWh($start_date, $end_date, $warehouse_id = NULL)
    {
        if ($start_date) {
            $start_date = $start_date . ' 00:00:00';
            $end_date = $end_date . ' 23:59:59';
        } else {
            $start_date = date('Y-m-d H:i:s');
            $end_date = date('Y-m-d H:i:s');
        }

        $this->db->select("COUNT(sma_sale_items.id) as record");
        $this->db->join('sale_items', 'sales.id = sale_items.sale_id', 'left');
        $this->db->where('sales.date >=', $start_date)->where('sales.date <=', $end_date);
        if ($warehouse_id) {
            $this->db->where('sales.warehouse_id', $warehouse_id);
            $this->db->where('sale_items.warehouse_id', $warehouse_id);
        }

        //echo $this->db->get_compiled_select(); die;
        $q = $this->db->get('sma_sales');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                return $row->record;
            }
        }
        return FALSE;
    }

    function item_sale_delivered($what, $start, $end)
    {
        $start = $start . ' 00:00:00';
        $end = $end . ' 23:59:59';
        $this->db->select('count(id) as total', FALSE);
        if ($what == 'number_of_sale') {
            $this->db->where('sale_status =', 'completed');
        } elseif ($what == 'returned') {
            $this->db->where('sale_status =', 'returned');
        }
        $this->db->where('date >=', $start);
        $this->db->where('date <=', $end);
        //echo $this->db->get_compiled_select(); die;
        $q = $this->db->get('sales');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                return $row->total;
            }
        }
    }

    //Yearly Graph data get
    function yearly_graph_data($what)
    {

        if ($what == 'sale') {
            for ($month = 1; $month <= 12; $month++) {
                $q = $this->db->query("SELECT SUM(total) as total FROM sma_sales 
                 WHERE MONTH(date) = $month AND YEAR(date) = YEAR(CURDATE())");
                if ($q->num_rows() > 0) {
                    foreach (($q->result()) as $row) {
                        $get_sale[] = str_replace(',', '', $this->sma->formatMoney($row->total));
                    }
                }
            }
            return $get_sale;
        } elseif ($what == 'profit') {
            for ($month = 1; $month <= 12; $month++) {
                $current_year = date("Y");
                $costing = $this->getCosting(NULL, NULL, $current_year, $month);
                $discount = $this->getOrderDiscount(NULL, NULL, $current_year, $month);
                $expenses = $this->getExpenses(NULL, NULL, $current_year, $month);

                $get_profit[] = str_replace(',', '', $this->sma->formatMoney($costing->sales - $costing->cost - $discount->order_discount - $expenses->total));
            }
            return $get_profit;
        } elseif ($what == 'number_of_orders') {
            for ($month = 1; $month <= 12; $month++) {
                $q = $this->db->query("SELECT COUNT(id) as total FROM sma_sales 
                 WHERE MONTH(date) = $month AND YEAR(date) = YEAR(CURDATE())");
                if ($q->num_rows() > 0) {
                    foreach (($q->result()) as $row) {
                        $get_order[] = str_replace(',', '', $this->sma->formatMoney($row->total));
                    }
                }
            }
            return $get_order;
        }
    }

    public function getCosting($date, $warehouse_id = NULL, $year = NULL, $month = NULL)
    {
        $this->db->select('SUM( COALESCE( purchase_unit_cost, 0 ) * quantity ) AS cost, SUM( COALESCE( sale_unit_price, 0 ) * quantity ) AS sales, SUM( COALESCE( purchase_net_unit_cost, 0 ) * quantity ) AS net_cost, SUM( COALESCE( sale_net_unit_price, 0 ) * quantity ) AS net_sales', FALSE);
        if ($date) {
            $this->db->where('costing.date', $date);
        } elseif ($month) {
            $this->load->helper('date');
            $last_day = days_in_month($month, $year);
            $this->db->where('costing.date >=', $year . '-' . $month . '-01 00:00:00');
            $this->db->where('costing.date <=', $year . '-' . $month . '-' . $last_day . ' 23:59:59');
        }

        if ($warehouse_id) {
            $this->db->join('sales', 'sales.id=costing.sale_id')
                ->where('sales.warehouse_id', $warehouse_id);
        }

        $q = $this->db->get('costing');
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return false;
    }

    public function getOrderDiscount($date, $warehouse_id = NULL, $year = NULL, $month = NULL)
    {
        $sdate = $date . ' 00:00:00';
        $edate = $date . ' 23:59:59';
        $this->db->select('SUM( COALESCE( order_discount, 0 ) ) AS order_discount', FALSE);
        if ($date) {
            $this->db->where('date >=', $sdate)->where('date <=', $edate);
        } elseif ($month) {
            $this->load->helper('date');
            $last_day = days_in_month($month, $year);
            $this->db->where('date >=', $year . '-' . $month . '-01 00:00:00');
            $this->db->where('date <=', $year . '-' . $month . '-' . $last_day . ' 23:59:59');
        }

        if ($warehouse_id) {
            $this->db->where('warehouse_id', $warehouse_id);
        }

        $q = $this->db->get('sales');
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return false;
    }

    public function getExpenses($date, $warehouse_id = NULL, $year = NULL, $month = NULL)
    {
        $sdate = $date . ' 00:00:00';
        $edate = $date . ' 23:59:59';
        $this->db->select('SUM( COALESCE( amount, 0 ) ) AS total', FALSE);
        if ($date) {
            $this->db->where('date >=', $sdate)->where('date <=', $edate);
        } elseif ($month) {
            $this->load->helper('date');
            $last_day = days_in_month($month, $year);
            $this->db->where('date >=', $year . '-' . $month . '-01 00:00:00');
            $this->db->where('date <=', $year . '-' . $month . '-' . $last_day . ' 23:59:59');
        }


        if ($warehouse_id) {
            $this->db->where('warehouse_id', $warehouse_id);
        }

        $q = $this->db->get('expenses');
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return false;
    }

    public function getReturns($date, $warehouse_id = NULL, $year = NULL, $month = NULL)
    {
        $sdate = $date . ' 00:00:00';
        $edate = $date . ' 23:59:59';
        $this->db->select('SUM( COALESCE( grand_total, 0 ) ) AS total', FALSE)
            ->where('sale_status', 'returned');
        if ($date) {
            $this->db->where('date >=', $sdate)->where('date <=', $edate);
        } elseif ($month) {
            $this->load->helper('date');
            $last_day = days_in_month($month, $year);
            $this->db->where('date >=', $year . '-' . $month . '-01 00:00:00');
            $this->db->where('date <=', $year . '-' . $month . '-' . $last_day . ' 23:59:59');
        }

        if ($warehouse_id) {
            $this->db->where('warehouse_id', $warehouse_id);
        }

        $q = $this->db->get('sales');
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return false;
    }


    function getPosSettings()
    {
        $q = $this->db->get('pos_settings');
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }


    function getSizingGuideName($size_id)
    {
        $this->db->select('title');
        $this->db->where('id', $size_id);
        $q = $this->db->get('sizing_info');
        if ($q->num_rows() > 0) {
            return $q->row()->title;
        }
        return FALSE;
    }

    function getSizingGuideDetailByID($size_id)
    {
        $this->db->where('id', $size_id);
        $q = $this->db->get('sizing_info');
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }


    function getProductDiscountedPrice($product_id, $option_id, $option_price, $group_id)
    {
        $this->db->select('price');
        $this->db->where('product_id', $product_id);
        $this->db->where('option_id', $option_id);
        $this->db->where('price_group_id', $group_id);
        $q = $this->db->get('product_prices');
        if ($q->num_rows() > 0) {
            return $option_price - $q->row()->price;
        }
        return FALSE;
    }

    function getProductGroupExist($group_id)
    {
        $this->db->select('price');
        $this->db->where('price_group_id', $group_id);
        $q = $this->db->get('product_prices');
        if ($q->num_rows() > 0) {
            return TRUE;
        }
        return FALSE;
    }

    function getProductTaxRate($pid)
    {
        $this->db->select('tax_rate,tax_method');
        $this->db->where('id', $pid);
        $q = $this->db->get('products');
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    function getProductDetailONID($pid)
    {
        $this->db->select('*');
        $this->db->where('id', $pid);
        $q = $this->db->get('products');
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }


    function getProductVariableLowestAndHeighestPrice($product_id)
    {
       /* $q = $this->db->query("SELECT *, MIN(price) AS  lowest , MAX(price) AS  highest  FROM  sma_product_variants WHERE product_id = " . $product_id . " AND (price=(SELECT
  MAX(price) FROM sma_product_variants WHERE product_id =" . $product_id . " ) OR price=(SELECT
  MIN(price) FROM sma_product_variants WHERE product_id =" . $product_id . ")) GROUP BY price");*/


        $q = $this->db->query("SELECT MIN(price) AS lowest , MAX(price) AS highest, (SELECT id FROM sma_product_variants WHERE product_id =$product_id AND price=(SELECT MAX(price) FROM sma_product_variants WHERE product_id =$product_id) ORDER BY name LIMIT 1) AS highest_id, (SELECT id FROM sma_product_variants WHERE product_id =$product_id AND price=(SELECT MIN(price) FROM sma_product_variants WHERE product_id =$product_id) ORDER BY name LIMIT 1) AS lowest_id FROM sma_product_variants WHERE product_id =$product_id");

        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $product = $this->getProductDetailONID($product_id);

                $product_group_price_variant_data_lowest = $this->site->getProductGroupPrice($product->id, $this->Settings->country_price_group, $row->lowest_id);
                if ($product_group_price_variant_data_lowest->price != '') {
                    $row->lowest = $product_group_price_variant_data_lowest->price;
                } else {
                $row->lowest = $product->price + $row->lowest;
                }

                $product_group_price_variant_data_highest = $this->site->getProductGroupPrice($product->id, $this->Settings->country_price_group, $row->highest_id);
                if ($product_group_price_variant_data_highest->price != '') {
                    $row->highest = $product_group_price_variant_data_highest->price;
                } else {
                $row->highest = $product->price + $row->highest;
                }

                /*$row->lowest = $product->price + $row->lowest;
                $row->highest = $product->price + $row->highest;*/

                $account_POS_settings = $this->getPosSettings();

                if ($product->tax_rate == '') {
                    $tax_ratex = $this->getProductTaxRate($product->id);
                    $product->tax_rate = $tax_ratex->tax_rate;
                    $product->tax_method = $tax_ratex->tax_method;
                }
                if ($this->Settings->country_vat == 1) {
                $tax_rate = $this->getTaxRateByID($product->tax_rate);
                $ctax = $this->calculateTax($product, $tax_rate, $row->lowest);
                $tax_amount_lowest = $this->sma->formatDecimal($ctax['amount']);

                $tax_rate = $this->getTaxRateByID($product->tax_rate);
                $ctax = $this->calculateTax($product, $tax_rate, $row->highest);
                $tax_amount_highest = $this->sma->formatDecimal($ctax['amount']);

                $row->lowest = $row->lowest + $tax_amount_lowest;
                $row->highest = $row->highest + $tax_amount_highest;
                }

                if ($account_POS_settings->rounding != 0) {
                    $row->lowest = $this->sma->roundNumber($row->lowest, $account_POS_settings->rounding);
                    $row->highest = $this->sma->roundNumber($row->highest, $account_POS_settings->rounding);
                    return $row;
                } else {
                    return $row;
                }
            }
        }

    }

    function getAllCountryList()
    {
        $this->db->where('status', 1);
        $q = $this->db->get('country');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    function getAllRegionList($country)
    {
        $this->db->where('name', $country);
        $q = $this->db->get('country');
        if ($q->num_rows() > 0) {
            $country_id = $q->row()->country_id;

            $this->db->where('status', 1);
            $this->db->where('country_id', $country_id);
            $query = $this->db->get('zone');
            if ($query->num_rows() > 0) {
                foreach (($query->result()) as $row) {
                    $data[] = $row;
                }
                return $data;
            }

        }
        return FALSE;
    }

    function getAllGeoZones()
    {
        $q = $this->db->get('geo_zone');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }


    function getPaymentMethodAgainstCountry($country)
    {
        $this->db->where('name', $country);
        $q = $this->db->get('country');
        if ($q->num_rows() > 0) {
            //select country id
            $country_id = $q->row()->country_id;

            //serch zone id against country id
            //$this->db->where('country_id', $country_id);
            //$query = $this->db->get('zone_to_geo_zone');
            //if ($query->num_rows() > 0) {
                //$geo_zone_id = $query->row()->geo_zone_id;

				$this->db->where('status' , 1);
				$this->db->order_by("sort_order", "asc");
                //$this->db->where('geo_zone_id', $geo_zone_id);
                //$this->db->or_where('geo_zone_id', 0);
                $this->db->or_where('geo_zone_id = 0 AND status = 1');
                $query2 = $this->db->get('payment_methods');
                if ($query2->num_rows() > 0) {
                    foreach (($query2->result()) as $row) {
                        $data[] = $row;
                    }
                    return $data;
                }
            //}
        }
        return FALSE;
    }


    function getAllOrderStatus()
    {
        return array("Pending", "Processing", "Processed", "Canceled");
    }

    function getActiveDeliveryMethods($country)
    {
        $cart_total_amount = $this->cart->total();

        $this->db->where('name', $country);
        $q = $this->db->get('country');
        if ($q->num_rows() > 0) {
            //select country id
            $country_id = $q->row()->country_id;
        }

            //serch zone id against country id
            $this->db->where('country_id', $country_id);
            $query = $this->db->get('zone_to_geo_zone');
            if ($query->num_rows() > 0) {
                $geo_zone_id = $query->row()->geo_zone_id;
            }

            if($geo_zone_id && $country = 'United Arab Emirates'){

                $this->db->where('status', 1);
                $this->db->where('geo_zone_id', $geo_zone_id);
                $query2 = $this->db->get('shipment_methods');
                if ($query2->num_rows() > 0) {
                    $free_ship_amount = $query2->row()->free_ship_amount;

                    if( $cart_total_amount >=  $free_ship_amount){
                        $this->db->where('status', 1);
                        $this->db->where('free_amount_status', 'above');
                        $this->db->where('geo_zone_id', $geo_zone_id);
                        $query2 = $this->db->get('shipment_methods');
                        if ($query2->num_rows() > 0) {
                            foreach (($query2->result()) as $row) {
                                $data[] = $row;
                            }
                            return $data;
                        }

                    }else{
                        $this->db->where('status', 1);
                        $this->db->where('free_amount_status', 'below');
                        $this->db->where('geo_zone_id', $geo_zone_id);
                        $query2 = $this->db->get('shipment_methods');
                        if ($query2->num_rows() > 0) {
                            foreach (($query2->result()) as $row) {
                                $data[] = $row;
                            }
                            return $data;
                        }
                    }
                }
            }else{
				//$this->db->where('status' , 1);
                $this->db->where('geo_zone_id !=', 5);
                //$this->db->or_where('geo_zone_id = 0 AND status = 1');
                $query2 = $this->db->get('shipment_methods');
                if ($query2->num_rows() > 0) {
                    foreach (($query2->result()) as $row) {
                        $data[] = $row;
                    }
                    return $data;
                }
            }

        return FALSE;
    }


    function AramexRequestAction($aramex_data)
    {

        //To check that the destination coutnry is UAE or another
        if ($aramex_data['destination_country_code'] == 'AE') {
            $dom = 'DOM';
            $onp = 'CDS';
        } else {
            $dom = 'EXP';
            $onp = 'EPX';
            //$onp = $aramex_data['shipment_type'];
        }

        $params = array(
            'ClientInfo' => array(
                'AccountCountryCode' => $aramex_data['account_country_code'],
                'AccountEntity' => $aramex_data['account_entity'],
                'AccountNumber' => $aramex_data['account_number'],
                'AccountPin' => $aramex_data['account_pin'],
                'UserName' => $aramex_data['user_name'],
                'Password' => $aramex_data['password'],
                'Version' => $aramex_data['version']
            ),

            'Transaction' => array(
                'Reference1' => '001'
            ),

            'OriginAddress' => array(
                'City' => $aramex_data['origin_city'],
                'CountryCode' => $aramex_data['origin_country_code']
            ),

            'DestinationAddress' => array(
                'City' => $aramex_data['destination_city_name'],
                'CountryCode' => $aramex_data['destination_country_code'],
                'PostCode' => $aramex_data['destination_postal_code']
            ),
            'ShipmentDetails' => array(
                'PaymentType' => 'P',
                'ProductGroup' => $dom,
                'ProductType' => $onp,
                'ActualWeight' => array('Value' => $aramex_data['total_weight'], 'Unit' => 'KG'),
                'ChargeableWeight' => array('Value' => $aramex_data['total_weight'], 'Unit' => 'KG'),
                'NumberOfPieces' => $aramex_data['total_quantity']
            )
        );

        $soapClient = new SoapClient(base_url() . 'vendor/aramex/aramex-rates-calculator-wsdl.wsdl', array('trace' => 1));
        return $results = $soapClient->CalculateRate($params);

    }


    function calculateAramexCharges($aramex_data)
    {
        if ($aramex_data['user_name']) {
            $response = $this->AramexRequestAction($aramex_data);
            return $response->TotalAmount->Value;

        }

    }


    function calculateProductWeight($pid, $option_id = NULL)
    {

        if ($option_id) {
            $this->db->select('weight');
            $this->db->where('product_id', $pid);
            $this->db->where('id', $option_id);
            $q = $this->db->get('product_variants');
            if ($q->num_rows() > 0) {
                if ($q->row()->weight > 0) {
                    return $q->row()->weight;
                }
                return 5;
            }
        } else {
        $this->db->select('weight');
        $this->db->where('id', $pid);
        $q = $this->db->get('products');
        if ($q->num_rows() > 0) {
            if ($q->row()->weight > 0) {
                return $q->row()->weight;
            }
            return 5;
        }
        return 5;
    }
        return 5;
    }


    function countryCode($countr_name)
    {
        $this->db->select('iso_code_2');
        $this->db->where('name', $countr_name);
        $q = $this->db->get('country');
        if ($q->num_rows() > 0) {
            return $q->row()->iso_code_2;
        }
    }

    function locationAddressValidationAramex($aramex_data,$iso_code_val,$address_city_val,$postal_code_val){
        $soapClient = new SoapClient(base_url() . 'vendor/aramex/Location-APIs-WSDL.wsdl');

            $params = array(

            'ClientInfo'  			=> array(
                'AccountCountryCode'	=> $aramex_data['account_country_code'],
                'AccountEntity'		=> $aramex_data['account_entity'],
                'AccountNumber'		=> $aramex_data['account_number'],
                'AccountPin'		=> $aramex_data['account_pin'],
                'UserName'			=> $aramex_data['user_name'],
                'Password'		 	=> $aramex_data['password'],
                'Version'		 	=> $aramex_data['version'],
                'Source' 			=> NULL
            ),  


            'Transaction' 			=> array(
                'Reference1'			=> '001',
                'Reference2'			=> '002',
                'Reference3'			=> '003',
                'Reference4'			=> '004',
                'Reference5'			=> '005'

            ),
            'Address' 			=> array(
                'Line1'			=> '001',
                'Line2'			=> '',
                'Line3'			=> '',
                'City'			=> $address_city_val,
                'StateOrProvinceCode'			=> '',
                'PostCode'			=> $postal_code_val,
                'CountryCode'			=> $iso_code_val
            )
        );

        try {
            $auth_call = $soapClient->ValidateAddress($params);
            return $auth_call;

        } catch (SoapFault $fault) {
            die('Error : ' . $fault->faultstring);
        }


    }

    function getGiftCardExpiry($gift_card_number){
        $this->db->select('expiry');
        $this->db->where('card_no', $gift_card_number);
        $q = $this->db->get('gift_cards');
        if ($q->num_rows() > 0) {
            return $q->row()->expiry;
        }
        return ;
    }

    function getExpiryAfterSettingMonth(){
        $account_POS_settings = $this->getPosSettings();
        $month = $account_POS_settings->gift_card_expiry_month;
        return date('d/m/Y', strtotime('+'.$month.' months'));
    }


    function getCountryIOS($country_code){
        $this->db->where('name', $country_code);
        $this->db->where('status', 1);
        $q = $this->db->get('country');
        if ($q->num_rows() > 0) {
            return $q->row()->iso_code_2;
        }
        return FALSE;
    }


    function postal_code_validationByRequest($postal_code_val,$address_city_val,$iso_code_val ){

        $delivery_list = $this->getActiveDeliveryMethods();
        foreach ($delivery_list as $list) {
            if ($list->secret_code == 'aramex') {
                $aramex_data = [
                    'user_name' => $list->user_name,
                    'password' => $list->password,
                    'version' => $list->version,
                    'account_pin' => $list->account_pin,
                    'account_number' => $list->account_number,
                    'account_entity' => $list->account_entity,
                    'account_country_code' => $list->account_country_code,
                    'origin_city' => $list->origin_city,
                    'origin_country_code' => $list->origin_country_code,
                ];
            }
        }

        $message = '';
        if($iso_code_val && $postal_code_val && $address_city_val){
            $address_validation = $this->locationAddressValidationAramex($aramex_data,$iso_code_val,$address_city_val,$postal_code_val);
            if($address_validation->HasErrors == 1){
                return $message = $address_validation->Notifications->Notification->Message;
            }else{
                return $message = 1;
            }
        }
        return  $message = 'Please fill CountryName, CityName, PostalCode.';
    }


}