<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
    <section class="page-contents">
    <div class="container">
    <div class="row">
    <div class="col-md-12">
    <div class="row">

    <div class="col-sm-12">
        <h1>Shopping Cart</h1>

        <div class="table-responsive">
            <table id="cart-table" class="table table-bordered">
                <thead>
                <tr>
                    <td class="text-center">Image</td>
                    <td class="text-left">Product Name</td>
                    <td class="text-left">Model</td>
                    <td class="text-left">Quantity</td>
                    <td class="text-right">Unit Price</td>
                    <td class="text-right">Total</td>
                </tr>
                </thead>
                <tbody></tbody>
            </table>
        </div>

        <div class="panel panel-default margin-top-lg hide">
            <div class="panel-heading text-bold">
                <i class="fa fa-shopping-cart margin-right-sm"></i> <?= lang('shopping_cart'); ?>
                [ <?= lang('items'); ?>: <span id="total-items"></span> ]
                <a href="<?= shop_url('products'); ?>" class="pull-right hidden-xs">
                    <i class="fa fa-share"></i>
                    <?= lang('continue_shopping'); ?>
                </a>
            </div>
            <div class="panel-body" style="padding:0;">
                <div class="cart-empty-msg <?= ($this->cart->total_items() > 1) ? 'hide' : ''; ?>">
                    <?= '<h4 class="text-bold">' . lang('empty_cart') . '</h4>'; ?>
                </div>
                <div class="cart-contents">
                    <div class="table-responsive">
                        <table id="cart-tablez"
                               class="table table-condensed table-striped table-cart margin-bottom-no">
                            <thead>
                            <tr>
                                <th><i class="text-grey fa fa-trash-o"></i></th>
                                <th>Image</th>
                                <th class="col-xs-4" colspan="2"><?= lang('product'); ?></th>
                                <th class="col-xs-3">Model</th>
                                <th class="col-xs-1">Quantity</th>
                                <th class="col-xs-2">Unit Price</th>
                                <th class="col-xs-2"><?= lang('total'); ?></th>
                            </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="cart-contents">
                <div id="cart-helper" class="panel panel-footer margin-bottom-no">
                    <a href="<?= site_url('cart/destroy'); ?>" id="empty-cart"
                       class="btn btn-danger btn-sm">
                        <?= lang('empty_cart'); ?>
                    </a>
                    <a href="<?= shop_url('products'); ?>"
                       class="btn btn-primary btn-sm pull-right">
                        <i class="fa fa-share"></i>
                        <?= lang('continue_shopping'); ?>
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div class="clearfix"></div>
        <input type="hidden" id="giftcart_amount" value="<?php if($this->session->userdata('gift_cart_amount')){ echo $this->session->userdata('gift_cart_amount');} ?>">

    <div class="cart-contents">
    <div class="col-sm-12 cart_adjustment">
    <div class="panel panel-default">
    <div class="panel-heading">
        <h4 class="panel-title"><a href="#collapse-voucher" data-toggle="collapse"
                                   data-parent="#accordion"
                                   class="accordion-toggle collapsed"
                                   aria-expanded="false">Use Gift Certificate <i
                        class="fa fa-caret-down"></i></a></h4>
    </div>
    <div id="collapse-voucher" class="panel-collapse collapse" aria-expanded="false"
         style="height: 0px;">
    <div class="panel-body">
    <label class="col-sm-2 control-label" for="input-voucher">Enter your gift
        certificate code here</label>
    <div class="input-group">
        <input type="text" name="voucher"
               placeholder="Enter your gift certificate code here"
               id="input-voucher" class="form-control" value="<?php if($this->session->userdata('gift_cart_no')){ echo $this->session->userdata('gift_cart_no');} ?>"
            <?php if($this->session->userdata('gift_cart_no')){ echo'readonly';} ?>
        >
        <span class="input-group-btn">
        <input type="submit" value="Apply Gift Certificate" id="button-voucher" data-loading-text="Loading..."
               class="btn btn-primary"
            <?php if($this->session->userdata('gift_cart_no')){ echo'disabled';} ?>
        >
        </span></div>
<?php

if (!$this->loggedIn) {
    ?>
<!--<input type="hidden" id="customer_id" value="no">-->
    <?php
}else{
    ?>
<!--<input type="hidden" id="customer_id" value="<?php /*echo $this->session->userdata('user_id'); */?>">-->

<?php } ?>


    <script src="<?= $assets ?>js/sweetalert.min.js"></script>
    <script type="text/javascript">
        $('#button-voucher').on('click', function () {
            var that = $(this);

            var customer_id = <?php echo $customer_id; ?>;
            if(customer_id=='0'){
                window.location.href = site.base_url+"login";
            }
            var cn = $('input[name=\'voucher\']').val();
            if (cn != '') {
                $.ajax({
                    type: "get", async: false,
                    url: site.base_url + "admin/sales/validate_gift_card/" + cn,
                    dataType: "json",
                    success: function (data) {
                        console.log(data);
                        if (data === false) {
                            $('#giftcart_amount').val();
                            swal('<?=lang('incorrect_gift_card')?>');
                        } else if (data.customer_id !== null && parseInt(data.customer_id) !== customer_id) {
                            swal('<?=lang('gift_card_not_for_customer')?>');
                        } else {
                            $('#giftcart_amount').val(data.balance);
                            update_cart_gift(data.balance,cn);
                            swal('Balnace updated');
                            that.prop('disabled',true);
                            $('input[name=\'voucher\']').prop('disabled', true);
                            // $('#gc_details').html('<small>Card No: ' + data.card_no + '<br>Value: ' + data.value + ' - Balance: ' + data.balance + '</small>');
                            //$('#gift_card_no').parent('.form-group').removeClass('has-error');
                        }
                    }
                });
            }

        });
    </script>
    </div>
    </div>
    </div>


    <div class="cart-contents">
        <div class="col-sm-4 col-sm-offset-8">
            <table id="cart-totals" class="table table-bordered  cart-totals">
                <tbody>
                </tbody>
            </table>
        </div>

        <!-- <table id="cart-totals"
                class="table table-borderless table-striped cart-totals"></table>-->
        <!--<a href="<? /*= site_url('cart/checkout'); */ ?>"
                                               class="btn btn-primary btn-lg btn-block"><? /*= lang('checkout'); */ ?></a>-->
        <div class="col-sm-12">
            <div class="buttons clearfix">
                <div class="pull-left"><a href="<?= site_url('shop/products'); ?>"
                                          class="btn btn-default">Continue Shopping</a></div>
                <div class="pull-right"><a href="<?= site_url('cart/checkout'); ?>"
                                           class="btn btn-primary">Checkout</a></div>
            </div>

        </div>

    </div>
    </div>
    </div>

    <!--   <div class="cart-contents">

                            <div class="col-sm-4 col-sm-offset-8">
                                <table class="table table-bordered">
                                    <tbody>
                                    <tr>
                                        <td class="text-right"><strong>Sub-Total:</strong></td>
                                        <td class="text-right">AED 1,000</td>
                                    </tr>
                                    <tr>
                                        <td class="text-right"><strong>Total:</strong></td>
                                        <td class="text-right">AED 1,000</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="col-sm-12">
                                <div class="buttons clearfix">
                                    <div class="pull-left"><a href="<? /*= site_url('shop/products'); */ ?>"
                                                              class="btn btn-default">Continue Shopping</a></div>
                                    <div class="pull-right"><a href="<? /*= site_url('cart/checkout'); */ ?>"
                                                               class="btn btn-primary">Checkout</a></div>
                                </div>

                            </div>
                        </div>-->


    </div>
    <code class="text-muted">* <?= lang('shipping_rate_info'); ?></code>
    </div>
    </div>
    </div>
    </section>


    <?php
    $this->load->helper('shop_helper');
    if ($upcoming_promotions != '' && multi_array_search($upcoming_promotions[0]->item_id, $this->cart->contents()) == false) {
        require 'show_upselling.php';
    }

    if (!isset($_COOKIE['cart_upselling'])) {
        //start of set cookies
        //$cart_upselling = "cart_upselling";
        //$cookie_value = "showed";
        //setcookie($cart_upselling, $cookie_value, time() + (86400 * 30), "/");
        //End of set Cookies
        //require 'show_upselling.php';
    } else {
        //setcookie('cart_upselling', 'showed' , time() - 3600, "/");
    }


    ?>