<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<style>
    .breadcrumb-list li:last-child a {
        color: #505050;
        font-weight: bold;
    }
    .breadcrumb>li+li:before {
        content: "\003e" !important;
    }
    @media (min-width: 768px) {
        .owl-carousel .owl-stage-outer, .single-product {
            overflow: inherit;
        }
        .single-product:hover .pro-img .secondary-img {
            opacity: 0;
        }
        .list .main-single-product.fix{
            overflow: inherit;
        }
        .overflowhide{
            overflow: hidden;
        }

    }
    .breadcrumb-content h1 {
        font-size: 45px;
        line-height: 60px;
        margin: 0 0 10px;
        font-weight: 300;
    }
    .padding50{
        padding-top: 30px;
        padding-bottom: 50px;

    }
    .breadcrumb-list li a{
        font-size: 14px;
    }

    @media (max-width: 767px) {
        .container {
            width: auto !important;
        }
        .homeproduct_slider_img{
            height: 220px;
        }
        .custom_listing_height{
            height: 340px !important;
        }
        .pro-content {
            padding: 10px 0 20px;
            position: relative;
        }
    }
    @media only screen and (max-width: 480px) {
        .breadcrumb-content h1 {
            font-size: 35px;
            line-height: 35px;
            margin: 0 0 25px;
        }
        .homeproduct_slider_img{
            height: 150px;
        }
        .custom_listing_height{
            height: 230px !important;
        }
        .pro-content {
            padding: 10px 0 20px;
            position: relative;
        }

    }
    .small_list_products{
        width: 100%;
    }
    .small_list_products .homeproduct_slider_img{
        height: 345px;
        width: 500px;
    }
</style>
<!-- Page Breadcrumb Start -->
<?php if($how_much_off_cart_notify == 1 || $promotion == 1){ ?>
<div class="" style="margin-bottom: 20px;">
    <?php }else{ ?>
<div class="">
    <?php } ?>
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="breadcrumb-content text-center padding50">
                    <h1>
                        <?php


                        if(!empty($filters['category'])){
                                echo $filters['category']->name;
                        }else{
                            echo 'Products';
                        }?>
                    </h1>
                    <ul class="breadcrumb-list breadcrumb">
                       
                        <li><a href="<?= base_url().'shop/products' ?>">Products</a></li>
                        <?php


                        if(!empty($filters['category'])){
                            ?>
                            <?= $breadcrumb; ?>
                       <!-- <li><a href="#"><?/*= $filters['category']->name; */?></a></li>-->
                          <?php
                        }?>
                    </ul>
                </div>
            </div>
        </div>
    </div>



</div>
<!-- Page Breadcrumb End -->


<div class="all-categories pb-100">
    <div class="container">
        <div class="row">
    <!-- Categories Product Start -->
    <?php if($how_much_off_cart_notify == 1){ ?>
        <div class="alert alert-success alert-dismissible">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            <strong>Note!</strong> You will get  <?= $how_much_off_cart_percentage; ?>% Discount on above <?= $how_much_off_cart_promotion; ?> cart value.
        </div>
    <?php } else if($promotion == 1){ ?>
        <div class="alert alert-success alert-dismissible">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            <strong>Note!</strong> This category is on promotion with <?= $how_much_off; ?>% Discount.
        </div>
    <?php } ?>



            <!-- Sidebar Content Start -->
            <div class="col-md-9 col-md-push-3">
                <!-- Sidebar Right Top Content Start -->
                <div class="sidebar-desc-content">
                   <!-- <p>Example of category description text</p><hr>-->
                </div>
                <!-- Sidebar Right Top Content Start -->
                <!-- Best Seller Product Start -->
                <div class="best-seller">
                    <div class="row mt-20">
                        <div class="col-md-3 col-sm-4 pull-left">
                            <div class="grid-list-view">
                                <ul class="list-inline">
                                    <li class="active two-col cpoiter"><i class="zmdi zmdi-view-dashboard"></i><i class="pe-7s-keypad"></i></li>
                                    <li class="three-col cpoiter"><i class="zmdi zmdi-view-list"></i><i class="pe-7s-menu"></i></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-5 pull-right">
                            <select name="shorer" id="grid-sort" class="form-control select-varient sortdropdown">
                                <option value="name-desc">Sort By:Default</option>
                                <option  id="name-asc" class="sorting" value="name-asc">Sort By:Name (A - Z)</option>
                                <option id="name-desc" class="sorting" value="name-desc">Sort By:Name (Z - A)</option>
                                <option id="price-asc" class="sorting" value="price-asc">Sort By:Price (Low > High)</option>
                                <option id="price-desc" class="sorting" value="price-desc">Sort By:Price (High > Low)</option>
                               <!-- <option value="#">Sort By:Rating (Highest)</option>
                                <option value="#">Sort By:Rating (Lowest)</option>
                                <option value="#">Sort By:Model (A - Z)</option>
                                <option value="#">Sort By:Model (Z - A)</option>-->
                            </select>
                        </div>
                      <!--  <div class="col-md-3 col-sm-3 pull-right">
                            <select name="shorter" id="#" class="form-control select-varient">
                                <option value="#">Show: 9</option>
                                <option value="#">Show: 25</option>
                                <option value="#">Show: 50</option>
                                <option value="#">Show: 75</option>
                                <option value="#">Show: 100</option>
                            </select>
                        </div>-->
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="tab-content categorie-list ">
                                <div id="results" class="product_listing_result"></div>
                            </div>
                            <!-- .Tab Content End -->
                        </div>
                    </div>
                    <div class="row mt-40 mb-70">
                        <div class="col-sm-6">
                            <ul id="pagination" class="blog-pagination">
                            </ul>
                            <!-- End of .blog-pagination -->
                        </div>
                        <div class="col-sm-6">
                            <div class="pro-list-details text-right">
                                <span class="page-info line-height-xl hidden-xs hidden-sm"></span>
                            </div>
                            <!-- Pro List Details End -->
                        </div>
                    </div>
                    <!-- Row End -->
                </div>
                <!-- Best Seller Product End -->
            </div>
            <!-- Sidebar Content End -->
            <!-- Sidebar Start -->
            <div class="col-md-3 col-md-pull-9">
                <aside class="categorie-sidebar mb-100">
                    <!-- Categories Module Start -->
                    <div class="categorie-module mb-80">
                        <h3>categories</h3>
                        <ul class="categorie-list">
                            <?php
                            foreach($categories as $pc) {
                                ?>

                            <li class="active">
                                <a href="<?= site_url('category/'.$pc->slug);?>"><?= $pc->name;?> (<?= $pc->product_count ?>) </a>
                                <ul class="sub-categorie pl-30">
                                    <?php
                                    foreach($pc->subcategories as $sc) {
                                    ?>
                                    <li><a href="<?= site_url('category/'.$pc->slug.'/'.$sc->slug) ?>"><?= $sc->name; ?>  </a></li>
                                    <?php } ?>
                                </ul>
                            </li>
                            <?php
                            }
                            ?>
                            <li><a href="<?= base_url().'shop/products'; ?>">All Products </a></li>

                        </ul>
                    </div>
                    <!-- Categories Module End -->
                    <!-- Filter Option Start -->
                    <div class="flter-option mb-80">
                        <h3>PRICE FILTER</h3>
                            <div id="slider-range" style="cursor:pointer"></div>
                            <input type="text" id="amount" class="amount" readonly>
                        <input type="hidden" id="min-price" value="" placeholder="Min" class="form-control"></input>
                        <input type="hidden" id="max-price" value="" placeholder="Max" class="form-control"></input>
                    </div>
                    <!-- Filter Option End -->
                    <!-- Most Viewed Product Start -->
                    <div class="most-viewed">
                        <h3>most viewed</h3>
                        <!-- Most Viewed Product Activation Start -->
                        <div class="most-viewed-product owl-carousel">
                            <!-- Triple Product Start -->
                            <div class="triple-product">


                                <?php
                                $r = 0;
                                foreach ($side_featured as $fp) {
                                    if ($r < 3) {
                                        ?>

                                        <div class="single-product overflowhide mb-25">
                                            <!-- Product Image Start -->
                                            <div class="pro-img">
                                                <a href="<?= site_url('product/' . $fp->slug); ?>">
                                                    <img class="primary-img"
                                                         src="<?= base_url('ucloud/plugins/filepreviewer/site/resize_image_inline.php?f=' . $fp->image.'&w=1100&h=900&m=medium&uh=&o=jpg.jpg'); ?>"
                                                         alt="single-product">
                                                    <img class="secondary-img"
                                                         src="<?= base_url('ucloud/plugins/filepreviewer/site/resize_image_inline.php?f=' . $fp->image.'&w=1100&h=900&m=medium&uh=&o=jpg.jpg'); ?>"
                                                         alt="single-product">
                                                </a>
                                            </div>
                                            <!-- Product Image End -->
                                            <!-- Product Content Start -->
                                            <div class="pro-content">
                                                <h4>
                                                    <a href="<?= site_url('product/' . $fp->slug); ?>"><?= $fp->name; ?></a>
                                                </h4>
                                                <p class="price"><span>
                                                        <?php
                                                        if ($fp->promotion) {
                                                            echo '<del class="text-red">' . $this->sma->convertMoney($fp->price) . '</del><br>';
                                                            echo $this->sma->convertMoney($fp->promo_price);
                                                        } else if($fp->show_variable_price_on_shop == 1) {
                                                            $product_variable_prices = $this->site->getProductVariableLowestAndHeighestPrice($fp->id);
                                                            echo str_replace('.0000','',$product_variable_prices->lowest) .'-'. str_replace('.0000','',$product_variable_prices->highest)  . ' AED';
                                                        } else {
                                                            echo $this->sma->convertMoney($fp->price);
                                                        }
                                                        ?></span></p>
                                            </div>
                                            <!-- Product Content End -->
                                        </div>
                                        <?php
                                        $r++;
                                    }
                                }
                                ?>
                            </div>
                        </div>
                        <!-- Most Viewed Product Activation End -->
                    </div>
                    <!-- Most Viewed Product End -->
                </aside>
            </div>
            <!-- Sidebar End -->
        </div>
        <!-- Row End -->
    </div>
    <!-- Container End -->
</div>
<!-- Categories Product End -->