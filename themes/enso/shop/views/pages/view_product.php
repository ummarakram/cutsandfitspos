<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<style>
    .custom_promo_price{
        color:#aea8a8;
        font-size: 14px;
    }
    .owlimage{
        height: 90px;
    }

    .main-breadcrumb{
        width: max-content;
        padding: 10px 5px;
        margin-bottom: 10px
    }


</style>



<?php
if ($product->extra_size > 0) {
    $chart_size_data = $this->site->getSizeGuideWithID($product->extra_size);
}
?>


<div class="main-product-thumbnail pb-100" id="img-custom-product">
    <div class="container">

        <!-- Header Area End -->
        <!-- Page Breadcrumb Start -->
        <div class="main-breadcrumb ">

            <div class="breadcrumb-content">
                <ul class="breadcrumb-list breadcrumb">
                    <li><a href="<?php /*echo base_url(); */?>">DETAIL PRODUCT</a></li>
                    <li><a href="#">Product Details</a></li>
                </ul>
            </div>
           <!-- <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="breadcrumb-content">
                            <ul class="breadcrumb-list breadcrumb">
                                <li><a href="<?php /*echo base_url(); */?>">DETAIL PRODUCT</a></li>
                                <li><a href="#">Product Details</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <!-- Row End -->
            <!--</div>-->
            <!-- Container End -->
        </div>
        <!-- Page Breadcrumb End -->
        <!-- Wish List Start -->

        <div class="row">

            <?php if($how_much_off_cart_notify == 1){ ?>
            <div class="col-sm-12">
            <div class="alert alert-success alert-dismissible">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <strong>Note!</strong> You will get  <?= $how_much_off_cart_percentage; ?>% Discount on above <?= $how_much_off_cart_promotion; ?> cart value.
            </div>
            </div>
            <?php } else if($promotion == 1){ ?>
            <div class="col-sm-12"  style=" margin-top: 10px;">
            <div class="alert alert-success alert-dismissible">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <strong>Note!</strong> This product is on promotion with <?= $how_much_off; ?>% Discount.
            </div>
            </div>
            <?php } ?>
            <div class="col-sm-5">
                <img id="big-img" class="img-responsive" src="<?= base_url('ucloud/plugins/filepreviewer/site/resize_image_inline.php?f='. $product->image .'&w=1100&h=900&m=medium&uh=&o=jpg.jpg')?>"
                     data-zoom-image="<?= base_url('ucloud/plugins/filepreviewer/site/resize_image_inline.php?f='. $product->image .'&w=1100&h=900&m=medium&uh=&o=jpg.jpg')?>" alt="<?= $product->image_alt ? $product->image_alt : 'product-image' ?>"/>

                <div id="small-img" class="mt-20">

                    <div class="thumb-menu owl-carousel">

                        <?php
                        if (!empty($images)) {
                            foreach ($images as $ph) {
                                ?>
                                <a href="#" data-image="<?= base_url('ucloud/plugins/filepreviewer/site/resize_image_inline.php?f=' . $ph->photo.'&w=1000&h=900&m=medium&uh=&o=jpg.jpg'); ?>"
                                   data-zoom-image="<?= base_url('ucloud/plugins/filepreviewer/site/resize_image_inline.php?f=' . $ph->photo.'&w=1000&h=900&m=medium&uh=&o=jpg.jpg'); ?>">
                                    <img class="owlimage" src="<?= base_url('ucloud/plugins/filepreviewer/site/resize_image_inline.php?f=' . $ph->photo.'&w=1000&h=900&m=medium&uh=&o=jpg.jpg'); ?>" alt="<?= $ph->alt_text ? $ph->alt_text : 'product-image' ?>"/>
                                </a>
                                <?php
                            }
                        }
                        ?>
                    </div>
                </div>
            </div>
            <!-- Thumbnail Description Start -->
            <div class="col-sm-7">
                <div class="thubnail-desc fix">
                    <h2 class="product-header"><?= $product->name; ?></h2>
                    <!-- Product Rating Start -->
                    <!--   <div class="rating-summary fix mtb-20">
                           <div class="rating f-left mr-10">
                               <i class="fa fa-star"></i>
                               <i class="fa fa-star"></i>
                               <i class="fa fa-star"></i>
                               <i class="fa fa-star-o"></i>
                           </div>
                           <div class="rating-feedback f-left">
                               <a href="#">0 reviews</a> /
                               <a href="#">Write a review</a>
                           </div>
                       </div>-->
                    <!-- Product Rating End -->
                    <!-- Product Price Start -->
                    <div class="pro-price mb-20" style="margin-top: 10px;">
                        <ul class="pro-price-list">

                            <?php if ($product->promotion) { ?>

                                <li class="price">
                                    <!-- --><?/*= $this->Settings->default_currency; */?>
                                    <?= $this->Settings->default_currency.' '.$this->sma->convertMoney($product->promo_price); ?>
                                </li>

                                <li class="price"><?= $this->sma->convertMoney($product->price) . ' AED'; ?></li>
                                <li class="price"> - You Save&nbsp;<span
                                            class="noWrap"><?= $this->sma->convertMoney($product->price - $product->promo_price) . ' AED'; ?></span></span>
                                </li>
                                <?php
                                /*echo '<tr><td>' . lang("promotion") . '</td><td>' . $this->sma->convertMoney($product->promo_price) . ' ('.$this->sma->hrsd($product->start_date).' - '.$this->sma->hrsd($product->start_date).')</td></tr>';*/
                            } else {
                                if($product->show_variable_price_on_shop){
                                    $product_variable_prices = $this->site->getProductVariableLowestAndHeighestPrice($product->id); ?>
                                    <!--<li class="price">   <?/*= $this->Settings->default_currency.' <span id="newvprice">'. str_replace('.0000','',$product_variable_prices->lowest) .'-'. str_replace('.0000','',$product_variable_prices->highest).'</span>'; */?></li>-->
                                    <li class="price">   <?= ' <span id="newvprice">'. $this->sma->convertMoney($product_variable_prices->lowest).'-'. $this->sma->convertMoney($product_variable_prices->highest).'</span>'; ?></li>
                                <?php }else{

                                    if($promotion){
                                            $single_promotion_price = $this->site->calCulateProductPriceWithPromotionPercentagePos($product->price, $how_much_off);
                                            $real_price = $product->price;
                                            $final_single_price = $real_price - $single_promotion_price;
                                        ?>

                                        <li class="price">   <?= ' <span id="newvprice"><del>'.$this->sma->convertMoney($product->price).'</del><span>'.$this->sma->convertMoney($single_promotion_price).'</span><br><span class="custom_promo_price">You can save '.$this->sma->convertMoney($final_single_price).'</span></span>'; ?></li>
                                <?php }else{ ?>
                                <li class="price">   <?= ' <span id="newvprice">'.$this->sma->convertMoney($product->price).'</span>'; ?></li>
                                    <?php } ?>

                               <?php }

                                } ?>

                        </ul>
                    </div>
                    <!-- Product Price End -->
                    <!-- Product Price Description Start -->
                    <div class="product-price-desc">
                        <ul class="pro-desc-list">
                            <li><b>Product Code:</b> <span><?= $product->code; ?></span></li>
                            <?php if ($this->Settings->country_vat == 1) { ?>
                            <li><b>Prices inclusive UAE VAT</b></li>
                            <?php } ?>
                            <!--    <li>Reward Points: <span>1000</span></li>-->
                            <?php $in_stock = TRUE; if ($in_stock || $this->Settings->overselling) { ?>
                                <li><b>Availability</b>: <span>in Stock</span></li>
                            <?php }  ?>

                        </ul>
                    </div>
                    <!-- Product Price Description End -->
                    <!-- Product Box Quantity Start -->


                    <p>
                        <?php $in_stock = TRUE; if ($in_stock || $this->Settings->overselling) { ?>

                        <?= form_open('cart/add/'.$product->id, 'class="validate testform"'); ?>
                        <input type="hidden" id="image_product" name="image_product" value="<?= $product->image;?>">
                    <div class="row">

                        <div class="col-md-12">
                            <?php
                            if($saved_variants){
                                /* echo "<pre>";
                                 print_r($saved_variants);
                                 exit('safsdf');*/
                                $select_variables=array();
                                foreach ($saved_variants as $key => $value){

                                    $select_variables = explode(",",$value['variant_value']);
                                    if(!empty($select_variables)){ ?>
                                        <div class="clearfix"></div>
                                        <span class="col-md-4" style="padding-left: 0px;"><b class="mt-10"><?= $value['variant'] ?> :</b></span>
                                        <br><div class="clearfix"></div>

                                        <div class="btn-group pull-left" style="padding-left: 0px;padding-right: 0px;" role="group">
                                            <?php
                                            /* echo "<pre>";
                                             print_r($select_variables);*/

                                            foreach ($select_variables as $ky => $dt) {
                                                $name =str_replace("/","",$dt);
                                                $name_real =$dt;
                                                ?>

                                                <?php
                                                if (in_array($name, $saved_variableofcombinations))
                                                {
                                                    ?>




                                                <label id="atributes_label" class="btn btn-default btn-checkbox variable-checkbox label_<?= $name ?>" style="margin-left:0px">
                                            <span class="glyphicon glyphicon-unchecked"
                                                  data-icon-on="glyphicon glyphicon-check"
                                                  data-icon-off="glyphicon glyphicon-unchecked"></span>
                                                    <input name="radio[<?= $key ?>]" type="radio"  class="variable variable_<?= $name ?>" value="<?= $dt; ?>" style="display: none"
                                                           autocomplete="false"/> <?= $dt; ?>
                                                </label>
                                                <?php }else{ ?>

                                                    <label class="btn btn-default label_ disabled hide">
                                                        <span data-icon-on="glyphicon glyphicon-check" data-icon-off="glyphicon glyphicon-unchecked" class="glyphicon glyphicon-unchecked" style="width: 15px;"></span>
                                                        <input name="radiodisabled" type="radio" class="variable_ disabled" value="" style="display: none" autocomplete="false" disabled="disabled"> <?= $name_real ?>
                                                    </label>

                                                    <?php } ?>

                                                <?php
                                            }
                                            ?>
                                        </div>


                                        <?php
                                        if(!empty($value['additional_guide']) &&  $value['additional_guide'] > 0){
                                            echo '<span class="col-md-4" style="margin-left:4%;padding-left: 0px;margin-top: 8px;"><a data-id="'.$value['additional_guide'].'" id="'.$value['additional_guide'].'" class="additional_guide_pop curso_hand" data-toggle="modal" data-target="#myModalGuide" >';
                                            echo '<img style="width: 32px;" alt="Scale" src='.site_url('assets/images/scale.png').'>';
                                            echo $title = ' <u>'.$this->site->getSizingGuideName($value['additional_guide']).'</u>';
                                            echo '</a></span>';

                                        }
                                        ?>

                                        <br>
                                        <?php
                                    }


                                }

                                //  exit('asd');

                                /*
                                                        echo "<pre>";
                                                        print_r($saved_variants);
                                                        exit('asd');*/

                            }

                            ?>
                        </div>
                    </div>


                    <!-- <div class="form-group  required <?php /*echo !empty($variants) ? 'show' : 'hide' */?>" style="width: 100%;" >
                        <label class="control-label" for="input-option264">Select Size</label>
                        <?php
                    /*                        if ($variants) {
                                                foreach ($variants as $variant) {
                                                    $opts[$variant->id] = $variant->name.($variant->price > 0 ? ' (+'.$this->sma->convertMoney($variant->price, TRUE, FALSE).')' : ($variant->price == 0 ? '' : ' (+'.$this->sma->convertMoney($variant->price, TRUE, FALSE).')'));
                                                }
                                              //  echo form_dropdown('option', $opts, '', 'class="form-control" required="required"');
                                            }
                                            */?>
                    </div>-->
                    <div class="box-quantity mtb-20">
                        <div class="quantity-item">
                            <label><b>Qty:</b> </label>
                            <div class="cart-plus-minus">
                                <input class="cart-plus-minus-box quantity-input" id="input-quantity" type="text" name="quantity" value="1" required="required">
                            </div>
                        </div>
                    </div>

                    <div class="product-button-actions">
                        <button type="default" id="button-cart" class="customaddtocart">add to cart</button>
                        <a href="#" data-toggle="tooltip" title="Add to Wishlist" class="same-btn mr-15 add-to-wishlist" data-id="<?= $product->id; ?>"><i
                                    class="pe-7s-like"></i></a>
                        <!-- <button data-toggle="tooltip" title="Compare this Product" class="same-btn"><i
                                     class="pe-7s-repeat"></i></button>-->
                    </div>


                    <div class="form-group">
                        <!-- <div class="input-group col-xs-6 <?/*= $chart_size_data !='' ? 'pull-left' : '' */?>">
                                            <span class="input-group-addon pointer btn-minus"><span class="fa fa-minus"></span></span>
                                            <input type="text" name="quantity" class="form-control text-center quantity-input" value="1" required="required">
                                            <span class="input-group-addon pointer btn-plus"><span class="fa fa-plus"></span></span>
                                        </div>-->

                        <?php if($chart_size_data){ ?>
                            <!--  <div class="input-group col-xs-6">
                                                <a class="pull-right curso_hand" data-toggle="modal" data-target="#myModal">
                                                    <img alt="Scale" width="32" src="<?/*= site_url('assets/images/scale.png'); */?>"> Sizing info
                                                </a>
                                            </div>-->
                        <?php } ?>
                    </div>
                    <?= $chart_size_data !='' ? '<br/>' : '' ?>
                    <!-- <input type="hidden" name="quantity" class="form-control text-center" value="1"> -->
                    <!--<div class="form-group" >
                        <strong>Instructions: </strong>
                        <div class="special_instructions">
                            <textarea class="form-control special_inc_textarea" placeholder="Write your Instructions here..."></textarea>
                        </div>
                    </div>-->

                    <!-- <div class="form-group">
                                        <div class="btn-group" role="group" aria-label="...">
                                            <button class="btn btn-info btn-lg add-to-wishlist" data-id="<?/*= $product->id; */?>"><i class="fa fa-heart-o"></i></button>
                                            <button type="submit" class="btn btn-theme btn-lg"><i class="fa fa-shopping-cart padding-right-md"></i> <?/*= lang('add_to_cart'); */?></button>
                                        </div>
                                    </div>-->
                    <?= form_close(); ?>
                    <?php } else {
                        echo '<strong>'.lang('item_out_of_stock').'</strong>';
                    } ?>

                    </p>
                    <a class="link_viewfull_pro mrgin-left-10 hidden" href="<?= site_url('product/' . $product->slug); ?>"></a>

                    <div class="social-shared">
                        <ul>
                            <li class="f-book">
                                <a href="" onclick="shareit()">
                                    <span><i class="fa fa-thumbs-o-up" aria-hidden="true"></i></span>
                                    <span>like</span>
                                    <span>1</span>
                                </a>
                            </li>
                            <li class="twitter">
                                <a href="<?= 'https://twitter.com/share?url='. base_url().'product/'.$product->slug.'/&amp;text='. $product->name .'&amp;hashtags='.$product->name ?>" >
                                    <span><i class="fa fa-twitter" aria-hidden="true"></i></span>
                                    <span>tweet</span>
                                </a>
                            </li>
                            <li class="pinterest">
                                <a href="<?= 'https://plus.google.com/share?url='. base_url().'product/'.$product->slug ; ?>">
                                    <span><i class="fa fa-google" aria-hidden="true"></i></span>
                                    <span>plus</span>
                                </a>
                            </li>
                            <!-- Product Social Link Share Dropdown Start -->
                            <!-- <li class="share-post">
                                 <a href="#">
                                     <span><i class="fa fa-plus-square" aria-hidden="true"></i></span>
                                     <span>share</span>
                                 </a>
                                 <ul class="sharable-dropdown">
                                     <li><a href="#"><i class="fa fa-facebook-official"
                                                        aria-hidden="true"></i>facebook</a></li>
                                     <li><a href="#"><i class="fa fa-twitter-square" aria-hidden="true"></i>twitter</a>
                                     </li>
                                     <li><a href="#"><i class="fa fa-print" aria-hidden="true"></i>print</a></li>
                                     <li><a href="#"><i class="fa fa-envelope-o" aria-hidden="true"></i>email</a></li>
                                     <li><a href="#"><i class="fa fa-pinterest-square"
                                                        aria-hidden="true"></i>pinterest</a></li>
                                     <li><a href="#"><i class="fa fa-google-plus-square"
                                                        aria-hidden="true"></i>google+</a></li>
                                     <li><a href="#"><i class="fa fa-plus-square" aria-hidden="true"></i>more(99)</a>
                                     </li>
                                 </ul>
                             </li>-->
                            <!-- Product Social Link Share Dropdown End -->
                        </ul>
                    </div>
                    <!-- Product Social Link Share End -->
                </div>
            </div>
            <!-- Thumbnail Description End -->
        </div>
        <!-- Row End -->
    </div>
    <!-- Container End -->
</div>
<!-- Product Thumbnail Description Start -->
<div class="thumnail-desc pb-50">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <ul class="main-thumb-desc text-center list-inline">
                    <li class="active"><a data-toggle="tab" href="#detail">Details</a></li>
                    <!--   <li><a data-toggle="tab" href="#review">Reviews (0)</a></li>-->
                </ul>
                <!-- Product Thumbnail Tab Content Start -->
                <div class="tab-content thumb-content">
                    <div id="detail" class="tab-pane fade in active pb-40">
                        <p class="mb-10"><?= $product->product_details; ?></p>
                    </div>
                    <!--  <div id="review" class="tab-pane fade">
                          <!-- Reviews Start -->
                    <!-- <div class="review">
                         <p class="mb-10">There are no reviews for this product.</p>
                         <h2>WRITE A REVIEW</h2>
                     </div>-->
                    <!-- Reviews End -->
                    <!-- Reviews Field Start -->
                    <!--   <div class="riview-field mt-30">
                           <form autocomplete="off" action="#">
                               <div class="form-group">
                                   <label class="req" for="sure-name">name</label>
                                   <input type="text" class="form-control" id="sure-name" required="required">
                               </div>
                               <div class="form-group">
                                   <label class="req" for="comments">your Review</label>
                                   <textarea class="form-control" rows="5" id="comments" required="required"></textarea>
                                   <div class="help-block">
                                       <span class="text-danger">Note:</span> HTML is not translated!
                                   </div>
                               </div>
                               <div class="form-group required radio-label">
                                   <div class="row">
                                       <div class="col-sm-12">
                                           <label class="control-label req">Rating</label> &nbsp;&nbsp;&nbsp; Bad&nbsp;
                                           <input type="radio" name="rating" value="1"> &nbsp;
                                           <input type="radio" name="rating" value="2"> &nbsp;
                                           <input type="radio" name="rating" value="3"> &nbsp;
                                           <input type="radio" name="rating" value="4"> &nbsp;
                                           <input type="radio" name="rating" value="5"> &nbsp;Good
                                       </div>
                                   </div>
                               </div>
                               <div class="pull-right">
                                   <button type="submit" id="button-review">Continue</button>
                               </div>
                           </form>
                       </div>-->
                    <!-- Reviews Field Start -->
                    <!--/div>--->
                </div>
                <!-- Product Thumbnail Tab Content End -->
            </div>
        </div>
        <!-- Row End -->
    </div>
    <!-- Container End -->
</div>

<?php


include('related_products.php'); ?>



<!-- Product Thumbnail Description End -->

<div id="lightbox" class="modal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-middle">
        <div class="modal-content">
            <button type="button" class="close hidden" data-dismiss="modal" aria-hidden="true">×</button>
            <div class="modal-body">
                <img src="" alt=""/>
            </div>
        </div>
    </div>
</div>


<!-- Modal For size chart-->

<div class="modal fade" id="myModalGuide" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog cus_sizechart_modal_width" style="position: relative;margin: 0 auto;top: 25%;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>

                </button>
                <h3 class="modal-title" id="myModalLabel">Info</h3>

            </div>
            <div class="modal-body">
                <div role="tabpanel" id="modalpanel">
                    <!-- Nav tabs -->
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript" src="<?= base_url('themes/enso/admin/assets/js/jquery-2.0.3.min.js') ?>"></script>

<script type="text/javascript" src="<?= base_url('themes/enso/admin/assets/js/jquery.combinations.1.0.min.js')?>"></script>

<!--<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>-->

<script>
    var saved_combination = <?=json_encode($saved_combinations);?>;
    var saved_combinations_with_price = <?=json_encode($saved_combinations_with_price); ?>;
    var saved_variants_names = <?=json_encode($saved_variants_names);?>;
    console.log(saved_variants_names);

    var final_results =$.combinations(saved_variants_names);
    var final_results_name =[];
    var difference=[];

    for (var i = 0; i < final_results.length; i++) {
        var newdata = final_results[i];
        var name = '';
        for (j = 0; j < newdata.length; j++) {
            if (j > 0) {
                if (newdata[0] != '') {
                    name += ' / ' + newdata[j];
                } else {
                    name += newdata[j];
                }

            } else {
                name += newdata[j];
            }
        }

        final_results_name.push(name);
        if(jQuery.inArray(name, saved_combination) !== -1){

        }else{
            difference.push(newdata);
        }
        //  html += '<tr class="attr1"><td><input type="hidden" name="attr_name[]" value="' + name + '"><span>' + name + '</span></td><td class="code1 text-center form-group "><input type="text"  class=" form-control" name="attr_supplier_code[]" value="0"></td><td class="supplier_price text-center form-group"><input type="text"  class="form-control" name="attr_supplier_price[]" value="0"></td><td class="price text-center form-group"><input type="text" class="form-control" name="attr_price[]" value="0"></span></td><td class="price text-center form-group"><label class="switch"> <input type="checkbox" class="slidet" checked > <span class="slider round"></span> </label>  <input id="website_status_val" type="hidden" value="1" name="website_status[]"> </td><td class="text-center"><i class="fa fa-times delAttr"></i></td></tr>';


    }
    //console.log(final_results);
    //console.log(final_results_name);
   // console.log(difference);




    $('body').on('click','.variable-checkbox',function(e) {


        $('.variable-checkbox').addClass('btn-checkbox');
        $('.variable-checkbox').addClass('variable-checkbox');
        $('.newclick').addClass('btn-checkbox');
        $('.newclick').addClass('variable-checkbox');
        $('.newclick').removeClass('disabled');
        $('.variable').removeClass('disabled');
        $('.variable-checkbox').removeClass('newclick');
        $('.variable').attr('disabled',false);
        var current_variable = $(this).find('.variable').val();
        var count_combination=0;
        if(difference.length !== 0){
            for (var i = 0; i < difference.length; i++) {
                var newdata1 = difference[i];
                if(jQuery.inArray(current_variable, newdata1) !== -1){
                    console.log(newdata1);
                    $.each(newdata1, function (key, value) {
                        if(value !=current_variable){
                            console.log('.label_'+value.replace('/', ''));
                            $('.label_'+value.replace('/', '')).removeClass('btn-checkbox');
                            $('.label_'+value.replace('/', '')).removeClass('variable-checkbox');
                            $('.label_'+value.replace('/', '')).addClass('disabled');
                            $('.label_'+value.replace('/', '')).addClass('newclick');
                            $('.label_'+value.replace('/', '')).find('span').removeClass('glyphicon-check');
                            $('.label_'+value.replace('/', '')).find('span').addClass('glyphicon-unchecked');
                            $('.variable_'+value.replace('/', '')).addClass('disabled');
                            $('.variable_'+value.replace('/', '')).attr('disabled',true);
                            $('.variable_'+value.replace('/', '')).prop('checked', false);
                            $('.variable_'+value.replace('/', '')).removeClass('active');
                        }
                    });
                }else{
                    //  alert(count_combination);
                    count_combination= count_combination + 1;

                }
            }
            /* if(count_combination == 1){
             alert('This variant has no combination please select other');
             }*/
        }
        var variableimages = <?=json_encode($variableimages);?>;
        var urlimag = '<?= site_url() . 'ucloud/plugins/filepreviewer/site/resize_image_inline.php?f='; ?>';
        var mainImage = '<?= site_url() . 'ucloud/plugins/filepreviewer/site/resize_image_inline.php?f='.$product->image; ?>';
        var done=0;
        $.each(variableimages, function (key, value) {
            if(value.variable == current_variable){

                if (window.matchMedia('(max-width: 767px)').matches) {
                    $('#big-img').attr('src', urlimag+value.image+'&w=1100&h=900&m=medium&uh=&o=jpg.jpg');

                } else {
                    $('#big-img').attr('src',urlimag+value.image+'&w=1100&h=900&m=medium&uh=&o=jpg.jpg');
                    $('#big-img').attr('data-zoom-image',urlimag+value.image+'&w=1100&h=900&m=medium&uh=&o=jpg.jpg');
                    $("#big-img").data('zoom-image', urlimag+value.image+'&w=1100&h=900&m=medium&uh=&o=jpg.jpg').elevateZoom({
                        constrainType: "width",
                        containLensZoom: true,
                        gallery: 'small-img',
                        cursor: 'pointer',
                        galleryActiveClass: "active"
                    });
                }
                done=1;
                $('#image_product').val(value.image);

            }else{


            }
        });
        if(done == 0){
           /* $('#big-img').attr('src',mainImage);
            $('#big-img').attr('data-zoom-image',mainImage);
            $("#big-img").data('zoom-image',mainImage).elevateZoom({
                constrainType: "width",
                containLensZoom: true,
                gallery: 'small-img',
                cursor: 'pointer',
                galleryActiveClass: "active"
            });*/
        }
    });
    var variants_names = <?=json_encode($saved_variants);?>;
    //console.log(variants_names);


    $('#button-cart').on('click',function(e) {
        var submitcheck =0;
        // e.preventDefault();
        $.each(variants_names, function (key, value) {
            var names = 'radio['+key+']';
            if (!$("input[name='"+names+"']:checked").val()) {
                submitcheck =1;
                alert('Please select attribute "'+value.variant+'" first' );
            }
            else {
                //alert('One of the radio buttons is checked!');
            }
        });
        if(submitcheck == 1){
            return false;
        }else{

            var i = $(this).parents("#img-custom-product").find("img").eq(0),
                a = $(".main-selection ul li.cartlist svg:visible");
            if (i) {
                var o = i.clone().offset({
                    top: i.offset().top,
                    left: i.offset().left
                }).css({
                    opacity: "0.5",
                    position: "absolute",
                    height: "150px",
                    width: "150px",
                    "z-index": "1000"
                }).appendTo($("body")).animate({
                    top: a.offset().top + 10,
                    left: a.offset().left + 10,
                    width: "50px",
                    height: "50px"
                }, 1100);
                o.animate({
                    width: 0,
                    height: 0
                }, function () {
                    $(this).detach()
                })
            }

            $.ajax({
                url: site.site_url + 'cart/add_ajax/<?= $product->id ?>',
                type: 'POST',
                dataType: 'json',
                data: $("form").serialize(),

            }).done(function (data) {
                         update_mini_cart(data);
                        //$("label#atributes_label").removeClass("active");
                        //$("label#atributes_label span.glyphicon").removeClass("glyphicon-check");
                        //$("label#atributes_label span.glyphicon").addClass("glyphicon-unchecked");
                        //sa_alert('Success!', '<?=lang('item_added_to_cart');?>', 'success', true);
             });
            return true;
        }

    });

    $(function() {

        $('body').on('click','.btn-checkbox',function(e) {
            e.stopPropagation();
            e.preventDefault();
            /* if ( $( this ).hasClass( "disabled" ) ) {
             // alert('s');
             // return false;
             }*/
            var $checkbox = $(this).find(':input[type=checkbox]');
            if ($checkbox.length) {
                var $icon = $(this).find('[data-icon-on]');
                if ($checkbox.is(':checked')) {
                    unchecked($checkbox);
                } else {
                    checked($checkbox);
                }
                return;
            }
            var $radio = $(this).find(':input[type=radio]');
            if ($radio.length) {
                var $group = $radio.closest('.btn-group');
                $group.find(':input[type=radio]').not($radio).each(function(){
                    unchecked($(this));
                })
                var $icon = $(this).find('[data-icon-on]');
                if ($radio.is(':checked')) {
                    unchecked($radio);
                } else {
                    checked($radio);
                }
                return;
            }




        });
    });


    function checked($input) {
        var $button = $input.closest('.btn');
        var $icon = $button.find('[data-icon-on]');
        $button.addClass('active');
        $input.prop('checked', true);
        $icon.css('width',$icon.width());
        $icon.removeAttr('class').addClass($icon.data('icon-on'));
        $input.trigger('change');


        var actualprice = <?=$product->price; ?>;

        var result = "success";
        var count=0;
        var req_combination='';
        $(".testform  input[type=radio]:checked").each(function() {
            if(this.checked == true)
            {

                if(count == 0){
                    req_combination += this.value;
                }else{
                    req_combination += ' / '+this.value;
                }
                count=count+1;
            }
        });
		
        if(saved_combinations_with_price.length !== 0){
            var newcheck=0;
			var price_from_to = $('#newvprice').val(); 
            $.each(saved_combinations_with_price, function (key, value) {
               if(value.name == req_combination){
                 //  alert(value.price);
                   var price_save = value.price_save  ;
                   var priceformat = value.priceformat  ;
                   var promoprice = value.promo;
                   var orignal_price =  value.price;
                   var price_currency =  value.currency_symbol;
                   var show_price = 0;
                   var show_price_text = '';

                   if(promoprice > 0){
                       show_price = '<del>'+orignal_price+'</del>'+ ' ' +'<span>'+promoprice + price_currency+'</span>';
                       show_price_text = '<span class="custom_promo_price">You can save ' + price_save +price_currency+'</span>';
                   }else{
                       show_price = orignal_price
                   }

                   $('#newvprice').html(show_price +'<br/>'+ show_price_text);
                   newcheck=1;
               }
			   
            });
            if(newcheck == 0){
                //$('#newvprice').html(parseFloat(actualprice).toFixed(2));
                //$('#newvprice').html(parseFloat(actualprice).toFixed(2));
            }
        }
        //console.log(saved_combinations_with_price);
       // alert(req_combination);




    }

    function unchecked($input) {
        var $button = $input.closest('.btn');
        var $icon = $button.find('[data-icon-on]');
        $button.removeClass('active');
        $input.prop('checked', false);
        $icon.css('width',$icon.width());
        $icon.removeAttr('class').addClass($icon.data('icon-off'));
        $input.trigger('change');
    }
</script>



<script type="text/javascript">

    function shareit_f() {
        //$("head").append("<meta property='og:image' content='<?= base_url() ?>assets/uploads/<?= $product->image ?>' />");
        var url = '<?= site_url() . 'product/' . $product->slug; ?>'; //Set desired URL here
      //  var img = '<?= base_url() ?>ucloud/plugins/filepreviewer/site/resize_image_inline.php?f=<?= $product->image ?>&w=1100&h=900&m=medium&uh=&o=jpg.jpg' //Set Desired Image here
        var img = '<?= base_url('ucloud/plugins/filepreviewer/site/resize_image_inline.php?f='.$product->image .'&w=1100&h=900&m=medium&uh=&o=jpg.jpg') ?>'; //Set Desired Image here
        var totalurl = encodeURIComponent(url + '?img=' + img);
        window.open('http://www.facebook.com/sharer.php?u=' + totalurl, '', 'width=500, height=500, scrollbars=yes, resizable=no');
    }


    $(document).on('click', '.additional_guide_pop', function (e) {
        e.preventDefault();
        var guide_id = $(this).attr('data-id');
        $.ajax({
            url: site.site_url + 'shop/product_size_guide/' + guide_id, type: 'GET', dataType: 'json', data: {
                //ty: qty_input.val()
            }
        }).done(function (data) {
            //alert(JSON.stringify(data));
            //alert(JSON.stringify(data.free_shipment));
            $("#myModalGuide #myModalLabel").html(data.heading);
            $("#myModalGuide .modal-body #modalpanel").html(data.tab_heading + data.tab_description);

            $("#myModalGuide").modal('show');
        });

    });


</script>