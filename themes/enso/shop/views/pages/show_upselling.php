<script src="<?= base_url($assets.'js/libs.min.js')?>"></script>
<div class="container">
    <!-- Modal -->
    <div class="modal fade" id="onCartPopup" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <!--<button type="button" class="close" data-dismiss="modal">&times;</button>-->
                    <h3 class="modal-title"><?= $upcoming_promotions[0]->title; ?></h3>
                </div>
                <div class="modal-body nopadding">
                    <div class="custom_modal_img">
                    <?php
                        if(!empty($upcoming_promotions_img_path[0]->image)){ ?>
                            <img class="img-responsive" src="<?= base_url('assets/uploads/'.$upcoming_promotions_img_path[0]->image); ?>" />
                        <?php }else{ ?>
                            <img class="img-responsive" src="<?= base_url('assets/uploads/no_image.png'); ?>" />
                        <?php } ?>
                    </div>
                    <h4 class="modal_custom_padding"><?= $upcoming_promotions[0]->name; ?></h4>
                    <span class="modal_promotion"><?= $upcoming_promotions[0]->description; ?></span>
                    <span class="modal_promotion_price"><strong>AED <?= $this->sma->convertMoney($upcoming_promotions_img_path[0]->price); ?></strong></span>


                </div>
                <div class="modal-footer">
                    <div class="btn add-to-cart1 btn-success cus_btn_prom" data-backdrop="static" data-keyboard="false" data-id="<?= $upcoming_promotions[0]->item_id; ?>"><i class="fa fa-shopping-cart"></i> <?= lang('add_to_cart'); ?></div>
                    <button type="button" class="btn btn-default cus_btn_prom" data-dismiss="modal">No Thanks</button>
                </div>
            </div>

        </div>
    </div>

</div>


<script type="text/javascript">

    $(document).on('click', '.add-to-cart1', function(e) {
        e.preventDefault();
        var id = $(this).attr('data-id');
        var cart = $('.shopping-cart:visible');
        var qty_input = $(this).parents('.product-bottom').find('.quantity-input');
        $.ajax({url: site.site_url+'cart/add/'+id, type: 'GET', dataType: 'json', data: {}})
            .done(function(data) { cart = data; update_mini_cart(data); });

        $("#onCartPopup").modal('hide');
    });

    $('#onCartPopup').modal({  backdrop: 'static', keyboard: false, });

    $(document).on('click', '.add-to-cart1', function(e) {  location.reload(); });

</script>

<?php

?>