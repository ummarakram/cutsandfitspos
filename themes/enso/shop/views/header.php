<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?><!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!--<script type="text/javascript">if (parent.frames.length !== 0) { top.location = '<? /*= site_url(); */ ?>'; }</script>-->
    <title><?= $meta_title; ?></title>
    <meta name="description" content="<?= $meta_description; ?>">
    <!--<meta property="og:image" content="<?/*= base_url('assets/uploads/' . $image_path); */?>"/>-->

    <meta property="og:title" content="<?= $og_title; ?>" />
    <meta property="og:type" content="<?= $og_type; ?>" />
    <meta property="og:site_name" content="<?= $og_site_name; ?>" />
    <meta property="og:description" content="<?= $og_description; ?>" />
    <meta property="og:url" content="<?= $og_url; ?>" />
    <meta property="og:image" content="<?= $og_image; ?>" />
    <link rel="shortcut icon" href="<?= base_url($assets.'images/icon.png')?>">



    <link href="<?= base_url($assets.'css/allstyles.css')?>" rel="stylesheet">

    <!-- place favicon.ico in the root directory -->
    <link rel="shortcut icon" type="image/x-icon" href="<?= base_url($assets.'images/icon.png')?>">
    <!-- mobile menu css -->
    <link rel="stylesheet" href="<?= base_url($assets.'enso-css/allencostyle.min.css')?>">
    <!--<link rel="stylesheet" href="<?/*= base_url($assets.'themes/enso/admin/assets/styles/helpers/select2.css')*/?>">-->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/css/select2.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/flag-icon-css/0.8.2/css/flag-icon.min.css">

    <!-- modernizr js -->

    <script src="<?= base_url($assets.'enso-js/vendor/modernizr-2.8.3.min.js')?>"></script>

    <style>
        .topnavbackground{
            background: black !important;
            color: #f0f0eb !important;
            /* max-height: 15px !important;*/
        }

        .heighttopnav{
            /*  padding-top: 5px;*/
            font-size: 12px;
        }

        .boldh{
            font-weight: bold;;
        }

        .currency_btn a {
            background: #cc0000;
            color: #ffffff;
            display: inline-block;
            font-size: 14px;
            font-weight: 700;
            height: 40px;
            line-height: 40px;
            margin-top: 10px;
            padding: 0 10px;
            text-transform: none;
            border-radius: 10px;
            width: 100%;
            text-align: center;
        }

        .currency_btn a:hover {
            background: #ea3a3c;
        }

        .cpointor {
            cursor: pointer;
        }

        .colorwhite {
            color: white;
        }
        .flag-text { margin-left: 10px; }
        .select2-dropdown {
            z-index: 99999999;
            /* visibility: hidden;*/
        }
        .selection-list li:hover ul.ht-dropdown_flage > .select2-container{
            visibility: visible;
            opacity: 1;
        }
        .selection-list li:hover ul.ht-dropdown_flage,select2-container:hover ul.ht-dropdown_flage {
            opacity: 1;
            visibility: visible;
        }
        .selection-list > li {
            padding-bottom: 8px;
        }
        .select2-container{
            width:100% !important;
            height: 35px !important;
        }
        .select2-selection{
            width:100% !important;
            height: 35px !important;
        }
        .select2-container--default .select2-selection--single .select2-selection__rendered {
            line-height: 35px !important;
        }
        #mmpi-107 a{ color: #cc0000; }
        /*header*/
        .mobile-menu.visible-sm.visible-xs.mean-container {
            float: right;
        }
        .pe-7s-like{
            font-size: 28px !important; font-weight: bold;
        }
        .mean-container .mean-bar {
            float: right;
        }

        .selection-list>li {
            margin-left: 10px;
        }
        .mobile_logo {
            margin-left: 0px !important;
            padding-left: 0px;
            width: 80px; height: 40px;
        }
        li svg {
            height: 20px;
        }
        .selection-list>li>span{
            top:7px;
        }

        @media (max-width: 767px) {
            .mean-container a.meanmenu-reveal {
                right: 0 !important;
                left: auto !important;
                top: -38px;
            }
            .full-col{
                padding-left: 0px;
                width: 90%;
            }
            .main-selection {
                margin-top: 10px;
            }
            .header_svg svg{
                height: 20px;
            }
            .nav_icon{
                padding-top: 2px;}

            .header-sticky{
                border-bottom: 1px #1a1a1a solid;
            }
            .navbar-text {
                margin-bottom: 0px;
            }
            .pe-7s-like{
                font-size: 22px !important;
            }
            .selection-list.text-right {
                padding-right: 0px;
              padding-top: 0px;
            }
            .selection_list_mobile{
                float: right;
            }
            .ht-dropdown_flage{
                left: auto;
                right: 0;
            }
            .selection-list>li>span {
              top: ;
            }
            .sticky .mean-container a.meanmenu-reveal {

            }

        }
        @media (max-width: 768px) and (min-width: 700px) {
            .mean-container a.meanmenu-reveal {
                top: -40px !important;
            }
        }
        @media (max-width: 769px) and (min-width: 766px) {
            .mean-container a.meanmenu-reveal {
                top: -50px !important;
            }
            .sticky .mean-container a.meanmenu-reveal {
                top: -42px !important;
            }

            .nav_icon_search_mobile {
                display: none !important;
            }
        }
        @media only screen and (max-width: 480px) {
            .like_icon{
                margin-left: 5px !important;}
            .mean-container a.meanmenu-reveal {
                top: -38px;
            }
            .nav_wrapper{
                width: 100%;
            }
            .selection_list_mobile{
                float: none;
            }

        }




    </style>
</head>
<body class="common-home home1">
<!--[if lt IE 8]>
<p class="browserupgrade hidden-xs">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade
    your browser</a> to improve your experience.</p>
<![endif]-->
<!-- Wrapper Start -->
<div class="container-fluid topnavbackground" style="padding-right: 0px; padding-left: 0px;">
    <nav class="navbar navbar-default topnavbackground  hidden-xs"
         style="margin-bottom: 0px; border-color: black; border-radius: 0px; min-height: 40px">

        <div class="container-fluid topnavbackground " >
            <!--  <p  class="navbar-text navbar-left topnavbackground hidden-xs" style="font-size: 12px; margin-top: 15px">UpCycled Products | Free Shipping In The UAE</a></p>-->
            <p  class="navbar-text navbar-left topnavbackground hidden-xs" style="font-size: 12px; margin-top: 15px">UpCycled Products | Free Shipping In The UAE on orders above 200 AED</a></p>

            <ul class="nav navbar-nav navbar-right topnavbackground heighttopnav hidden-xs" style="padding-top: 5px !important;">

                <li class="flagmenus">

                    <ul class="selection-list">
                        <li class="" style="">
                            <p class="cpointor cur" style="color: white; display: inline">Ship To: <span class="flag-icon flag-icon-<?= strtolower($selected_country->iso_code_2); ?> "></span> <?= $selected_country->iso_code_2 . ' / '. $selected_currency->symbol.' ('.$selected_currency->code.')'; ?>
                                <i style="color: white; display: inline; padding-top: 10px; position: relative;top:5px"
                                   class="pe-7s-angle-down c_arrow"></i></p>
                            <ul class="ht-dropdown_flage cur dropdown_c" style="display: none;">
                                <li>
                                    <div class="form-group">
                                        <label class="colorwhite" for="sel1">Ship To:</label>
                                        <select class="form-control select_flags" id="ship_to" name="country">
                                            <?php if (!$shop_settings->hide_price && !empty($countries)) { ?>
                                                <?php
                                                foreach ($countries as $country) {
                                                    ?>
                                                    <?php
                                                    if ($selected_country->iso_code_2 != $default_country->iso_code_2) {
                                                        ?>
                                                        <option value="<?= $country->iso_code_2; ?>"
                                                                data_country_code="<?= $country->iso_code_2; ?>"
                                                                data_country_currency_id="<?= $country->currency_id; ?>"
                                                                data_country_currency_vat="<?= $country->vat; ?>" <?php if ($country->iso_code_2 == $selected_country->iso_code_2) {
                                                            echo 'selected';
                                                        } ?>> <?= $country->name; ?></option>
                                                    <?php } else { ?>
                                                        <option value="<?= $country->iso_code_2; ?>"
                                                                data_country_code="<?= $country->iso_code_2; ?>"
                                                                data_country_currency_id="<?= $country->currency_id; ?>"
                                                                data_country_currency_vat="<?= $country->vat; ?>" <?php if ($country->iso_code_2 == $default_country->iso_code_2) {
                                                            echo 'selected';
                                                        } ?>> <?= $country->name; ?></option>
                                                        <?php
                                                    }
                                                    ?>
                                                <?php }
                                            } ?>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label class="colorwhite" for="sel1">Select Currency:</label>
                                        <select class="form-control" id="currency_dropdown">
                                            <?php if (!$shop_settings->hide_price && !empty($currencies)) { ?>
                                                <?php
                                                foreach ($currencies as $currency) {
                                                    ?>
                                                    <?php
                                                    if ($default_currency->code != $selected_currency->code) {
                                                        ?>
                                                        <option value="<?= $currency->id; ?>"
                                                                data_currency_code="<?= $currency->code; ?>" <?php if ($currency->code == $selected_currency->code) {
                                                            echo 'selected';
                                                        } ?>> <?= $currency->symbol; ?> (<?= $currency->code; ?>)</option>
                                                    <?php } else { ?>
                                                        <option value="<?= $currency->id; ?>"
                                                                data_currency_code="<?= $currency->code; ?>" <?php if ($currency->code == $default_currency->code) {
                                                            echo 'selected';
                                                        } ?>> <?= $currency->symbol; ?> (<?= $currency->code; ?>)</option>

                                                        <?php
                                                    }
                                                    ?>

                                                <?php }
                                            } ?>
                                        </select>
                                    </div>
                                    <div class="currency_btn">
                                        <a class="cpointor colorwhite currency_change" style="color: white;">Save</a>
                                    </div>

                                </li>


                                <?php if (!$shop_settings->hide_price && !empty($currencies)) { ?>

                                    <?php
                                    foreach ($currencies as $currency) {
                                        // echo '<li><a href="' . site_url('main/currency/' . $currency->code) . '">' . $currency->symbol . ' ' . $currency->code . '</a></li>';
                                    }
                                    ?>

                                <?php } ?>

                            </ul>
                        </li>
                    </ul>
                </li>


                <li class="active li_hide"><a class="topnavbackground boldh" href="<?php echo base_url() . 'page/about-us'; ?>">ABOUT
                        US <span class="sr-only">(current)</span></a></li>
                <!--<li class="active li_hide"><a class="topnavbackground boldh"
                                              href="<?php base_url() . 'page/hospitality'; ?>">HOSPITALITY <span
                                class="sr-only">(current)</span></a></li>-->
                <li class="active li_hide"><a class="topnavbackground boldh"
                                              href="<?php echo base_url() . 'page/contact-us'; ?>">CONTACT US <span
                                class="sr-only">(current)</span></a></li>
            </ul>
        </div>
    </nav>
    <nav class="navbar navbar-default topnavbackground  hidden-sm hidden-md hidden-lg"
         style="margin-bottom: 0px; border-color: black; border-radius: 0px; min-height: 40px">
        <div class="container-fluid topnavbackground text-center" >
            <div class="col-xs-6 nav_wrapper">
            <p class="navbar-text topnavbackground " style="font-size: 12px; margin-top: 10px">Free Shipping In The UAE on orders above 200 AED</a>
            </p>
            </div>
            <!--  <p class="navbar-text topnavbackground" style="font-size: 12px; margin-top: 10px">UpCycled Products | Free
                  Shipping In The UAE</a>
              </p>-->
            <div class="col-xs-6 nav_wrapper">
            <p>
                <ul class="selection-list selection_list_mobile">
                    <li class="" style="padding-top: 3px;">
            <p class="cpointor cur" style="color: white; display: inline">Ship To: <span class="flag-icon flag-icon-<?= strtolower($selected_country->iso_code_2); ?> "></span> <?= $selected_country->iso_code_2 . ' / '. $selected_currency->symbol.' ('.$selected_currency->code.')'; ?>
                <i style="color: white; display: inline; padding-top: 10px; position: relative;top:5px"
                   class="pe-7s-angle-down c_arrow"></i></p>
            <ul class="ht-dropdown_flage cur dropdown_c" style="display: none;">
                <li>
                    <div class="form-group">
                        <label class="colorwhite" for="sel1">Ship To:</label>
                        <select class="form-control select_flags ship_to" id="ship_to_sm" name="country">
                            <?php if (!$shop_settings->hide_price && !empty($countries)) { ?>
                                <?php
                                foreach ($countries as $country) {
                                    ?>
                                    <?php
                                    if ($selected_country->iso_code_2 != $default_country->iso_code_2) {
                                        ?>
                                        <option value="<?= $country->iso_code_2; ?>"
                                                data_country_code="<?= $country->iso_code_2; ?>"
                                                data_country_currency_id="<?= $country->currency_id; ?>"
                                                data_country_currency_vat="<?= $country->vat; ?>" <?php if ($country->iso_code_2 == $selected_country->iso_code_2) {
                                            echo 'selected';
                                        } ?>> <?= $country->name; ?></option>
                                    <?php } else { ?>
                                        <option value="<?= $country->iso_code_2; ?>"
                                                data_country_code="<?= $country->iso_code_2; ?>"
                                                data_country_currency_id="<?= $country->currency_id; ?>"
                                                data_country_currency_vat="<?= $country->vat; ?>" <?php if ($country->iso_code_2 == $default_country->iso_code_2) {
                                            echo 'selected';
                                        } ?>> <?= $country->name; ?></option>
                                        <?php
                                    }
                                    ?>
                                <?php }
                            } ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label class="colorwhite" for="sel1">Select Currency:</label>
                        <select class="form-control currency_dropdown" id="currency_dropdown_sm">
                            <?php if (!$shop_settings->hide_price && !empty($currencies)) { ?>
                                <?php
                                foreach ($currencies as $currency) {
                                    ?>
                                    <?php
                                    if ($default_currency->code != $selected_currency->code) {
                                        ?>
                                        <option value="<?= $currency->id; ?>"
                                                data_currency_code="<?= $currency->code; ?>" <?php if ($currency->code == $selected_currency->code) {
                                            echo 'selected';
                                        } ?>> <?= $currency->symbol; ?> (<?= $currency->code; ?>)</option>
                                    <?php } else { ?>
                                        <option value="<?= $currency->id; ?>"
                                                data_currency_code="<?= $currency->code; ?>" <?php if ($currency->code == $default_currency->code) {
                                            echo 'selected';
                                        } ?>> <?= $currency->symbol; ?> (<?= $currency->code; ?>)</option>

                                        <?php
                                    }
                                    ?>

                                <?php }
                            } ?>
                        </select>
                    </div>
                    <div class="currency_btn">
                        <a class="cpointor colorwhite currency_change_sm" style="color: white;">Save</a>
                    </div>

                </li>


                <?php if (!$shop_settings->hide_price && !empty($currencies)) { ?>

                    <?php
                    foreach ($currencies as $currency) {
                        // echo '<li><a href="' . site_url('main/currency/' . $currency->code) . '">' . $currency->symbol . ' ' . $currency->code . '</a></li>';
                    }
                    ?>

                <?php } ?>

            </ul>
            </p>
            </div>

        </div>
        <div class="clearfix"></div>
    </nav>
</div>
<div class="clearfix"></div>
<div class="wrapper header_wrap">
    <!-- Preloader Start -->
    <div class="preloader">
        <div class="loading-center">
            <div class="loading-center-absolute">
                <div class="object object_one"></div>
                <div class="object object_two"></div>
                <div class="object object_three"></div>
            </div>
        </div>
    </div>
    <!-- Newsletter Popup Start -->
    <div class="popup_wrapper hide">
        <div class="test" style="width: 50%; padding: 20px;">
            <!-- <span class="popup_off">Close</span>-->
            <div class="subscribe_area text-center">
                <h3 style="padding: 10px;" class="hidden-xs hidden-sm hidden-md" >Select your preferred Location</h3>
                <h5 style="padding: 10px;" class="hidden-lg " >Select your preferred Location</h5>

                <div class="list-group">
                    <a href="<?= base_url();?>main/currency?currency=<?= $this->Settings->current_country_currency;?>&country=<?= $this->Settings->current_country_code;?>" class="list-group-item hidden-xs hidden-sm">
                        <div class="col-sm-3">
                            <span style="height: 100px; width: 100px;" class="flag-icon flag-icon-<?= strtolower($this->Settings->current_country_code); ?> "></span>
                        </div>
                        <div class="col-sm-7">
                            <p style="padding-top:40px;">Go to <b><?= $this->Settings->current_country->name; ?></b> Website</p>
                        </div>
                        <div class="col-sm-2">
                            <span style="padding-top:40px;" class="glyphicon glyphicon-chevron-right"></span>
                        </div>
                        <div class="clearfix"></div>
                    </a>

                    <a href="<?= base_url();?>main/currency?currency=<?= $this->default_currency->symbol ;?>&country=<?= $this->Settings->default_country;?>" class="list-group-item hidden-xs hidden-sm ">
                        <div class="col-sm-3">
                            <span style="height: 100px; width: 100px;" class="flag-icon flag-icon-<?= strtolower($default_country->iso_code_2); ?> "></span>
                        </div>
                        <div class="col-sm-7">
                            <p style="padding-top:40px;">Continue with <b> <?= $default_country->name; ?></b> Website</p>
                        </div>
                        <div class="col-sm-2">
                            <span style="padding-top:40px;" class="glyphicon glyphicon-chevron-right"></span>
                        </div>
                        <div class="clearfix"></div>
                    </a>
                    <a href="<?= base_url();?>main/currency?currency=<?= $this->Settings->current_country_currency;?>&country=<?= $this->Settings->current_country_code;?>" class="list-group-item hidden-lg hidden-md">
                        <p class="cpointor cur" style=" display: inline"><span class="flag-icon flag-icon-<?= strtolower($this->Settings->current_country_code); ?> "></span> Go to <b><?= $this->Settings->current_country->name; ?></b> Website
                            <span class="glyphicon glyphicon-chevron-right"></span></p>

                        <div class="clearfix"></div>
                    </a>
                    <a href="<?= base_url();?>main/currency?currency=<?= $this->default_currency->symbol ;?>&country=<?= $this->Settings->default_country;?>" class="list-group-item hidden-lg hidden-md">
                        <p class="cpointor cur" style=" display: inline"><span class="flag-icon flag-icon-<?= strtolower($default_country->iso_code_2); ?> "></span> Continue with <b> <?= $default_country->name; ?></b> Website
                            <span class="glyphicon glyphicon-chevron-right"></span></p>
                    </a>

                </div>


                <!--<span style="height: 100px; width: 100px;" class="flag-icon flag-icon-<?/*= strtolower($selected_country->iso_code_2); */?> "></span>
                <span style="height: 100px; width: 100px;" class="flag-icon flag-icon-<?/*= strtolower($this->Settings->current_country); */?> "></span>
                <div class="img-thumbnail flag flag-icon-background flag-icon-<?/*= strtolower($selected_country->iso_code_2); */?>" title="ad" id="ad"></div>
                <div class="img-thumbnail flag flag-icon-background flag-icon-<?/*= strtolower($selected_country->iso_code_2); */?>" title="ad" id="ad"></div>
                <p>Subscribe to the Gatcomart mailing list to receive updates on new arrivals, special offers and other discount information.</p>
                <div class="subscribe-form-group">
                    <form action="#">
                        <input autocomplete="off" type="text" name="message" id="message" placeholder="Enter your email address">
                        <button type="submit">subscribe</button>
                    </form>
                </div>
                <div class="subscribe-bottom mt-15">
                    <input type="checkbox" id="newsletter-permission">
                    <label for="newsletter-permission">Don't show this popup again</label>
                </div>-->
            </div>
        </div>
    </div>
    <!-- Newsletter Popup End -->

    <div class="preloader_pro" id="gg_pro" style="display: none">
        <div class="loading-center">
            <div class="loading-center-absolute">
                <div class="object object_one"></div>
                <div class="object object_two"></div>
                <div class="object object_three"></div>
            </div>
        </div>
    </div>
    <!-- Newsletter Popup End -->
    <!-- Header Area Start -->

    <header>

        <div class="container-fluid header-top-area header-sticky">
            <div class="row">
                <!-- Logo Start -->
                <div class="col-lg-2 col-md-2 col-sm-6 col-xs-5 full-col pl-0 center_logo hidden-xs">
                    <div class="logo">
                        <a href="<?= base_url(); ?>"><img class="headerlogo hidden-xs"
                                                          src="<?= base_url(); ?>assets/uploads/logos/sma-shop.png"
                                                          title="Enso" alt="Enso"></a>
                        <a href="<?= base_url(); ?>"><img class=" hidden-sm  hidden-md hidden-lg "
                                                          src="<?= base_url(); ?>assets/uploads/logos/sma-shop.png"
                                                          title="Enso" alt="Enso"></a>
                    </div>
                </div>
                <!-- Logo End -->
                <!--<div class="col-xs-12 visible-xs visible-control">
                    <ul class="search-form mobile-form">
                        <li>
                            <?/*= shop_form_open('products', 'id="product-search-form1" class="serform1" method="GET"'); */?>
                            <div id="search">
                                <input type="text"  name="query" class="search" name="search" placeholder="Search for products...">
                            </div>
                            <i class="pe-7s-search tes1"></i>
                            <?/*= form_close(); */?>
                        </li>
                    </ul>
                </div>-->
                <!-- Primary-Menu Start -->


                <link rel="stylesheet" href="<?php echo base_url('assets/assets-menu/css/menustyles.css');?>">
                <script type="text/javascript" src="<?php echo base_url('assets/assets-menu/js/jquery-3.2.1.min.js');?>"></script>
                <div class="col-lg-7 col-md-7 col-sm-12  hidden-sm hidden-xs megaresponsive">
                    <style>
                        .megam-main-nav-wrap{
                            box-shadow: none;
                            top:-10px;
                        }

                        .megam-main-nav-trigger {
                            background-color: #DB0B4E;
                            font-size: 16px;
                            padding: 0px;
                            padding-bottom: 5px;
                            padding-left: 5px;
                            padding-right: 5px;

                        /* margin-top: 80px;*/
                        .megam-main-nav-wrap a, .megam-main-nav > li > a {
                            text-align: center !important;
                            background: #ffffff;
                        }

                        .megam-main-nav-wrap a, .megam-main-nav > li > a:hover {
                            text-align: center !important;
                            background: #ffffff;
                        }
                    </style>
                    <?php  echo build_tree('enso-main');?>

                </div>
                <script type="text/javascript" src="<?php echo base_url('assets/assets-menu/js/megamenuscript.js');?>"></script>

                <!-- Primary-Menu End -->
                <!-- Header All Shopping Selection Start -->
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-7 full-col pr-0 megaresponsive_small">
                    <div class="main-selection">
                        <ul class="selection-list text-right">
                            <!-- Searcch Box Start -->
                            <li class="pull-left mobile_logo">
                                <a href="<?= base_url(); ?>"><img class=" hidden-sm  hidden-md hidden-lg "
                                                                  src="<?= base_url(); ?>assets/uploads/logos/sma-shop.png"
                                                                  title="Enso" alt="Enso"></a>
                            </li>
                            <?php if ($this->loggedIn) {
                                $length=7;
                                $uname= ucfirst($this->session->userdata('username'));

                                if(strlen($uname)<= $length)
                                {
                                }
                                else
                                {
                                    $y=substr($uname,0,$length) . '...';
                                    $uname= $y;
                                }

                                ?>

                                <li class=" nav_icon">
                                    <a href="<?= base_url() . 'profile'; ?>">
                                        <i class="pe-7s-user"></i>
                                        <p class="hidden-xs" style=" display: inline; position: relative; top:-5px;"><?= $uname;?></p>
                                    </a>
                                </li>
                            <?php } ?>

                            <li class="hidden-control hidden-xs header_svg nav_icon"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 48 48">
                                    <path d="M45.6,48c-0.4,0-0.8-0.2-1.1-0.5L31.8,34.3L31,34.9c-3.2,2.3-7,3.5-11,3.5c-10.6,0-19.2-8.6-19.2-19.2C0.9,8.6,9.5,0,20,0
                                c10.6,0,19.2,8.6,19.2,19.2c0,4.5-1.6,8.9-4.5,12.3l-0.6,0.8l12.6,13.1c0.3,0.3,0.4,0.7,0.4,1.1c0,0.4-0.2,0.8-0.5,1.1
                                C46.4,47.8,46,48,45.6,48z M20,3.1C11.2,3.1,4,10.3,4,19.2C4,28,11.2,35.3,20,35.3c8.9,0,16.1-7.2,16.1-16.1
                                C36.1,10.3,28.9,3.1,20,3.1z"></path>
                                </svg>
                                <ul class="search-form ht-dropdown">
                                    <li>
                                        <?= shop_form_open('products', 'id="product-search-form1" class="serform" method="GET"'); ?>
                                        <div id="search">
                                            <input type="text" name="query" class="search" id="product-search"
                                                   placeholder="Search for products...">
                                            <i class="pe-7s-search tes"></i>
                                        </div>
                                        <?= form_close(); ?>
                                    </li>
                                </ul>
                            </li>
                            <li class=" header_svg nav_icon hidden-lg hidden-sm- hidden-md nav_icon_search_mobile"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 48 48">
                                    <path d="M45.6,48c-0.4,0-0.8-0.2-1.1-0.5L31.8,34.3L31,34.9c-3.2,2.3-7,3.5-11,3.5c-10.6,0-19.2-8.6-19.2-19.2C0.9,8.6,9.5,0,20,0
                                c10.6,0,19.2,8.6,19.2,19.2c0,4.5-1.6,8.9-4.5,12.3l-0.6,0.8l12.6,13.1c0.3,0.3,0.4,0.7,0.4,1.1c0,0.4-0.2,0.8-0.5,1.1
                                C46.4,47.8,46,48,45.6,48z M20,3.1C11.2,3.1,4,10.3,4,19.2C4,28,11.2,35.3,20,35.3c8.9,0,16.1-7.2,16.1-16.1
                                C36.1,10.3,28.9,3.1,20,3.1z"></path>
                                </svg>
                                <ul class="search-form ht-dropdown" style="right: -140px;">
                                    <li>
                                        <?= shop_form_open('products', 'id="product-search-form1" class="serform" method="GET"'); ?>
                                        <div id="search">
                                            <input type="text" name="query" class="search" id="product-search"
                                                   placeholder="Search for products...">
                                            <i class="pe-7s-search tes"></i>
                                        </div>
                                        <?= form_close(); ?>
                                    </li>
                                </ul>
                            </li>
                            <!-- Search Box End -->
                            <li class="like_icon nav_icon"><a href="<?= shop_url('wishlist'); ?>" id="wishlist-total" title="(0)"><i
                                            class="pe-7s-like"></i>
                                    <span
                                            id="total-wishlist"><?= $wishlist; ?></span></a></li>
                            <li class="cartlist header_svg">
                               <!-- <i class="pe-7s-cart"> </i>-->

                                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 48 48">
                                    <g>
                                        <path d="M9.6,10.7v1.5h28.8V12l-0.1,0.3l0.1,0V12l-0.1,0.3l0.1-0.1l-0.1,0.1l0,0l0.1-0.1l-0.1,0.1l0,0l0,0l0,0l0,0l0,0
                                    c0,0,0.1,0.2,0.1,0.4l0,0l1.5,32.1l0,0l0,0l-0.1,0.2l0,0l0,0l0,0l0,0l0,0l0,0l0.1,0.2v-0.2l-0.1,0l0.1,0.2v-0.2H8.1v0.2l0.1-0.2
                                    l-0.1,0v0.2l0.1-0.2l0,0l-0.1-0.2l0,0l1.5-32.1l0,0c0-0.2,0.1-0.3,0.1-0.4l0,0l-0.1-0.2v0.2l0.1,0l-0.1-0.2v0.2V10.7V9.2
                                    c-0.5,0-0.9,0.1-1.3,0.3c-0.6,0.3-1,0.8-1.3,1.3c-0.3,0.5-0.4,1.1-0.5,1.7l0,0L5.1,44.6l0,0.2c0,0.9,0.3,1.6,0.9,2.2
                                    c0.5,0.6,1.3,1,2.2,1h31.8c0.9,0,1.7-0.4,2.2-1c0.5-0.6,0.8-1.4,0.9-2.2l0-0.2l-1.5-32.1l0,0c0-0.8-0.3-1.5-0.8-2.2
                                    c-0.3-0.3-0.6-0.6-0.9-0.8c-0.4-0.2-0.8-0.3-1.3-0.3H9.6V10.7z"></path>
                                        <path d="M15.9,14.6v1.5c0.4,0,0.7,0.3,0.7,0.7c0,0.4-0.3,0.7-0.7,0.7c-0.4,0-0.7-0.3-0.7-0.7c0-0.4,0.3-0.7,0.7-0.7V14.6V13
                                    c-2.1,0-3.7,1.7-3.7,3.7c0,2.1,1.7,3.7,3.7,3.7c2.1,0,3.7-1.7,3.7-3.7c0-2.1-1.7-3.7-3.7-3.7V14.6z"></path>
                                        <path d="M31.4,14.6v1.5c0.4,0,0.7,0.3,0.7,0.7c0,0.4-0.3,0.7-0.7,0.7c-0.4,0-0.7-0.3-0.7-0.7c0-0.4,0.3-0.7,0.7-0.7V14.6V13
                                    c-2.1,0-3.7,1.7-3.7,3.7c0,2.1,1.7,3.7,3.7,3.7c2.1,0,3.7-1.7,3.7-3.7c0-2.1-1.7-3.7-3.7-3.7V14.6z"></path>
                                        <path d="M32.7,15.7V9.1c0-5-4.1-9.1-9.1-9.1c-5,0-9.1,4.1-9.1,9.1v6.6h3.1l0-6.6c0-1.6,0.7-3.1,1.8-4.2c1.1-1.1,2.6-1.8,4.2-1.8
                                    c1.6,0,3.1,0.7,4.2,1.8c1.1,1.1,1.8,2.6,1.8,4.2v6.6H32.7L32.7,15.7z"></path>
                                    </g>
                                </svg>

                                <span class="cart-total-items"></span>
                                <ul class="ht-dropdown main-cart-box">
                                    <li>
                                        <!-- Cart Box Start -->
                                        <div id="cart-contents">
                                            <div id="cart-items"></div>
                                            <table class="table table-condensed table-striped table-cart"
                                                   id="cart-items"></table>
                                        </div>
                                    <li id="cart-empty">
                                        <p class="text-center cart-empty">Your shopping cart is empty!</p>
                                    </li>
                                    </li>
                                </ul>
                            </li>
                            <!-- Dropdown Currency Selection Start -->
                            <li class="header_svg nav_icon">
                                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 48 48">
                                    <g>
                                        <path d="M24,29.6c-8.2,0-14.8-6.6-14.8-14.8C9.2,6.6,15.8,0,24,0c8.2,0,14.8,6.6,14.8,14.8C38.8,23,32.2,29.6,24,29.6z M24,3
                                        c-6.5,0-11.8,5.3-11.8,11.8c0,6.5,5.3,11.8,11.8,11.8s11.8-5.3,11.8-11.8C35.8,8.3,30.5,3,24,3z"></path>
                                        <path d="M6.8,48c-0.4,0-0.8-0.2-1.1-0.5c-0.3-0.3-0.4-0.7-0.4-1.2c0.8-6.8,2.6-11.8,5.6-15.2c0.3-0.3,0.7-0.5,1.1-0.5
                                        c0.4,0,0.7,0.1,1,0.4c0.3,0.3,0.5,0.6,0.5,1c0,0.4-0.1,0.8-0.4,1.1c-2.2,2.5-3.6,6-4.5,10.7L8.5,45h31l-0.2-1.1
                                        c-0.8-4.8-2.3-8.3-4.5-10.7c-0.3-0.3-0.4-0.7-0.4-1.1c0-0.4,0.2-0.8,0.5-1c0.3-0.2,0.6-0.4,1-0.4c0.4,0,0.8,0.2,1.1,0.5
                                        c3,3.4,4.8,8.3,5.6,15.2c0,0.4-0.1,0.8-0.4,1.2C42,47.8,41.6,48,41.2,48H6.8z"></path>
                                    </g>
                                </svg>
                                <ul class="ht-dropdown currrency">
                                    <?php if ($this->loggedIn) { ?>
                                        <li>
                                            <h3> Hi, <?= ucfirst($this->session->userdata('username')); ?></h3>
                                        </li>
                                        <li>
                                            <h3>User Deatil</h3>
                                            <ul class="mb-15">
                                                <ul>
                                                    <li><a href="<?= base_url() . 'profile'; ?>">Profile</a></li>
                                                    <li><a href="<?= base_url() . 'shop/orders'; ?>">Orders</a></li>
                                                    <li><a href="<?= base_url() . 'shop/quotes'; ?>">Quotations</a></li>
                                                    <li><a href="<?= base_url() . 'shop/downloads'; ?>">Downloads</a>
                                                    </li>
                                                    <li><a href="<?= base_url() . 'shop/addresses'; ?>">Addresses</a>
                                                    </li>
                                                </ul>
                                            </ul>
                                        </li>
                                        <li>
                                            <h3>my account</h3>
                                            <ul>
                                                <li><a href="<?= base_url() . 'logout'; ?>">log out</a></li>

                                            </ul>
                                        </li>
                                    <?php } else { ?>
                                        <li>
                                            <h3>my account</h3>
                                            <ul>

                                                <li><a href="<?= base_url() . 'register'; ?>">register</a></li>
                                                <li><a href="<?= base_url() . 'login'; ?>">log in</a></li>
                                            </ul>
                                        </li>
                                    <?php } ?>
                                </ul>
                            </li>
                            <!-- Dropdown Currency Selection End -->
                        </ul>
                    </div>
                </div>
                <!-- Header All Shopping Selection End -->
                <!-- Mobile Menu  Start -->

                <div class="mobile-menu visible-sm visible-xs">
                    <nav>
                        <ul>
                            
                            <li><a href="<?php echo base_url() . 'category/hijab-collection'; ?>">Hijab Collection</a>
                                <ul>
                                    <li><a href="<?php echo base_url().'category/hijab-collection/fancy-hijab'; ?>">Fancy Hijab</a></li>
                                    <li><a href="<?php echo base_url().'category/hijab-collection/summer-hijab'; ?>">Summer Hijab</a></li>
                                </ul>
                            </li>
                            
                            <li><a href="<?php echo base_url() . 'category/girls-fashion'; ?>">Girls Fashion</a>
                                <ul>
                                    <li><a href="<?php echo base_url().'category/girls-fashion/girls-aaccessories'; ?>">Girls Accessories</a></li>
                                    <li><a href="<?php echo base_url().'category/girls-fashion/girls-fancy-and-gowns'; ?>">Girls Fancy & Gowns</a></li>
                                    <li><a href="<?php echo base_url().'category/girls-fashion/girls-tops-shirts'; ?>">Girls Tops Shirts</a></li>
                                    <li><a href="<?php echo base_url().'category/girls-fashion/girls-winter-wears-'; ?>">Girls Winter Wears-</a></li>
                                    <li><a href="<?php echo base_url().'category/girls-fashion/toddler-Collection'; ?>">Toddler Collection</a></li>
                                    <li><a href="<?php echo base_url().'category/girls-fashion/Turkish-gowns-collection'; ?>">Turkish Gowns Collection</a></li>                                    
                                </ul>
                            </li>                            

                            <li><a href="<?php echo base_url() . 'category/boys-fashion'; ?>">Boys Fashion</a>
                                <ul>
                                    <li><a href="<?php echo base_url().'category/boys-fashion/boys-formal-suits-'; ?>">Boys Formal Suits</a></li>
                                    <li><a href="<?php echo base_url().'category/boys-fashion/boys-jackets-blazers'; ?>">Boys Jackets & Blazers</a></li>
                                    <li><a href="<?php echo base_url().'category/boys-fashion/boys-t-shirt'; ?>">Boys T-Shirt</a></li>
                                    <li><a href="<?php echo base_url().'category/boys-fashion/Boys-Trendy-Summers'; ?>">Boys Trendy Summers</a></li>
                                </ul>
                            </li>                            
                            
                            
                            <li><a href="<?php echo base_url().'category/ballerina'; ?>">Ballerina</a>
                                <!-- Mobile Menu Dropdown Start -->
                                <ul>
                                    <li><a href="<?php echo base_url().'category/ballerina/ballerina-leotard-dresses'; ?>">Ballerina Leotard Dresses</a></li>
                                    <li><a href="<?php echo base_url().'category/ballerina/ballet-dance-shoes'; ?>">Ballet Dance Shoes</a></li>
                                    <li><a href="<?php echo base_url().'category/ballerina/stockings-tights'; ?>">Stockings Tights</a></li>
                                </ul>
                                <!-- Mobile Menu Dropdown End -->
                            </li>

                            <li><a href="<?php echo base_url() . 'category/cosplay-costumes'; ?>">Cosplay Costumes</a>
                                <ul>
                                    <li><a href="<?php echo base_url().'category/cosplay-costumes/boys-super-hero-costumes-'; ?>">Boys Super Hero Costumes</a></li>
                                    <li><a href="<?php echo base_url().'category/cosplay-costumes/character-costumes'; ?>">Character Costumes</a></li>
                                    <li><a href="<?php echo base_url().'category/cosplay-costumes/princess-costumes'; ?>">Princess Costumes</a></li>
                                </ul>
                            </li>

                            <li><a href="<?php echo base_url() . 'category/tutu-collection'; ?>">TuTu Collection</a>
                                <ul>
                                    <li><a href="<?php echo base_url().'category/tutu-collection/petti-skirts-'; ?>">Petti Skirts</a></li>
                                    <li><a href="<?php echo base_url().'category/tutu-collection/Birthday-Tutu-Set'; ?>">Birthday Tutu Set</a></li>
                                    <li><a href="<?php echo base_url().'category/tutu-collection/hand-made-tutu-skirts'; ?>">Hand Made Tutu Skirts</a></li>
                                    <li><a href="<?php echo base_url().'category/tutu-collection/flower-girl-tutu-dresses'; ?>">Flower Girl Tutu Dresses</a></li>                                    
                                </ul>
                            </li>
                            

                            <li><a href="<?php echo base_url() . 'category/women-fashion'; ?>">Women Fashion</a>
                                <ul>
                                    <li><a href="<?php echo base_url().'category/women-fashion/chiffon-tunics'; ?>">Chiffon Tunics</a></li>
                                    <li><a href="<?php echo base_url().'category/women-fashion/women-Tulle-skirts'; ?>">Women Tulle Skirts</a></li>
                                    <li><a href="<?php echo base_url().'category/women-fashion/womens-collar-knots'; ?>">Women's Collar Knots</a></li>
                                    <li><a href="<?php echo base_url().'category/women-fashion/chiffon-capes-poncho'; ?>">Chiffon Capes & Poncho</a></li>
                                    <li><a href="<?php echo base_url().'category/women-fashion/women-Tops-Dresses'; ?>">Women Tops And Dresses</a></li>                                    
                                </ul>
                            </li>     
                                      
                            <li><a href="<?php echo base_url() . 'category/mermaid-swimwear'; ?>">Mermaid Swimwear</a></li>     
                            <li><a href="<?php echo base_url().'category/on-sale'; ?>">On Sale</a></li>
                        </ul>
                    </nav>
                </div>
                <!-- Mobile Menu  End -->
            </div>
        </div>
    </header>


    <!-- Header Area End -->
    <!-- Slider Area Start -->
    <div class="wrapper"></div>
</div>

