<style>
    .nivo-control{
        color: #1a1a1a !important;
    }
    .margintop20{
        margin-top: 20px;
    }

    @media (min-width: 768px) {
        .owl-carousel .owl-stage-outer, .single-product {
            overflow: inherit;
        }
        .single-product:hover .pro-img .secondary-img {
            opacity: 0;
        }
    }

</style>
<?php
$home_slider_1 = getSliderData('home-slider-1');
if(!empty($home_slider_1)){
    $counter_slider2 = $counter_slider3 = (count($home_slider_1) +1);
}else{
    $counter_slider2 = $counter_slider3 = 1;
}
?>
</div>
<?php if(!empty($home_slider_1)){ ?>
<div class="slider-area pb-100">
    <!-- Main Slider Area Start -->
    <div class="slider-wrapper theme-default">
        <!-- Slider Background  Image Start-->
        <div id="slider" class="nivoSlider">

            <?php
            $counter_slider =1;
            foreach ($home_slider_1 as $s1 => $slider) {
                ?>
                <img src="<?= base_url('ucloud/plugins/filepreviewer/site/resize_image_inline.php?f=' . $slider->image.'&w=1100&h=900&m=medium&uh=&o=jpg.jpg'); ?>" data-thumb="<?= base_url('ucloud/plugins/filepreviewer/site/resize_image_inline.php?f=' . $slider->image.'&w=1100&h=900&m=medium&uh=&o=jpg.jpg'); ?>" alt="<?= $slider->image_alt ? $slider->image_alt : 'slider-image' ?>" title="#htmlcaption<?= $counter_slider;?>"/>
                <?php
                $counter_slider ++;
            }
            ?>
        </div>
        <?php
        $counter_slider1 =1;
        foreach ($home_slider_1 as $s1 => $slider) {
            ?>
            <?php if($counter_slider1 == 1 && $slider->slider_id == 2){ ?>


                <div id="htmlcaption<?= $counter_slider1;?>" class="nivo-html-caption slider-caption">

                    <div class="slider-text slidertext2nd">
                        <h2 class="wow fadeInLeft font-white" data-wow-delay="1s"><?= strip_tags($slider->caption); ?></h2>
                  <?php if($slider->link != ''){ ?>
                        <a class="wow bounceInDown font-white margintop20" data-wow-delay="0.8s" href="<?= base_url().$slider->link ?>">shop now</a>
                        <?php } ?>
                    </div>

                </div>
            <?php }else if($counter_slider1 == 2 && $slider->slider_id == 2){ ?>
                <div id="htmlcaption<?= $counter_slider1;?>" class="nivo-html-caption slider-caption">

                    <div class="slider-text slidertext2nd" style="top:52%">
                        <h2 class="wow zoomInUp" data-wow-delay="0.5s"><?= strip_tags($slider->caption); ?></h2>
                <?php if($slider->link != ''){ ?>
                        <a class="wow zoomInUp margintop20 shopfrontmargintop" style="border: 1px solid #1a1a1a; color: #1a1a1a;" data-wow-delay="1s" href="<?= base_url().$slider->link ?>">shop now</a>
                <?php } ?>
                    </div>

                </div>

            <?php }else if($counter_slider1 == 3 && $slider->slider_id == 2){ ?>
                <div id="htmlcaption<?= $counter_slider1;?>" class="nivo-html-caption slider-caption">

                    <div class="slider-text slidertext2nd">
                        <h2 class="wow zoomInUp" data-wow-delay="0.5s"><?= strip_tags($slider->caption); ?></h2>
                <?php if($slider->link != ''){ ?>
                        <a class="wow zoomInUp margintop20" style="border: 1px solid #1a1a1a; color: #1a1a1a" data-wow-delay="1s" href="<?= base_url().$slider->link ?>">shop now</a>
                <?php } ?>
                    </div>
                </div>

            <?php }else{ ?>
                <div id="htmlcaption<?= $counter_slider1;?>" class="nivo-html-caption slider-caption">

                    <div class="slider-text slidertext2nd">
                        <h2 class="wow zoomInUp" data-wow-delay="0.5s"><?= strip_tags($slider->caption); ?></h2>
                <?php if($slider->link != ''){ ?>
                        <a class="wow zoomInUp margintop20" style="border: 1px solid #1a1a1a; color: #1a1a1a" data-wow-delay="1s" href="<?= base_url().$slider->link ?>">shop now</a>
                <?php } ?>
                    </div>
                </div>

            <?php } ?>

            <?php
            $counter_slider1 ++;
        }
        ?>


    </div>
</div>
<?php } ?>


<div class="wrapper">

    <!-- Slider Area End -->
    <!-- New Products Selection Start -->
    <div class="row">
        <div class="col-xs-12">
            <?php foreach ($landing_page_blocks as $landing_page_block) { ?>


                <div class="best-seller-products pb-100">
                    <div class="container">
                        <div class="row">
                            <!-- Section Title Start -->
                            <div class="col-xs-12">
                                <div class="section-title text-center mb-40">
                                    <!-- <span class="section-desc mb-15">OUR FAVORITES</span>-->
                                    <h3 class="section-info"><?= $landing_page_block->title; ?></h3>
                                </div>
                            </div>
                            <!-- Section Title End -->
                        </div>
                        <?php
                        $get_products = $this->shop_model->getAllProductsAgainstBlockId($landing_page_block->id, $landing_page_block->number_of_products);
                        //print_r($get_products);
                        ?>
                        <!-- Row End -->
                        <div class="row">
                            <div class="col-sm-12">
                                <!-- Best Seller Product Activation Start -->
                                <div class="best-seller new-products owl-carousel">
                                    <!-- Single Product Start -->


                                    <?php foreach ($get_products as $key => $product){
                                        $promo_text = $this->site->getPromotionPercentage_promotext($product->id);
                                        ?>

                                        <div class="single-product">

                                            <div class="box"><div class="ribbon"><span><?= $promo_text ?></span></div></div>

                                            <!-- Product Image Start -->
                                            <div class="pro-img">
                                                <a href="<?= site_url('product/' . $product->slug); ?>">
                                                    <img class="primary-img homeproduct_slider_img" src="<?= base_url('ucloud/plugins/filepreviewer/site/resize_image_inline.php?f=' . $product->image.'&w=1100&h=900&m=medium&uh=&o=jpg.jpg'); ?>" alt="<?= $product->image_alt ? $product->image_alt : 'product-image' ?>">
                                                    <img class="secondary-img homeproduct_slider_img" src="<?= base_url('ucloud/plugins/filepreviewer/site/resize_image_inline.php?f=' . $product->image.'&w=1100&h=900&m=medium&uh=&o=jpg.jpg'); ?>" alt="<?= $product->image_alt ? $product->image_alt : 'product-image' ?>">
                                                </a>
                                                <span class="sticker-new"><?= $promo_text ? 'Promo' : 'New'  ; ?></span>
                                            </div>
                                            <!-- Product Image End -->
                                            <!-- Product Content Start -->
                                            <div class="pro-content text-center">
                                                <h4><a href="<?= site_url('product/' . $product->slug); ?>"><?= substr($product->name, 0, 40); ?></a></h4>
                                                <p class="price"><span>


                                         <?php
                                         if ($fp->promotion) {
                                             echo '<del class="text-red was">' . $this->sma->convertMoney($product->price) . ' AED</del><br>';
                                             echo '<span class="is_price_tab">' . $this->sma->convertMoney($product->promo_price) . ' AED</span>';
                                         } else {
                                             if($product->show_variable_price_on_shop == 1){
                                                 $product_variable_prices = $this->site->getProductVariableLowestAndHeighestPrice($product->id);
                                                 /*echo '<span class="is_price_tab">' . str_replace('.0000','',$product_variable_prices->lowest) .'-'. str_replace('.0000','',$product_variable_prices->highest)  . ' AED</span>';*/
                                                 echo '<span class="is_price_tab">' .$this->sma->convertMoney($product_variable_prices->lowest) .'-'. $this->sma->convertMoney($product_variable_prices->highest)  . '</span>';
                                             }else{
                                             echo '<span class="is_price_tab">' . $this->sma->convertMoney($product->price) . '</span>';
                                         }

                                         }
                                         ?>


                                    </span></p>
                                                <div class="action-links2">
                                                    <a data-toggle="tooltip" title="Add to Cart" href="<?= site_url('product/' . $product->slug); ?>">add to cart</a>
                                                </div>
                                            </div>
                                            <!-- Product Content End -->
                                        </div>
                                    <?php } ?>
                                </div>
                                <!-- Best Seller Product Activation Start -->
                            </div>
                        </div>
                        <!-- Row End -->
                        <?php if($promo_text){ echo '<a style="color: #cc0000" href="/category/on-sale">See all</a>'; } ?>
                    </div>
                    <!-- Container End -->
                </div>
            <?php } ?>
        </div>
    </div>

</div>
<?php
$home_slider_2 = getSliderData('home-slider-2');
?>
<?php if(!empty($home_slider_2)){ ?>
<div class="slider-area pb-20">
    <!-- Main Slider Area Start -->
    <div class="slider-wrapper theme-default">
        <!-- Slider Background  Image Start-->
        <div id="slider_2nd" class="nivoSlider2">

            <?php
            foreach ($home_slider_2 as $s2 => $slider2) {
                ?>
                <img src="<?= base_url('ucloud/plugins/filepreviewer/site/resize_image_inline.php?f=' . $slider2->image.'&w=1100&h=900&m=medium&uh=&o=jpg.jpg'); ?>" data-thumb="<?= base_url('ucloud/plugins/filepreviewer/site/resize_image_inline.php?f=' . $slider2->image.'&w=1100&h=900&m=medium&uh=&o=jpg.jpg'); ?>" alt="<?= $slider2->image_alt ? $slider2->image_alt : 'slider-image' ?>" title="#htmlcaption<?= $counter_slider2;?>"/>
                <?php
                $counter_slider2 ++;
            }
            ?>
        </div>
        <?php

        foreach ($home_slider_2 as $s2 => $slider2) {
            ?>


            <div id="htmlcaption<?= $counter_slider3;?>" class="nivo-html-caption slider-caption" >
                <!-- Slider Text Start -->
                <div class="slider-text slidertext2nd">
                    <h2 class="wow fadeInLeft font-white" data-wow-delay="1s" style="color: white;"><?= strip_tags($slider2->caption); ?></h2>
                    <?php if($slider2->link != ''){ ?>
                        <a class="wow bounceInDown font-white"  data-wow-delay="0.8s" href="<?= base_url().$slider2->link ?>">shop now</a>
                    <?php } ?>
                </div>
            </div>

            <?php
            $counter_slider3 ++;
        }
        ?>


    </div>

</div>
<?php } ?>
<div class="wrapper">