!function(s){"use strict";function n(t){return t.is('[type="checkbox"]')?t.prop("checked"):t.is('[type="radio"]')?!!s('[name="'+t.attr("name")+'"]:checked').length:t.is("select[multiple]")?(t.val()||[]).length:t.val()}var i=function(t,e){this.options=e,this.validators=s.extend({},i.VALIDATORS,e.custom),this.$element=s(t),this.$btn=s('button[type="submit"], input[type="submit"]').filter('[form="'+this.$element.attr("id")+'"]').add(this.$element.find('input[type="submit"], button[type="submit"]')),this.update(),this.$element.on("input.bs.validator change.bs.validator focusout.bs.validator",s.proxy(this.onInput,this)),this.$element.on("submit.bs.validator",s.proxy(this.onSubmit,this)),this.$element.on("reset.bs.validator",s.proxy(this.reset,this)),this.$element.find("[data-match]").each(function(){var e=s(this),t=e.attr("data-match");s(t).on("input.bs.validator",function(t){n(e)&&e.trigger("input.bs.validator")})}),this.$inputs.filter(function(){return n(s(this))&&!s(this).closest(".has-error").length}).trigger("focusout"),this.$element.attr("novalidate",!0)};function e(a){return this.each(function(){var t=s(this),e=s.extend({},i.DEFAULTS,t.data(),"object"==typeof a&&a),r=t.data("bs.validator");(r||"destroy"!=a)&&(r||t.data("bs.validator",r=new i(this,e)),"string"==typeof a&&r[a]())})}i.VERSION="0.11.9",i.INPUT_SELECTOR=':input:not([type="hidden"], [type="submit"], [type="reset"], button)',i.FOCUS_OFFSET=20,i.DEFAULTS={delay:500,html:!1,disable:!0,focus:!0,custom:{},errors:{match:"Does not match",minlength:"Not long enough"},feedback:{success:"glyphicon-ok",error:"glyphicon-remove"}},i.VALIDATORS={native:function(t){var e=t[0];if(e.checkValidity)return!e.checkValidity()&&!e.validity.valid&&(e.validationMessage||"error!")},match:function(t){var e=t.attr("data-match");return t.val()!==s(e).val()&&i.DEFAULTS.errors.match},minlength:function(t){var e=t.attr("data-minlength");return t.val().length<e&&i.DEFAULTS.errors.minlength}},i.prototype.update=function(){var t=this;return this.$inputs=this.$element.find(i.INPUT_SELECTOR).add(this.$element.find('[data-validate="true"]')).not(this.$element.find('[data-validate="false"]').each(function(){t.clearErrors(s(this))})),this.toggleSubmit(),this},i.prototype.onInput=function(t){var e=this,r=s(t.target),a="focusout"!==t.type;this.$inputs.is(r)&&this.validateInput(r,a).done(function(){e.toggleSubmit()})},i.prototype.validateInput=function(e,r){n(e);var a=e.data("bs.validator.errors");e.is('[type="radio"]')&&(e=this.$element.find('input[name="'+e.attr("name")+'"]'));var i=s.Event("validate.bs.validator",{relatedTarget:e[0]});if(this.$element.trigger(i),!i.isDefaultPrevented()){var o=this;return this.runValidators(e).done(function(t){e.data("bs.validator.errors",t),t.length?r?o.defer(e,o.showErrors):o.showErrors(e):o.clearErrors(e),a&&t.toString()===a.toString()||(i=t.length?s.Event("invalid.bs.validator",{relatedTarget:e[0],detail:t}):s.Event("valid.bs.validator",{relatedTarget:e[0],detail:a}),o.$element.trigger(i)),o.toggleSubmit(),o.$element.trigger(s.Event("validated.bs.validator",{relatedTarget:e[0]}))})}},i.prototype.runValidators=function(a){var i=[],e=s.Deferred();function o(t){return r=t,a.attr("data-"+r+"-error")||((e=a[0].validity).typeMismatch?a.attr("data-type-error"):e.patternMismatch?a.attr("data-pattern-error"):e.stepMismatch?a.attr("data-step-error"):e.rangeOverflow?a.attr("data-max-error"):e.rangeUnderflow?a.attr("data-min-error"):e.valueMissing?a.attr("data-required-error"):null)||a.attr("data-error");var e,r}return a.data("bs.validator.deferred")&&a.data("bs.validator.deferred").reject(),a.data("bs.validator.deferred",e),s.each(this.validators,s.proxy(function(t,e){var r=null;!n(a)&&!a.attr("required")||void 0===a.attr("data-"+t)&&"native"!=t||!(r=e.call(this,a))||(r=o(t)||r,!~i.indexOf(r)&&i.push(r))},this)),!i.length&&n(a)&&a.attr("data-remote")?this.defer(a,function(){var t={};t[a.attr("name")]=n(a),s.get(a.attr("data-remote"),t).fail(function(t,e,r){i.push(o("remote")||r)}).always(function(){e.resolve(i)})}):e.resolve(i),e.promise()},i.prototype.validate=function(){var e=this;return s.when(this.$inputs.map(function(t){return e.validateInput(s(this),!1)})).then(function(){e.toggleSubmit(),e.focusError()}),this},i.prototype.focusError=function(){if(this.options.focus){var t=this.$element.find(".has-error :input:first");0!==t.length&&(s("html, body").animate({scrollTop:t.offset().top-i.FOCUS_OFFSET},250),t.focus())}},i.prototype.showErrors=function(t){var e=this.options.html?"html":"text",r=t.data("bs.validator.errors"),a=t.closest(".form-group"),i=a.find(".help-block.with-errors"),o=a.find(".form-control-feedback");r.length&&(r=s("<ul/>").addClass("list-unstyled").append(s.map(r,function(t){return s("<li/>")[e](t)})),void 0===i.data("bs.validator.originalContent")&&i.data("bs.validator.originalContent",i.html()),i.empty().append(r),a.addClass("has-error has-danger"),a.hasClass("has-feedback")&&o.removeClass(this.options.feedback.success)&&o.addClass(this.options.feedback.error)&&a.removeClass("has-success"))},i.prototype.clearErrors=function(t){var e=t.closest(".form-group"),r=e.find(".help-block.with-errors"),a=e.find(".form-control-feedback");r.html(r.data("bs.validator.originalContent")),e.removeClass("has-error has-danger has-success"),e.hasClass("has-feedback")&&a.removeClass(this.options.feedback.error)&&a.removeClass(this.options.feedback.success)&&n(t)&&a.addClass(this.options.feedback.success)&&e.addClass("has-success")},i.prototype.hasErrors=function(){return!!this.$inputs.filter(function(){return!!(s(this).data("bs.validator.errors")||[]).length}).length},i.prototype.isIncomplete=function(){return!!this.$inputs.filter("[required]").filter(function(){var t=n(s(this));return!("string"==typeof t?s.trim(t):t)}).length},i.prototype.onSubmit=function(t){this.validate(),(this.isIncomplete()||this.hasErrors())&&t.preventDefault()},i.prototype.toggleSubmit=function(){this.options.disable&&this.$btn.toggleClass("disabled",this.isIncomplete()||this.hasErrors())},i.prototype.defer=function(t,e){if(e=s.proxy(e,this,t),!this.options.delay)return e();window.clearTimeout(t.data("bs.validator.timeout")),t.data("bs.validator.timeout",window.setTimeout(e,this.options.delay))},i.prototype.reset=function(){return this.$element.find(".form-control-feedback").removeClass(this.options.feedback.error).removeClass(this.options.feedback.success),this.$inputs.removeData(["bs.validator.errors","bs.validator.deferred"]).each(function(){var t=s(this),e=t.data("bs.validator.timeout");window.clearTimeout(e)&&t.removeData("bs.validator.timeout")}),this.$element.find(".help-block.with-errors").each(function(){var t=s(this),e=t.data("bs.validator.originalContent");t.removeData("bs.validator.originalContent").html(e)}),this.$btn.removeClass("disabled"),this.$element.find(".has-error, .has-danger, .has-success").removeClass("has-error has-danger has-success"),this},i.prototype.destroy=function(){return this.reset(),this.$element.removeAttr("novalidate").removeData("bs.validator").off(".bs.validator"),this.$inputs.off(".bs.validator"),this.options=null,this.validators=null,this.$element=null,this.$btn=null,this.$inputs=null,this};var t=s.fn.validator;s.fn.validator=e,s.fn.validator.Constructor=i,s.fn.validator.noConflict=function(){return s.fn.validator=t,this},s(window).on("load",function(){s('form[data-toggle="validator"]').each(function(){var t=s(this);e.call(t,t.data())})})}(jQuery);
/*! jQuery v1.12.4 | (c) jQuery Foundation | jquery.org/license */ ! function(a, b) {
    "object" == typeof module && "object" == typeof module.exports ? module.exports = a.document ? b(a, !0) : function(a) {
        if (!a.document) throw new Error("jQuery requires a window with a document");
        return b(a)
    } : b(a)
}("undefined" != typeof window ? window : this, function(a, b) {
    var c = [],
        d = a.document,
        e = c.slice,
        f = c.concat,
        g = c.push,
        h = c.indexOf,
        i = {},
        j = i.toString,
        k = i.hasOwnProperty,
        l = {},
        m = "1.12.4",
        n = function(a, b) {
            return new n.fn.init(a, b)
        },
        o = /^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g,
        p = /^-ms-/,
        q = /-([\da-z])/gi,
        r = function(a, b) {
            return b.toUpperCase()
        };
    n.fn = n.prototype = {
        jquery: m,
        constructor: n,
        selector: "",
        length: 0,
        toArray: function() {
            return e.call(this)
        },
        get: function(a) {
            return null != a ? 0 > a ? this[a + this.length] : this[a] : e.call(this)
        },
        pushStack: function(a) {
            var b = n.merge(this.constructor(), a);
            return b.prevObject = this, b.context = this.context, b
        },
        each: function(a) {
            return n.each(this, a)
        },
        map: function(a) {
            return this.pushStack(n.map(this, function(b, c) {
                return a.call(b, c, b)
            }))
        },
        slice: function() {
            return this.pushStack(e.apply(this, arguments))
        },
        first: function() {
            return this.eq(0)
        },
        last: function() {
            return this.eq(-1)
        },
        eq: function(a) {
            var b = this.length,
                c = +a + (0 > a ? b : 0);
            return this.pushStack(c >= 0 && b > c ? [this[c]] : [])
        },
        end: function() {
            return this.prevObject || this.constructor()
        },
        push: g,
        sort: c.sort,
        splice: c.splice
    }, n.extend = n.fn.extend = function() {
        var a, b, c, d, e, f, g = arguments[0] || {},
            h = 1,
            i = arguments.length,
            j = !1;
        for ("boolean" == typeof g && (j = g, g = arguments[h] || {}, h++), "object" == typeof g || n.isFunction(g) || (g = {}), h === i && (g = this, h--); i > h; h++)
            if (null != (e = arguments[h]))
                for (d in e) a = g[d], c = e[d], g !== c && (j && c && (n.isPlainObject(c) || (b = n.isArray(c))) ? (b ? (b = !1, f = a && n.isArray(a) ? a : []) : f = a && n.isPlainObject(a) ? a : {}, g[d] = n.extend(j, f, c)) : void 0 !== c && (g[d] = c));
        return g
    }, n.extend({
        expando: "jQuery" + (m + Math.random()).replace(/\D/g, ""),
        isReady: !0,
        error: function(a) {
            throw new Error(a)
        },
        noop: function() {},
        isFunction: function(a) {
            return "function" === n.type(a)
        },
        isArray: Array.isArray || function(a) {
            return "array" === n.type(a)
        },
        isWindow: function(a) {
            return null != a && a == a.window
        },
        isNumeric: function(a) {
            var b = a && a.toString();
            return !n.isArray(a) && b - parseFloat(b) + 1 >= 0
        },
        isEmptyObject: function(a) {
            var b;
            for (b in a) return !1;
            return !0
        },
        isPlainObject: function(a) {
            var b;
            if (!a || "object" !== n.type(a) || a.nodeType || n.isWindow(a)) return !1;
            try {
                if (a.constructor && !k.call(a, "constructor") && !k.call(a.constructor.prototype, "isPrototypeOf")) return !1
            } catch (c) {
                return !1
            }
            if (!l.ownFirst)
                for (b in a) return k.call(a, b);
            for (b in a);
            return void 0 === b || k.call(a, b)
        },
        type: function(a) {
            return null == a ? a + "" : "object" == typeof a || "function" == typeof a ? i[j.call(a)] || "object" : typeof a
        },
        globalEval: function(b) {
            b && n.trim(b) && (a.execScript || function(b) {
                a.eval.call(a, b)
            })(b)
        },
        camelCase: function(a) {
            return a.replace(p, "ms-").replace(q, r)
        },
        nodeName: function(a, b) {
            return a.nodeName && a.nodeName.toLowerCase() === b.toLowerCase()
        },
        each: function(a, b) {
            var c, d = 0;
            if (s(a)) {
                for (c = a.length; c > d; d++)
                    if (b.call(a[d], d, a[d]) === !1) break
            } else
                for (d in a)
                    if (b.call(a[d], d, a[d]) === !1) break; return a
        },
        trim: function(a) {
            return null == a ? "" : (a + "").replace(o, "")
        },
        makeArray: function(a, b) {
            var c = b || [];
            return null != a && (s(Object(a)) ? n.merge(c, "string" == typeof a ? [a] : a) : g.call(c, a)), c
        },
        inArray: function(a, b, c) {
            var d;
            if (b) {
                if (h) return h.call(b, a, c);
                for (d = b.length, c = c ? 0 > c ? Math.max(0, d + c) : c : 0; d > c; c++)
                    if (c in b && b[c] === a) return c
            }
            return -1
        },
        merge: function(a, b) {
            var c = +b.length,
                d = 0,
                e = a.length;
            while (c > d) a[e++] = b[d++];
            if (c !== c)
                while (void 0 !== b[d]) a[e++] = b[d++];
            return a.length = e, a
        },
        grep: function(a, b, c) {
            for (var d, e = [], f = 0, g = a.length, h = !c; g > f; f++) d = !b(a[f], f), d !== h && e.push(a[f]);
            return e
        },
        map: function(a, b, c) {
            var d, e, g = 0,
                h = [];
            if (s(a))
                for (d = a.length; d > g; g++) e = b(a[g], g, c), null != e && h.push(e);
            else
                for (g in a) e = b(a[g], g, c), null != e && h.push(e);
            return f.apply([], h)
        },
        guid: 1,
        proxy: function(a, b) {
            var c, d, f;
            return "string" == typeof b && (f = a[b], b = a, a = f), n.isFunction(a) ? (c = e.call(arguments, 2), d = function() {
                return a.apply(b || this, c.concat(e.call(arguments)))
            }, d.guid = a.guid = a.guid || n.guid++, d) : void 0
        },
        now: function() {
            return +new Date
        },
        support: l
    }), "function" == typeof Symbol && (n.fn[Symbol.iterator] = c[Symbol.iterator]), n.each("Boolean Number String Function Array Date RegExp Object Error Symbol".split(" "), function(a, b) {
        i["[object " + b + "]"] = b.toLowerCase()
    });

    function s(a) {
        var b = !!a && "length" in a && a.length,
            c = n.type(a);
        return "function" === c || n.isWindow(a) ? !1 : "array" === c || 0 === b || "number" == typeof b && b > 0 && b - 1 in a
    }
    var t = function(a) {
        var b, c, d, e, f, g, h, i, j, k, l, m, n, o, p, q, r, s, t, u = "sizzle" + 1 * new Date,
            v = a.document,
            w = 0,
            x = 0,
            y = ga(),
            z = ga(),
            A = ga(),
            B = function(a, b) {
                return a === b && (l = !0), 0
            },
            C = 1 << 31,
            D = {}.hasOwnProperty,
            E = [],
            F = E.pop,
            G = E.push,
            H = E.push,
            I = E.slice,
            J = function(a, b) {
                for (var c = 0, d = a.length; d > c; c++)
                    if (a[c] === b) return c;
                return -1
            },
            K = "checked|selected|async|autofocus|autoplay|controls|defer|disabled|hidden|ismap|loop|multiple|open|readonly|required|scoped",
            L = "[\\x20\\t\\r\\n\\f]",
            M = "(?:\\\\.|[\\w-]|[^\\x00-\\xa0])+",
            N = "\\[" + L + "*(" + M + ")(?:" + L + "*([*^$|!~]?=)" + L + "*(?:'((?:\\\\.|[^\\\\'])*)'|\"((?:\\\\.|[^\\\\\"])*)\"|(" + M + "))|)" + L + "*\\]",
            O = ":(" + M + ")(?:\\((('((?:\\\\.|[^\\\\'])*)'|\"((?:\\\\.|[^\\\\\"])*)\")|((?:\\\\.|[^\\\\()[\\]]|" + N + ")*)|.*)\\)|)",
            P = new RegExp(L + "+", "g"),
            Q = new RegExp("^" + L + "+|((?:^|[^\\\\])(?:\\\\.)*)" + L + "+$", "g"),
            R = new RegExp("^" + L + "*," + L + "*"),
            S = new RegExp("^" + L + "*([>+~]|" + L + ")" + L + "*"),
            T = new RegExp("=" + L + "*([^\\]'\"]*?)" + L + "*\\]", "g"),
            U = new RegExp(O),
            V = new RegExp("^" + M + "$"),
            W = {
                ID: new RegExp("^#(" + M + ")"),
                CLASS: new RegExp("^\\.(" + M + ")"),
                TAG: new RegExp("^(" + M + "|[*])"),
                ATTR: new RegExp("^" + N),
                PSEUDO: new RegExp("^" + O),
                CHILD: new RegExp("^:(only|first|last|nth|nth-last)-(child|of-type)(?:\\(" + L + "*(even|odd|(([+-]|)(\\d*)n|)" + L + "*(?:([+-]|)" + L + "*(\\d+)|))" + L + "*\\)|)", "i"),
                bool: new RegExp("^(?:" + K + ")$", "i"),
                needsContext: new RegExp("^" + L + "*[>+~]|:(even|odd|eq|gt|lt|nth|first|last)(?:\\(" + L + "*((?:-\\d)?\\d*)" + L + "*\\)|)(?=[^-]|$)", "i")
            },
            X = /^(?:input|select|textarea|button)$/i,
            Y = /^h\d$/i,
            Z = /^[^{]+\{\s*\[native \w/,
            $ = /^(?:#([\w-]+)|(\w+)|\.([\w-]+))$/,
            _ = /[+~]/,
            aa = /'|\\/g,
            ba = new RegExp("\\\\([\\da-f]{1,6}" + L + "?|(" + L + ")|.)", "ig"),
            ca = function(a, b, c) {
                var d = "0x" + b - 65536;
                return d !== d || c ? b : 0 > d ? String.fromCharCode(d + 65536) : String.fromCharCode(d >> 10 | 55296, 1023 & d | 56320)
            },
            da = function() {
                m()
            };
        try {
            H.apply(E = I.call(v.childNodes), v.childNodes), E[v.childNodes.length].nodeType
        } catch (ea) {
            H = {
                apply: E.length ? function(a, b) {
                    G.apply(a, I.call(b))
                } : function(a, b) {
                    var c = a.length,
                        d = 0;
                    while (a[c++] = b[d++]);
                    a.length = c - 1
                }
            }
        }

        function fa(a, b, d, e) {
            var f, h, j, k, l, o, r, s, w = b && b.ownerDocument,
                x = b ? b.nodeType : 9;
            if (d = d || [], "string" != typeof a || !a || 1 !== x && 9 !== x && 11 !== x) return d;
            if (!e && ((b ? b.ownerDocument || b : v) !== n && m(b), b = b || n, p)) {
                if (11 !== x && (o = $.exec(a)))
                    if (f = o[1]) {
                        if (9 === x) {
                            if (!(j = b.getElementById(f))) return d;
                            if (j.id === f) return d.push(j), d
                        } else if (w && (j = w.getElementById(f)) && t(b, j) && j.id === f) return d.push(j), d
                    } else {
                        if (o[2]) return H.apply(d, b.getElementsByTagName(a)), d;
                        if ((f = o[3]) && c.getElementsByClassName && b.getElementsByClassName) return H.apply(d, b.getElementsByClassName(f)), d
                    }
                if (c.qsa && !A[a + " "] && (!q || !q.test(a))) {
                    if (1 !== x) w = b, s = a;
                    else if ("object" !== b.nodeName.toLowerCase()) {
                        (k = b.getAttribute("id")) ? k = k.replace(aa, "\\$&"): b.setAttribute("id", k = u), r = g(a), h = r.length, l = V.test(k) ? "#" + k : "[id='" + k + "']";
                        while (h--) r[h] = l + " " + qa(r[h]);
                        s = r.join(","), w = _.test(a) && oa(b.parentNode) || b
                    }
                    if (s) try {
                        return H.apply(d, w.querySelectorAll(s)), d
                    } catch (y) {} finally {
                        k === u && b.removeAttribute("id")
                    }
                }
            }
            return i(a.replace(Q, "$1"), b, d, e)
        }

        function ga() {
            var a = [];

            function b(c, e) {
                return a.push(c + " ") > d.cacheLength && delete b[a.shift()], b[c + " "] = e
            }
            return b
        }

        function ha(a) {
            return a[u] = !0, a
        }

        function ia(a) {
            var b = n.createElement("div");
            try {
                return !!a(b)
            } catch (c) {
                return !1
            } finally {
                b.parentNode && b.parentNode.removeChild(b), b = null
            }
        }

        function ja(a, b) {
            var c = a.split("|"),
                e = c.length;
            while (e--) d.attrHandle[c[e]] = b
        }

        function ka(a, b) {
            var c = b && a,
                d = c && 1 === a.nodeType && 1 === b.nodeType && (~b.sourceIndex || C) - (~a.sourceIndex || C);
            if (d) return d;
            if (c)
                while (c = c.nextSibling)
                    if (c === b) return -1;
            return a ? 1 : -1
        }

        function la(a) {
            return function(b) {
                var c = b.nodeName.toLowerCase();
                return "input" === c && b.type === a
            }
        }

        function ma(a) {
            return function(b) {
                var c = b.nodeName.toLowerCase();
                return ("input" === c || "button" === c) && b.type === a
            }
        }

        function na(a) {
            return ha(function(b) {
                return b = +b, ha(function(c, d) {
                    var e, f = a([], c.length, b),
                        g = f.length;
                    while (g--) c[e = f[g]] && (c[e] = !(d[e] = c[e]))
                })
            })
        }

        function oa(a) {
            return a && "undefined" != typeof a.getElementsByTagName && a
        }
        c = fa.support = {}, f = fa.isXML = function(a) {
            var b = a && (a.ownerDocument || a).documentElement;
            return b ? "HTML" !== b.nodeName : !1
        }, m = fa.setDocument = function(a) {
            var b, e, g = a ? a.ownerDocument || a : v;
            return g !== n && 9 === g.nodeType && g.documentElement ? (n = g, o = n.documentElement, p = !f(n), (e = n.defaultView) && e.top !== e && (e.addEventListener ? e.addEventListener("unload", da, !1) : e.attachEvent && e.attachEvent("onunload", da)), c.attributes = ia(function(a) {
                return a.className = "i", !a.getAttribute("className")
            }), c.getElementsByTagName = ia(function(a) {
                return a.appendChild(n.createComment("")), !a.getElementsByTagName("*").length
            }), c.getElementsByClassName = Z.test(n.getElementsByClassName), c.getById = ia(function(a) {
                return o.appendChild(a).id = u, !n.getElementsByName || !n.getElementsByName(u).length
            }), c.getById ? (d.find.ID = function(a, b) {
                if ("undefined" != typeof b.getElementById && p) {
                    var c = b.getElementById(a);
                    return c ? [c] : []
                }
            }, d.filter.ID = function(a) {
                var b = a.replace(ba, ca);
                return function(a) {
                    return a.getAttribute("id") === b
                }
            }) : (delete d.find.ID, d.filter.ID = function(a) {
                var b = a.replace(ba, ca);
                return function(a) {
                    var c = "undefined" != typeof a.getAttributeNode && a.getAttributeNode("id");
                    return c && c.value === b
                }
            }), d.find.TAG = c.getElementsByTagName ? function(a, b) {
                return "undefined" != typeof b.getElementsByTagName ? b.getElementsByTagName(a) : c.qsa ? b.querySelectorAll(a) : void 0
            } : function(a, b) {
                var c, d = [],
                    e = 0,
                    f = b.getElementsByTagName(a);
                if ("*" === a) {
                    while (c = f[e++]) 1 === c.nodeType && d.push(c);
                    return d
                }
                return f
            }, d.find.CLASS = c.getElementsByClassName && function(a, b) {
                    return "undefined" != typeof b.getElementsByClassName && p ? b.getElementsByClassName(a) : void 0
                }, r = [], q = [], (c.qsa = Z.test(n.querySelectorAll)) && (ia(function(a) {
                o.appendChild(a).innerHTML = "<a id='" + u + "'></a><select id='" + u + "-\r\\' msallowcapture=''><option selected=''></option></select>", a.querySelectorAll("[msallowcapture^='']").length && q.push("[*^$]=" + L + "*(?:''|\"\")"), a.querySelectorAll("[selected]").length || q.push("\\[" + L + "*(?:value|" + K + ")"), a.querySelectorAll("[id~=" + u + "-]").length || q.push("~="), a.querySelectorAll(":checked").length || q.push(":checked"), a.querySelectorAll("a#" + u + "+*").length || q.push(".#.+[+~]")
            }), ia(function(a) {
                var b = n.createElement("input");
                b.setAttribute("type", "hidden"), a.appendChild(b).setAttribute("name", "D"), a.querySelectorAll("[name=d]").length && q.push("name" + L + "*[*^$|!~]?="), a.querySelectorAll(":enabled").length || q.push(":enabled", ":disabled"), a.querySelectorAll("*,:x"), q.push(",.*:")
            })), (c.matchesSelector = Z.test(s = o.matches || o.webkitMatchesSelector || o.mozMatchesSelector || o.oMatchesSelector || o.msMatchesSelector)) && ia(function(a) {
                c.disconnectedMatch = s.call(a, "div"), s.call(a, "[s!='']:x"), r.push("!=", O)
            }), q = q.length && new RegExp(q.join("|")), r = r.length && new RegExp(r.join("|")), b = Z.test(o.compareDocumentPosition), t = b || Z.test(o.contains) ? function(a, b) {
                var c = 9 === a.nodeType ? a.documentElement : a,
                    d = b && b.parentNode;
                return a === d || !(!d || 1 !== d.nodeType || !(c.contains ? c.contains(d) : a.compareDocumentPosition && 16 & a.compareDocumentPosition(d)))
            } : function(a, b) {
                if (b)
                    while (b = b.parentNode)
                        if (b === a) return !0;
                return !1
            }, B = b ? function(a, b) {
                if (a === b) return l = !0, 0;
                var d = !a.compareDocumentPosition - !b.compareDocumentPosition;
                return d ? d : (d = (a.ownerDocument || a) === (b.ownerDocument || b) ? a.compareDocumentPosition(b) : 1, 1 & d || !c.sortDetached && b.compareDocumentPosition(a) === d ? a === n || a.ownerDocument === v && t(v, a) ? -1 : b === n || b.ownerDocument === v && t(v, b) ? 1 : k ? J(k, a) - J(k, b) : 0 : 4 & d ? -1 : 1)
            } : function(a, b) {
                if (a === b) return l = !0, 0;
                var c, d = 0,
                    e = a.parentNode,
                    f = b.parentNode,
                    g = [a],
                    h = [b];
                if (!e || !f) return a === n ? -1 : b === n ? 1 : e ? -1 : f ? 1 : k ? J(k, a) - J(k, b) : 0;
                if (e === f) return ka(a, b);
                c = a;
                while (c = c.parentNode) g.unshift(c);
                c = b;
                while (c = c.parentNode) h.unshift(c);
                while (g[d] === h[d]) d++;
                return d ? ka(g[d], h[d]) : g[d] === v ? -1 : h[d] === v ? 1 : 0
            }, n) : n
        }, fa.matches = function(a, b) {
            return fa(a, null, null, b)
        }, fa.matchesSelector = function(a, b) {
            if ((a.ownerDocument || a) !== n && m(a), b = b.replace(T, "='$1']"), c.matchesSelector && p && !A[b + " "] && (!r || !r.test(b)) && (!q || !q.test(b))) try {
                var d = s.call(a, b);
                if (d || c.disconnectedMatch || a.document && 11 !== a.document.nodeType) return d
            } catch (e) {}
            return fa(b, n, null, [a]).length > 0
        }, fa.contains = function(a, b) {
            return (a.ownerDocument || a) !== n && m(a), t(a, b)
        }, fa.attr = function(a, b) {
            (a.ownerDocument || a) !== n && m(a);
            var e = d.attrHandle[b.toLowerCase()],
                f = e && D.call(d.attrHandle, b.toLowerCase()) ? e(a, b, !p) : void 0;
            return void 0 !== f ? f : c.attributes || !p ? a.getAttribute(b) : (f = a.getAttributeNode(b)) && f.specified ? f.value : null
        }, fa.error = function(a) {
            throw new Error("Syntax error, unrecognized expression: " + a)
        }, fa.uniqueSort = function(a) {
            var b, d = [],
                e = 0,
                f = 0;
            if (l = !c.detectDuplicates, k = !c.sortStable && a.slice(0), a.sort(B), l) {
                while (b = a[f++]) b === a[f] && (e = d.push(f));
                while (e--) a.splice(d[e], 1)
            }
            return k = null, a
        }, e = fa.getText = function(a) {
            var b, c = "",
                d = 0,
                f = a.nodeType;
            if (f) {
                if (1 === f || 9 === f || 11 === f) {
                    if ("string" == typeof a.textContent) return a.textContent;
                    for (a = a.firstChild; a; a = a.nextSibling) c += e(a)
                } else if (3 === f || 4 === f) return a.nodeValue
            } else
                while (b = a[d++]) c += e(b);
            return c
        }, d = fa.selectors = {
            cacheLength: 50,
            createPseudo: ha,
            match: W,
            attrHandle: {},
            find: {},
            relative: {
                ">": {
                    dir: "parentNode",
                    first: !0
                },
                " ": {
                    dir: "parentNode"
                },
                "+": {
                    dir: "previousSibling",
                    first: !0
                },
                "~": {
                    dir: "previousSibling"
                }
            },
            preFilter: {
                ATTR: function(a) {
                    return a[1] = a[1].replace(ba, ca), a[3] = (a[3] || a[4] || a[5] || "").replace(ba, ca), "~=" === a[2] && (a[3] = " " + a[3] + " "), a.slice(0, 4)
                },
                CHILD: function(a) {
                    return a[1] = a[1].toLowerCase(), "nth" === a[1].slice(0, 3) ? (a[3] || fa.error(a[0]), a[4] = +(a[4] ? a[5] + (a[6] || 1) : 2 * ("even" === a[3] || "odd" === a[3])), a[5] = +(a[7] + a[8] || "odd" === a[3])) : a[3] && fa.error(a[0]), a
                },
                PSEUDO: function(a) {
                    var b, c = !a[6] && a[2];
                    return W.CHILD.test(a[0]) ? null : (a[3] ? a[2] = a[4] || a[5] || "" : c && U.test(c) && (b = g(c, !0)) && (b = c.indexOf(")", c.length - b) - c.length) && (a[0] = a[0].slice(0, b), a[2] = c.slice(0, b)), a.slice(0, 3))
                }
            },
            filter: {
                TAG: function(a) {
                    var b = a.replace(ba, ca).toLowerCase();
                    return "*" === a ? function() {
                        return !0
                    } : function(a) {
                        return a.nodeName && a.nodeName.toLowerCase() === b
                    }
                },
                CLASS: function(a) {
                    var b = y[a + " "];
                    return b || (b = new RegExp("(^|" + L + ")" + a + "(" + L + "|$)")) && y(a, function(a) {
                            return b.test("string" == typeof a.className && a.className || "undefined" != typeof a.getAttribute && a.getAttribute("class") || "")
                        })
                },
                ATTR: function(a, b, c) {
                    return function(d) {
                        var e = fa.attr(d, a);
                        return null == e ? "!=" === b : b ? (e += "", "=" === b ? e === c : "!=" === b ? e !== c : "^=" === b ? c && 0 === e.indexOf(c) : "*=" === b ? c && e.indexOf(c) > -1 : "$=" === b ? c && e.slice(-c.length) === c : "~=" === b ? (" " + e.replace(P, " ") + " ").indexOf(c) > -1 : "|=" === b ? e === c || e.slice(0, c.length + 1) === c + "-" : !1) : !0
                    }
                },
                CHILD: function(a, b, c, d, e) {
                    var f = "nth" !== a.slice(0, 3),
                        g = "last" !== a.slice(-4),
                        h = "of-type" === b;
                    return 1 === d && 0 === e ? function(a) {
                        return !!a.parentNode
                    } : function(b, c, i) {
                        var j, k, l, m, n, o, p = f !== g ? "nextSibling" : "previousSibling",
                            q = b.parentNode,
                            r = h && b.nodeName.toLowerCase(),
                            s = !i && !h,
                            t = !1;
                        if (q) {
                            if (f) {
                                while (p) {
                                    m = b;
                                    while (m = m[p])
                                        if (h ? m.nodeName.toLowerCase() === r : 1 === m.nodeType) return !1;
                                    o = p = "only" === a && !o && "nextSibling"
                                }
                                return !0
                            }
                            if (o = [g ? q.firstChild : q.lastChild], g && s) {
                                m = q, l = m[u] || (m[u] = {}), k = l[m.uniqueID] || (l[m.uniqueID] = {}), j = k[a] || [], n = j[0] === w && j[1], t = n && j[2], m = n && q.childNodes[n];
                                while (m = ++n && m && m[p] || (t = n = 0) || o.pop())
                                    if (1 === m.nodeType && ++t && m === b) {
                                        k[a] = [w, n, t];
                                        break
                                    }
                            } else if (s && (m = b, l = m[u] || (m[u] = {}), k = l[m.uniqueID] || (l[m.uniqueID] = {}), j = k[a] || [], n = j[0] === w && j[1], t = n), t === !1)
                                while (m = ++n && m && m[p] || (t = n = 0) || o.pop())
                                    if ((h ? m.nodeName.toLowerCase() === r : 1 === m.nodeType) && ++t && (s && (l = m[u] || (m[u] = {}), k = l[m.uniqueID] || (l[m.uniqueID] = {}), k[a] = [w, t]), m === b)) break;
                            return t -= e, t === d || t % d === 0 && t / d >= 0
                        }
                    }
                },
                PSEUDO: function(a, b) {
                    var c, e = d.pseudos[a] || d.setFilters[a.toLowerCase()] || fa.error("unsupported pseudo: " + a);
                    return e[u] ? e(b) : e.length > 1 ? (c = [a, a, "", b], d.setFilters.hasOwnProperty(a.toLowerCase()) ? ha(function(a, c) {
                        var d, f = e(a, b),
                            g = f.length;
                        while (g--) d = J(a, f[g]), a[d] = !(c[d] = f[g])
                    }) : function(a) {
                        return e(a, 0, c)
                    }) : e
                }
            },
            pseudos: {
                not: ha(function(a) {
                    var b = [],
                        c = [],
                        d = h(a.replace(Q, "$1"));
                    return d[u] ? ha(function(a, b, c, e) {
                        var f, g = d(a, null, e, []),
                            h = a.length;
                        while (h--)(f = g[h]) && (a[h] = !(b[h] = f))
                    }) : function(a, e, f) {
                        return b[0] = a, d(b, null, f, c), b[0] = null, !c.pop()
                    }
                }),
                has: ha(function(a) {
                    return function(b) {
                        return fa(a, b).length > 0
                    }
                }),
                contains: ha(function(a) {
                    return a = a.replace(ba, ca),
                        function(b) {
                            return (b.textContent || b.innerText || e(b)).indexOf(a) > -1
                        }
                }),
                lang: ha(function(a) {
                    return V.test(a || "") || fa.error("unsupported lang: " + a), a = a.replace(ba, ca).toLowerCase(),
                        function(b) {
                            var c;
                            do
                                if (c = p ? b.lang : b.getAttribute("xml:lang") || b.getAttribute("lang")) return c = c.toLowerCase(), c === a || 0 === c.indexOf(a + "-");
                            while ((b = b.parentNode) && 1 === b.nodeType);
                            return !1
                        }
                }),
                target: function(b) {
                    var c = a.location && a.location.hash;
                    return c && c.slice(1) === b.id
                },
                root: function(a) {
                    return a === o
                },
                focus: function(a) {
                    return a === n.activeElement && (!n.hasFocus || n.hasFocus()) && !!(a.type || a.href || ~a.tabIndex)
                },
                enabled: function(a) {
                    return a.disabled === !1
                },
                disabled: function(a) {
                    return a.disabled === !0
                },
                checked: function(a) {
                    var b = a.nodeName.toLowerCase();
                    return "input" === b && !!a.checked || "option" === b && !!a.selected
                },
                selected: function(a) {
                    return a.parentNode && a.parentNode.selectedIndex, a.selected === !0
                },
                empty: function(a) {
                    for (a = a.firstChild; a; a = a.nextSibling)
                        if (a.nodeType < 6) return !1;
                    return !0
                },
                parent: function(a) {
                    return !d.pseudos.empty(a)
                },
                header: function(a) {
                    return Y.test(a.nodeName)
                },
                input: function(a) {
                    return X.test(a.nodeName)
                },
                button: function(a) {
                    var b = a.nodeName.toLowerCase();
                    return "input" === b && "button" === a.type || "button" === b
                },
                text: function(a) {
                    var b;
                    return "input" === a.nodeName.toLowerCase() && "text" === a.type && (null == (b = a.getAttribute("type")) || "text" === b.toLowerCase())
                },
                first: na(function() {
                    return [0]
                }),
                last: na(function(a, b) {
                    return [b - 1]
                }),
                eq: na(function(a, b, c) {
                    return [0 > c ? c + b : c]
                }),
                even: na(function(a, b) {
                    for (var c = 0; b > c; c += 2) a.push(c);
                    return a
                }),
                odd: na(function(a, b) {
                    for (var c = 1; b > c; c += 2) a.push(c);
                    return a
                }),
                lt: na(function(a, b, c) {
                    for (var d = 0 > c ? c + b : c; --d >= 0;) a.push(d);
                    return a
                }),
                gt: na(function(a, b, c) {
                    for (var d = 0 > c ? c + b : c; ++d < b;) a.push(d);
                    return a
                })
            }
        }, d.pseudos.nth = d.pseudos.eq;
        for (b in {
            radio: !0,
            checkbox: !0,
            file: !0,
            password: !0,
            image: !0
        }) d.pseudos[b] = la(b);
        for (b in {
            submit: !0,
            reset: !0
        }) d.pseudos[b] = ma(b);

        function pa() {}
        pa.prototype = d.filters = d.pseudos, d.setFilters = new pa, g = fa.tokenize = function(a, b) {
            var c, e, f, g, h, i, j, k = z[a + " "];
            if (k) return b ? 0 : k.slice(0);
            h = a, i = [], j = d.preFilter;
            while (h) {
                c && !(e = R.exec(h)) || (e && (h = h.slice(e[0].length) || h), i.push(f = [])), c = !1, (e = S.exec(h)) && (c = e.shift(), f.push({
                    value: c,
                    type: e[0].replace(Q, " ")
                }), h = h.slice(c.length));
                for (g in d.filter) !(e = W[g].exec(h)) || j[g] && !(e = j[g](e)) || (c = e.shift(), f.push({
                    value: c,
                    type: g,
                    matches: e
                }), h = h.slice(c.length));
                if (!c) break
            }
            return b ? h.length : h ? fa.error(a) : z(a, i).slice(0)
        };

        function qa(a) {
            for (var b = 0, c = a.length, d = ""; c > b; b++) d += a[b].value;
            return d
        }

        function ra(a, b, c) {
            var d = b.dir,
                e = c && "parentNode" === d,
                f = x++;
            return b.first ? function(b, c, f) {
                while (b = b[d])
                    if (1 === b.nodeType || e) return a(b, c, f)
            } : function(b, c, g) {
                var h, i, j, k = [w, f];
                if (g) {
                    while (b = b[d])
                        if ((1 === b.nodeType || e) && a(b, c, g)) return !0
                } else
                    while (b = b[d])
                        if (1 === b.nodeType || e) {
                            if (j = b[u] || (b[u] = {}), i = j[b.uniqueID] || (j[b.uniqueID] = {}), (h = i[d]) && h[0] === w && h[1] === f) return k[2] = h[2];
                            if (i[d] = k, k[2] = a(b, c, g)) return !0
                        }
            }
        }

        function sa(a) {
            return a.length > 1 ? function(b, c, d) {
                var e = a.length;
                while (e--)
                    if (!a[e](b, c, d)) return !1;
                return !0
            } : a[0]
        }

        function ta(a, b, c) {
            for (var d = 0, e = b.length; e > d; d++) fa(a, b[d], c);
            return c
        }

        function ua(a, b, c, d, e) {
            for (var f, g = [], h = 0, i = a.length, j = null != b; i > h; h++)(f = a[h]) && (c && !c(f, d, e) || (g.push(f), j && b.push(h)));
            return g
        }

        function va(a, b, c, d, e, f) {
            return d && !d[u] && (d = va(d)), e && !e[u] && (e = va(e, f)), ha(function(f, g, h, i) {
                var j, k, l, m = [],
                    n = [],
                    o = g.length,
                    p = f || ta(b || "*", h.nodeType ? [h] : h, []),
                    q = !a || !f && b ? p : ua(p, m, a, h, i),
                    r = c ? e || (f ? a : o || d) ? [] : g : q;
                if (c && c(q, r, h, i), d) {
                    j = ua(r, n), d(j, [], h, i), k = j.length;
                    while (k--)(l = j[k]) && (r[n[k]] = !(q[n[k]] = l))
                }
                if (f) {
                    if (e || a) {
                        if (e) {
                            j = [], k = r.length;
                            while (k--)(l = r[k]) && j.push(q[k] = l);
                            e(null, r = [], j, i)
                        }
                        k = r.length;
                        while (k--)(l = r[k]) && (j = e ? J(f, l) : m[k]) > -1 && (f[j] = !(g[j] = l))
                    }
                } else r = ua(r === g ? r.splice(o, r.length) : r), e ? e(null, g, r, i) : H.apply(g, r)
            })
        }

        function wa(a) {
            for (var b, c, e, f = a.length, g = d.relative[a[0].type], h = g || d.relative[" "], i = g ? 1 : 0, k = ra(function(a) {
                return a === b
            }, h, !0), l = ra(function(a) {
                return J(b, a) > -1
            }, h, !0), m = [function(a, c, d) {
                var e = !g && (d || c !== j) || ((b = c).nodeType ? k(a, c, d) : l(a, c, d));
                return b = null, e
            }]; f > i; i++)
                if (c = d.relative[a[i].type]) m = [ra(sa(m), c)];
                else {
                    if (c = d.filter[a[i].type].apply(null, a[i].matches), c[u]) {
                        for (e = ++i; f > e; e++)
                            if (d.relative[a[e].type]) break;
                        return va(i > 1 && sa(m), i > 1 && qa(a.slice(0, i - 1).concat({
                                value: " " === a[i - 2].type ? "*" : ""
                            })).replace(Q, "$1"), c, e > i && wa(a.slice(i, e)), f > e && wa(a = a.slice(e)), f > e && qa(a))
                    }
                    m.push(c)
                }
            return sa(m)
        }

        function xa(a, b) {
            var c = b.length > 0,
                e = a.length > 0,
                f = function(f, g, h, i, k) {
                    var l, o, q, r = 0,
                        s = "0",
                        t = f && [],
                        u = [],
                        v = j,
                        x = f || e && d.find.TAG("*", k),
                        y = w += null == v ? 1 : Math.random() || .1,
                        z = x.length;
                    for (k && (j = g === n || g || k); s !== z && null != (l = x[s]); s++) {
                        if (e && l) {
                            o = 0, g || l.ownerDocument === n || (m(l), h = !p);
                            while (q = a[o++])
                                if (q(l, g || n, h)) {
                                    i.push(l);
                                    break
                                }
                            k && (w = y)
                        }
                        c && ((l = !q && l) && r--, f && t.push(l))
                    }
                    if (r += s, c && s !== r) {
                        o = 0;
                        while (q = b[o++]) q(t, u, g, h);
                        if (f) {
                            if (r > 0)
                                while (s--) t[s] || u[s] || (u[s] = F.call(i));
                            u = ua(u)
                        }
                        H.apply(i, u), k && !f && u.length > 0 && r + b.length > 1 && fa.uniqueSort(i)
                    }
                    return k && (w = y, j = v), t
                };
            return c ? ha(f) : f
        }
        return h = fa.compile = function(a, b) {
            var c, d = [],
                e = [],
                f = A[a + " "];
            if (!f) {
                b || (b = g(a)), c = b.length;
                while (c--) f = wa(b[c]), f[u] ? d.push(f) : e.push(f);
                f = A(a, xa(e, d)), f.selector = a
            }
            return f
        }, i = fa.select = function(a, b, e, f) {
            var i, j, k, l, m, n = "function" == typeof a && a,
                o = !f && g(a = n.selector || a);
            if (e = e || [], 1 === o.length) {
                if (j = o[0] = o[0].slice(0), j.length > 2 && "ID" === (k = j[0]).type && c.getById && 9 === b.nodeType && p && d.relative[j[1].type]) {
                    if (b = (d.find.ID(k.matches[0].replace(ba, ca), b) || [])[0], !b) return e;
                    n && (b = b.parentNode), a = a.slice(j.shift().value.length)
                }
                i = W.needsContext.test(a) ? 0 : j.length;
                while (i--) {
                    if (k = j[i], d.relative[l = k.type]) break;
                    if ((m = d.find[l]) && (f = m(k.matches[0].replace(ba, ca), _.test(j[0].type) && oa(b.parentNode) || b))) {
                        if (j.splice(i, 1), a = f.length && qa(j), !a) return H.apply(e, f), e;
                        break
                    }
                }
            }
            return (n || h(a, o))(f, b, !p, e, !b || _.test(a) && oa(b.parentNode) || b), e
        }, c.sortStable = u.split("").sort(B).join("") === u, c.detectDuplicates = !!l, m(), c.sortDetached = ia(function(a) {
            return 1 & a.compareDocumentPosition(n.createElement("div"))
        }), ia(function(a) {
            return a.innerHTML = "<a href='#'></a>", "#" === a.firstChild.getAttribute("href")
        }) || ja("type|href|height|width", function(a, b, c) {
            return c ? void 0 : a.getAttribute(b, "type" === b.toLowerCase() ? 1 : 2)
        }), c.attributes && ia(function(a) {
            return a.innerHTML = "<input/>", a.firstChild.setAttribute("value", ""), "" === a.firstChild.getAttribute("value")
        }) || ja("value", function(a, b, c) {
            return c || "input" !== a.nodeName.toLowerCase() ? void 0 : a.defaultValue
        }), ia(function(a) {
            return null == a.getAttribute("disabled")
        }) || ja(K, function(a, b, c) {
            var d;
            return c ? void 0 : a[b] === !0 ? b.toLowerCase() : (d = a.getAttributeNode(b)) && d.specified ? d.value : null
        }), fa
    }(a);
    n.find = t, n.expr = t.selectors, n.expr[":"] = n.expr.pseudos, n.uniqueSort = n.unique = t.uniqueSort, n.text = t.getText, n.isXMLDoc = t.isXML, n.contains = t.contains;
    var u = function(a, b, c) {
            var d = [],
                e = void 0 !== c;
            while ((a = a[b]) && 9 !== a.nodeType)
                if (1 === a.nodeType) {
                    if (e && n(a).is(c)) break;
                    d.push(a)
                }
            return d
        },
        v = function(a, b) {
            for (var c = []; a; a = a.nextSibling) 1 === a.nodeType && a !== b && c.push(a);
            return c
        },
        w = n.expr.match.needsContext,
        x = /^<([\w-]+)\s*\/?>(?:<\/\1>|)$/,
        y = /^.[^:#\[\.,]*$/;

    function z(a, b, c) {
        if (n.isFunction(b)) return n.grep(a, function(a, d) {
            return !!b.call(a, d, a) !== c
        });
        if (b.nodeType) return n.grep(a, function(a) {
            return a === b !== c
        });
        if ("string" == typeof b) {
            if (y.test(b)) return n.filter(b, a, c);
            b = n.filter(b, a)
        }
        return n.grep(a, function(a) {
            return n.inArray(a, b) > -1 !== c
        })
    }
    n.filter = function(a, b, c) {
        var d = b[0];
        return c && (a = ":not(" + a + ")"), 1 === b.length && 1 === d.nodeType ? n.find.matchesSelector(d, a) ? [d] : [] : n.find.matches(a, n.grep(b, function(a) {
            return 1 === a.nodeType
        }))
    }, n.fn.extend({
        find: function(a) {
            var b, c = [],
                d = this,
                e = d.length;
            if ("string" != typeof a) return this.pushStack(n(a).filter(function() {
                for (b = 0; e > b; b++)
                    if (n.contains(d[b], this)) return !0
            }));
            for (b = 0; e > b; b++) n.find(a, d[b], c);
            return c = this.pushStack(e > 1 ? n.unique(c) : c), c.selector = this.selector ? this.selector + " " + a : a, c
        },
        filter: function(a) {
            return this.pushStack(z(this, a || [], !1))
        },
        not: function(a) {
            return this.pushStack(z(this, a || [], !0))
        },
        is: function(a) {
            return !!z(this, "string" == typeof a && w.test(a) ? n(a) : a || [], !1).length
        }
    });
    var A, B = /^(?:\s*(<[\w\W]+>)[^>]*|#([\w-]*))$/,
        C = n.fn.init = function(a, b, c) {
            var e, f;
            if (!a) return this;
            if (c = c || A, "string" == typeof a) {
                if (e = "<" === a.charAt(0) && ">" === a.charAt(a.length - 1) && a.length >= 3 ? [null, a, null] : B.exec(a), !e || !e[1] && b) return !b || b.jquery ? (b || c).find(a) : this.constructor(b).find(a);
                if (e[1]) {
                    if (b = b instanceof n ? b[0] : b, n.merge(this, n.parseHTML(e[1], b && b.nodeType ? b.ownerDocument || b : d, !0)), x.test(e[1]) && n.isPlainObject(b))
                        for (e in b) n.isFunction(this[e]) ? this[e](b[e]) : this.attr(e, b[e]);
                    return this
                }
                if (f = d.getElementById(e[2]), f && f.parentNode) {
                    if (f.id !== e[2]) return A.find(a);
                    this.length = 1, this[0] = f
                }
                return this.context = d, this.selector = a, this
            }
            return a.nodeType ? (this.context = this[0] = a, this.length = 1, this) : n.isFunction(a) ? "undefined" != typeof c.ready ? c.ready(a) : a(n) : (void 0 !== a.selector && (this.selector = a.selector, this.context = a.context), n.makeArray(a, this))
        };
    C.prototype = n.fn, A = n(d);
    var D = /^(?:parents|prev(?:Until|All))/,
        E = {
            children: !0,
            contents: !0,
            next: !0,
            prev: !0
        };
    n.fn.extend({
        has: function(a) {
            var b, c = n(a, this),
                d = c.length;
            return this.filter(function() {
                for (b = 0; d > b; b++)
                    if (n.contains(this, c[b])) return !0
            })
        },
        closest: function(a, b) {
            for (var c, d = 0, e = this.length, f = [], g = w.test(a) || "string" != typeof a ? n(a, b || this.context) : 0; e > d; d++)
                for (c = this[d]; c && c !== b; c = c.parentNode)
                    if (c.nodeType < 11 && (g ? g.index(c) > -1 : 1 === c.nodeType && n.find.matchesSelector(c, a))) {
                        f.push(c);
                        break
                    }
            return this.pushStack(f.length > 1 ? n.uniqueSort(f) : f)
        },
        index: function(a) {
            return a ? "string" == typeof a ? n.inArray(this[0], n(a)) : n.inArray(a.jquery ? a[0] : a, this) : this[0] && this[0].parentNode ? this.first().prevAll().length : -1
        },
        add: function(a, b) {
            return this.pushStack(n.uniqueSort(n.merge(this.get(), n(a, b))))
        },
        addBack: function(a) {
            return this.add(null == a ? this.prevObject : this.prevObject.filter(a))
        }
    });

    function F(a, b) {
        do a = a[b]; while (a && 1 !== a.nodeType);
        return a
    }
    n.each({
        parent: function(a) {
            var b = a.parentNode;
            return b && 11 !== b.nodeType ? b : null
        },
        parents: function(a) {
            return u(a, "parentNode")
        },
        parentsUntil: function(a, b, c) {
            return u(a, "parentNode", c)
        },
        next: function(a) {
            return F(a, "nextSibling")
        },
        prev: function(a) {
            return F(a, "previousSibling")
        },
        nextAll: function(a) {
            return u(a, "nextSibling")
        },
        prevAll: function(a) {
            return u(a, "previousSibling")
        },
        nextUntil: function(a, b, c) {
            return u(a, "nextSibling", c)
        },
        prevUntil: function(a, b, c) {
            return u(a, "previousSibling", c)
        },
        siblings: function(a) {
            return v((a.parentNode || {}).firstChild, a)
        },
        children: function(a) {
            return v(a.firstChild)
        },
        contents: function(a) {
            return n.nodeName(a, "iframe") ? a.contentDocument || a.contentWindow.document : n.merge([], a.childNodes)
        }
    }, function(a, b) {
        n.fn[a] = function(c, d) {
            var e = n.map(this, b, c);
            return "Until" !== a.slice(-5) && (d = c), d && "string" == typeof d && (e = n.filter(d, e)), this.length > 1 && (E[a] || (e = n.uniqueSort(e)), D.test(a) && (e = e.reverse())), this.pushStack(e)
        }
    });
    var G = /\S+/g;

    function H(a) {
        var b = {};
        return n.each(a.match(G) || [], function(a, c) {
            b[c] = !0
        }), b
    }
    n.Callbacks = function(a) {
        a = "string" == typeof a ? H(a) : n.extend({}, a);
        var b, c, d, e, f = [],
            g = [],
            h = -1,
            i = function() {
                for (e = a.once, d = b = !0; g.length; h = -1) {
                    c = g.shift();
                    while (++h < f.length) f[h].apply(c[0], c[1]) === !1 && a.stopOnFalse && (h = f.length, c = !1)
                }
                a.memory || (c = !1), b = !1, e && (f = c ? [] : "")
            },
            j = {
                add: function() {
                    return f && (c && !b && (h = f.length - 1, g.push(c)), function d(b) {
                        n.each(b, function(b, c) {
                            n.isFunction(c) ? a.unique && j.has(c) || f.push(c) : c && c.length && "string" !== n.type(c) && d(c)
                        })
                    }(arguments), c && !b && i()), this
                },
                remove: function() {
                    return n.each(arguments, function(a, b) {
                        var c;
                        while ((c = n.inArray(b, f, c)) > -1) f.splice(c, 1), h >= c && h--
                    }), this
                },
                has: function(a) {
                    return a ? n.inArray(a, f) > -1 : f.length > 0
                },
                empty: function() {
                    return f && (f = []), this
                },
                disable: function() {
                    return e = g = [], f = c = "", this
                },
                disabled: function() {
                    return !f
                },
                lock: function() {
                    return e = !0, c || j.disable(), this
                },
                locked: function() {
                    return !!e
                },
                fireWith: function(a, c) {
                    return e || (c = c || [], c = [a, c.slice ? c.slice() : c], g.push(c), b || i()), this
                },
                fire: function() {
                    return j.fireWith(this, arguments), this
                },
                fired: function() {
                    return !!d
                }
            };
        return j
    }, n.extend({
        Deferred: function(a) {
            var b = [
                    ["resolve", "done", n.Callbacks("once memory"), "resolved"],
                    ["reject", "fail", n.Callbacks("once memory"), "rejected"],
                    ["notify", "progress", n.Callbacks("memory")]
                ],
                c = "pending",
                d = {
                    state: function() {
                        return c
                    },
                    always: function() {
                        return e.done(arguments).fail(arguments), this
                    },
                    then: function() {
                        var a = arguments;
                        return n.Deferred(function(c) {
                            n.each(b, function(b, f) {
                                var g = n.isFunction(a[b]) && a[b];
                                e[f[1]](function() {
                                    var a = g && g.apply(this, arguments);
                                    a && n.isFunction(a.promise) ? a.promise().progress(c.notify).done(c.resolve).fail(c.reject) : c[f[0] + "With"](this === d ? c.promise() : this, g ? [a] : arguments)
                                })
                            }), a = null
                        }).promise()
                    },
                    promise: function(a) {
                        return null != a ? n.extend(a, d) : d
                    }
                },
                e = {};
            return d.pipe = d.then, n.each(b, function(a, f) {
                var g = f[2],
                    h = f[3];
                d[f[1]] = g.add, h && g.add(function() {
                    c = h
                }, b[1 ^ a][2].disable, b[2][2].lock), e[f[0]] = function() {
                    return e[f[0] + "With"](this === e ? d : this, arguments), this
                }, e[f[0] + "With"] = g.fireWith
            }), d.promise(e), a && a.call(e, e), e
        },
        when: function(a) {
            var b = 0,
                c = e.call(arguments),
                d = c.length,
                f = 1 !== d || a && n.isFunction(a.promise) ? d : 0,
                g = 1 === f ? a : n.Deferred(),
                h = function(a, b, c) {
                    return function(d) {
                        b[a] = this, c[a] = arguments.length > 1 ? e.call(arguments) : d, c === i ? g.notifyWith(b, c) : --f || g.resolveWith(b, c)
                    }
                },
                i, j, k;
            if (d > 1)
                for (i = new Array(d), j = new Array(d), k = new Array(d); d > b; b++) c[b] && n.isFunction(c[b].promise) ? c[b].promise().progress(h(b, j, i)).done(h(b, k, c)).fail(g.reject) : --f;
            return f || g.resolveWith(k, c), g.promise()
        }
    });
    var I;
    n.fn.ready = function(a) {
        return n.ready.promise().done(a), this
    }, n.extend({
        isReady: !1,
        readyWait: 1,
        holdReady: function(a) {
            a ? n.readyWait++ : n.ready(!0)
        },
        ready: function(a) {
            (a === !0 ? --n.readyWait : n.isReady) || (n.isReady = !0, a !== !0 && --n.readyWait > 0 || (I.resolveWith(d, [n]), n.fn.triggerHandler && (n(d).triggerHandler("ready"), n(d).off("ready"))))
        }
    });

    function J() {
        d.addEventListener ? (d.removeEventListener("DOMContentLoaded", K), a.removeEventListener("load", K)) : (d.detachEvent("onreadystatechange", K), a.detachEvent("onload", K))
    }

    function K() {
        (d.addEventListener || "load" === a.event.type || "complete" === d.readyState) && (J(), n.ready())
    }
    n.ready.promise = function(b) {
        if (!I)
            if (I = n.Deferred(), "complete" === d.readyState || "loading" !== d.readyState && !d.documentElement.doScroll) a.setTimeout(n.ready);
            else if (d.addEventListener) d.addEventListener("DOMContentLoaded", K), a.addEventListener("load", K);
            else {
                d.attachEvent("onreadystatechange", K), a.attachEvent("onload", K);
                var c = !1;
                try {
                    c = null == a.frameElement && d.documentElement
                } catch (e) {}
                c && c.doScroll && ! function f() {
                    if (!n.isReady) {
                        try {
                            c.doScroll("left")
                        } catch (b) {
                            return a.setTimeout(f, 50)
                        }
                        J(), n.ready()
                    }
                }()
            }
        return I.promise(b)
    }, n.ready.promise();
    var L;
    for (L in n(l)) break;
    l.ownFirst = "0" === L, l.inlineBlockNeedsLayout = !1, n(function() {
        var a, b, c, e;
        c = d.getElementsByTagName("body")[0], c && c.style && (b = d.createElement("div"), e = d.createElement("div"), e.style.cssText = "position:absolute;border:0;width:0;height:0;top:0;left:-9999px", c.appendChild(e).appendChild(b), "undefined" != typeof b.style.zoom && (b.style.cssText = "display:inline;margin:0;border:0;padding:1px;width:1px;zoom:1", l.inlineBlockNeedsLayout = a = 3 === b.offsetWidth, a && (c.style.zoom = 1)), c.removeChild(e))
    }),
        function() {
            var a = d.createElement("div");
            l.deleteExpando = !0;
            try {
                delete a.test
            } catch (b) {
                l.deleteExpando = !1
            }
            a = null
        }();
    var M = function(a) {
            var b = n.noData[(a.nodeName + " ").toLowerCase()],
                c = +a.nodeType || 1;
            return 1 !== c && 9 !== c ? !1 : !b || b !== !0 && a.getAttribute("classid") === b
        },
        N = /^(?:\{[\w\W]*\}|\[[\w\W]*\])$/,
        O = /([A-Z])/g;

    function P(a, b, c) {
        if (void 0 === c && 1 === a.nodeType) {
            var d = "data-" + b.replace(O, "-$1").toLowerCase();
            if (c = a.getAttribute(d), "string" == typeof c) {
                try {
                    c = "true" === c ? !0 : "false" === c ? !1 : "null" === c ? null : +c + "" === c ? +c : N.test(c) ? n.parseJSON(c) : c
                } catch (e) {}
                n.data(a, b, c)
            } else c = void 0;
        }
        return c
    }

    function Q(a) {
        var b;
        for (b in a)
            if (("data" !== b || !n.isEmptyObject(a[b])) && "toJSON" !== b) return !1;
        return !0
    }

    function R(a, b, d, e) {
        if (M(a)) {
            var f, g, h = n.expando,
                i = a.nodeType,
                j = i ? n.cache : a,
                k = i ? a[h] : a[h] && h;
            if (k && j[k] && (e || j[k].data) || void 0 !== d || "string" != typeof b) return k || (k = i ? a[h] = c.pop() || n.guid++ : h), j[k] || (j[k] = i ? {} : {
                toJSON: n.noop
            }), "object" != typeof b && "function" != typeof b || (e ? j[k] = n.extend(j[k], b) : j[k].data = n.extend(j[k].data, b)), g = j[k], e || (g.data || (g.data = {}), g = g.data), void 0 !== d && (g[n.camelCase(b)] = d), "string" == typeof b ? (f = g[b], null == f && (f = g[n.camelCase(b)])) : f = g, f
        }
    }

    function S(a, b, c) {
        if (M(a)) {
            var d, e, f = a.nodeType,
                g = f ? n.cache : a,
                h = f ? a[n.expando] : n.expando;
            if (g[h]) {
                if (b && (d = c ? g[h] : g[h].data)) {
                    n.isArray(b) ? b = b.concat(n.map(b, n.camelCase)) : b in d ? b = [b] : (b = n.camelCase(b), b = b in d ? [b] : b.split(" ")), e = b.length;
                    while (e--) delete d[b[e]];
                    if (c ? !Q(d) : !n.isEmptyObject(d)) return
                }(c || (delete g[h].data, Q(g[h]))) && (f ? n.cleanData([a], !0) : l.deleteExpando || g != g.window ? delete g[h] : g[h] = void 0)
            }
        }
    }
    n.extend({
        cache: {},
        noData: {
            "applet ": !0,
            "embed ": !0,
            "object ": "clsid:D27CDB6E-AE6D-11cf-96B8-444553540000"
        },
        hasData: function(a) {
            return a = a.nodeType ? n.cache[a[n.expando]] : a[n.expando], !!a && !Q(a)
        },
        data: function(a, b, c) {
            return R(a, b, c)
        },
        removeData: function(a, b) {
            return S(a, b)
        },
        _data: function(a, b, c) {
            return R(a, b, c, !0)
        },
        _removeData: function(a, b) {
            return S(a, b, !0)
        }
    }), n.fn.extend({
        data: function(a, b) {
            var c, d, e, f = this[0],
                g = f && f.attributes;
            if (void 0 === a) {
                if (this.length && (e = n.data(f), 1 === f.nodeType && !n._data(f, "parsedAttrs"))) {
                    c = g.length;
                    while (c--) g[c] && (d = g[c].name, 0 === d.indexOf("data-") && (d = n.camelCase(d.slice(5)), P(f, d, e[d])));
                    n._data(f, "parsedAttrs", !0)
                }
                return e
            }
            return "object" == typeof a ? this.each(function() {
                n.data(this, a)
            }) : arguments.length > 1 ? this.each(function() {
                n.data(this, a, b)
            }) : f ? P(f, a, n.data(f, a)) : void 0
        },
        removeData: function(a) {
            return this.each(function() {
                n.removeData(this, a)
            })
        }
    }), n.extend({
        queue: function(a, b, c) {
            var d;
            return a ? (b = (b || "fx") + "queue", d = n._data(a, b), c && (!d || n.isArray(c) ? d = n._data(a, b, n.makeArray(c)) : d.push(c)), d || []) : void 0
        },
        dequeue: function(a, b) {
            b = b || "fx";
            var c = n.queue(a, b),
                d = c.length,
                e = c.shift(),
                f = n._queueHooks(a, b),
                g = function() {
                    n.dequeue(a, b)
                };
            "inprogress" === e && (e = c.shift(), d--), e && ("fx" === b && c.unshift("inprogress"), delete f.stop, e.call(a, g, f)), !d && f && f.empty.fire()
        },
        _queueHooks: function(a, b) {
            var c = b + "queueHooks";
            return n._data(a, c) || n._data(a, c, {
                    empty: n.Callbacks("once memory").add(function() {
                        n._removeData(a, b + "queue"), n._removeData(a, c)
                    })
                })
        }
    }), n.fn.extend({
        queue: function(a, b) {
            var c = 2;
            return "string" != typeof a && (b = a, a = "fx", c--), arguments.length < c ? n.queue(this[0], a) : void 0 === b ? this : this.each(function() {
                var c = n.queue(this, a, b);
                n._queueHooks(this, a), "fx" === a && "inprogress" !== c[0] && n.dequeue(this, a)
            })
        },
        dequeue: function(a) {
            return this.each(function() {
                n.dequeue(this, a)
            })
        },
        clearQueue: function(a) {
            return this.queue(a || "fx", [])
        },
        promise: function(a, b) {
            var c, d = 1,
                e = n.Deferred(),
                f = this,
                g = this.length,
                h = function() {
                    --d || e.resolveWith(f, [f])
                };
            "string" != typeof a && (b = a, a = void 0), a = a || "fx";
            while (g--) c = n._data(f[g], a + "queueHooks"), c && c.empty && (d++, c.empty.add(h));
            return h(), e.promise(b)
        }
    }),
        function() {
            var a;
            l.shrinkWrapBlocks = function() {
                if (null != a) return a;
                a = !1;
                var b, c, e;
                return c = d.getElementsByTagName("body")[0], c && c.style ? (b = d.createElement("div"), e = d.createElement("div"), e.style.cssText = "position:absolute;border:0;width:0;height:0;top:0;left:-9999px", c.appendChild(e).appendChild(b), "undefined" != typeof b.style.zoom && (b.style.cssText = "-webkit-box-sizing:content-box;-moz-box-sizing:content-box;box-sizing:content-box;display:block;margin:0;border:0;padding:1px;width:1px;zoom:1", b.appendChild(d.createElement("div")).style.width = "5px", a = 3 !== b.offsetWidth), c.removeChild(e), a) : void 0
            }
        }();
    var T = /[+-]?(?:\d*\.|)\d+(?:[eE][+-]?\d+|)/.source,
        U = new RegExp("^(?:([+-])=|)(" + T + ")([a-z%]*)$", "i"),
        V = ["Top", "Right", "Bottom", "Left"],
        W = function(a, b) {
            return a = b || a, "none" === n.css(a, "display") || !n.contains(a.ownerDocument, a)
        };

    function X(a, b, c, d) {
        var e, f = 1,
            g = 20,
            h = d ? function() {
                return d.cur()
            } : function() {
                return n.css(a, b, "")
            },
            i = h(),
            j = c && c[3] || (n.cssNumber[b] ? "" : "px"),
            k = (n.cssNumber[b] || "px" !== j && +i) && U.exec(n.css(a, b));
        if (k && k[3] !== j) {
            j = j || k[3], c = c || [], k = +i || 1;
            do f = f || ".5", k /= f, n.style(a, b, k + j); while (f !== (f = h() / i) && 1 !== f && --g)
        }
        return c && (k = +k || +i || 0, e = c[1] ? k + (c[1] + 1) * c[2] : +c[2], d && (d.unit = j, d.start = k, d.end = e)), e
    }
    var Y = function(a, b, c, d, e, f, g) {
            var h = 0,
                i = a.length,
                j = null == c;
            if ("object" === n.type(c)) {
                e = !0;
                for (h in c) Y(a, b, h, c[h], !0, f, g)
            } else if (void 0 !== d && (e = !0, n.isFunction(d) || (g = !0), j && (g ? (b.call(a, d), b = null) : (j = b, b = function(a, b, c) {
                    return j.call(n(a), c)
                })), b))
                for (; i > h; h++) b(a[h], c, g ? d : d.call(a[h], h, b(a[h], c)));
            return e ? a : j ? b.call(a) : i ? b(a[0], c) : f
        },
        Z = /^(?:checkbox|radio)$/i,
        $ = /<([\w:-]+)/,
        _ = /^$|\/(?:java|ecma)script/i,
        aa = /^\s+/,
        ba = "abbr|article|aside|audio|bdi|canvas|data|datalist|details|dialog|figcaption|figure|footer|header|hgroup|main|mark|meter|nav|output|picture|progress|section|summary|template|time|video";

    function ca(a) {
        var b = ba.split("|"),
            c = a.createDocumentFragment();
        if (c.createElement)
            while (b.length) c.createElement(b.pop());
        return c
    }! function() {
        var a = d.createElement("div"),
            b = d.createDocumentFragment(),
            c = d.createElement("input");
        a.innerHTML = "  <link/><table></table><a href='/a'>a</a><input type='checkbox'/>", l.leadingWhitespace = 3 === a.firstChild.nodeType, l.tbody = !a.getElementsByTagName("tbody").length, l.htmlSerialize = !!a.getElementsByTagName("link").length, l.html5Clone = "<:nav></:nav>" !== d.createElement("nav").cloneNode(!0).outerHTML, c.type = "checkbox", c.checked = !0, b.appendChild(c), l.appendChecked = c.checked, a.innerHTML = "<textarea>x</textarea>", l.noCloneChecked = !!a.cloneNode(!0).lastChild.defaultValue, b.appendChild(a), c = d.createElement("input"), c.setAttribute("type", "radio"), c.setAttribute("checked", "checked"), c.setAttribute("name", "t"), a.appendChild(c), l.checkClone = a.cloneNode(!0).cloneNode(!0).lastChild.checked, l.noCloneEvent = !!a.addEventListener, a[n.expando] = 1, l.attributes = !a.getAttribute(n.expando)
    }();
    var da = {
        option: [1, "<select multiple='multiple'>", "</select>"],
        legend: [1, "<fieldset>", "</fieldset>"],
        area: [1, "<map>", "</map>"],
        param: [1, "<object>", "</object>"],
        thead: [1, "<table>", "</table>"],
        tr: [2, "<table><tbody>", "</tbody></table>"],
        col: [2, "<table><tbody></tbody><colgroup>", "</colgroup></table>"],
        td: [3, "<table><tbody><tr>", "</tr></tbody></table>"],
        _default: l.htmlSerialize ? [0, "", ""] : [1, "X<div>", "</div>"]
    };
    da.optgroup = da.option, da.tbody = da.tfoot = da.colgroup = da.caption = da.thead, da.th = da.td;

    function ea(a, b) {
        var c, d, e = 0,
            f = "undefined" != typeof a.getElementsByTagName ? a.getElementsByTagName(b || "*") : "undefined" != typeof a.querySelectorAll ? a.querySelectorAll(b || "*") : void 0;
        if (!f)
            for (f = [], c = a.childNodes || a; null != (d = c[e]); e++) !b || n.nodeName(d, b) ? f.push(d) : n.merge(f, ea(d, b));
        return void 0 === b || b && n.nodeName(a, b) ? n.merge([a], f) : f
    }

    function fa(a, b) {
        for (var c, d = 0; null != (c = a[d]); d++) n._data(c, "globalEval", !b || n._data(b[d], "globalEval"))
    }
    var ga = /<|&#?\w+;/,
        ha = /<tbody/i;

    function ia(a) {
        Z.test(a.type) && (a.defaultChecked = a.checked)
    }

    function ja(a, b, c, d, e) {
        for (var f, g, h, i, j, k, m, o = a.length, p = ca(b), q = [], r = 0; o > r; r++)
            if (g = a[r], g || 0 === g)
                if ("object" === n.type(g)) n.merge(q, g.nodeType ? [g] : g);
                else if (ga.test(g)) {
                    i = i || p.appendChild(b.createElement("div")), j = ($.exec(g) || ["", ""])[1].toLowerCase(), m = da[j] || da._default, i.innerHTML = m[1] + n.htmlPrefilter(g) + m[2], f = m[0];
                    while (f--) i = i.lastChild;
                    if (!l.leadingWhitespace && aa.test(g) && q.push(b.createTextNode(aa.exec(g)[0])), !l.tbody) {
                        g = "table" !== j || ha.test(g) ? "<table>" !== m[1] || ha.test(g) ? 0 : i : i.firstChild, f = g && g.childNodes.length;
                        while (f--) n.nodeName(k = g.childNodes[f], "tbody") && !k.childNodes.length && g.removeChild(k)
                    }
                    n.merge(q, i.childNodes), i.textContent = "";
                    while (i.firstChild) i.removeChild(i.firstChild);
                    i = p.lastChild
                } else q.push(b.createTextNode(g));
        i && p.removeChild(i), l.appendChecked || n.grep(ea(q, "input"), ia), r = 0;
        while (g = q[r++])
            if (d && n.inArray(g, d) > -1) e && e.push(g);
            else if (h = n.contains(g.ownerDocument, g), i = ea(p.appendChild(g), "script"), h && fa(i), c) {
                f = 0;
                while (g = i[f++]) _.test(g.type || "") && c.push(g)
            }
        return i = null, p
    }! function() {
        var b, c, e = d.createElement("div");
        for (b in {
            submit: !0,
            change: !0,
            focusin: !0
        }) c = "on" + b, (l[b] = c in a) || (e.setAttribute(c, "t"), l[b] = e.attributes[c].expando === !1);
        e = null
    }();
    var ka = /^(?:input|select|textarea)$/i,
        la = /^key/,
        ma = /^(?:mouse|pointer|contextmenu|drag|drop)|click/,
        na = /^(?:focusinfocus|focusoutblur)$/,
        oa = /^([^.]*)(?:\.(.+)|)/;

    function pa() {
        return !0
    }

    function qa() {
        return !1
    }

    function ra() {
        try {
            return d.activeElement
        } catch (a) {}
    }

    function sa(a, b, c, d, e, f) {
        var g, h;
        if ("object" == typeof b) {
            "string" != typeof c && (d = d || c, c = void 0);
            for (h in b) sa(a, h, c, d, b[h], f);
            return a
        }
        if (null == d && null == e ? (e = c, d = c = void 0) : null == e && ("string" == typeof c ? (e = d, d = void 0) : (e = d, d = c, c = void 0)), e === !1) e = qa;
        else if (!e) return a;
        return 1 === f && (g = e, e = function(a) {
            return n().off(a), g.apply(this, arguments)
        }, e.guid = g.guid || (g.guid = n.guid++)), a.each(function() {
            n.event.add(this, b, e, d, c)
        })
    }
    n.event = {
        global: {},
        add: function(a, b, c, d, e) {
            var f, g, h, i, j, k, l, m, o, p, q, r = n._data(a);
            if (r) {
                c.handler && (i = c, c = i.handler, e = i.selector), c.guid || (c.guid = n.guid++), (g = r.events) || (g = r.events = {}), (k = r.handle) || (k = r.handle = function(a) {
                    return "undefined" == typeof n || a && n.event.triggered === a.type ? void 0 : n.event.dispatch.apply(k.elem, arguments)
                }, k.elem = a), b = (b || "").match(G) || [""], h = b.length;
                while (h--) f = oa.exec(b[h]) || [], o = q = f[1], p = (f[2] || "").split(".").sort(), o && (j = n.event.special[o] || {}, o = (e ? j.delegateType : j.bindType) || o, j = n.event.special[o] || {}, l = n.extend({
                    type: o,
                    origType: q,
                    data: d,
                    handler: c,
                    guid: c.guid,
                    selector: e,
                    needsContext: e && n.expr.match.needsContext.test(e),
                    namespace: p.join(".")
                }, i), (m = g[o]) || (m = g[o] = [], m.delegateCount = 0, j.setup && j.setup.call(a, d, p, k) !== !1 || (a.addEventListener ? a.addEventListener(o, k, !1) : a.attachEvent && a.attachEvent("on" + o, k))), j.add && (j.add.call(a, l), l.handler.guid || (l.handler.guid = c.guid)), e ? m.splice(m.delegateCount++, 0, l) : m.push(l), n.event.global[o] = !0);
                a = null
            }
        },
        remove: function(a, b, c, d, e) {
            var f, g, h, i, j, k, l, m, o, p, q, r = n.hasData(a) && n._data(a);
            if (r && (k = r.events)) {
                b = (b || "").match(G) || [""], j = b.length;
                while (j--)
                    if (h = oa.exec(b[j]) || [], o = q = h[1], p = (h[2] || "").split(".").sort(), o) {
                        l = n.event.special[o] || {}, o = (d ? l.delegateType : l.bindType) || o, m = k[o] || [], h = h[2] && new RegExp("(^|\\.)" + p.join("\\.(?:.*\\.|)") + "(\\.|$)"), i = f = m.length;
                        while (f--) g = m[f], !e && q !== g.origType || c && c.guid !== g.guid || h && !h.test(g.namespace) || d && d !== g.selector && ("**" !== d || !g.selector) || (m.splice(f, 1), g.selector && m.delegateCount--, l.remove && l.remove.call(a, g));
                        i && !m.length && (l.teardown && l.teardown.call(a, p, r.handle) !== !1 || n.removeEvent(a, o, r.handle), delete k[o])
                    } else
                        for (o in k) n.event.remove(a, o + b[j], c, d, !0);
                n.isEmptyObject(k) && (delete r.handle, n._removeData(a, "events"))
            }
        },
        trigger: function(b, c, e, f) {
            var g, h, i, j, l, m, o, p = [e || d],
                q = k.call(b, "type") ? b.type : b,
                r = k.call(b, "namespace") ? b.namespace.split(".") : [];
            if (i = m = e = e || d, 3 !== e.nodeType && 8 !== e.nodeType && !na.test(q + n.event.triggered) && (q.indexOf(".") > -1 && (r = q.split("."), q = r.shift(), r.sort()), h = q.indexOf(":") < 0 && "on" + q, b = b[n.expando] ? b : new n.Event(q, "object" == typeof b && b), b.isTrigger = f ? 2 : 3, b.namespace = r.join("."), b.rnamespace = b.namespace ? new RegExp("(^|\\.)" + r.join("\\.(?:.*\\.|)") + "(\\.|$)") : null, b.result = void 0, b.target || (b.target = e), c = null == c ? [b] : n.makeArray(c, [b]), l = n.event.special[q] || {}, f || !l.trigger || l.trigger.apply(e, c) !== !1)) {
                if (!f && !l.noBubble && !n.isWindow(e)) {
                    for (j = l.delegateType || q, na.test(j + q) || (i = i.parentNode); i; i = i.parentNode) p.push(i), m = i;
                    m === (e.ownerDocument || d) && p.push(m.defaultView || m.parentWindow || a)
                }
                o = 0;
                while ((i = p[o++]) && !b.isPropagationStopped()) b.type = o > 1 ? j : l.bindType || q, g = (n._data(i, "events") || {})[b.type] && n._data(i, "handle"), g && g.apply(i, c), g = h && i[h], g && g.apply && M(i) && (b.result = g.apply(i, c), b.result === !1 && b.preventDefault());
                if (b.type = q, !f && !b.isDefaultPrevented() && (!l._default || l._default.apply(p.pop(), c) === !1) && M(e) && h && e[q] && !n.isWindow(e)) {
                    m = e[h], m && (e[h] = null), n.event.triggered = q;
                    try {
                        e[q]()
                    } catch (s) {}
                    n.event.triggered = void 0, m && (e[h] = m)
                }
                return b.result
            }
        },
        dispatch: function(a) {
            a = n.event.fix(a);
            var b, c, d, f, g, h = [],
                i = e.call(arguments),
                j = (n._data(this, "events") || {})[a.type] || [],
                k = n.event.special[a.type] || {};
            if (i[0] = a, a.delegateTarget = this, !k.preDispatch || k.preDispatch.call(this, a) !== !1) {
                h = n.event.handlers.call(this, a, j), b = 0;
                while ((f = h[b++]) && !a.isPropagationStopped()) {
                    a.currentTarget = f.elem, c = 0;
                    while ((g = f.handlers[c++]) && !a.isImmediatePropagationStopped()) a.rnamespace && !a.rnamespace.test(g.namespace) || (a.handleObj = g, a.data = g.data, d = ((n.event.special[g.origType] || {}).handle || g.handler).apply(f.elem, i), void 0 !== d && (a.result = d) === !1 && (a.preventDefault(), a.stopPropagation()))
                }
                return k.postDispatch && k.postDispatch.call(this, a), a.result
            }
        },
        handlers: function(a, b) {
            var c, d, e, f, g = [],
                h = b.delegateCount,
                i = a.target;
            if (h && i.nodeType && ("click" !== a.type || isNaN(a.button) || a.button < 1))
                for (; i != this; i = i.parentNode || this)
                    if (1 === i.nodeType && (i.disabled !== !0 || "click" !== a.type)) {
                        for (d = [], c = 0; h > c; c++) f = b[c], e = f.selector + " ", void 0 === d[e] && (d[e] = f.needsContext ? n(e, this).index(i) > -1 : n.find(e, this, null, [i]).length), d[e] && d.push(f);
                        d.length && g.push({
                            elem: i,
                            handlers: d
                        })
                    }
            return h < b.length && g.push({
                elem: this,
                handlers: b.slice(h)
            }), g
        },
        fix: function(a) {
            if (a[n.expando]) return a;
            var b, c, e, f = a.type,
                g = a,
                h = this.fixHooks[f];
            h || (this.fixHooks[f] = h = ma.test(f) ? this.mouseHooks : la.test(f) ? this.keyHooks : {}), e = h.props ? this.props.concat(h.props) : this.props, a = new n.Event(g), b = e.length;
            while (b--) c = e[b], a[c] = g[c];
            return a.target || (a.target = g.srcElement || d), 3 === a.target.nodeType && (a.target = a.target.parentNode), a.metaKey = !!a.metaKey, h.filter ? h.filter(a, g) : a
        },
        props: "altKey bubbles cancelable ctrlKey currentTarget detail eventPhase metaKey relatedTarget shiftKey target timeStamp view which".split(" "),
        fixHooks: {},
        keyHooks: {
            props: "char charCode key keyCode".split(" "),
            filter: function(a, b) {
                return null == a.which && (a.which = null != b.charCode ? b.charCode : b.keyCode), a
            }
        },
        mouseHooks: {
            props: "button buttons clientX clientY fromElement offsetX offsetY pageX pageY screenX screenY toElement".split(" "),
            filter: function(a, b) {
                var c, e, f, g = b.button,
                    h = b.fromElement;
                return null == a.pageX && null != b.clientX && (e = a.target.ownerDocument || d, f = e.documentElement, c = e.body, a.pageX = b.clientX + (f && f.scrollLeft || c && c.scrollLeft || 0) - (f && f.clientLeft || c && c.clientLeft || 0), a.pageY = b.clientY + (f && f.scrollTop || c && c.scrollTop || 0) - (f && f.clientTop || c && c.clientTop || 0)), !a.relatedTarget && h && (a.relatedTarget = h === a.target ? b.toElement : h), a.which || void 0 === g || (a.which = 1 & g ? 1 : 2 & g ? 3 : 4 & g ? 2 : 0), a
            }
        },
        special: {
            load: {
                noBubble: !0
            },
            focus: {
                trigger: function() {
                    if (this !== ra() && this.focus) try {
                        return this.focus(), !1
                    } catch (a) {}
                },
                delegateType: "focusin"
            },
            blur: {
                trigger: function() {
                    return this === ra() && this.blur ? (this.blur(), !1) : void 0
                },
                delegateType: "focusout"
            },
            click: {
                trigger: function() {
                    return n.nodeName(this, "input") && "checkbox" === this.type && this.click ? (this.click(), !1) : void 0
                },
                _default: function(a) {
                    return n.nodeName(a.target, "a")
                }
            },
            beforeunload: {
                postDispatch: function(a) {
                    void 0 !== a.result && a.originalEvent && (a.originalEvent.returnValue = a.result)
                }
            }
        },
        simulate: function(a, b, c) {
            var d = n.extend(new n.Event, c, {
                type: a,
                isSimulated: !0
            });
            n.event.trigger(d, null, b), d.isDefaultPrevented() && c.preventDefault()
        }
    }, n.removeEvent = d.removeEventListener ? function(a, b, c) {
        a.removeEventListener && a.removeEventListener(b, c)
    } : function(a, b, c) {
        var d = "on" + b;
        a.detachEvent && ("undefined" == typeof a[d] && (a[d] = null), a.detachEvent(d, c))
    }, n.Event = function(a, b) {
        return this instanceof n.Event ? (a && a.type ? (this.originalEvent = a, this.type = a.type, this.isDefaultPrevented = a.defaultPrevented || void 0 === a.defaultPrevented && a.returnValue === !1 ? pa : qa) : this.type = a, b && n.extend(this, b), this.timeStamp = a && a.timeStamp || n.now(), void(this[n.expando] = !0)) : new n.Event(a, b)
    }, n.Event.prototype = {
        constructor: n.Event,
        isDefaultPrevented: qa,
        isPropagationStopped: qa,
        isImmediatePropagationStopped: qa,
        preventDefault: function() {
            var a = this.originalEvent;
            this.isDefaultPrevented = pa, a && (a.preventDefault ? a.preventDefault() : a.returnValue = !1)
        },
        stopPropagation: function() {
            var a = this.originalEvent;
            this.isPropagationStopped = pa, a && !this.isSimulated && (a.stopPropagation && a.stopPropagation(), a.cancelBubble = !0)
        },
        stopImmediatePropagation: function() {
            var a = this.originalEvent;
            this.isImmediatePropagationStopped = pa, a && a.stopImmediatePropagation && a.stopImmediatePropagation(), this.stopPropagation()
        }
    }, n.each({
        mouseenter: "mouseover",
        mouseleave: "mouseout",
        pointerenter: "pointerover",
        pointerleave: "pointerout"
    }, function(a, b) {
        n.event.special[a] = {
            delegateType: b,
            bindType: b,
            handle: function(a) {
                var c, d = this,
                    e = a.relatedTarget,
                    f = a.handleObj;
                return e && (e === d || n.contains(d, e)) || (a.type = f.origType, c = f.handler.apply(this, arguments), a.type = b), c
            }
        }
    }), l.submit || (n.event.special.submit = {
        setup: function() {
            return n.nodeName(this, "form") ? !1 : void n.event.add(this, "click._submit keypress._submit", function(a) {
                var b = a.target,
                    c = n.nodeName(b, "input") || n.nodeName(b, "button") ? n.prop(b, "form") : void 0;
                c && !n._data(c, "submit") && (n.event.add(c, "submit._submit", function(a) {
                    a._submitBubble = !0
                }), n._data(c, "submit", !0))
            })
        },
        postDispatch: function(a) {
            a._submitBubble && (delete a._submitBubble, this.parentNode && !a.isTrigger && n.event.simulate("submit", this.parentNode, a))
        },
        teardown: function() {
            return n.nodeName(this, "form") ? !1 : void n.event.remove(this, "._submit")
        }
    }), l.change || (n.event.special.change = {
        setup: function() {
            return ka.test(this.nodeName) ? ("checkbox" !== this.type && "radio" !== this.type || (n.event.add(this, "propertychange._change", function(a) {
                "checked" === a.originalEvent.propertyName && (this._justChanged = !0)
            }), n.event.add(this, "click._change", function(a) {
                this._justChanged && !a.isTrigger && (this._justChanged = !1), n.event.simulate("change", this, a)
            })), !1) : void n.event.add(this, "beforeactivate._change", function(a) {
                var b = a.target;
                ka.test(b.nodeName) && !n._data(b, "change") && (n.event.add(b, "change._change", function(a) {
                    !this.parentNode || a.isSimulated || a.isTrigger || n.event.simulate("change", this.parentNode, a)
                }), n._data(b, "change", !0))
            })
        },
        handle: function(a) {
            var b = a.target;
            return this !== b || a.isSimulated || a.isTrigger || "radio" !== b.type && "checkbox" !== b.type ? a.handleObj.handler.apply(this, arguments) : void 0
        },
        teardown: function() {
            return n.event.remove(this, "._change"), !ka.test(this.nodeName)
        }
    }), l.focusin || n.each({
        focus: "focusin",
        blur: "focusout"
    }, function(a, b) {
        var c = function(a) {
            n.event.simulate(b, a.target, n.event.fix(a))
        };
        n.event.special[b] = {
            setup: function() {
                var d = this.ownerDocument || this,
                    e = n._data(d, b);
                e || d.addEventListener(a, c, !0), n._data(d, b, (e || 0) + 1)
            },
            teardown: function() {
                var d = this.ownerDocument || this,
                    e = n._data(d, b) - 1;
                e ? n._data(d, b, e) : (d.removeEventListener(a, c, !0), n._removeData(d, b))
            }
        }
    }), n.fn.extend({
        on: function(a, b, c, d) {
            return sa(this, a, b, c, d)
        },
        one: function(a, b, c, d) {
            return sa(this, a, b, c, d, 1)
        },
        off: function(a, b, c) {
            var d, e;
            if (a && a.preventDefault && a.handleObj) return d = a.handleObj, n(a.delegateTarget).off(d.namespace ? d.origType + "." + d.namespace : d.origType, d.selector, d.handler), this;
            if ("object" == typeof a) {
                for (e in a) this.off(e, b, a[e]);
                return this
            }
            return b !== !1 && "function" != typeof b || (c = b, b = void 0), c === !1 && (c = qa), this.each(function() {
                n.event.remove(this, a, c, b)
            })
        },
        trigger: function(a, b) {
            return this.each(function() {
                n.event.trigger(a, b, this)
            })
        },
        triggerHandler: function(a, b) {
            var c = this[0];
            return c ? n.event.trigger(a, b, c, !0) : void 0
        }
    });
    var ta = / jQuery\d+="(?:null|\d+)"/g,
        ua = new RegExp("<(?:" + ba + ")[\\s/>]", "i"),
        va = /<(?!area|br|col|embed|hr|img|input|link|meta|param)(([\w:-]+)[^>]*)\/>/gi,
        wa = /<script|<style|<link/i,
        xa = /checked\s*(?:[^=]|=\s*.checked.)/i,
        ya = /^true\/(.*)/,
        za = /^\s*<!(?:\[CDATA\[|--)|(?:\]\]|--)>\s*$/g,
        Aa = ca(d),
        Ba = Aa.appendChild(d.createElement("div"));

    function Ca(a, b) {
        return n.nodeName(a, "table") && n.nodeName(11 !== b.nodeType ? b : b.firstChild, "tr") ? a.getElementsByTagName("tbody")[0] || a.appendChild(a.ownerDocument.createElement("tbody")) : a
    }

    function Da(a) {
        return a.type = (null !== n.find.attr(a, "type")) + "/" + a.type, a
    }

    function Ea(a) {
        var b = ya.exec(a.type);
        return b ? a.type = b[1] : a.removeAttribute("type"), a
    }

    function Fa(a, b) {
        if (1 === b.nodeType && n.hasData(a)) {
            var c, d, e, f = n._data(a),
                g = n._data(b, f),
                h = f.events;
            if (h) {
                delete g.handle, g.events = {};
                for (c in h)
                    for (d = 0, e = h[c].length; e > d; d++) n.event.add(b, c, h[c][d])
            }
            g.data && (g.data = n.extend({}, g.data))
        }
    }

    function Ga(a, b) {
        var c, d, e;
        if (1 === b.nodeType) {
            if (c = b.nodeName.toLowerCase(), !l.noCloneEvent && b[n.expando]) {
                e = n._data(b);
                for (d in e.events) n.removeEvent(b, d, e.handle);
                b.removeAttribute(n.expando)
            }
            "script" === c && b.text !== a.text ? (Da(b).text = a.text, Ea(b)) : "object" === c ? (b.parentNode && (b.outerHTML = a.outerHTML), l.html5Clone && a.innerHTML && !n.trim(b.innerHTML) && (b.innerHTML = a.innerHTML)) : "input" === c && Z.test(a.type) ? (b.defaultChecked = b.checked = a.checked, b.value !== a.value && (b.value = a.value)) : "option" === c ? b.defaultSelected = b.selected = a.defaultSelected : "input" !== c && "textarea" !== c || (b.defaultValue = a.defaultValue)
        }
    }

    function Ha(a, b, c, d) {
        b = f.apply([], b);
        var e, g, h, i, j, k, m = 0,
            o = a.length,
            p = o - 1,
            q = b[0],
            r = n.isFunction(q);
        if (r || o > 1 && "string" == typeof q && !l.checkClone && xa.test(q)) return a.each(function(e) {
            var f = a.eq(e);
            r && (b[0] = q.call(this, e, f.html())), Ha(f, b, c, d)
        });
        if (o && (k = ja(b, a[0].ownerDocument, !1, a, d), e = k.firstChild, 1 === k.childNodes.length && (k = e), e || d)) {
            for (i = n.map(ea(k, "script"), Da), h = i.length; o > m; m++) g = k, m !== p && (g = n.clone(g, !0, !0), h && n.merge(i, ea(g, "script"))), c.call(a[m], g, m);
            if (h)
                for (j = i[i.length - 1].ownerDocument, n.map(i, Ea), m = 0; h > m; m++) g = i[m], _.test(g.type || "") && !n._data(g, "globalEval") && n.contains(j, g) && (g.src ? n._evalUrl && n._evalUrl(g.src) : n.globalEval((g.text || g.textContent || g.innerHTML || "").replace(za, "")));
            k = e = null
        }
        return a
    }

    function Ia(a, b, c) {
        for (var d, e = b ? n.filter(b, a) : a, f = 0; null != (d = e[f]); f++) c || 1 !== d.nodeType || n.cleanData(ea(d)), d.parentNode && (c && n.contains(d.ownerDocument, d) && fa(ea(d, "script")), d.parentNode.removeChild(d));
        return a
    }
    n.extend({
        htmlPrefilter: function(a) {
            return a.replace(va, "<$1></$2>")
        },
        clone: function(a, b, c) {
            var d, e, f, g, h, i = n.contains(a.ownerDocument, a);
            if (l.html5Clone || n.isXMLDoc(a) || !ua.test("<" + a.nodeName + ">") ? f = a.cloneNode(!0) : (Ba.innerHTML = a.outerHTML, Ba.removeChild(f = Ba.firstChild)), !(l.noCloneEvent && l.noCloneChecked || 1 !== a.nodeType && 11 !== a.nodeType || n.isXMLDoc(a)))
                for (d = ea(f), h = ea(a), g = 0; null != (e = h[g]); ++g) d[g] && Ga(e, d[g]);
            if (b)
                if (c)
                    for (h = h || ea(a), d = d || ea(f), g = 0; null != (e = h[g]); g++) Fa(e, d[g]);
                else Fa(a, f);
            return d = ea(f, "script"), d.length > 0 && fa(d, !i && ea(a, "script")), d = h = e = null, f
        },
        cleanData: function(a, b) {
            for (var d, e, f, g, h = 0, i = n.expando, j = n.cache, k = l.attributes, m = n.event.special; null != (d = a[h]); h++)
                if ((b || M(d)) && (f = d[i], g = f && j[f])) {
                    if (g.events)
                        for (e in g.events) m[e] ? n.event.remove(d, e) : n.removeEvent(d, e, g.handle);
                    j[f] && (delete j[f], k || "undefined" == typeof d.removeAttribute ? d[i] = void 0 : d.removeAttribute(i), c.push(f))
                }
        }
    }), n.fn.extend({
        domManip: Ha,
        detach: function(a) {
            return Ia(this, a, !0)
        },
        remove: function(a) {
            return Ia(this, a)
        },
        text: function(a) {
            return Y(this, function(a) {
                return void 0 === a ? n.text(this) : this.empty().append((this[0] && this[0].ownerDocument || d).createTextNode(a))
            }, null, a, arguments.length)
        },
        append: function() {
            return Ha(this, arguments, function(a) {
                if (1 === this.nodeType || 11 === this.nodeType || 9 === this.nodeType) {
                    var b = Ca(this, a);
                    b.appendChild(a)
                }
            })
        },
        prepend: function() {
            return Ha(this, arguments, function(a) {
                if (1 === this.nodeType || 11 === this.nodeType || 9 === this.nodeType) {
                    var b = Ca(this, a);
                    b.insertBefore(a, b.firstChild)
                }
            })
        },
        before: function() {
            return Ha(this, arguments, function(a) {
                this.parentNode && this.parentNode.insertBefore(a, this)
            })
        },
        after: function() {
            return Ha(this, arguments, function(a) {
                this.parentNode && this.parentNode.insertBefore(a, this.nextSibling)
            })
        },
        empty: function() {
            for (var a, b = 0; null != (a = this[b]); b++) {
                1 === a.nodeType && n.cleanData(ea(a, !1));
                while (a.firstChild) a.removeChild(a.firstChild);
                a.options && n.nodeName(a, "select") && (a.options.length = 0)
            }
            return this
        },
        clone: function(a, b) {
            return a = null == a ? !1 : a, b = null == b ? a : b, this.map(function() {
                return n.clone(this, a, b)
            })
        },
        html: function(a) {
            return Y(this, function(a) {
                var b = this[0] || {},
                    c = 0,
                    d = this.length;
                if (void 0 === a) return 1 === b.nodeType ? b.innerHTML.replace(ta, "") : void 0;
                if ("string" == typeof a && !wa.test(a) && (l.htmlSerialize || !ua.test(a)) && (l.leadingWhitespace || !aa.test(a)) && !da[($.exec(a) || ["", ""])[1].toLowerCase()]) {
                    a = n.htmlPrefilter(a);
                    try {
                        for (; d > c; c++) b = this[c] || {}, 1 === b.nodeType && (n.cleanData(ea(b, !1)), b.innerHTML = a);
                        b = 0
                    } catch (e) {}
                }
                b && this.empty().append(a)
            }, null, a, arguments.length)
        },
        replaceWith: function() {
            var a = [];
            return Ha(this, arguments, function(b) {
                var c = this.parentNode;
                n.inArray(this, a) < 0 && (n.cleanData(ea(this)), c && c.replaceChild(b, this))
            }, a)
        }
    }), n.each({
        appendTo: "append",
        prependTo: "prepend",
        insertBefore: "before",
        insertAfter: "after",
        replaceAll: "replaceWith"
    }, function(a, b) {
        n.fn[a] = function(a) {
            for (var c, d = 0, e = [], f = n(a), h = f.length - 1; h >= d; d++) c = d === h ? this : this.clone(!0), n(f[d])[b](c), g.apply(e, c.get());
            return this.pushStack(e)
        }
    });
    var Ja, Ka = {
        HTML: "block",
        BODY: "block"
    };

    function La(a, b) {
        var c = n(b.createElement(a)).appendTo(b.body),
            d = n.css(c[0], "display");
        return c.detach(), d
    }

    function Ma(a) {
        var b = d,
            c = Ka[a];
        return c || (c = La(a, b), "none" !== c && c || (Ja = (Ja || n("<iframe frameborder='0' width='0' height='0'/>")).appendTo(b.documentElement), b = (Ja[0].contentWindow || Ja[0].contentDocument).document, b.write(), b.close(), c = La(a, b), Ja.detach()), Ka[a] = c), c
    }
    var Na = /^margin/,
        Oa = new RegExp("^(" + T + ")(?!px)[a-z%]+$", "i"),
        Pa = function(a, b, c, d) {
            var e, f, g = {};
            for (f in b) g[f] = a.style[f], a.style[f] = b[f];
            e = c.apply(a, d || []);
            for (f in b) a.style[f] = g[f];
            return e
        },
        Qa = d.documentElement;
    ! function() {
        var b, c, e, f, g, h, i = d.createElement("div"),
            j = d.createElement("div");
        if (j.style) {
            j.style.cssText = "float:left;opacity:.5", l.opacity = "0.5" === j.style.opacity, l.cssFloat = !!j.style.cssFloat, j.style.backgroundClip = "content-box", j.cloneNode(!0).style.backgroundClip = "", l.clearCloneStyle = "content-box" === j.style.backgroundClip, i = d.createElement("div"), i.style.cssText = "border:0;width:8px;height:0;top:0;left:-9999px;padding:0;margin-top:1px;position:absolute", j.innerHTML = "", i.appendChild(j), l.boxSizing = "" === j.style.boxSizing || "" === j.style.MozBoxSizing || "" === j.style.WebkitBoxSizing, n.extend(l, {
                reliableHiddenOffsets: function() {
                    return null == b && k(), f
                },
                boxSizingReliable: function() {
                    return null == b && k(), e
                },
                pixelMarginRight: function() {
                    return null == b && k(), c
                },
                pixelPosition: function() {
                    return null == b && k(), b
                },
                reliableMarginRight: function() {
                    return null == b && k(), g
                },
                reliableMarginLeft: function() {
                    return null == b && k(), h
                }
            });

            function k() {
                var k, l, m = d.documentElement;
                m.appendChild(i), j.style.cssText = "-webkit-box-sizing:border-box;box-sizing:border-box;position:relative;display:block;margin:auto;border:1px;padding:1px;top:1%;width:50%", b = e = h = !1, c = g = !0, a.getComputedStyle && (l = a.getComputedStyle(j), b = "1%" !== (l || {}).top, h = "2px" === (l || {}).marginLeft, e = "4px" === (l || {
                        width: "4px"
                    }).width, j.style.marginRight = "50%", c = "4px" === (l || {
                        marginRight: "4px"
                    }).marginRight, k = j.appendChild(d.createElement("div")), k.style.cssText = j.style.cssText = "-webkit-box-sizing:content-box;-moz-box-sizing:content-box;box-sizing:content-box;display:block;margin:0;border:0;padding:0", k.style.marginRight = k.style.width = "0", j.style.width = "1px", g = !parseFloat((a.getComputedStyle(k) || {}).marginRight), j.removeChild(k)), j.style.display = "none", f = 0 === j.getClientRects().length, f && (j.style.display = "", j.innerHTML = "<table><tr><td></td><td>t</td></tr></table>", j.childNodes[0].style.borderCollapse = "separate", k = j.getElementsByTagName("td"), k[0].style.cssText = "margin:0;border:0;padding:0;display:none", f = 0 === k[0].offsetHeight, f && (k[0].style.display = "", k[1].style.display = "none", f = 0 === k[0].offsetHeight)), m.removeChild(i)
            }
        }
    }();
    var Ra, Sa, Ta = /^(top|right|bottom|left)$/;
    a.getComputedStyle ? (Ra = function(b) {
        var c = b.ownerDocument.defaultView;
        return c && c.opener || (c = a), c.getComputedStyle(b)
    }, Sa = function(a, b, c) {
        var d, e, f, g, h = a.style;
        return c = c || Ra(a), g = c ? c.getPropertyValue(b) || c[b] : void 0, "" !== g && void 0 !== g || n.contains(a.ownerDocument, a) || (g = n.style(a, b)), c && !l.pixelMarginRight() && Oa.test(g) && Na.test(b) && (d = h.width, e = h.minWidth, f = h.maxWidth, h.minWidth = h.maxWidth = h.width = g, g = c.width, h.width = d, h.minWidth = e, h.maxWidth = f), void 0 === g ? g : g + ""
    }) : Qa.currentStyle && (Ra = function(a) {
            return a.currentStyle
        }, Sa = function(a, b, c) {
            var d, e, f, g, h = a.style;
            return c = c || Ra(a), g = c ? c[b] : void 0, null == g && h && h[b] && (g = h[b]), Oa.test(g) && !Ta.test(b) && (d = h.left, e = a.runtimeStyle, f = e && e.left, f && (e.left = a.currentStyle.left), h.left = "fontSize" === b ? "1em" : g, g = h.pixelLeft + "px", h.left = d, f && (e.left = f)), void 0 === g ? g : g + "" || "auto"
        });

    function Ua(a, b) {
        return {
            get: function() {
                return a() ? void delete this.get : (this.get = b).apply(this, arguments)
            }
        }
    }
    var Va = /alpha\([^)]*\)/i,
        Wa = /opacity\s*=\s*([^)]*)/i,
        Xa = /^(none|table(?!-c[ea]).+)/,
        Ya = new RegExp("^(" + T + ")(.*)$", "i"),
        Za = {
            position: "absolute",
            visibility: "hidden",
            display: "block"
        },
        $a = {
            letterSpacing: "0",
            fontWeight: "400"
        },
        _a = ["Webkit", "O", "Moz", "ms"],
        ab = d.createElement("div").style;

    function bb(a) {
        if (a in ab) return a;
        var b = a.charAt(0).toUpperCase() + a.slice(1),
            c = _a.length;
        while (c--)
            if (a = _a[c] + b, a in ab) return a
    }

    function cb(a, b) {
        for (var c, d, e, f = [], g = 0, h = a.length; h > g; g++) d = a[g], d.style && (f[g] = n._data(d, "olddisplay"), c = d.style.display, b ? (f[g] || "none" !== c || (d.style.display = ""), "" === d.style.display && W(d) && (f[g] = n._data(d, "olddisplay", Ma(d.nodeName)))) : (e = W(d), (c && "none" !== c || !e) && n._data(d, "olddisplay", e ? c : n.css(d, "display"))));
        for (g = 0; h > g; g++) d = a[g], d.style && (b && "none" !== d.style.display && "" !== d.style.display || (d.style.display = b ? f[g] || "" : "none"));
        return a
    }

    function db(a, b, c) {
        var d = Ya.exec(b);
        return d ? Math.max(0, d[1] - (c || 0)) + (d[2] || "px") : b
    }

    function eb(a, b, c, d, e) {
        for (var f = c === (d ? "border" : "content") ? 4 : "width" === b ? 1 : 0, g = 0; 4 > f; f += 2) "margin" === c && (g += n.css(a, c + V[f], !0, e)), d ? ("content" === c && (g -= n.css(a, "padding" + V[f], !0, e)), "margin" !== c && (g -= n.css(a, "border" + V[f] + "Width", !0, e))) : (g += n.css(a, "padding" + V[f], !0, e), "padding" !== c && (g += n.css(a, "border" + V[f] + "Width", !0, e)));
        return g
    }

    function fb(a, b, c) {
        var d = !0,
            e = "width" === b ? a.offsetWidth : a.offsetHeight,
            f = Ra(a),
            g = l.boxSizing && "border-box" === n.css(a, "boxSizing", !1, f);
        if (0 >= e || null == e) {
            if (e = Sa(a, b, f), (0 > e || null == e) && (e = a.style[b]), Oa.test(e)) return e;
            d = g && (l.boxSizingReliable() || e === a.style[b]), e = parseFloat(e) || 0
        }
        return e + eb(a, b, c || (g ? "border" : "content"), d, f) + "px"
    }
    n.extend({
        cssHooks: {
            opacity: {
                get: function(a, b) {
                    if (b) {
                        var c = Sa(a, "opacity");
                        return "" === c ? "1" : c
                    }
                }
            }
        },
        cssNumber: {
            animationIterationCount: !0,
            columnCount: !0,
            fillOpacity: !0,
            flexGrow: !0,
            flexShrink: !0,
            fontWeight: !0,
            lineHeight: !0,
            opacity: !0,
            order: !0,
            orphans: !0,
            widows: !0,
            zIndex: !0,
            zoom: !0
        },
        cssProps: {
            "float": l.cssFloat ? "cssFloat" : "styleFloat"
        },
        style: function(a, b, c, d) {
            if (a && 3 !== a.nodeType && 8 !== a.nodeType && a.style) {
                var e, f, g, h = n.camelCase(b),
                    i = a.style;
                if (b = n.cssProps[h] || (n.cssProps[h] = bb(h) || h), g = n.cssHooks[b] || n.cssHooks[h], void 0 === c) return g && "get" in g && void 0 !== (e = g.get(a, !1, d)) ? e : i[b];
                if (f = typeof c, "string" === f && (e = U.exec(c)) && e[1] && (c = X(a, b, e), f = "number"), null != c && c === c && ("number" === f && (c += e && e[3] || (n.cssNumber[h] ? "" : "px")), l.clearCloneStyle || "" !== c || 0 !== b.indexOf("background") || (i[b] = "inherit"), !(g && "set" in g && void 0 === (c = g.set(a, c, d))))) try {
                    i[b] = c
                } catch (j) {}
            }
        },
        css: function(a, b, c, d) {
            var e, f, g, h = n.camelCase(b);
            return b = n.cssProps[h] || (n.cssProps[h] = bb(h) || h), g = n.cssHooks[b] || n.cssHooks[h], g && "get" in g && (f = g.get(a, !0, c)), void 0 === f && (f = Sa(a, b, d)), "normal" === f && b in $a && (f = $a[b]), "" === c || c ? (e = parseFloat(f), c === !0 || isFinite(e) ? e || 0 : f) : f
        }
    }), n.each(["height", "width"], function(a, b) {
        n.cssHooks[b] = {
            get: function(a, c, d) {
                return c ? Xa.test(n.css(a, "display")) && 0 === a.offsetWidth ? Pa(a, Za, function() {
                    return fb(a, b, d)
                }) : fb(a, b, d) : void 0
            },
            set: function(a, c, d) {
                var e = d && Ra(a);
                return db(a, c, d ? eb(a, b, d, l.boxSizing && "border-box" === n.css(a, "boxSizing", !1, e), e) : 0)
            }
        }
    }), l.opacity || (n.cssHooks.opacity = {
        get: function(a, b) {
            return Wa.test((b && a.currentStyle ? a.currentStyle.filter : a.style.filter) || "") ? .01 * parseFloat(RegExp.$1) + "" : b ? "1" : ""
        },
        set: function(a, b) {
            var c = a.style,
                d = a.currentStyle,
                e = n.isNumeric(b) ? "alpha(opacity=" + 100 * b + ")" : "",
                f = d && d.filter || c.filter || "";
            c.zoom = 1, (b >= 1 || "" === b) && "" === n.trim(f.replace(Va, "")) && c.removeAttribute && (c.removeAttribute("filter"), "" === b || d && !d.filter) || (c.filter = Va.test(f) ? f.replace(Va, e) : f + " " + e)
        }
    }), n.cssHooks.marginRight = Ua(l.reliableMarginRight, function(a, b) {
        return b ? Pa(a, {
            display: "inline-block"
        }, Sa, [a, "marginRight"]) : void 0
    }), n.cssHooks.marginLeft = Ua(l.reliableMarginLeft, function(a, b) {
        return b ? (parseFloat(Sa(a, "marginLeft")) || (n.contains(a.ownerDocument, a) ? a.getBoundingClientRect().left - Pa(a, {
                    marginLeft: 0
                }, function() {
                    return a.getBoundingClientRect().left
                }) : 0)) + "px" : void 0
    }), n.each({
        margin: "",
        padding: "",
        border: "Width"
    }, function(a, b) {
        n.cssHooks[a + b] = {
            expand: function(c) {
                for (var d = 0, e = {}, f = "string" == typeof c ? c.split(" ") : [c]; 4 > d; d++) e[a + V[d] + b] = f[d] || f[d - 2] || f[0];
                return e
            }
        }, Na.test(a) || (n.cssHooks[a + b].set = db)
    }), n.fn.extend({
        css: function(a, b) {
            return Y(this, function(a, b, c) {
                var d, e, f = {},
                    g = 0;
                if (n.isArray(b)) {
                    for (d = Ra(a), e = b.length; e > g; g++) f[b[g]] = n.css(a, b[g], !1, d);
                    return f
                }
                return void 0 !== c ? n.style(a, b, c) : n.css(a, b)
            }, a, b, arguments.length > 1)
        },
        show: function() {
            return cb(this, !0)
        },
        hide: function() {
            return cb(this)
        },
        toggle: function(a) {
            return "boolean" == typeof a ? a ? this.show() : this.hide() : this.each(function() {
                W(this) ? n(this).show() : n(this).hide()
            })
        }
    });

    function gb(a, b, c, d, e) {
        return new gb.prototype.init(a, b, c, d, e)
    }
    n.Tween = gb, gb.prototype = {
        constructor: gb,
        init: function(a, b, c, d, e, f) {
            this.elem = a, this.prop = c, this.easing = e || n.easing._default, this.options = b, this.start = this.now = this.cur(), this.end = d, this.unit = f || (n.cssNumber[c] ? "" : "px")
        },
        cur: function() {
            var a = gb.propHooks[this.prop];
            return a && a.get ? a.get(this) : gb.propHooks._default.get(this)
        },
        run: function(a) {
            var b, c = gb.propHooks[this.prop];
            return this.options.duration ? this.pos = b = n.easing[this.easing](a, this.options.duration * a, 0, 1, this.options.duration) : this.pos = b = a, this.now = (this.end - this.start) * b + this.start, this.options.step && this.options.step.call(this.elem, this.now, this), c && c.set ? c.set(this) : gb.propHooks._default.set(this), this
        }
    }, gb.prototype.init.prototype = gb.prototype, gb.propHooks = {
        _default: {
            get: function(a) {
                var b;
                return 1 !== a.elem.nodeType || null != a.elem[a.prop] && null == a.elem.style[a.prop] ? a.elem[a.prop] : (b = n.css(a.elem, a.prop, ""), b && "auto" !== b ? b : 0)
            },
            set: function(a) {
                n.fx.step[a.prop] ? n.fx.step[a.prop](a) : 1 !== a.elem.nodeType || null == a.elem.style[n.cssProps[a.prop]] && !n.cssHooks[a.prop] ? a.elem[a.prop] = a.now : n.style(a.elem, a.prop, a.now + a.unit)
            }
        }
    }, gb.propHooks.scrollTop = gb.propHooks.scrollLeft = {
        set: function(a) {
            a.elem.nodeType && a.elem.parentNode && (a.elem[a.prop] = a.now)
        }
    }, n.easing = {
        linear: function(a) {
            return a
        },
        swing: function(a) {
            return .5 - Math.cos(a * Math.PI) / 2
        },
        _default: "swing"
    }, n.fx = gb.prototype.init, n.fx.step = {};
    var hb, ib, jb = /^(?:toggle|show|hide)$/,
        kb = /queueHooks$/;

    function lb() {
        return a.setTimeout(function() {
            hb = void 0
        }), hb = n.now()
    }

    function mb(a, b) {
        var c, d = {
                height: a
            },
            e = 0;
        for (b = b ? 1 : 0; 4 > e; e += 2 - b) c = V[e], d["margin" + c] = d["padding" + c] = a;
        return b && (d.opacity = d.width = a), d
    }

    function nb(a, b, c) {
        for (var d, e = (qb.tweeners[b] || []).concat(qb.tweeners["*"]), f = 0, g = e.length; g > f; f++)
            if (d = e[f].call(c, b, a)) return d
    }

    function ob(a, b, c) {
        var d, e, f, g, h, i, j, k, m = this,
            o = {},
            p = a.style,
            q = a.nodeType && W(a),
            r = n._data(a, "fxshow");
        c.queue || (h = n._queueHooks(a, "fx"), null == h.unqueued && (h.unqueued = 0, i = h.empty.fire, h.empty.fire = function() {
            h.unqueued || i()
        }), h.unqueued++, m.always(function() {
            m.always(function() {
                h.unqueued--, n.queue(a, "fx").length || h.empty.fire()
            })
        })), 1 === a.nodeType && ("height" in b || "width" in b) && (c.overflow = [p.overflow, p.overflowX, p.overflowY], j = n.css(a, "display"), k = "none" === j ? n._data(a, "olddisplay") || Ma(a.nodeName) : j, "inline" === k && "none" === n.css(a, "float") && (l.inlineBlockNeedsLayout && "inline" !== Ma(a.nodeName) ? p.zoom = 1 : p.display = "inline-block")), c.overflow && (p.overflow = "hidden", l.shrinkWrapBlocks() || m.always(function() {
            p.overflow = c.overflow[0], p.overflowX = c.overflow[1], p.overflowY = c.overflow[2]
        }));
        for (d in b)
            if (e = b[d], jb.exec(e)) {
                if (delete b[d], f = f || "toggle" === e, e === (q ? "hide" : "show")) {
                    if ("show" !== e || !r || void 0 === r[d]) continue;
                    q = !0
                }
                o[d] = r && r[d] || n.style(a, d)
            } else j = void 0;
        if (n.isEmptyObject(o)) "inline" === ("none" === j ? Ma(a.nodeName) : j) && (p.display = j);
        else {
            r ? "hidden" in r && (q = r.hidden) : r = n._data(a, "fxshow", {}), f && (r.hidden = !q), q ? n(a).show() : m.done(function() {
                n(a).hide()
            }), m.done(function() {
                var b;
                n._removeData(a, "fxshow");
                for (b in o) n.style(a, b, o[b])
            });
            for (d in o) g = nb(q ? r[d] : 0, d, m), d in r || (r[d] = g.start, q && (g.end = g.start, g.start = "width" === d || "height" === d ? 1 : 0))
        }
    }

    function pb(a, b) {
        var c, d, e, f, g;
        for (c in a)
            if (d = n.camelCase(c), e = b[d], f = a[c], n.isArray(f) && (e = f[1], f = a[c] = f[0]), c !== d && (a[d] = f, delete a[c]), g = n.cssHooks[d], g && "expand" in g) {
                f = g.expand(f), delete a[d];
                for (c in f) c in a || (a[c] = f[c], b[c] = e)
            } else b[d] = e
    }

    function qb(a, b, c) {
        var d, e, f = 0,
            g = qb.prefilters.length,
            h = n.Deferred().always(function() {
                delete i.elem
            }),
            i = function() {
                if (e) return !1;
                for (var b = hb || lb(), c = Math.max(0, j.startTime + j.duration - b), d = c / j.duration || 0, f = 1 - d, g = 0, i = j.tweens.length; i > g; g++) j.tweens[g].run(f);
                return h.notifyWith(a, [j, f, c]), 1 > f && i ? c : (h.resolveWith(a, [j]), !1)
            },
            j = h.promise({
                elem: a,
                props: n.extend({}, b),
                opts: n.extend(!0, {
                    specialEasing: {},
                    easing: n.easing._default
                }, c),
                originalProperties: b,
                originalOptions: c,
                startTime: hb || lb(),
                duration: c.duration,
                tweens: [],
                createTween: function(b, c) {
                    var d = n.Tween(a, j.opts, b, c, j.opts.specialEasing[b] || j.opts.easing);
                    return j.tweens.push(d), d
                },
                stop: function(b) {
                    var c = 0,
                        d = b ? j.tweens.length : 0;
                    if (e) return this;
                    for (e = !0; d > c; c++) j.tweens[c].run(1);
                    return b ? (h.notifyWith(a, [j, 1, 0]), h.resolveWith(a, [j, b])) : h.rejectWith(a, [j, b]), this
                }
            }),
            k = j.props;
        for (pb(k, j.opts.specialEasing); g > f; f++)
            if (d = qb.prefilters[f].call(j, a, k, j.opts)) return n.isFunction(d.stop) && (n._queueHooks(j.elem, j.opts.queue).stop = n.proxy(d.stop, d)), d;
        return n.map(k, nb, j), n.isFunction(j.opts.start) && j.opts.start.call(a, j), n.fx.timer(n.extend(i, {
            elem: a,
            anim: j,
            queue: j.opts.queue
        })), j.progress(j.opts.progress).done(j.opts.done, j.opts.complete).fail(j.opts.fail).always(j.opts.always)
    }
    n.Animation = n.extend(qb, {
        tweeners: {
            "*": [function(a, b) {
                var c = this.createTween(a, b);
                return X(c.elem, a, U.exec(b), c), c
            }]
        },
        tweener: function(a, b) {
            n.isFunction(a) ? (b = a, a = ["*"]) : a = a.match(G);
            for (var c, d = 0, e = a.length; e > d; d++) c = a[d], qb.tweeners[c] = qb.tweeners[c] || [], qb.tweeners[c].unshift(b)
        },
        prefilters: [ob],
        prefilter: function(a, b) {
            b ? qb.prefilters.unshift(a) : qb.prefilters.push(a)
        }
    }), n.speed = function(a, b, c) {
        var d = a && "object" == typeof a ? n.extend({}, a) : {
            complete: c || !c && b || n.isFunction(a) && a,
            duration: a,
            easing: c && b || b && !n.isFunction(b) && b
        };
        return d.duration = n.fx.off ? 0 : "number" == typeof d.duration ? d.duration : d.duration in n.fx.speeds ? n.fx.speeds[d.duration] : n.fx.speeds._default, null != d.queue && d.queue !== !0 || (d.queue = "fx"), d.old = d.complete, d.complete = function() {
            n.isFunction(d.old) && d.old.call(this), d.queue && n.dequeue(this, d.queue)
        }, d
    }, n.fn.extend({
        fadeTo: function(a, b, c, d) {
            return this.filter(W).css("opacity", 0).show().end().animate({
                opacity: b
            }, a, c, d)
        },
        animate: function(a, b, c, d) {
            var e = n.isEmptyObject(a),
                f = n.speed(b, c, d),
                g = function() {
                    var b = qb(this, n.extend({}, a), f);
                    (e || n._data(this, "finish")) && b.stop(!0)
                };
            return g.finish = g, e || f.queue === !1 ? this.each(g) : this.queue(f.queue, g)
        },
        stop: function(a, b, c) {
            var d = function(a) {
                var b = a.stop;
                delete a.stop, b(c)
            };
            return "string" != typeof a && (c = b, b = a, a = void 0), b && a !== !1 && this.queue(a || "fx", []), this.each(function() {
                var b = !0,
                    e = null != a && a + "queueHooks",
                    f = n.timers,
                    g = n._data(this);
                if (e) g[e] && g[e].stop && d(g[e]);
                else
                    for (e in g) g[e] && g[e].stop && kb.test(e) && d(g[e]);
                for (e = f.length; e--;) f[e].elem !== this || null != a && f[e].queue !== a || (f[e].anim.stop(c), b = !1, f.splice(e, 1));
                !b && c || n.dequeue(this, a)
            })
        },
        finish: function(a) {
            return a !== !1 && (a = a || "fx"), this.each(function() {
                var b, c = n._data(this),
                    d = c[a + "queue"],
                    e = c[a + "queueHooks"],
                    f = n.timers,
                    g = d ? d.length : 0;
                for (c.finish = !0, n.queue(this, a, []), e && e.stop && e.stop.call(this, !0), b = f.length; b--;) f[b].elem === this && f[b].queue === a && (f[b].anim.stop(!0), f.splice(b, 1));
                for (b = 0; g > b; b++) d[b] && d[b].finish && d[b].finish.call(this);
                delete c.finish
            })
        }
    }), n.each(["toggle", "show", "hide"], function(a, b) {
        var c = n.fn[b];
        n.fn[b] = function(a, d, e) {
            return null == a || "boolean" == typeof a ? c.apply(this, arguments) : this.animate(mb(b, !0), a, d, e)
        }
    }), n.each({
        slideDown: mb("show"),
        slideUp: mb("hide"),
        slideToggle: mb("toggle"),
        fadeIn: {
            opacity: "show"
        },
        fadeOut: {
            opacity: "hide"
        },
        fadeToggle: {
            opacity: "toggle"
        }
    }, function(a, b) {
        n.fn[a] = function(a, c, d) {
            return this.animate(b, a, c, d)
        }
    }), n.timers = [], n.fx.tick = function() {
        var a, b = n.timers,
            c = 0;
        for (hb = n.now(); c < b.length; c++) a = b[c], a() || b[c] !== a || b.splice(c--, 1);
        b.length || n.fx.stop(), hb = void 0
    }, n.fx.timer = function(a) {
        n.timers.push(a), a() ? n.fx.start() : n.timers.pop()
    }, n.fx.interval = 13, n.fx.start = function() {
        ib || (ib = a.setInterval(n.fx.tick, n.fx.interval))
    }, n.fx.stop = function() {
        a.clearInterval(ib), ib = null
    }, n.fx.speeds = {
        slow: 600,
        fast: 200,
        _default: 400
    }, n.fn.delay = function(b, c) {
        return b = n.fx ? n.fx.speeds[b] || b : b, c = c || "fx", this.queue(c, function(c, d) {
            var e = a.setTimeout(c, b);
            d.stop = function() {
                a.clearTimeout(e)
            }
        })
    },
        function() {
            var a, b = d.createElement("input"),
                c = d.createElement("div"),
                e = d.createElement("select"),
                f = e.appendChild(d.createElement("option"));
            c = d.createElement("div"), c.setAttribute("className", "t"), c.innerHTML = "  <link/><table></table><a href='/a'>a</a><input type='checkbox'/>", a = c.getElementsByTagName("a")[0], b.setAttribute("type", "checkbox"), c.appendChild(b), a = c.getElementsByTagName("a")[0], a.style.cssText = "top:1px", l.getSetAttribute = "t" !== c.className, l.style = /top/.test(a.getAttribute("style")), l.hrefNormalized = "/a" === a.getAttribute("href"), l.checkOn = !!b.value, l.optSelected = f.selected, l.enctype = !!d.createElement("form").enctype, e.disabled = !0, l.optDisabled = !f.disabled, b = d.createElement("input"), b.setAttribute("value", ""), l.input = "" === b.getAttribute("value"), b.value = "t", b.setAttribute("type", "radio"), l.radioValue = "t" === b.value
        }();
    var rb = /\r/g,
        sb = /[\x20\t\r\n\f]+/g;
    n.fn.extend({
        val: function(a) {
            var b, c, d, e = this[0]; {
                if (arguments.length) return d = n.isFunction(a), this.each(function(c) {
                    var e;
                    1 === this.nodeType && (e = d ? a.call(this, c, n(this).val()) : a, null == e ? e = "" : "number" == typeof e ? e += "" : n.isArray(e) && (e = n.map(e, function(a) {
                            return null == a ? "" : a + ""
                        })), b = n.valHooks[this.type] || n.valHooks[this.nodeName.toLowerCase()], b && "set" in b && void 0 !== b.set(this, e, "value") || (this.value = e))
                });
                if (e) return b = n.valHooks[e.type] || n.valHooks[e.nodeName.toLowerCase()], b && "get" in b && void 0 !== (c = b.get(e, "value")) ? c : (c = e.value, "string" == typeof c ? c.replace(rb, "") : null == c ? "" : c)
            }
        }
    }), n.extend({
        valHooks: {
            option: {
                get: function(a) {
                    var b = n.find.attr(a, "value");
                    return null != b ? b : n.trim(n.text(a)).replace(sb, " ")
                }
            },
            select: {
                get: function(a) {
                    for (var b, c, d = a.options, e = a.selectedIndex, f = "select-one" === a.type || 0 > e, g = f ? null : [], h = f ? e + 1 : d.length, i = 0 > e ? h : f ? e : 0; h > i; i++)
                        if (c = d[i], (c.selected || i === e) && (l.optDisabled ? !c.disabled : null === c.getAttribute("disabled")) && (!c.parentNode.disabled || !n.nodeName(c.parentNode, "optgroup"))) {
                            if (b = n(c).val(), f) return b;
                            g.push(b)
                        }
                    return g
                },
                set: function(a, b) {
                    var c, d, e = a.options,
                        f = n.makeArray(b),
                        g = e.length;
                    while (g--)
                        if (d = e[g], n.inArray(n.valHooks.option.get(d), f) > -1) try {
                            d.selected = c = !0
                        } catch (h) {
                            d.scrollHeight
                        } else d.selected = !1;
                    return c || (a.selectedIndex = -1), e
                }
            }
        }
    }), n.each(["radio", "checkbox"], function() {
        n.valHooks[this] = {
            set: function(a, b) {
                return n.isArray(b) ? a.checked = n.inArray(n(a).val(), b) > -1 : void 0
            }
        }, l.checkOn || (n.valHooks[this].get = function(a) {
            return null === a.getAttribute("value") ? "on" : a.value
        })
    });
    var tb, ub, vb = n.expr.attrHandle,
        wb = /^(?:checked|selected)$/i,
        xb = l.getSetAttribute,
        yb = l.input;
    n.fn.extend({
        attr: function(a, b) {
            return Y(this, n.attr, a, b, arguments.length > 1)
        },
        removeAttr: function(a) {
            return this.each(function() {
                n.removeAttr(this, a)
            })
        }
    }), n.extend({
        attr: function(a, b, c) {
            var d, e, f = a.nodeType;
            if (3 !== f && 8 !== f && 2 !== f) return "undefined" == typeof a.getAttribute ? n.prop(a, b, c) : (1 === f && n.isXMLDoc(a) || (b = b.toLowerCase(), e = n.attrHooks[b] || (n.expr.match.bool.test(b) ? ub : tb)), void 0 !== c ? null === c ? void n.removeAttr(a, b) : e && "set" in e && void 0 !== (d = e.set(a, c, b)) ? d : (a.setAttribute(b, c + ""), c) : e && "get" in e && null !== (d = e.get(a, b)) ? d : (d = n.find.attr(a, b), null == d ? void 0 : d))
        },
        attrHooks: {
            type: {
                set: function(a, b) {
                    if (!l.radioValue && "radio" === b && n.nodeName(a, "input")) {
                        var c = a.value;
                        return a.setAttribute("type", b), c && (a.value = c), b
                    }
                }
            }
        },
        removeAttr: function(a, b) {
            var c, d, e = 0,
                f = b && b.match(G);
            if (f && 1 === a.nodeType)
                while (c = f[e++]) d = n.propFix[c] || c, n.expr.match.bool.test(c) ? yb && xb || !wb.test(c) ? a[d] = !1 : a[n.camelCase("default-" + c)] = a[d] = !1 : n.attr(a, c, ""), a.removeAttribute(xb ? c : d)
        }
    }), ub = {
        set: function(a, b, c) {
            return b === !1 ? n.removeAttr(a, c) : yb && xb || !wb.test(c) ? a.setAttribute(!xb && n.propFix[c] || c, c) : a[n.camelCase("default-" + c)] = a[c] = !0, c
        }
    }, n.each(n.expr.match.bool.source.match(/\w+/g), function(a, b) {
        var c = vb[b] || n.find.attr;
        yb && xb || !wb.test(b) ? vb[b] = function(a, b, d) {
            var e, f;
            return d || (f = vb[b], vb[b] = e, e = null != c(a, b, d) ? b.toLowerCase() : null, vb[b] = f), e
        } : vb[b] = function(a, b, c) {
            return c ? void 0 : a[n.camelCase("default-" + b)] ? b.toLowerCase() : null
        }
    }), yb && xb || (n.attrHooks.value = {
        set: function(a, b, c) {
            return n.nodeName(a, "input") ? void(a.defaultValue = b) : tb && tb.set(a, b, c)
        }
    }), xb || (tb = {
        set: function(a, b, c) {
            var d = a.getAttributeNode(c);
            return d || a.setAttributeNode(d = a.ownerDocument.createAttribute(c)), d.value = b += "", "value" === c || b === a.getAttribute(c) ? b : void 0
        }
    }, vb.id = vb.name = vb.coords = function(a, b, c) {
        var d;
        return c ? void 0 : (d = a.getAttributeNode(b)) && "" !== d.value ? d.value : null
    }, n.valHooks.button = {
        get: function(a, b) {
            var c = a.getAttributeNode(b);
            return c && c.specified ? c.value : void 0
        },
        set: tb.set
    }, n.attrHooks.contenteditable = {
        set: function(a, b, c) {
            tb.set(a, "" === b ? !1 : b, c)
        }
    }, n.each(["width", "height"], function(a, b) {
        n.attrHooks[b] = {
            set: function(a, c) {
                return "" === c ? (a.setAttribute(b, "auto"), c) : void 0
            }
        }
    })), l.style || (n.attrHooks.style = {
        get: function(a) {
            return a.style.cssText || void 0
        },
        set: function(a, b) {
            return a.style.cssText = b + ""
        }
    });
    var zb = /^(?:input|select|textarea|button|object)$/i,
        Ab = /^(?:a|area)$/i;
    n.fn.extend({
        prop: function(a, b) {
            return Y(this, n.prop, a, b, arguments.length > 1)
        },
        removeProp: function(a) {
            return a = n.propFix[a] || a, this.each(function() {
                try {
                    this[a] = void 0, delete this[a]
                } catch (b) {}
            })
        }
    }), n.extend({
        prop: function(a, b, c) {
            var d, e, f = a.nodeType;
            if (3 !== f && 8 !== f && 2 !== f) return 1 === f && n.isXMLDoc(a) || (b = n.propFix[b] || b, e = n.propHooks[b]), void 0 !== c ? e && "set" in e && void 0 !== (d = e.set(a, c, b)) ? d : a[b] = c : e && "get" in e && null !== (d = e.get(a, b)) ? d : a[b]
        },
        propHooks: {
            tabIndex: {
                get: function(a) {
                    var b = n.find.attr(a, "tabindex");
                    return b ? parseInt(b, 10) : zb.test(a.nodeName) || Ab.test(a.nodeName) && a.href ? 0 : -1
                }
            }
        },
        propFix: {
            "for": "htmlFor",
            "class": "className"
        }
    }), l.hrefNormalized || n.each(["href", "src"], function(a, b) {
        n.propHooks[b] = {
            get: function(a) {
                return a.getAttribute(b, 4)
            }
        }
    }), l.optSelected || (n.propHooks.selected = {
        get: function(a) {
            var b = a.parentNode;
            return b && (b.selectedIndex, b.parentNode && b.parentNode.selectedIndex), null
        },
        set: function(a) {
            var b = a.parentNode;
            b && (b.selectedIndex, b.parentNode && b.parentNode.selectedIndex)
        }
    }), n.each(["tabIndex", "readOnly", "maxLength", "cellSpacing", "cellPadding", "rowSpan", "colSpan", "useMap", "frameBorder", "contentEditable"], function() {
        n.propFix[this.toLowerCase()] = this
    }), l.enctype || (n.propFix.enctype = "encoding");
    var Bb = /[\t\r\n\f]/g;

    function Cb(a) {
        return n.attr(a, "class") || ""
    }
    n.fn.extend({
        addClass: function(a) {
            var b, c, d, e, f, g, h, i = 0;
            if (n.isFunction(a)) return this.each(function(b) {
                n(this).addClass(a.call(this, b, Cb(this)))
            });
            if ("string" == typeof a && a) {
                b = a.match(G) || [];
                while (c = this[i++])
                    if (e = Cb(c), d = 1 === c.nodeType && (" " + e + " ").replace(Bb, " ")) {
                        g = 0;
                        while (f = b[g++]) d.indexOf(" " + f + " ") < 0 && (d += f + " ");
                        h = n.trim(d), e !== h && n.attr(c, "class", h)
                    }
            }
            return this
        },
        removeClass: function(a) {
            var b, c, d, e, f, g, h, i = 0;
            if (n.isFunction(a)) return this.each(function(b) {
                n(this).removeClass(a.call(this, b, Cb(this)))
            });
            if (!arguments.length) return this.attr("class", "");
            if ("string" == typeof a && a) {
                b = a.match(G) || [];
                while (c = this[i++])
                    if (e = Cb(c), d = 1 === c.nodeType && (" " + e + " ").replace(Bb, " ")) {
                        g = 0;
                        while (f = b[g++])
                            while (d.indexOf(" " + f + " ") > -1) d = d.replace(" " + f + " ", " ");
                        h = n.trim(d), e !== h && n.attr(c, "class", h)
                    }
            }
            return this
        },
        toggleClass: function(a, b) {
            var c = typeof a;
            return "boolean" == typeof b && "string" === c ? b ? this.addClass(a) : this.removeClass(a) : n.isFunction(a) ? this.each(function(c) {
                n(this).toggleClass(a.call(this, c, Cb(this), b), b)
            }) : this.each(function() {
                var b, d, e, f;
                if ("string" === c) {
                    d = 0, e = n(this), f = a.match(G) || [];
                    while (b = f[d++]) e.hasClass(b) ? e.removeClass(b) : e.addClass(b)
                } else void 0 !== a && "boolean" !== c || (b = Cb(this), b && n._data(this, "__className__", b), n.attr(this, "class", b || a === !1 ? "" : n._data(this, "__className__") || ""))
            })
        },
        hasClass: function(a) {
            var b, c, d = 0;
            b = " " + a + " ";
            while (c = this[d++])
                if (1 === c.nodeType && (" " + Cb(c) + " ").replace(Bb, " ").indexOf(b) > -1) return !0;
            return !1
        }
    }), n.each("blur focus focusin focusout load resize scroll unload click dblclick mousedown mouseup mousemove mouseover mouseout mouseenter mouseleave change select submit keydown keypress keyup error contextmenu".split(" "), function(a, b) {
        n.fn[b] = function(a, c) {
            return arguments.length > 0 ? this.on(b, null, a, c) : this.trigger(b)
        }
    }), n.fn.extend({
        hover: function(a, b) {
            return this.mouseenter(a).mouseleave(b || a)
        }
    });
    var Db = a.location,
        Eb = n.now(),
        Fb = /\?/,
        Gb = /(,)|(\[|{)|(}|])|"(?:[^"\\\r\n]|\\["\\\/bfnrt]|\\u[\da-fA-F]{4})*"\s*:?|true|false|null|-?(?!0\d)\d+(?:\.\d+|)(?:[eE][+-]?\d+|)/g;
    n.parseJSON = function(b) {
        if (a.JSON && a.JSON.parse) return a.JSON.parse(b + "");
        var c, d = null,
            e = n.trim(b + "");
        return e && !n.trim(e.replace(Gb, function(a, b, e, f) {
            return c && b && (d = 0), 0 === d ? a : (c = e || b, d += !f - !e, "")
        })) ? Function("return " + e)() : n.error("Invalid JSON: " + b)
    }, n.parseXML = function(b) {
        var c, d;
        if (!b || "string" != typeof b) return null;
        try {
            a.DOMParser ? (d = new a.DOMParser, c = d.parseFromString(b, "text/xml")) : (c = new a.ActiveXObject("Microsoft.XMLDOM"), c.async = "false", c.loadXML(b))
        } catch (e) {
            c = void 0
        }
        return c && c.documentElement && !c.getElementsByTagName("parsererror").length || n.error("Invalid XML: " + b), c
    };
    var Hb = /#.*$/,
        Ib = /([?&])_=[^&]*/,
        Jb = /^(.*?):[ \t]*([^\r\n]*)\r?$/gm,
        Kb = /^(?:about|app|app-storage|.+-extension|file|res|widget):$/,
        Lb = /^(?:GET|HEAD)$/,
        Mb = /^\/\//,
        Nb = /^([\w.+-]+:)(?:\/\/(?:[^\/?#]*@|)([^\/?#:]*)(?::(\d+)|)|)/,
        Ob = {},
        Pb = {},
        Qb = "*/".concat("*"),
        Rb = Db.href,
        Sb = Nb.exec(Rb.toLowerCase()) || [];

    function Tb(a) {
        return function(b, c) {
            "string" != typeof b && (c = b, b = "*");
            var d, e = 0,
                f = b.toLowerCase().match(G) || [];
            if (n.isFunction(c))
                while (d = f[e++]) "+" === d.charAt(0) ? (d = d.slice(1) || "*", (a[d] = a[d] || []).unshift(c)) : (a[d] = a[d] || []).push(c)
        }
    }

    function Ub(a, b, c, d) {
        var e = {},
            f = a === Pb;

        function g(h) {
            var i;
            return e[h] = !0, n.each(a[h] || [], function(a, h) {
                var j = h(b, c, d);
                return "string" != typeof j || f || e[j] ? f ? !(i = j) : void 0 : (b.dataTypes.unshift(j), g(j), !1)
            }), i
        }
        return g(b.dataTypes[0]) || !e["*"] && g("*")
    }

    function Vb(a, b) {
        var c, d, e = n.ajaxSettings.flatOptions || {};
        for (d in b) void 0 !== b[d] && ((e[d] ? a : c || (c = {}))[d] = b[d]);
        return c && n.extend(!0, a, c), a
    }

    function Wb(a, b, c) {
        var d, e, f, g, h = a.contents,
            i = a.dataTypes;
        while ("*" === i[0]) i.shift(), void 0 === e && (e = a.mimeType || b.getResponseHeader("Content-Type"));
        if (e)
            for (g in h)
                if (h[g] && h[g].test(e)) {
                    i.unshift(g);
                    break
                }
        if (i[0] in c) f = i[0];
        else {
            for (g in c) {
                if (!i[0] || a.converters[g + " " + i[0]]) {
                    f = g;
                    break
                }
                d || (d = g)
            }
            f = f || d
        }
        return f ? (f !== i[0] && i.unshift(f), c[f]) : void 0
    }

    function Xb(a, b, c, d) {
        var e, f, g, h, i, j = {},
            k = a.dataTypes.slice();
        if (k[1])
            for (g in a.converters) j[g.toLowerCase()] = a.converters[g];
        f = k.shift();
        while (f)
            if (a.responseFields[f] && (c[a.responseFields[f]] = b), !i && d && a.dataFilter && (b = a.dataFilter(b, a.dataType)), i = f, f = k.shift())
                if ("*" === f) f = i;
                else if ("*" !== i && i !== f) {
                    if (g = j[i + " " + f] || j["* " + f], !g)
                        for (e in j)
                            if (h = e.split(" "), h[1] === f && (g = j[i + " " + h[0]] || j["* " + h[0]])) {
                                g === !0 ? g = j[e] : j[e] !== !0 && (f = h[0], k.unshift(h[1]));
                                break
                            }
                    if (g !== !0)
                        if (g && a["throws"]) b = g(b);
                        else try {
                            b = g(b)
                        } catch (l) {
                            return {
                                state: "parsererror",
                                error: g ? l : "No conversion from " + i + " to " + f
                            }
                        }
                }
        return {
            state: "success",
            data: b
        }
    }
    n.extend({
        active: 0,
        lastModified: {},
        etag: {},
        ajaxSettings: {
            url: Rb,
            type: "GET",
            isLocal: Kb.test(Sb[1]),
            global: !0,
            processData: !0,
            async: !0,
            contentType: "application/x-www-form-urlencoded; charset=UTF-8",
            accepts: {
                "*": Qb,
                text: "text/plain",
                html: "text/html",
                xml: "application/xml, text/xml",
                json: "application/json, text/javascript"
            },
            contents: {
                xml: /\bxml\b/,
                html: /\bhtml/,
                json: /\bjson\b/
            },
            responseFields: {
                xml: "responseXML",
                text: "responseText",
                json: "responseJSON"
            },
            converters: {
                "* text": String,
                "text html": !0,
                "text json": n.parseJSON,
                "text xml": n.parseXML
            },
            flatOptions: {
                url: !0,
                context: !0
            }
        },
        ajaxSetup: function(a, b) {
            return b ? Vb(Vb(a, n.ajaxSettings), b) : Vb(n.ajaxSettings, a)
        },
        ajaxPrefilter: Tb(Ob),
        ajaxTransport: Tb(Pb),
        ajax: function(b, c) {
            "object" == typeof b && (c = b, b = void 0), c = c || {};
            var d, e, f, g, h, i, j, k, l = n.ajaxSetup({}, c),
                m = l.context || l,
                o = l.context && (m.nodeType || m.jquery) ? n(m) : n.event,
                p = n.Deferred(),
                q = n.Callbacks("once memory"),
                r = l.statusCode || {},
                s = {},
                t = {},
                u = 0,
                v = "canceled",
                w = {
                    readyState: 0,
                    getResponseHeader: function(a) {
                        var b;
                        if (2 === u) {
                            if (!k) {
                                k = {};
                                while (b = Jb.exec(g)) k[b[1].toLowerCase()] = b[2]
                            }
                            b = k[a.toLowerCase()]
                        }
                        return null == b ? null : b
                    },
                    getAllResponseHeaders: function() {
                        return 2 === u ? g : null
                    },
                    setRequestHeader: function(a, b) {
                        var c = a.toLowerCase();
                        return u || (a = t[c] = t[c] || a, s[a] = b), this
                    },
                    overrideMimeType: function(a) {
                        return u || (l.mimeType = a), this
                    },
                    statusCode: function(a) {
                        var b;
                        if (a)
                            if (2 > u)
                                for (b in a) r[b] = [r[b], a[b]];
                            else w.always(a[w.status]);
                        return this
                    },
                    abort: function(a) {
                        var b = a || v;
                        return j && j.abort(b), y(0, b), this
                    }
                };
            if (p.promise(w).complete = q.add, w.success = w.done, w.error = w.fail, l.url = ((b || l.url || Rb) + "").replace(Hb, "").replace(Mb, Sb[1] + "//"), l.type = c.method || c.type || l.method || l.type, l.dataTypes = n.trim(l.dataType || "*").toLowerCase().match(G) || [""], null == l.crossDomain && (d = Nb.exec(l.url.toLowerCase()), l.crossDomain = !(!d || d[1] === Sb[1] && d[2] === Sb[2] && (d[3] || ("http:" === d[1] ? "80" : "443")) === (Sb[3] || ("http:" === Sb[1] ? "80" : "443")))), l.data && l.processData && "string" != typeof l.data && (l.data = n.param(l.data, l.traditional)), Ub(Ob, l, c, w), 2 === u) return w;
            i = n.event && l.global, i && 0 === n.active++ && n.event.trigger("ajaxStart"), l.type = l.type.toUpperCase(), l.hasContent = !Lb.test(l.type), f = l.url, l.hasContent || (l.data && (f = l.url += (Fb.test(f) ? "&" : "?") + l.data, delete l.data), l.cache === !1 && (l.url = Ib.test(f) ? f.replace(Ib, "$1_=" + Eb++) : f + (Fb.test(f) ? "&" : "?") + "_=" + Eb++)), l.ifModified && (n.lastModified[f] && w.setRequestHeader("If-Modified-Since", n.lastModified[f]), n.etag[f] && w.setRequestHeader("If-None-Match", n.etag[f])), (l.data && l.hasContent && l.contentType !== !1 || c.contentType) && w.setRequestHeader("Content-Type", l.contentType), w.setRequestHeader("Accept", l.dataTypes[0] && l.accepts[l.dataTypes[0]] ? l.accepts[l.dataTypes[0]] + ("*" !== l.dataTypes[0] ? ", " + Qb + "; q=0.01" : "") : l.accepts["*"]);
            for (e in l.headers) w.setRequestHeader(e, l.headers[e]);
            if (l.beforeSend && (l.beforeSend.call(m, w, l) === !1 || 2 === u)) return w.abort();
            v = "abort";
            for (e in {
                success: 1,
                error: 1,
                complete: 1
            }) w[e](l[e]);
            if (j = Ub(Pb, l, c, w)) {
                if (w.readyState = 1, i && o.trigger("ajaxSend", [w, l]), 2 === u) return w;
                l.async && l.timeout > 0 && (h = a.setTimeout(function() {
                    w.abort("timeout")
                }, l.timeout));
                try {
                    u = 1, j.send(s, y)
                } catch (x) {
                    if (!(2 > u)) throw x;
                    y(-1, x)
                }
            } else y(-1, "No Transport");

            function y(b, c, d, e) {
                var k, s, t, v, x, y = c;
                2 !== u && (u = 2, h && a.clearTimeout(h), j = void 0, g = e || "", w.readyState = b > 0 ? 4 : 0, k = b >= 200 && 300 > b || 304 === b, d && (v = Wb(l, w, d)), v = Xb(l, v, w, k), k ? (l.ifModified && (x = w.getResponseHeader("Last-Modified"), x && (n.lastModified[f] = x), x = w.getResponseHeader("etag"), x && (n.etag[f] = x)), 204 === b || "HEAD" === l.type ? y = "nocontent" : 304 === b ? y = "notmodified" : (y = v.state, s = v.data, t = v.error, k = !t)) : (t = y, !b && y || (y = "error", 0 > b && (b = 0))), w.status = b, w.statusText = (c || y) + "", k ? p.resolveWith(m, [s, y, w]) : p.rejectWith(m, [w, y, t]), w.statusCode(r), r = void 0, i && o.trigger(k ? "ajaxSuccess" : "ajaxError", [w, l, k ? s : t]), q.fireWith(m, [w, y]), i && (o.trigger("ajaxComplete", [w, l]), --n.active || n.event.trigger("ajaxStop")))
            }
            return w
        },
        getJSON: function(a, b, c) {
            return n.get(a, b, c, "json")
        },
        getScript: function(a, b) {
            return n.get(a, void 0, b, "script")
        }
    }), n.each(["get", "post"], function(a, b) {
        n[b] = function(a, c, d, e) {
            return n.isFunction(c) && (e = e || d, d = c, c = void 0), n.ajax(n.extend({
                url: a,
                type: b,
                dataType: e,
                data: c,
                success: d
            }, n.isPlainObject(a) && a))
        }
    }), n._evalUrl = function(a) {
        return n.ajax({
            url: a,
            type: "GET",
            dataType: "script",
            cache: !0,
            async: !1,
            global: !1,
            "throws": !0
        })
    }, n.fn.extend({
        wrapAll: function(a) {
            if (n.isFunction(a)) return this.each(function(b) {
                n(this).wrapAll(a.call(this, b))
            });
            if (this[0]) {
                var b = n(a, this[0].ownerDocument).eq(0).clone(!0);
                this[0].parentNode && b.insertBefore(this[0]), b.map(function() {
                    var a = this;
                    while (a.firstChild && 1 === a.firstChild.nodeType) a = a.firstChild;
                    return a
                }).append(this)
            }
            return this
        },
        wrapInner: function(a) {
            return n.isFunction(a) ? this.each(function(b) {
                n(this).wrapInner(a.call(this, b))
            }) : this.each(function() {
                var b = n(this),
                    c = b.contents();
                c.length ? c.wrapAll(a) : b.append(a)
            })
        },
        wrap: function(a) {
            var b = n.isFunction(a);
            return this.each(function(c) {
                n(this).wrapAll(b ? a.call(this, c) : a)
            })
        },
        unwrap: function() {
            return this.parent().each(function() {
                n.nodeName(this, "body") || n(this).replaceWith(this.childNodes)
            }).end()
        }
    });

    function Yb(a) {
        return a.style && a.style.display || n.css(a, "display")
    }

    function Zb(a) {
        if (!n.contains(a.ownerDocument || d, a)) return !0;
        while (a && 1 === a.nodeType) {
            if ("none" === Yb(a) || "hidden" === a.type) return !0;
            a = a.parentNode
        }
        return !1
    }
    n.expr.filters.hidden = function(a) {
        return l.reliableHiddenOffsets() ? a.offsetWidth <= 0 && a.offsetHeight <= 0 && !a.getClientRects().length : Zb(a)
    }, n.expr.filters.visible = function(a) {
        return !n.expr.filters.hidden(a)
    };
    var $b = /%20/g,
        _b = /\[\]$/,
        ac = /\r?\n/g,
        bc = /^(?:submit|button|image|reset|file)$/i,
        cc = /^(?:input|select|textarea|keygen)/i;

    function dc(a, b, c, d) {
        var e;
        if (n.isArray(b)) n.each(b, function(b, e) {
            c || _b.test(a) ? d(a, e) : dc(a + "[" + ("object" == typeof e && null != e ? b : "") + "]", e, c, d)
        });
        else if (c || "object" !== n.type(b)) d(a, b);
        else
            for (e in b) dc(a + "[" + e + "]", b[e], c, d)
    }
    n.param = function(a, b) {
        var c, d = [],
            e = function(a, b) {
                b = n.isFunction(b) ? b() : null == b ? "" : b, d[d.length] = encodeURIComponent(a) + "=" + encodeURIComponent(b)
            };
        if (void 0 === b && (b = n.ajaxSettings && n.ajaxSettings.traditional), n.isArray(a) || a.jquery && !n.isPlainObject(a)) n.each(a, function() {
            e(this.name, this.value)
        });
        else
            for (c in a) dc(c, a[c], b, e);
        return d.join("&").replace($b, "+")
    }, n.fn.extend({
        serialize: function() {
            return n.param(this.serializeArray())
        },
        serializeArray: function() {
            return this.map(function() {
                var a = n.prop(this, "elements");
                return a ? n.makeArray(a) : this
            }).filter(function() {
                var a = this.type;
                return this.name && !n(this).is(":disabled") && cc.test(this.nodeName) && !bc.test(a) && (this.checked || !Z.test(a))
            }).map(function(a, b) {
                var c = n(this).val();
                return null == c ? null : n.isArray(c) ? n.map(c, function(a) {
                    return {
                        name: b.name,
                        value: a.replace(ac, "\r\n")
                    }
                }) : {
                    name: b.name,
                    value: c.replace(ac, "\r\n")
                }
            }).get()
        }
    }), n.ajaxSettings.xhr = void 0 !== a.ActiveXObject ? function() {
        return this.isLocal ? ic() : d.documentMode > 8 ? hc() : /^(get|post|head|put|delete|options)$/i.test(this.type) && hc() || ic()
    } : hc;
    var ec = 0,
        fc = {},
        gc = n.ajaxSettings.xhr();
    a.attachEvent && a.attachEvent("onunload", function() {
        for (var a in fc) fc[a](void 0, !0)
    }), l.cors = !!gc && "withCredentials" in gc, gc = l.ajax = !!gc, gc && n.ajaxTransport(function(b) {
        if (!b.crossDomain || l.cors) {
            var c;
            return {
                send: function(d, e) {
                    var f, g = b.xhr(),
                        h = ++ec;
                    if (g.open(b.type, b.url, b.async, b.username, b.password), b.xhrFields)
                        for (f in b.xhrFields) g[f] = b.xhrFields[f];
                    b.mimeType && g.overrideMimeType && g.overrideMimeType(b.mimeType), b.crossDomain || d["X-Requested-With"] || (d["X-Requested-With"] = "XMLHttpRequest");
                    for (f in d) void 0 !== d[f] && g.setRequestHeader(f, d[f] + "");
                    g.send(b.hasContent && b.data || null), c = function(a, d) {
                        var f, i, j;
                        if (c && (d || 4 === g.readyState))
                            if (delete fc[h], c = void 0, g.onreadystatechange = n.noop, d) 4 !== g.readyState && g.abort();
                            else {
                                j = {}, f = g.status, "string" == typeof g.responseText && (j.text = g.responseText);
                                try {
                                    i = g.statusText
                                } catch (k) {
                                    i = ""
                                }
                                f || !b.isLocal || b.crossDomain ? 1223 === f && (f = 204) : f = j.text ? 200 : 404
                            }
                        j && e(f, i, j, g.getAllResponseHeaders())
                    }, b.async ? 4 === g.readyState ? a.setTimeout(c) : g.onreadystatechange = fc[h] = c : c()
                },
                abort: function() {
                    c && c(void 0, !0)
                }
            }
        }
    });

    function hc() {
        try {
            return new a.XMLHttpRequest
        } catch (b) {}
    }

    function ic() {
        try {
            return new a.ActiveXObject("Microsoft.XMLHTTP")
        } catch (b) {}
    }
    n.ajaxSetup({
        accepts: {
            script: "text/javascript, application/javascript, application/ecmascript, application/x-ecmascript"
        },
        contents: {
            script: /\b(?:java|ecma)script\b/
        },
        converters: {
            "text script": function(a) {
                return n.globalEval(a), a
            }
        }
    }), n.ajaxPrefilter("script", function(a) {
        void 0 === a.cache && (a.cache = !1), a.crossDomain && (a.type = "GET", a.global = !1)
    }), n.ajaxTransport("script", function(a) {
        if (a.crossDomain) {
            var b, c = d.head || n("head")[0] || d.documentElement;
            return {
                send: function(e, f) {
                    b = d.createElement("script"), b.async = !0, a.scriptCharset && (b.charset = a.scriptCharset), b.src = a.url, b.onload = b.onreadystatechange = function(a, c) {
                        (c || !b.readyState || /loaded|complete/.test(b.readyState)) && (b.onload = b.onreadystatechange = null, b.parentNode && b.parentNode.removeChild(b), b = null, c || f(200, "success"))
                    }, c.insertBefore(b, c.firstChild)
                },
                abort: function() {
                    b && b.onload(void 0, !0)
                }
            }
        }
    });
    var jc = [],
        kc = /(=)\?(?=&|$)|\?\?/;
    n.ajaxSetup({
        jsonp: "callback",
        jsonpCallback: function() {
            var a = jc.pop() || n.expando + "_" + Eb++;
            return this[a] = !0, a
        }
    }), n.ajaxPrefilter("json jsonp", function(b, c, d) {
        var e, f, g, h = b.jsonp !== !1 && (kc.test(b.url) ? "url" : "string" == typeof b.data && 0 === (b.contentType || "").indexOf("application/x-www-form-urlencoded") && kc.test(b.data) && "data");
        return h || "jsonp" === b.dataTypes[0] ? (e = b.jsonpCallback = n.isFunction(b.jsonpCallback) ? b.jsonpCallback() : b.jsonpCallback, h ? b[h] = b[h].replace(kc, "$1" + e) : b.jsonp !== !1 && (b.url += (Fb.test(b.url) ? "&" : "?") + b.jsonp + "=" + e), b.converters["script json"] = function() {
            return g || n.error(e + " was not called"), g[0]
        }, b.dataTypes[0] = "json", f = a[e], a[e] = function() {
            g = arguments
        }, d.always(function() {
            void 0 === f ? n(a).removeProp(e) : a[e] = f, b[e] && (b.jsonpCallback = c.jsonpCallback, jc.push(e)), g && n.isFunction(f) && f(g[0]), g = f = void 0
        }), "script") : void 0
    }), n.parseHTML = function(a, b, c) {
        if (!a || "string" != typeof a) return null;
        "boolean" == typeof b && (c = b, b = !1), b = b || d;
        var e = x.exec(a),
            f = !c && [];
        return e ? [b.createElement(e[1])] : (e = ja([a], b, f), f && f.length && n(f).remove(), n.merge([], e.childNodes))
    };
    var lc = n.fn.load;
    n.fn.load = function(a, b, c) {
        if ("string" != typeof a && lc) return lc.apply(this, arguments);
        var d, e, f, g = this,
            h = a.indexOf(" ");
        return h > -1 && (d = n.trim(a.slice(h, a.length)), a = a.slice(0, h)), n.isFunction(b) ? (c = b, b = void 0) : b && "object" == typeof b && (e = "POST"), g.length > 0 && n.ajax({
            url: a,
            type: e || "GET",
            dataType: "html",
            data: b
        }).done(function(a) {
            f = arguments, g.html(d ? n("<div>").append(n.parseHTML(a)).find(d) : a)
        }).always(c && function(a, b) {
                g.each(function() {
                    c.apply(this, f || [a.responseText, b, a])
                })
            }), this
    }, n.each(["ajaxStart", "ajaxStop", "ajaxComplete", "ajaxError", "ajaxSuccess", "ajaxSend"], function(a, b) {
        n.fn[b] = function(a) {
            return this.on(b, a)
        }
    }), n.expr.filters.animated = function(a) {
        return n.grep(n.timers, function(b) {
            return a === b.elem
        }).length
    };

    function mc(a) {
        return n.isWindow(a) ? a : 9 === a.nodeType ? a.defaultView || a.parentWindow : !1
    }
    n.offset = {
        setOffset: function(a, b, c) {
            var d, e, f, g, h, i, j, k = n.css(a, "position"),
                l = n(a),
                m = {};
            "static" === k && (a.style.position = "relative"), h = l.offset(), f = n.css(a, "top"), i = n.css(a, "left"), j = ("absolute" === k || "fixed" === k) && n.inArray("auto", [f, i]) > -1, j ? (d = l.position(), g = d.top, e = d.left) : (g = parseFloat(f) || 0, e = parseFloat(i) || 0), n.isFunction(b) && (b = b.call(a, c, n.extend({}, h))), null != b.top && (m.top = b.top - h.top + g), null != b.left && (m.left = b.left - h.left + e), "using" in b ? b.using.call(a, m) : l.css(m)
        }
    }, n.fn.extend({
        offset: function(a) {
            if (arguments.length) return void 0 === a ? this : this.each(function(b) {
                n.offset.setOffset(this, a, b)
            });
            var b, c, d = {
                    top: 0,
                    left: 0
                },
                e = this[0],
                f = e && e.ownerDocument;
            if (f) return b = f.documentElement, n.contains(b, e) ? ("undefined" != typeof e.getBoundingClientRect && (d = e.getBoundingClientRect()), c = mc(f), {
                top: d.top + (c.pageYOffset || b.scrollTop) - (b.clientTop || 0),
                left: d.left + (c.pageXOffset || b.scrollLeft) - (b.clientLeft || 0)
            }) : d
        },
        position: function() {
            if (this[0]) {
                var a, b, c = {
                        top: 0,
                        left: 0
                    },
                    d = this[0];
                return "fixed" === n.css(d, "position") ? b = d.getBoundingClientRect() : (a = this.offsetParent(), b = this.offset(), n.nodeName(a[0], "html") || (c = a.offset()), c.top += n.css(a[0], "borderTopWidth", !0), c.left += n.css(a[0], "borderLeftWidth", !0)), {
                    top: b.top - c.top - n.css(d, "marginTop", !0),
                    left: b.left - c.left - n.css(d, "marginLeft", !0)
                }
            }
        },
        offsetParent: function() {
            return this.map(function() {
                var a = this.offsetParent;
                while (a && !n.nodeName(a, "html") && "static" === n.css(a, "position")) a = a.offsetParent;
                return a || Qa
            })
        }
    }), n.each({
        scrollLeft: "pageXOffset",
        scrollTop: "pageYOffset"
    }, function(a, b) {
        var c = /Y/.test(b);
        n.fn[a] = function(d) {
            return Y(this, function(a, d, e) {
                var f = mc(a);
                return void 0 === e ? f ? b in f ? f[b] : f.document.documentElement[d] : a[d] : void(f ? f.scrollTo(c ? n(f).scrollLeft() : e, c ? e : n(f).scrollTop()) : a[d] = e)
            }, a, d, arguments.length, null)
        }
    }), n.each(["top", "left"], function(a, b) {
        n.cssHooks[b] = Ua(l.pixelPosition, function(a, c) {
            return c ? (c = Sa(a, b), Oa.test(c) ? n(a).position()[b] + "px" : c) : void 0
        })
    }), n.each({
        Height: "height",
        Width: "width"
    }, function(a, b) {
        n.each({
            padding: "inner" + a,
            content: b,
            "": "outer" + a
        }, function(c, d) {
            n.fn[d] = function(d, e) {
                var f = arguments.length && (c || "boolean" != typeof d),
                    g = c || (d === !0 || e === !0 ? "margin" : "border");
                return Y(this, function(b, c, d) {
                    var e;
                    return n.isWindow(b) ? b.document.documentElement["client" + a] : 9 === b.nodeType ? (e = b.documentElement, Math.max(b.body["scroll" + a], e["scroll" + a], b.body["offset" + a], e["offset" + a], e["client" + a])) : void 0 === d ? n.css(b, c, g) : n.style(b, c, d, g)
                }, b, f ? d : void 0, f, null)
            }
        })
    }), n.fn.extend({
        bind: function(a, b, c) {
            return this.on(a, null, b, c)
        },
        unbind: function(a, b) {
            return this.off(a, null, b)
        },
        delegate: function(a, b, c, d) {
            return this.on(b, a, c, d)
        },
        undelegate: function(a, b, c) {
            return 1 === arguments.length ? this.off(a, "**") : this.off(b, a || "**", c)
        }
    }), n.fn.size = function() {
        return this.length
    }, n.fn.andSelf = n.fn.addBack, "function" == typeof define && define.amd && define("jquery", [], function() {
        return n
    });
    var nc = a.jQuery,
        oc = a.$;
    return n.noConflict = function(b) {
        return a.$ === n && (a.$ = oc), b && a.jQuery === n && (a.jQuery = nc), n
    }, b || (a.jQuery = a.$ = n), n
});

/*
 Sticky-kit v1.1.3 | MIT | Leaf Corcoran 2015 | http://leafo.net
 */
(function() {
    var c, f;
    c = window.jQuery;
    f = c(window);
    c.fn.stick_in_parent = function(b) {
        var A, w, J, n, B, K, p, q, L, k, E, t;
        null == b && (b = {});
        t = b.sticky_class;
        B = b.inner_scrolling;
        E = b.recalc_every;
        k = b.parent;
        q = b.offset_top;
        p = b.spacer;
        w = b.bottoming;
        null == q && (q = 0);
        null == k && (k = void 0);
        null == B && (B = !0);
        null == t && (t = "is_stuck");
        A = c(document);
        null == w && (w = !0);
        L = function(a) {
            var b;
            return window.getComputedStyle ? (a = window.getComputedStyle(a[0]), b = parseFloat(a.getPropertyValue("width")) + parseFloat(a.getPropertyValue("margin-left")) +
                parseFloat(a.getPropertyValue("margin-right")), "border-box" !== a.getPropertyValue("box-sizing") && (b += parseFloat(a.getPropertyValue("border-left-width")) + parseFloat(a.getPropertyValue("border-right-width")) + parseFloat(a.getPropertyValue("padding-left")) + parseFloat(a.getPropertyValue("padding-right"))), b) : a.outerWidth(!0)
        };
        J = function(a, b, n, C, F, u, r, G) {
            var v, H, m, D, I, d, g, x, y, z, h, l;
            if (!a.data("sticky_kit")) {
                a.data("sticky_kit", !0);
                I = A.height();
                g = a.parent();
                null != k && (g = g.closest(k));
                if (!g.length) throw "failed to find stick parent";
                v = m = !1;
                (h = null != p ? p && a.closest(p) : c("<div />")) && h.css("position", a.css("position"));
                x = function() {
                    var d, f, e;
                    if (!G && (I = A.height(), d = parseInt(g.css("border-top-width"), 10), f = parseInt(g.css("padding-top"), 10), b = parseInt(g.css("padding-bottom"), 10), n = g.offset().top + d + f, C = g.height(), m && (v = m = !1, null == p && (a.insertAfter(h), h.detach()), a.css({
                            position: "",
                            top: "",
                            width: "",
                            bottom: ""
                        }).removeClass(t), e = !0), F = a.offset().top - (parseInt(a.css("margin-top"), 10) || 0) - q, u = a.outerHeight(!0), r = a.css("float"), h && h.css({
                            width: L(a),
                            height: u,
                            display: a.css("display"),
                            "vertical-align": a.css("vertical-align"),
                            "float": r
                        }), e)) return l()
                };
                x();
                if (u !== C) return D = void 0, d = q, z = E, l = function() {
                    var c, l, e, k;
                    if (!G && (e = !1, null != z && (--z, 0 >= z && (z = E, x(), e = !0)), e || A.height() === I || x(), e = f.scrollTop(), null != D && (l = e - D), D = e, m ? (w && (k = e + u + d > C + n, v && !k && (v = !1, a.css({
                            position: "fixed",
                            bottom: "",
                            top: d
                        }).trigger("sticky_kit:unbottom"))), e < F && (m = !1, d = q, null == p && ("left" !== r && "right" !== r || a.insertAfter(h), h.detach()), c = {
                            position: "",
                            width: "",
                            top: ""
                        }, a.css(c).removeClass(t).trigger("sticky_kit:unstick")),
                        B && (c = f.height(), u + q > c && !v && (d -= l, d = Math.max(c - u, d), d = Math.min(q, d), m && a.css({
                            top: d + "px"
                        })))) : e > F && (m = !0, c = {
                                position: "fixed",
                                top: d
                            }, c.width = "border-box" === a.css("box-sizing") ? a.outerWidth() + "px" : a.width() + "px", a.css(c).addClass(t), null == p && (a.after(h), "left" !== r && "right" !== r || h.append(a)), a.trigger("sticky_kit:stick")), m && w && (null == k && (k = e + u + d > C + n), !v && k))) return v = !0, "static" === g.css("position") && g.css({
                        position: "relative"
                    }), a.css({
                        position: "absolute",
                        bottom: b,
                        top: "auto"
                    }).trigger("sticky_kit:bottom")
                },
                    y = function() {
                        x();
                        return l()
                    }, H = function() {
                    G = !0;
                    f.off("touchmove", l);
                    f.off("scroll", l);
                    f.off("resize", y);
                    c(document.body).off("sticky_kit:recalc", y);
                    a.off("sticky_kit:detach", H);
                    a.removeData("sticky_kit");
                    a.css({
                        position: "",
                        bottom: "",
                        top: "",
                        width: ""
                    });
                    g.position("position", "");
                    if (m) return null == p && ("left" !== r && "right" !== r || a.insertAfter(h), h.remove()), a.removeClass(t)
                }, f.on("touchmove", l), f.on("scroll", l), f.on("resize", y), c(document.body).on("sticky_kit:recalc", y), a.on("sticky_kit:detach", H), setTimeout(l,
                    0)
            }
        };
        n = 0;
        for (K = this.length; n < K; n++) b = this[n], J(c(b));
        return this
    }
}).call(this);

/*!
 * accounting.js v0.4.2, copyright 2014 Open Exchange Rates, MIT license, http://openexchangerates.github.io/accounting.js
 */
(function(p, z) {
    function q(a) {
        return !!("" === a || a && a.charCodeAt && a.substr)
    }

    function m(a) {
        return u ? u(a) : "[object Array]" === v.call(a)
    }

    function r(a) {
        return "[object Object]" === v.call(a)
    }

    function s(a, b) {
        var d, a = a || {},
            b = b || {};
        for (d in b) b.hasOwnProperty(d) && null == a[d] && (a[d] = b[d]);
        return a
    }

    function j(a, b, d) {
        var c = [],
            e, h;
        if (!a) return c;
        if (w && a.map === w) return a.map(b, d);
        for (e = 0, h = a.length; e < h; e++) c[e] = b.call(d, a[e], e, a);
        return c
    }

    function n(a, b) {
        a = Math.round(Math.abs(a));
        return isNaN(a) ? b : a
    }

    function x(a) {
        var b = c.settings.currency.format;
        "function" === typeof a && (a = a());
        return q(a) && a.match("%v") ? {
            pos: a,
            neg: a.replace("-", "").replace("%v", "-%v"),
            zero: a
        } : !a || !a.pos || !a.pos.match("%v") ? !q(b) ? b : c.settings.currency.format = {
            pos: b,
            neg: b.replace("%v", "-%v"),
            zero: b
        } : a
    }
    var c = {
            version: "0.4.1",
            settings: {
                currency: {
                    symbol: "$",
                    format: "%s%v",
                    decimal: ".",
                    thousand: ",",
                    precision: 2,
                    grouping: 3
                },
                number: {
                    precision: 0,
                    grouping: 3,
                    thousand: ",",
                    decimal: "."
                }
            }
        },
        w = Array.prototype.map,
        u = Array.isArray,
        v = Object.prototype.toString,
        o = c.unformat = c.parse = function(a, b) {
            if (m(a)) return j(a, function(a) {
                return o(a, b)
            });
            a = a || 0;
            if ("number" === typeof a) return a;
            var b = b || ".",
                c = RegExp("[^0-9-" + b + "]", ["g"]),
                c = parseFloat(("" + a).replace(/\((.*)\)/, "-$1").replace(c, "").replace(b, "."));
            return !isNaN(c) ? c : 0
        },
        y = c.toFixed = function(a, b) {
            var b = n(b, c.settings.number.precision),
                d = Math.pow(10, b);
            return (Math.round(c.unformat(a) * d) / d).toFixed(b)
        },
        t = c.formatNumber = c.format = function(a, b, d, i) {
            if (m(a)) return j(a, function(a) {
                return t(a, b, d, i)
            });
            var a = o(a),
                e = s(r(b) ? b : {
                    precision: b,
                    thousand: d,
                    decimal: i
                }, c.settings.number),
                h = n(e.precision),
                f = 0 > a ? "-" : "",
                g = parseInt(y(Math.abs(a || 0), h), 10) + "",
                l = 3 < g.length ? g.length % 3 : 0;
            return f + (l ? g.substr(0, l) + e.thousand : "") + g.substr(l).replace(/(\d{3})(?=\d)/g, "$1" + e.thousand) + (h ? e.decimal + y(Math.abs(a), h).split(".")[1] : "")
        },
        A = c.formatMoney = function(a, b, d, i, e, h) {
            if (m(a)) return j(a, function(a) {
                return A(a, b, d, i, e, h)
            });
            var a = o(a),
                f = s(r(b) ? b : {
                    symbol: b,
                    precision: d,
                    thousand: i,
                    decimal: e,
                    format: h
                }, c.settings.currency),
                g = x(f.format);
            return (0 < a ? g.pos : 0 > a ? g.neg : g.zero).replace("%s", f.symbol).replace("%v", t(Math.abs(a), n(f.precision), f.thousand, f.decimal))
        };
    c.formatColumn = function(a, b, d, i, e, h) {
        if (!a) return [];
        var f = s(r(b) ? b : {
                symbol: b,
                precision: d,
                thousand: i,
                decimal: e,
                format: h
            }, c.settings.currency),
            g = x(f.format),
            l = g.pos.indexOf("%s") < g.pos.indexOf("%v") ? !0 : !1,
            k = 0,
            a = j(a, function(a) {
                if (m(a)) return c.formatColumn(a, f);
                a = o(a);
                a = (0 < a ? g.pos : 0 > a ? g.neg : g.zero).replace("%s", f.symbol).replace("%v", t(Math.abs(a), n(f.precision), f.thousand, f.decimal));
                if (a.length > k) k = a.length;
                return a
            });
        return j(a, function(a) {
            return q(a) && a.length < k ? l ? a.replace(f.symbol, f.symbol + Array(k - a.length + 1).join(" ")) : Array(k - a.length + 1).join(" ") + a : a
        })
    };
    if ("undefined" !== typeof exports) {
        if ("undefined" !== typeof module && module.exports) exports = module.exports = c;
        exports.accounting = c
    } else "function" === typeof define && define.amd ? define([], function() {
        return c
    }) : (c.noConflict = function(a) {
        return function() {
            p.accounting = a;
            c.noConflict = z;
            return c
        }
    }(p.accounting), p.accounting = c)
})(this);

/*!
 * Bootstrap v3.3.7 (http://getbootstrap.com)
 * Copyright 2011-2016 Twitter, Inc.
 * Licensed under the MIT license
 */
if ("undefined" == typeof jQuery) throw new Error("Bootstrap's JavaScript requires jQuery"); + function(a) {
    "use strict";
    var b = a.fn.jquery.split(" ")[0].split(".");
    if (b[0] < 2 && b[1] < 9 || 1 == b[0] && 9 == b[1] && b[2] < 1 || b[0] > 3) throw new Error("Bootstrap's JavaScript requires jQuery version 1.9.1 or higher, but lower than version 4")
}(jQuery), + function(a) {
    "use strict";

    function b() {
        var a = document.createElement("bootstrap"),
            b = {
                WebkitTransition: "webkitTransitionEnd",
                MozTransition: "transitionend",
                OTransition: "oTransitionEnd otransitionend",
                transition: "transitionend"
            };
        for (var c in b)
            if (void 0 !== a.style[c]) return {
                end: b[c]
            };
        return !1
    }
    a.fn.emulateTransitionEnd = function(b) {
        var c = !1,
            d = this;
        a(this).one("bsTransitionEnd", function() {
            c = !0
        });
        var e = function() {
            c || a(d).trigger(a.support.transition.end)
        };
        return setTimeout(e, b), this
    }, a(function() {
        a.support.transition = b(), a.support.transition && (a.event.special.bsTransitionEnd = {
            bindType: a.support.transition.end,
            delegateType: a.support.transition.end,
            handle: function(b) {
                if (a(b.target).is(this)) return b.handleObj.handler.apply(this, arguments)
            }
        })
    })
}(jQuery), + function(a) {
    "use strict";

    function b(b) {
        return this.each(function() {
            var c = a(this),
                e = c.data("bs.alert");
            e || c.data("bs.alert", e = new d(this)), "string" == typeof b && e[b].call(c)
        })
    }
    var c = '[data-dismiss="alert"]',
        d = function(b) {
            a(b).on("click", c, this.close)
        };
    d.VERSION = "3.3.7", d.TRANSITION_DURATION = 150, d.prototype.close = function(b) {
        function c() {
            g.detach().trigger("closed.bs.alert").remove()
        }
        var e = a(this),
            f = e.attr("data-target");
        f || (f = e.attr("href"), f = f && f.replace(/.*(?=#[^\s]*$)/, ""));
        var g = a("#" === f ? [] : f);
        b && b.preventDefault(), g.length || (g = e.closest(".alert")), g.trigger(b = a.Event("close.bs.alert")), b.isDefaultPrevented() || (g.removeClass("in"), a.support.transition && g.hasClass("fade") ? g.one("bsTransitionEnd", c).emulateTransitionEnd(d.TRANSITION_DURATION) : c())
    };
    var e = a.fn.alert;
    a.fn.alert = b, a.fn.alert.Constructor = d, a.fn.alert.noConflict = function() {
        return a.fn.alert = e, this
    }, a(document).on("click.bs.alert.data-api", c, d.prototype.close)
}(jQuery), + function(a) {
    "use strict";

    function b(b) {
        return this.each(function() {
            var d = a(this),
                e = d.data("bs.button"),
                f = "object" == typeof b && b;
            e || d.data("bs.button", e = new c(this, f)), "toggle" == b ? e.toggle() : b && e.setState(b)
        })
    }
    var c = function(b, d) {
        this.$element = a(b), this.options = a.extend({}, c.DEFAULTS, d), this.isLoading = !1
    };
    c.VERSION = "3.3.7", c.DEFAULTS = {
        loadingText: "loading..."
    }, c.prototype.setState = function(b) {
        var c = "disabled",
            d = this.$element,
            e = d.is("input") ? "val" : "html",
            f = d.data();
        b += "Text", null == f.resetText && d.data("resetText", d[e]()), setTimeout(a.proxy(function() {
            d[e](null == f[b] ? this.options[b] : f[b]), "loadingText" == b ? (this.isLoading = !0, d.addClass(c).attr(c, c).prop(c, !0)) : this.isLoading && (this.isLoading = !1, d.removeClass(c).removeAttr(c).prop(c, !1))
        }, this), 0)
    }, c.prototype.toggle = function() {
        var a = !0,
            b = this.$element.closest('[data-toggle="buttons"]');
        if (b.length) {
            var c = this.$element.find("input");
            "radio" == c.prop("type") ? (c.prop("checked") && (a = !1), b.find(".active").removeClass("active"), this.$element.addClass("active")) : "checkbox" == c.prop("type") && (c.prop("checked") !== this.$element.hasClass("active") && (a = !1), this.$element.toggleClass("active")), c.prop("checked", this.$element.hasClass("active")), a && c.trigger("change")
        } else this.$element.attr("aria-pressed", !this.$element.hasClass("active")), this.$element.toggleClass("active")
    };
    var d = a.fn.button;
    a.fn.button = b, a.fn.button.Constructor = c, a.fn.button.noConflict = function() {
        return a.fn.button = d, this
    }, a(document).on("click.bs.button.data-api", '[data-toggle^="button"]', function(c) {
        var d = a(c.target).closest(".btn");
        b.call(d, "toggle"), a(c.target).is('input[type="radio"], input[type="checkbox"]') || (c.preventDefault(), d.is("input,button") ? d.trigger("focus") : d.find("input:visible,button:visible").first().trigger("focus"))
    }).on("focus.bs.button.data-api blur.bs.button.data-api", '[data-toggle^="button"]', function(b) {
        a(b.target).closest(".btn").toggleClass("focus", /^focus(in)?$/.test(b.type))
    })
}(jQuery), + function(a) {
    "use strict";

    function b(b) {
        return this.each(function() {

            var d = a(this),
                e = d.data("bs.carousel"),
                f = a.extend({}, c.DEFAULTS, d.data(), "object" == typeof b && b),
                g = "string" == typeof b ? b : f.slide;
            e || d.data("bs.carousel", e = new c(this, f)), "number" == typeof b ? e.to(b) : g ? e[g]() : f.interval && e.pause().cycle()
        })
    }
    var c = function(b, c) {
        this.$element = a(b), this.$indicators = this.$element.find(".carousel-indicators"), this.options = c, this.paused = null, this.sliding = null, this.interval = null, this.$active = null, this.$items = null, this.options.keyboard && this.$element.on("keydown.bs.carousel", a.proxy(this.keydown, this)), "hover" == this.options.pause && !("ontouchstart" in document.documentElement) && this.$element.on("mouseenter.bs.carousel", a.proxy(this.pause, this)).on("mouseleave.bs.carousel", a.proxy(this.cycle, this))
    };
    c.VERSION = "3.3.7", c.TRANSITION_DURATION = 600, c.DEFAULTS = {
        interval: 5e3,
        pause: "hover",
        wrap: !0,
        keyboard: !0
    }, c.prototype.keydown = function(a) {
        if (!/input|textarea/i.test(a.target.tagName)) {
            switch (a.which) {
                case 37:
                    this.prev();
                    break;
                case 39:
                    this.next();
                    break;
                default:
                    return
            }
            a.preventDefault()
        }
    }, c.prototype.cycle = function(b) {
        return b || (this.paused = !1), this.interval && clearInterval(this.interval), this.options.interval && !this.paused && (this.interval = setInterval(a.proxy(this.next, this), this.options.interval)), this
    }, c.prototype.getItemIndex = function(a) {
        return this.$items = a.parent().children(".item"), this.$items.index(a || this.$active)
    }, c.prototype.getItemForDirection = function(a, b) {
        var c = this.getItemIndex(b),
            d = "prev" == a && 0 === c || "next" == a && c == this.$items.length - 1;
        if (d && !this.options.wrap) return b;
        var e = "prev" == a ? -1 : 1,
            f = (c + e) % this.$items.length;
        return this.$items.eq(f)
    }, c.prototype.to = function(a) {
        var b = this,
            c = this.getItemIndex(this.$active = this.$element.find(".item.active"));
        if (!(a > this.$items.length - 1 || a < 0)) return this.sliding ? this.$element.one("slid.bs.carousel", function() {
            b.to(a)
        }) : c == a ? this.pause().cycle() : this.slide(a > c ? "next" : "prev", this.$items.eq(a))
    }, c.prototype.pause = function(b) {
        return b || (this.paused = !0), this.$element.find(".next, .prev").length && a.support.transition && (this.$element.trigger(a.support.transition.end), this.cycle(!0)), this.interval = clearInterval(this.interval), this
    }, c.prototype.next = function() {
        if (!this.sliding) return this.slide("next")
    }, c.prototype.prev = function() {
        if (!this.sliding) return this.slide("prev")
    }, c.prototype.slide = function(b, d) {
        // alert('sd');
        var e = this.$element.find(".item.active"),
            f = d || this.getItemForDirection(b, e),
            g = this.interval,
            h = "next" == b ? "left" : "right",
            i = this;
        if (f.hasClass("active")) return this.sliding = !1;
        var j = f[0],
            k = a.Event("slide.bs.carousel", {
                relatedTarget: j,
                direction: h
            });
        if (this.$element.trigger(k), !k.isDefaultPrevented()) {
            if (this.sliding = !0, g && this.pause(), this.$indicators.length) {
                this.$indicators.find(".active").removeClass("active");
                var l = a(this.$indicators.children()[this.getItemIndex(f)]);
                l && l.addClass("active")
            }
            var m = a.Event("slide.bs.carousel", {
                relatedTarget: j,
                direction: h
            });



            return a.support.transition && this.$element.hasClass("slide") ? (f.addClass(b), f[0].offsetWidth, e.addClass(h), f.addClass(h), e.one("bsTransitionEnd", function() {

                f.removeClass([b, h].join(" ")).addClass("active"), e.removeClass(["active", h].join(" ")), i.sliding = !1, setTimeout(function() {
                    i.$element.trigger(m)
                }, 0)
            }).emulateTransitionEnd(c.TRANSITION_DURATION)) : (e.removeClass("active"), f.addClass("active"), this.sliding = !1, this.$element.trigger(m)), g && this.cycle(), this
        }
    };
    var d = a.fn.carousel;
    a.fn.carousel = b, a.fn.carousel.Constructor = c, a.fn.carousel.noConflict = function() {
        return a.fn.carousel = d, this
    };
    var e = function(c) {
        var d, e = a(this),
            f = a(e.attr("data-target") || (d = e.attr("href")) && d.replace(/.*(?=#[^\s]+$)/, ""));
        if (f.hasClass("carousel")) {
            var g = a.extend({}, f.data(), e.data()),
                h = e.attr("data-slide-to");
            h && (g.interval = !1), b.call(f, g), h && f.data("bs.carousel").to(h), c.preventDefault()
        }
    };
    a(document).on("click.bs.carousel.data-api", "[data-slide]", e).on("click.bs.carousel.data-api", "[data-slide-to]", e), a(window).on("load", function() {
        a('[data-ride="carousel"]').each(function() {
            var c = a(this);
            $('.carousel-caption').hide();
            $('.carousel-caption').show(1000);

            var counter = 0;
            var progressBar;
            progressBar = setInterval(function() {
                counter += 1;
                if (counter > 100) clearInterval(progressBar);
                $(".progressbar").css("width", counter + "%");
            }, 20);
            b.call(c, c.data())
        })
    })
}(jQuery), + function(a) {

    "use strict";

    function b(b) {
        var c, d = b.attr("data-target") || (c = b.attr("href")) && c.replace(/.*(?=#[^\s]+$)/, "");
        return a(d)
    }

    function c(b) {
        return this.each(function() {
            var c = a(this),
                e = c.data("bs.collapse"),
                f = a.extend({}, d.DEFAULTS, c.data(), "object" == typeof b && b);
            !e && f.toggle && /show|hide/.test(b) && (f.toggle = !1), e || c.data("bs.collapse", e = new d(this, f)), "string" == typeof b && e[b]()
        })
    }
    var d = function(b, c) {
        this.$element = a(b), this.options = a.extend({}, d.DEFAULTS, c), this.$trigger = a('[data-toggle="collapse"][href="#' + b.id + '"],[data-toggle="collapse"][data-target="#' + b.id + '"]'), this.transitioning = null, this.options.parent ? this.$parent = this.getParent() : this.addAriaAndCollapsedClass(this.$element, this.$trigger), this.options.toggle && this.toggle()
    };
    d.VERSION = "3.3.7", d.TRANSITION_DURATION = 350, d.DEFAULTS = {
        toggle: !0
    }, d.prototype.dimension = function() {
        var a = this.$element.hasClass("width");
        return a ? "width" : "height"
    }, d.prototype.show = function() {
        if (!this.transitioning && !this.$element.hasClass("in")) {
            var b, e = this.$parent && this.$parent.children(".panel").children(".in, .collapsing");
            if (!(e && e.length && (b = e.data("bs.collapse"), b && b.transitioning))) {
                var f = a.Event("show.bs.collapse");
                if (this.$element.trigger(f), !f.isDefaultPrevented()) {
                    e && e.length && (c.call(e, "hide"), b || e.data("bs.collapse", null));
                    var g = this.dimension();
                    this.$element.removeClass("collapse").addClass("collapsing")[g](0).attr("aria-expanded", !0), this.$trigger.removeClass("collapsed").attr("aria-expanded", !0), this.transitioning = 1;
                    var h = function() {
                        this.$element.removeClass("collapsing").addClass("collapse in")[g](""), this.transitioning = 0, this.$element.trigger("shown.bs.collapse")
                    };
                    if (!a.support.transition) return h.call(this);
                    var i = a.camelCase(["scroll", g].join("-"));
                    this.$element.one("bsTransitionEnd", a.proxy(h, this)).emulateTransitionEnd(d.TRANSITION_DURATION)[g](this.$element[0][i])
                }
            }
        }
    }, d.prototype.hide = function() {
        if (!this.transitioning && this.$element.hasClass("in")) {
            var b = a.Event("hide.bs.collapse");
            if (this.$element.trigger(b), !b.isDefaultPrevented()) {
                var c = this.dimension();
                this.$element[c](this.$element[c]())[0].offsetHeight, this.$element.addClass("collapsing").removeClass("collapse in").attr("aria-expanded", !1), this.$trigger.addClass("collapsed").attr("aria-expanded", !1), this.transitioning = 1;
                var e = function() {
                    this.transitioning = 0, this.$element.removeClass("collapsing").addClass("collapse").trigger("hidden.bs.collapse")
                };
                return a.support.transition ? void this.$element[c](0).one("bsTransitionEnd", a.proxy(e, this)).emulateTransitionEnd(d.TRANSITION_DURATION) : e.call(this)
            }
        }
    }, d.prototype.toggle = function() {
        this[this.$element.hasClass("in") ? "hide" : "show"]()
    }, d.prototype.getParent = function() {
        return a(this.options.parent).find('[data-toggle="collapse"][data-parent="' + this.options.parent + '"]').each(a.proxy(function(c, d) {
            var e = a(d);
            this.addAriaAndCollapsedClass(b(e), e)
        }, this)).end()
    }, d.prototype.addAriaAndCollapsedClass = function(a, b) {
        var c = a.hasClass("in");
        a.attr("aria-expanded", c), b.toggleClass("collapsed", !c).attr("aria-expanded", c)
    };
    var e = a.fn.collapse;
    a.fn.collapse = c, a.fn.collapse.Constructor = d, a.fn.collapse.noConflict = function() {
        return a.fn.collapse = e, this
    }, a(document).on("click.bs.collapse.data-api", '[data-toggle="collapse"]', function(d) {
        var e = a(this);
        e.attr("data-target") || d.preventDefault();
        var f = b(e),
            g = f.data("bs.collapse"),
            h = g ? "toggle" : e.data();
        c.call(f, h)
    })
}(jQuery), + function(a) {
    "use strict";

    function b(b) {
        var c = b.attr("data-target");
        c || (c = b.attr("href"), c = c && /#[A-Za-z]/.test(c) && c.replace(/.*(?=#[^\s]*$)/, ""));
        var d = c && a(c);
        return d && d.length ? d : b.parent()
    }

    function c(c) {
        c && 3 === c.which || (a(e).remove(), a(f).each(function() {
            var d = a(this),
                e = b(d),
                f = {
                    relatedTarget: this
                };
            e.hasClass("open") && (c && "click" == c.type && /input|textarea/i.test(c.target.tagName) && a.contains(e[0], c.target) || (e.trigger(c = a.Event("hide.bs.dropdown", f)), c.isDefaultPrevented() || (d.attr("aria-expanded", "false"), e.removeClass("open").trigger(a.Event("hidden.bs.dropdown", f)))))
        }))
    }

    function d(b) {
        return this.each(function() {
            var c = a(this),
                d = c.data("bs.dropdown");
            d || c.data("bs.dropdown", d = new g(this)), "string" == typeof b && d[b].call(c)
        })
    }
    var e = ".dropdown-backdrop",
        f = '[data-toggle="dropdown"]',
        g = function(b) {
            a(b).on("click.bs.dropdown", this.toggle)
        };
    g.VERSION = "3.3.7", g.prototype.toggle = function(d) {
        var e = a(this);
        if (!e.is(".disabled, :disabled")) {
            var f = b(e),
                g = f.hasClass("open");
            if (c(), !g) {
                "ontouchstart" in document.documentElement && !f.closest(".navbar-nav").length && a(document.createElement("div")).addClass("dropdown-backdrop").insertAfter(a(this)).on("click", c);
                var h = {
                    relatedTarget: this
                };
                if (f.trigger(d = a.Event("show.bs.dropdown", h)), d.isDefaultPrevented()) return;
                e.trigger("focus").attr("aria-expanded", "true"), f.toggleClass("open").trigger(a.Event("shown.bs.dropdown", h))
            }
            return !1
        }
    }, g.prototype.keydown = function(c) {
        if (/(38|40|27|32)/.test(c.which) && !/input|textarea/i.test(c.target.tagName)) {
            var d = a(this);
            if (c.preventDefault(), c.stopPropagation(), !d.is(".disabled, :disabled")) {
                var e = b(d),
                    g = e.hasClass("open");
                if (!g && 27 != c.which || g && 27 == c.which) return 27 == c.which && e.find(f).trigger("focus"), d.trigger("click");
                var h = " li:not(.disabled):visible a",
                    i = e.find(".dropdown-menu" + h);
                if (i.length) {
                    var j = i.index(c.target);
                    38 == c.which && j > 0 && j--, 40 == c.which && j < i.length - 1 && j++, ~j || (j = 0), i.eq(j).trigger("focus")
                }
            }
        }
    };
    var h = a.fn.dropdown;
    a.fn.dropdown = d, a.fn.dropdown.Constructor = g, a.fn.dropdown.noConflict = function() {
        return a.fn.dropdown = h, this
    }, a(document).on("click.bs.dropdown.data-api", c).on("click.bs.dropdown.data-api", ".dropdown form", function(a) {
        a.stopPropagation()
    }).on("click.bs.dropdown.data-api", f, g.prototype.toggle).on("keydown.bs.dropdown.data-api", f, g.prototype.keydown).on("keydown.bs.dropdown.data-api", ".dropdown-menu", g.prototype.keydown)
}(jQuery), + function(a) {
    "use strict";

    function b(b, d) {
        return this.each(function() {
            var e = a(this),
                f = e.data("bs.modal"),
                g = a.extend({}, c.DEFAULTS, e.data(), "object" == typeof b && b);
            f || e.data("bs.modal", f = new c(this, g)), "string" == typeof b ? f[b](d) : g.show && f.show(d)
        })
    }
    var c = function(b, c) {
        this.options = c, this.$body = a(document.body), this.$element = a(b), this.$dialog = this.$element.find(".modal-dialog"), this.$backdrop = null, this.isShown = null, this.originalBodyPad = null, this.scrollbarWidth = 0, this.ignoreBackdropClick = !1, this.options.remote && this.$element.find(".modal-content").load(this.options.remote, a.proxy(function() {
            this.$element.trigger("loaded.bs.modal")
        }, this))
    };
    c.VERSION = "3.3.7", c.TRANSITION_DURATION = 300, c.BACKDROP_TRANSITION_DURATION = 150, c.DEFAULTS = {
        backdrop: !0,
        keyboard: !0,
        show: !0
    }, c.prototype.toggle = function(a) {
        return this.isShown ? this.hide() : this.show(a)
    }, c.prototype.show = function(b) {
        var d = this,
            e = a.Event("show.bs.modal", {
                relatedTarget: b
            });
        this.$element.trigger(e), this.isShown || e.isDefaultPrevented() || (this.isShown = !0, this.checkScrollbar(), this.setScrollbar(), this.$body.addClass("modal-open"), this.escape(), this.resize(), this.$element.on("click.dismiss.bs.modal", '[data-dismiss="modal"]', a.proxy(this.hide, this)), this.$dialog.on("mousedown.dismiss.bs.modal", function() {
            d.$element.one("mouseup.dismiss.bs.modal", function(b) {
                a(b.target).is(d.$element) && (d.ignoreBackdropClick = !0)
            })
        }), this.backdrop(function() {
            var e = a.support.transition && d.$element.hasClass("fade");
            d.$element.parent().length || d.$element.appendTo(d.$body), d.$element.show().scrollTop(0), d.adjustDialog(), e && d.$element[0].offsetWidth, d.$element.addClass("in"), d.enforceFocus();
            var f = a.Event("shown.bs.modal", {
                relatedTarget: b
            });
            e ? d.$dialog.one("bsTransitionEnd", function() {
                d.$element.trigger("focus").trigger(f)
            }).emulateTransitionEnd(c.TRANSITION_DURATION) : d.$element.trigger("focus").trigger(f)
        }))
    }, c.prototype.hide = function(b) {
        b && b.preventDefault(), b = a.Event("hide.bs.modal"), this.$element.trigger(b), this.isShown && !b.isDefaultPrevented() && (this.isShown = !1, this.escape(), this.resize(), a(document).off("focusin.bs.modal"), this.$element.removeClass("in").off("click.dismiss.bs.modal").off("mouseup.dismiss.bs.modal"), this.$dialog.off("mousedown.dismiss.bs.modal"), a.support.transition && this.$element.hasClass("fade") ? this.$element.one("bsTransitionEnd", a.proxy(this.hideModal, this)).emulateTransitionEnd(c.TRANSITION_DURATION) : this.hideModal())
    }, c.prototype.enforceFocus = function() {
        a(document).off("focusin.bs.modal").on("focusin.bs.modal", a.proxy(function(a) {
            document === a.target || this.$element[0] === a.target || this.$element.has(a.target).length || this.$element.trigger("focus")
        }, this))
    }, c.prototype.escape = function() {
        this.isShown && this.options.keyboard ? this.$element.on("keydown.dismiss.bs.modal", a.proxy(function(a) {
            27 == a.which && this.hide()
        }, this)) : this.isShown || this.$element.off("keydown.dismiss.bs.modal")
    }, c.prototype.resize = function() {
        this.isShown ? a(window).on("resize.bs.modal", a.proxy(this.handleUpdate, this)) : a(window).off("resize.bs.modal")
    }, c.prototype.hideModal = function() {
        var a = this;
        this.$element.hide(), this.backdrop(function() {
            a.$body.removeClass("modal-open"), a.resetAdjustments(), a.resetScrollbar(), a.$element.trigger("hidden.bs.modal")
        })
    }, c.prototype.removeBackdrop = function() {
        this.$backdrop && this.$backdrop.remove(), this.$backdrop = null
    }, c.prototype.backdrop = function(b) {
        var d = this,
            e = this.$element.hasClass("fade") ? "fade" : "";
        if (this.isShown && this.options.backdrop) {
            var f = a.support.transition && e;
            if (this.$backdrop = a(document.createElement("div")).addClass("modal-backdrop " + e).appendTo(this.$body), this.$element.on("click.dismiss.bs.modal", a.proxy(function(a) {
                    return this.ignoreBackdropClick ? void(this.ignoreBackdropClick = !1) : void(a.target === a.currentTarget && ("static" == this.options.backdrop ? this.$element[0].focus() : this.hide()))
                }, this)), f && this.$backdrop[0].offsetWidth, this.$backdrop.addClass("in"), !b) return;
            f ? this.$backdrop.one("bsTransitionEnd", b).emulateTransitionEnd(c.BACKDROP_TRANSITION_DURATION) : b()
        } else if (!this.isShown && this.$backdrop) {
            this.$backdrop.removeClass("in");
            var g = function() {
                d.removeBackdrop(), b && b()
            };
            a.support.transition && this.$element.hasClass("fade") ? this.$backdrop.one("bsTransitionEnd", g).emulateTransitionEnd(c.BACKDROP_TRANSITION_DURATION) : g()
        } else b && b()
    }, c.prototype.handleUpdate = function() {
        this.adjustDialog()
    }, c.prototype.adjustDialog = function() {
        var a = this.$element[0].scrollHeight > document.documentElement.clientHeight;
        this.$element.css({
            paddingLeft: !this.bodyIsOverflowing && a ? this.scrollbarWidth : "",
            paddingRight: this.bodyIsOverflowing && !a ? this.scrollbarWidth : ""
        })
    }, c.prototype.resetAdjustments = function() {
        this.$element.css({
            paddingLeft: "",
            paddingRight: ""
        })
    }, c.prototype.checkScrollbar = function() {
        var a = window.innerWidth;
        if (!a) {
            var b = document.documentElement.getBoundingClientRect();
            a = b.right - Math.abs(b.left)
        }
        this.bodyIsOverflowing = document.body.clientWidth < a, this.scrollbarWidth = this.measureScrollbar()
    }, c.prototype.setScrollbar = function() {
        var a = parseInt(this.$body.css("padding-right") || 0, 10);
        this.originalBodyPad = document.body.style.paddingRight || "", this.bodyIsOverflowing && this.$body.css("padding-right", a + this.scrollbarWidth)
    }, c.prototype.resetScrollbar = function() {
        this.$body.css("padding-right", this.originalBodyPad)
    }, c.prototype.measureScrollbar = function() {
        var a = document.createElement("div");
        a.className = "modal-scrollbar-measure", this.$body.append(a);
        var b = a.offsetWidth - a.clientWidth;
        return this.$body[0].removeChild(a), b
    };
    var d = a.fn.modal;
    a.fn.modal = b, a.fn.modal.Constructor = c, a.fn.modal.noConflict = function() {
        return a.fn.modal = d, this
    }, a(document).on("click.bs.modal.data-api", '[data-toggle="modal"]', function(c) {
        var d = a(this),
            e = d.attr("href"),
            f = a(d.attr("data-target") || e && e.replace(/.*(?=#[^\s]+$)/, "")),
            g = f.data("bs.modal") ? "toggle" : a.extend({
                remote: !/#/.test(e) && e
            }, f.data(), d.data());
        d.is("a") && c.preventDefault(), f.one("show.bs.modal", function(a) {
            a.isDefaultPrevented() || f.one("hidden.bs.modal", function() {
                d.is(":visible") && d.trigger("focus")
            })
        }), b.call(f, g, this)
    })
}(jQuery), + function(a) {
    "use strict";

    function b(b) {
        return this.each(function() {
            var d = a(this),
                e = d.data("bs.tooltip"),
                f = "object" == typeof b && b;
            !e && /destroy|hide/.test(b) || (e || d.data("bs.tooltip", e = new c(this, f)), "string" == typeof b && e[b]())
        })
    }
    var c = function(a, b) {
        this.type = null, this.options = null, this.enabled = null, this.timeout = null, this.hoverState = null, this.$element = null, this.inState = null, this.init("tooltip", a, b)
    };
    c.VERSION = "3.3.7", c.TRANSITION_DURATION = 150, c.DEFAULTS = {
        animation: !0,
        placement: "top",
        selector: !1,
        template: '<div class="tooltip" role="tooltip"><div class="tooltip-arrow"></div><div class="tooltip-inner"></div></div>',
        trigger: "hover focus",
        title: "",
        delay: 0,
        html: !1,
        container: !1,
        viewport: {
            selector: "body",
            padding: 0
        }
    }, c.prototype.init = function(b, c, d) {
        if (this.enabled = !0, this.type = b, this.$element = a(c), this.options = this.getOptions(d), this.$viewport = this.options.viewport && a(a.isFunction(this.options.viewport) ? this.options.viewport.call(this, this.$element) : this.options.viewport.selector || this.options.viewport), this.inState = {
                click: !1,
                hover: !1,
                focus: !1
            }, this.$element[0] instanceof document.constructor && !this.options.selector) throw new Error("`selector` option must be specified when initializing " + this.type + " on the window.document object!");
        for (var e = this.options.trigger.split(" "), f = e.length; f--;) {
            var g = e[f];
            if ("click" == g) this.$element.on("click." + this.type, this.options.selector, a.proxy(this.toggle, this));
            else if ("manual" != g) {
                var h = "hover" == g ? "mouseenter" : "focusin",
                    i = "hover" == g ? "mouseleave" : "focusout";
                this.$element.on(h + "." + this.type, this.options.selector, a.proxy(this.enter, this)), this.$element.on(i + "." + this.type, this.options.selector, a.proxy(this.leave, this))
            }
        }
        this.options.selector ? this._options = a.extend({}, this.options, {
            trigger: "manual",
            selector: ""
        }) : this.fixTitle()
    }, c.prototype.getDefaults = function() {
        return c.DEFAULTS
    }, c.prototype.getOptions = function(b) {
        return b = a.extend({}, this.getDefaults(), this.$element.data(), b), b.delay && "number" == typeof b.delay && (b.delay = {
            show: b.delay,
            hide: b.delay
        }), b
    }, c.prototype.getDelegateOptions = function() {
        var b = {},
            c = this.getDefaults();
        return this._options && a.each(this._options, function(a, d) {
            c[a] != d && (b[a] = d)
        }), b
    }, c.prototype.enter = function(b) {
        var c = b instanceof this.constructor ? b : a(b.currentTarget).data("bs." + this.type);
        return c || (c = new this.constructor(b.currentTarget, this.getDelegateOptions()), a(b.currentTarget).data("bs." + this.type, c)), b instanceof a.Event && (c.inState["focusin" == b.type ? "focus" : "hover"] = !0), c.tip().hasClass("in") || "in" == c.hoverState ? void(c.hoverState = "in") : (clearTimeout(c.timeout), c.hoverState = "in", c.options.delay && c.options.delay.show ? void(c.timeout = setTimeout(function() {
            "in" == c.hoverState && c.show()
        }, c.options.delay.show)) : c.show())
    }, c.prototype.isInStateTrue = function() {
        for (var a in this.inState)
            if (this.inState[a]) return !0;
        return !1
    }, c.prototype.leave = function(b) {
        var c = b instanceof this.constructor ? b : a(b.currentTarget).data("bs." + this.type);
        if (c || (c = new this.constructor(b.currentTarget, this.getDelegateOptions()), a(b.currentTarget).data("bs." + this.type, c)), b instanceof a.Event && (c.inState["focusout" == b.type ? "focus" : "hover"] = !1), !c.isInStateTrue()) return clearTimeout(c.timeout), c.hoverState = "out", c.options.delay && c.options.delay.hide ? void(c.timeout = setTimeout(function() {
            "out" == c.hoverState && c.hide()
        }, c.options.delay.hide)) : c.hide()
    }, c.prototype.show = function() {
        var b = a.Event("show.bs." + this.type);
        if (this.hasContent() && this.enabled) {
            this.$element.trigger(b);
            var d = a.contains(this.$element[0].ownerDocument.documentElement, this.$element[0]);
            if (b.isDefaultPrevented() || !d) return;
            var e = this,
                f = this.tip(),
                g = this.getUID(this.type);
            this.setContent(), f.attr("id", g), this.$element.attr("aria-describedby", g), this.options.animation && f.addClass("fade");
            var h = "function" == typeof this.options.placement ? this.options.placement.call(this, f[0], this.$element[0]) : this.options.placement,
                i = /\s?auto?\s?/i,
                j = i.test(h);
            j && (h = h.replace(i, "") || "top"), f.detach().css({
                top: 0,
                left: 0,
                display: "block"
            }).addClass(h).data("bs." + this.type, this), this.options.container ? f.appendTo(this.options.container) : f.insertAfter(this.$element), this.$element.trigger("inserted.bs." + this.type);
            var k = this.getPosition(),
                l = f[0].offsetWidth,
                m = f[0].offsetHeight;
            if (j) {
                var n = h,
                    o = this.getPosition(this.$viewport);
                h = "bottom" == h && k.bottom + m > o.bottom ? "top" : "top" == h && k.top - m < o.top ? "bottom" : "right" == h && k.right + l > o.width ? "left" : "left" == h && k.left - l < o.left ? "right" : h, f.removeClass(n).addClass(h)
            }
            var p = this.getCalculatedOffset(h, k, l, m);
            this.applyPlacement(p, h);
            var q = function() {
                var a = e.hoverState;
                e.$element.trigger("shown.bs." + e.type), e.hoverState = null, "out" == a && e.leave(e)
            };
            a.support.transition && this.$tip.hasClass("fade") ? f.one("bsTransitionEnd", q).emulateTransitionEnd(c.TRANSITION_DURATION) : q()
        }
    }, c.prototype.applyPlacement = function(b, c) {
        var d = this.tip(),
            e = d[0].offsetWidth,
            f = d[0].offsetHeight,
            g = parseInt(d.css("margin-top"), 10),
            h = parseInt(d.css("margin-left"), 10);
        isNaN(g) && (g = 0), isNaN(h) && (h = 0), b.top += g, b.left += h, a.offset.setOffset(d[0], a.extend({
            using: function(a) {
                d.css({
                    top: Math.round(a.top),
                    left: Math.round(a.left)
                })
            }
        }, b), 0), d.addClass("in");
        var i = d[0].offsetWidth,
            j = d[0].offsetHeight;
        "top" == c && j != f && (b.top = b.top + f - j);
        var k = this.getViewportAdjustedDelta(c, b, i, j);
        k.left ? b.left += k.left : b.top += k.top;
        var l = /top|bottom/.test(c),
            m = l ? 2 * k.left - e + i : 2 * k.top - f + j,
            n = l ? "offsetWidth" : "offsetHeight";
        d.offset(b), this.replaceArrow(m, d[0][n], l)
    }, c.prototype.replaceArrow = function(a, b, c) {
        this.arrow().css(c ? "left" : "top", 50 * (1 - a / b) + "%").css(c ? "top" : "left", "")
    }, c.prototype.setContent = function() {
        var a = this.tip(),
            b = this.getTitle();
        a.find(".tooltip-inner")[this.options.html ? "html" : "text"](b), a.removeClass("fade in top bottom left right")
    }, c.prototype.hide = function(b) {
        function d() {
            "in" != e.hoverState && f.detach(), e.$element && e.$element.removeAttr("aria-describedby").trigger("hidden.bs." + e.type), b && b()
        }
        var e = this,
            f = a(this.$tip),
            g = a.Event("hide.bs." + this.type);
        if (this.$element.trigger(g), !g.isDefaultPrevented()) return f.removeClass("in"), a.support.transition && f.hasClass("fade") ? f.one("bsTransitionEnd", d).emulateTransitionEnd(c.TRANSITION_DURATION) : d(), this.hoverState = null, this
    }, c.prototype.fixTitle = function() {
        var a = this.$element;
        (a.attr("title") || "string" != typeof a.attr("data-original-title")) && a.attr("data-original-title", a.attr("title") || "").attr("title", "")
    }, c.prototype.hasContent = function() {
        return this.getTitle()
    }, c.prototype.getPosition = function(b) {
        b = b || this.$element;
        var c = b[0],
            d = "BODY" == c.tagName,
            e = c.getBoundingClientRect();
        null == e.width && (e = a.extend({}, e, {
            width: e.right - e.left,
            height: e.bottom - e.top
        }));
        var f = window.SVGElement && c instanceof window.SVGElement,
            g = d ? {
                top: 0,
                left: 0
            } : f ? null : b.offset(),
            h = {
                scroll: d ? document.documentElement.scrollTop || document.body.scrollTop : b.scrollTop()
            },
            i = d ? {
                width: a(window).width(),
                height: a(window).height()
            } : null;
        return a.extend({}, e, h, i, g)
    }, c.prototype.getCalculatedOffset = function(a, b, c, d) {
        return "bottom" == a ? {
            top: b.top + b.height,
            left: b.left + b.width / 2 - c / 2
        } : "top" == a ? {
            top: b.top - d,
            left: b.left + b.width / 2 - c / 2
        } : "left" == a ? {
            top: b.top + b.height / 2 - d / 2,
            left: b.left - c
        } : {
            top: b.top + b.height / 2 - d / 2,
            left: b.left + b.width
        }
    }, c.prototype.getViewportAdjustedDelta = function(a, b, c, d) {
        var e = {
            top: 0,
            left: 0
        };
        if (!this.$viewport) return e;
        var f = this.options.viewport && this.options.viewport.padding || 0,
            g = this.getPosition(this.$viewport);
        if (/right|left/.test(a)) {
            var h = b.top - f - g.scroll,
                i = b.top + f - g.scroll + d;
            h < g.top ? e.top = g.top - h : i > g.top + g.height && (e.top = g.top + g.height - i)
        } else {
            var j = b.left - f,
                k = b.left + f + c;
            j < g.left ? e.left = g.left - j : k > g.right && (e.left = g.left + g.width - k)
        }
        return e
    }, c.prototype.getTitle = function() {
        var a, b = this.$element,
            c = this.options;
        return a = b.attr("data-original-title") || ("function" == typeof c.title ? c.title.call(b[0]) : c.title)
    }, c.prototype.getUID = function(a) {
        do a += ~~(1e6 * Math.random()); while (document.getElementById(a));
        return a
    }, c.prototype.tip = function() {
        if (!this.$tip && (this.$tip = a(this.options.template), 1 != this.$tip.length)) throw new Error(this.type + " `template` option must consist of exactly 1 top-level element!");
        return this.$tip
    }, c.prototype.arrow = function() {
        return this.$arrow = this.$arrow || this.tip().find(".tooltip-arrow")
    }, c.prototype.enable = function() {
        this.enabled = !0
    }, c.prototype.disable = function() {
        this.enabled = !1
    }, c.prototype.toggleEnabled = function() {
        this.enabled = !this.enabled
    }, c.prototype.toggle = function(b) {
        var c = this;
        b && (c = a(b.currentTarget).data("bs." + this.type), c || (c = new this.constructor(b.currentTarget, this.getDelegateOptions()), a(b.currentTarget).data("bs." + this.type, c))), b ? (c.inState.click = !c.inState.click, c.isInStateTrue() ? c.enter(c) : c.leave(c)) : c.tip().hasClass("in") ? c.leave(c) : c.enter(c)
    }, c.prototype.destroy = function() {
        var a = this;
        clearTimeout(this.timeout), this.hide(function() {
            a.$element.off("." + a.type).removeData("bs." + a.type), a.$tip && a.$tip.detach(), a.$tip = null, a.$arrow = null, a.$viewport = null, a.$element = null
        })
    };
    var d = a.fn.tooltip;
    a.fn.tooltip = b, a.fn.tooltip.Constructor = c, a.fn.tooltip.noConflict = function() {
        return a.fn.tooltip = d, this
    }
}(jQuery), + function(a) {
    "use strict";

    function b(b) {
        return this.each(function() {
            var d = a(this),
                e = d.data("bs.popover"),
                f = "object" == typeof b && b;
            !e && /destroy|hide/.test(b) || (e || d.data("bs.popover", e = new c(this, f)), "string" == typeof b && e[b]())
        })
    }
    var c = function(a, b) {
        this.init("popover", a, b)
    };
    if (!a.fn.tooltip) throw new Error("Popover requires tooltip.js");
    c.VERSION = "3.3.7", c.DEFAULTS = a.extend({}, a.fn.tooltip.Constructor.DEFAULTS, {
        placement: "right",
        trigger: "click",
        content: "",
        template: '<div class="popover" role="tooltip"><div class="arrow"></div><h3 class="popover-title"></h3><div class="popover-content"></div></div>'
    }), c.prototype = a.extend({}, a.fn.tooltip.Constructor.prototype), c.prototype.constructor = c, c.prototype.getDefaults = function() {
        return c.DEFAULTS
    }, c.prototype.setContent = function() {
        var a = this.tip(),
            b = this.getTitle(),
            c = this.getContent();
        a.find(".popover-title")[this.options.html ? "html" : "text"](b), a.find(".popover-content").children().detach().end()[this.options.html ? "string" == typeof c ? "html" : "append" : "text"](c), a.removeClass("fade top bottom left right in"), a.find(".popover-title").html() || a.find(".popover-title").hide()
    }, c.prototype.hasContent = function() {
        return this.getTitle() || this.getContent()
    }, c.prototype.getContent = function() {
        var a = this.$element,
            b = this.options;
        return a.attr("data-content") || ("function" == typeof b.content ? b.content.call(a[0]) : b.content)
    }, c.prototype.arrow = function() {
        return this.$arrow = this.$arrow || this.tip().find(".arrow")
    };
    var d = a.fn.popover;
    a.fn.popover = b, a.fn.popover.Constructor = c, a.fn.popover.noConflict = function() {
        return a.fn.popover = d, this
    }
}(jQuery), + function(a) {
    "use strict";

    function b(c, d) {
        this.$body = a(document.body), this.$scrollElement = a(a(c).is(document.body) ? window : c), this.options = a.extend({}, b.DEFAULTS, d), this.selector = (this.options.target || "") + " .nav li > a", this.offsets = [], this.targets = [], this.activeTarget = null, this.scrollHeight = 0, this.$scrollElement.on("scroll.bs.scrollspy", a.proxy(this.process, this)), this.refresh(), this.process()
    }

    function c(c) {
        return this.each(function() {
            var d = a(this),
                e = d.data("bs.scrollspy"),
                f = "object" == typeof c && c;
            e || d.data("bs.scrollspy", e = new b(this, f)), "string" == typeof c && e[c]()
        })
    }
    b.VERSION = "3.3.7", b.DEFAULTS = {
        offset: 10
    }, b.prototype.getScrollHeight = function() {
        return this.$scrollElement[0].scrollHeight || Math.max(this.$body[0].scrollHeight, document.documentElement.scrollHeight)
    }, b.prototype.refresh = function() {
        var b = this,
            c = "offset",
            d = 0;
        this.offsets = [], this.targets = [], this.scrollHeight = this.getScrollHeight(), a.isWindow(this.$scrollElement[0]) || (c = "position", d = this.$scrollElement.scrollTop()), this.$body.find(this.selector).map(function() {
            var b = a(this),
                e = b.data("target") || b.attr("href"),
                f = /^#./.test(e) && a(e);
            return f && f.length && f.is(":visible") && [
                    [f[c]().top + d, e]
                ] || null
        }).sort(function(a, b) {
            return a[0] - b[0]
        }).each(function() {
            b.offsets.push(this[0]), b.targets.push(this[1])
        })
    }, b.prototype.process = function() {
        var a, b = this.$scrollElement.scrollTop() + this.options.offset,
            c = this.getScrollHeight(),
            d = this.options.offset + c - this.$scrollElement.height(),
            e = this.offsets,
            f = this.targets,
            g = this.activeTarget;
        if (this.scrollHeight != c && this.refresh(), b >= d) return g != (a = f[f.length - 1]) && this.activate(a);
        if (g && b < e[0]) return this.activeTarget = null, this.clear();
        for (a = e.length; a--;) g != f[a] && b >= e[a] && (void 0 === e[a + 1] || b < e[a + 1]) && this.activate(f[a])
    }, b.prototype.activate = function(b) {
        this.activeTarget = b, this.clear();
        var c = this.selector + '[data-target="' + b + '"],' + this.selector + '[href="' + b + '"]',
            d = a(c).parents("li").addClass("active");
        d.parent(".dropdown-menu").length && (d = d.closest("li.dropdown").addClass("active")), d.trigger("activate.bs.scrollspy")
    }, b.prototype.clear = function() {
        a(this.selector).parentsUntil(this.options.target, ".active").removeClass("active")
    };
    var d = a.fn.scrollspy;
    a.fn.scrollspy = c, a.fn.scrollspy.Constructor = b, a.fn.scrollspy.noConflict = function() {
        return a.fn.scrollspy = d, this
    }, a(window).on("load.bs.scrollspy.data-api", function() {
        a('[data-spy="scroll"]').each(function() {
            var b = a(this);
            c.call(b, b.data())
        })
    })
}(jQuery), + function(a) {
    "use strict";

    function b(b) {
        return this.each(function() {
            var d = a(this),
                e = d.data("bs.tab");
            e || d.data("bs.tab", e = new c(this)), "string" == typeof b && e[b]()
        })
    }
    var c = function(b) {
        this.element = a(b)
    };
    c.VERSION = "3.3.7", c.TRANSITION_DURATION = 150, c.prototype.show = function() {
        var b = this.element,
            c = b.closest("ul:not(.dropdown-menu)"),
            d = b.data("target");
        if (d || (d = b.attr("href"), d = d && d.replace(/.*(?=#[^\s]*$)/, "")), !b.parent("li").hasClass("active")) {
            var e = c.find(".active:last a"),
                f = a.Event("hide.bs.tab", {
                    relatedTarget: b[0]
                }),
                g = a.Event("show.bs.tab", {
                    relatedTarget: e[0]
                });
            if (e.trigger(f), b.trigger(g), !g.isDefaultPrevented() && !f.isDefaultPrevented()) {
                var h = a(d);
                this.activate(b.closest("li"), c), this.activate(h, h.parent(), function() {
                    e.trigger({
                        type: "hidden.bs.tab",
                        relatedTarget: b[0]
                    }), b.trigger({
                        type: "shown.bs.tab",
                        relatedTarget: e[0]
                    })
                })
            }
        }
    }, c.prototype.activate = function(b, d, e) {
        function f() {
            g.removeClass("active").find("> .dropdown-menu > .active").removeClass("active").end().find('[data-toggle="tab"]').attr("aria-expanded", !1), b.addClass("active").find('[data-toggle="tab"]').attr("aria-expanded", !0), h ? (b[0].offsetWidth, b.addClass("in")) : b.removeClass("fade"), b.parent(".dropdown-menu").length && b.closest("li.dropdown").addClass("active").end().find('[data-toggle="tab"]').attr("aria-expanded", !0), e && e()
        }
        var g = d.find("> .active"),
            h = e && a.support.transition && (g.length && g.hasClass("fade") || !!d.find("> .fade").length);
        g.length && h ? g.one("bsTransitionEnd", f).emulateTransitionEnd(c.TRANSITION_DURATION) : f(), g.removeClass("in")
    };
    var d = a.fn.tab;
    a.fn.tab = b, a.fn.tab.Constructor = c, a.fn.tab.noConflict = function() {
        return a.fn.tab = d, this
    };
    var e = function(c) {
        c.preventDefault(), b.call(a(this), "show")
    };
    a(document).on("click.bs.tab.data-api", '[data-toggle="tab"]', e).on("click.bs.tab.data-api", '[data-toggle="pill"]', e)
}(jQuery), + function(a) {
    "use strict";

    function b(b) {
        return this.each(function() {
            var d = a(this),
                e = d.data("bs.affix"),
                f = "object" == typeof b && b;
            e || d.data("bs.affix", e = new c(this, f)), "string" == typeof b && e[b]()
        })
    }
    var c = function(b, d) {
        this.options = a.extend({}, c.DEFAULTS, d), this.$target = a(this.options.target).on("scroll.bs.affix.data-api", a.proxy(this.checkPosition, this)).on("click.bs.affix.data-api", a.proxy(this.checkPositionWithEventLoop, this)), this.$element = a(b), this.affixed = null, this.unpin = null, this.pinnedOffset = null, this.checkPosition()
    };
    c.VERSION = "3.3.7", c.RESET = "affix affix-top affix-bottom", c.DEFAULTS = {
        offset: 0,
        target: window
    }, c.prototype.getState = function(a, b, c, d) {
        var e = this.$target.scrollTop(),
            f = this.$element.offset(),
            g = this.$target.height();
        if (null != c && "top" == this.affixed) return e < c && "top";
        if ("bottom" == this.affixed) return null != c ? !(e + this.unpin <= f.top) && "bottom" : !(e + g <= a - d) && "bottom";
        var h = null == this.affixed,
            i = h ? e : f.top,
            j = h ? g : b;
        return null != c && e <= c ? "top" : null != d && i + j >= a - d && "bottom"
    }, c.prototype.getPinnedOffset = function() {
        if (this.pinnedOffset) return this.pinnedOffset;
        this.$element.removeClass(c.RESET).addClass("affix");
        var a = this.$target.scrollTop(),
            b = this.$element.offset();
        return this.pinnedOffset = b.top - a
    }, c.prototype.checkPositionWithEventLoop = function() {
        setTimeout(a.proxy(this.checkPosition, this), 1)
    }, c.prototype.checkPosition = function() {
        if (this.$element.is(":visible")) {
            var b = this.$element.height(),
                d = this.options.offset,
                e = d.top,
                f = d.bottom,
                g = Math.max(a(document).height(), a(document.body).height());
            "object" != typeof d && (f = e = d), "function" == typeof e && (e = d.top(this.$element)), "function" == typeof f && (f = d.bottom(this.$element));
            var h = this.getState(g, b, e, f);
            if (this.affixed != h) {
                null != this.unpin && this.$element.css("top", "");
                var i = "affix" + (h ? "-" + h : ""),
                    j = a.Event(i + ".bs.affix");
                if (this.$element.trigger(j), j.isDefaultPrevented()) return;
                this.affixed = h, this.unpin = "bottom" == h ? this.getPinnedOffset() : null, this.$element.removeClass(c.RESET).addClass(i).trigger(i.replace("affix", "affixed") + ".bs.affix")
            }
            "bottom" == h && this.$element.offset({
                top: g - b - f
            })
        }
    };
    var d = a.fn.affix;
    a.fn.affix = b, a.fn.affix.Constructor = c, a.fn.affix.noConflict = function() {
        return a.fn.affix = d, this
    }, a(window).on("load", function() {
        a('[data-spy="affix"]').each(function() {
            var c = a(this),
                d = c.data();
            d.offset = d.offset || {}, null != d.offsetBottom && (d.offset.bottom = d.offsetBottom), null != d.offsetTop && (d.offset.top = d.offsetTop), b.call(c, d)
        })
    })
}(jQuery);
! function(e, t) {
    "object" == typeof exports && "undefined" != typeof module ? module.exports = t() : "function" == typeof define && define.amd ? define(t) : e.Sweetalert2 = t()
}(this, function() {
    "use strict";
    var e = {
            title: "",
            titleText: "",
            text: "",
            html: "",
            type: null,
            customClass: "",
            target: "body",
            animation: !0,
            allowOutsideClick: !0,
            allowEscapeKey: !0,
            allowEnterKey: !0,
            showConfirmButton: !0,
            showCancelButton: !1,
            preConfirm: null,
            confirmButtonText: "OK",
            confirmButtonColor: "#3085d6",
            confirmButtonClass: null,
            cancelButtonText: "Cancel",
            cancelButtonColor: "#aaa",
            cancelButtonClass: null,
            buttonsStyling: !0,
            reverseButtons: !1,
            focusCancel: !1,
            showCloseButton: !1,
            showLoaderOnConfirm: !1,
            imageUrl: null,
            imageWidth: null,
            imageHeight: null,
            imageClass: null,
            timer: null,
            width: 500,
            padding: 20,
            background: "#fff",
            input: null,
            inputPlaceholder: "",
            inputValue: "",
            inputOptions: {},
            inputAutoTrim: !0,
            inputClass: null,
            inputAttributes: {},
            inputValidator: null,
            progressSteps: [],
            currentProgressStep: null,
            progressStepsDistance: "40px",
            onOpen: null,
            onClose: null
        },
        t = function(e) {
            var t = {};
            for (var n in e) t[e[n]] = "swal2-" + e[n];
            return t
        },
        n = t(["container", "shown", "iosfix", "modal", "overlay", "fade", "show", "hide", "noanimation", "close", "title", "content", "spacer", "confirm", "cancel", "icon", "image", "input", "file", "range", "select", "radio", "checkbox", "textarea", "inputerror", "validationerror", "progresssteps", "activeprogressstep", "progresscircle", "progressline", "loading", "styled"]),
        o = t(["success", "warning", "info", "question", "error"]),
        r = function(e, t) {
            e = String(e).replace(/[^0-9a-f]/gi, ""), e.length < 6 && (e = e[0] + e[0] + e[1] + e[1] + e[2] + e[2]), t = t || 0;
            for (var n = "#", o = 0; o < 3; o++) {
                var r = parseInt(e.substr(2 * o, 2), 16);
                r = Math.round(Math.min(Math.max(0, r + r * t), 255)).toString(16), n += ("00" + r).substr(r.length)
            }
            return n
        },
        i = {
            previousWindowKeyDown: null,
            previousActiveElement: null,
            previousBodyPadding: null
        },
        a = function(e) {
            if ("undefined" == typeof document) return void console.error("SweetAlert2 requires document to initialize");
            var t = document.createElement("div");
            t.className = n.container, t.innerHTML = l;
            var o = document.querySelector(e.target);
            o || (console.warn("SweetAlert2: Can't find the target \"" + e.target + '"'), o = document.body), o.appendChild(t);
            var r = u(),
                i = B(r, n.input),
                a = B(r, n.file),
                s = r.querySelector("." + n.range + " input"),
                c = r.querySelector("." + n.range + " output"),
                d = B(r, n.select),
                p = r.querySelector("." + n.checkbox + " input"),
                f = B(r, n.textarea);
            return i.oninput = function() {
                $.resetValidationError()
            }, i.onkeydown = function(t) {
                setTimeout(function() {
                    13 === t.keyCode && e.allowEnterKey && (t.stopPropagation(), $.clickConfirm())
                }, 0)
            }, a.onchange = function() {
                $.resetValidationError()
            }, s.oninput = function() {
                $.resetValidationError(), c.value = s.value
            }, s.onchange = function() {
                $.resetValidationError(), s.previousSibling.value = s.value
            }, d.onchange = function() {
                $.resetValidationError()
            }, p.onchange = function() {
                $.resetValidationError()
            }, f.oninput = function() {
                $.resetValidationError()
            }, r
        },
        l = ('\n <div  role="dialog" aria-labelledby="modalTitleId" aria-describedby="modalContentId" class="' + n.modal + '" tabIndex="-1" >\n   <ul class="' + n.progresssteps + '"></ul>\n   <div class="' + n.icon + " " + o.error + '">\n     <span class="x-mark"><span class="line left"></span><span class="line right"></span></span>\n   </div>\n   <div class="' + n.icon + " " + o.question + '">?</div>\n   <div class="' + n.icon + " " + o.warning + '">!</div>\n   <div class="' + n.icon + " " + o.info + '">i</div>\n   <div class="' + n.icon + " " + o.success + '">\n     <span class="line tip"></span> <span class="line long"></span>\n     <div class="placeholder"></div> <div class="fix"></div>\n   </div>\n   <img class="' + n.image + '">\n   <h2 class="' + n.title + '" id="modalTitleId"></h2>\n   <div id="modalContentId" class="' + n.content + '"></div>\n   <input class="' + n.input + '">\n   <input type="file" class="' + n.file + '">\n   <div class="' + n.range + '">\n     <output></output>\n     <input type="range">\n   </div>\n   <select class="' + n.select + '"></select>\n   <div class="' + n.radio + '"></div>\n   <label for="' + n.checkbox + '" class="' + n.checkbox + '">\n     <input type="checkbox">\n   </label>\n   <textarea class="' + n.textarea + '"></textarea>\n   <div class="' + n.validationerror + '"></div>\n   <hr class="' + n.spacer + '">\n   <button type="button" role="button" tabIndex="0" class="' + n.confirm + '">OK</button>\n   <button type="button" role="button" tabIndex="0" class="' + n.cancel + '">Cancel</button>\n   <span class="' + n.close + '">&times;</span>\n </div>\n').replace(/(^|\n)\s*/g, ""),
        s = function() {
            return document.body.querySelector("." + n.container)
        },
        u = function() {
            return s() ? s().querySelector("." + n.modal) : null
        },
        c = function() {
            return u().querySelectorAll("." + n.icon)
        },
        d = function(e) {
            return s() ? s().querySelector("." + e) : null
        },
        p = function() {
            return d(n.title)
        },
        f = function() {
            return d(n.content)
        },
        m = function() {
            return d(n.image)
        },
        v = function() {
            return d(n.spacer)
        },
        h = function() {
            return d(n.progresssteps)
        },
        y = function() {
            return d(n.validationerror)
        },
        g = function() {
            return d(n.confirm)
        },
        b = function() {
            return d(n.cancel)
        },
        w = function() {
            return d(n.close)
        },
        C = function(e) {
            var t = [g(), b()];
            return e && t.reverse(), t.concat(Array.prototype.slice.call(u().querySelectorAll("button:not([class^=swal2-]), input:not([type=hidden]), textarea, select")))
        },
        k = function(e, t) {
            return !!e.classList && e.classList.contains(t)
        },
        x = function(e) {
            if (e.focus(), "file" !== e.type) {
                var t = e.value;
                e.value = "", e.value = t
            }
        },
        S = function(e, t) {
            if (e && t) {
                t.split(/\s+/).filter(Boolean).forEach(function(t) {
                    e.classList.add(t)
                })
            }
        },
        E = function(e, t) {
            if (e && t) {
                t.split(/\s+/).filter(Boolean).forEach(function(t) {
                    e.classList.remove(t)
                })
            }
        },
        B = function(e, t) {
            for (var n = 0; n < e.childNodes.length; n++)
                if (k(e.childNodes[n], t)) return e.childNodes[n]
        },
        A = function(e, t) {
            t || (t = "block"), e.style.opacity = "", e.style.display = t
        },
        P = function(e) {
            e.style.opacity = "", e.style.display = "none"
        },
        T = function(e) {
            for (; e.firstChild;) e.removeChild(e.firstChild)
        },
        L = function(e) {
            return e.offsetWidth || e.offsetHeight || e.getClientRects().length
        },
        M = function(e, t) {
            e.style.removeProperty ? e.style.removeProperty(t) : e.style.removeAttribute(t)
        },
        q = function(e) {
            if (!L(e)) return !1;
            if ("function" == typeof MouseEvent) {
                var t = new MouseEvent("click", {
                    view: window,
                    bubbles: !1,
                    cancelable: !0
                });
                e.dispatchEvent(t)
            } else if (document.createEvent) {
                var n = document.createEvent("MouseEvents");
                n.initEvent("click", !1, !1), e.dispatchEvent(n)
            } else document.createEventObject ? e.fireEvent("onclick") : "function" == typeof e.onclick && e.onclick()
        },
        V = function() {
            var e = document.createElement("div"),
                t = {
                    WebkitAnimation: "webkitAnimationEnd",
                    OAnimation: "oAnimationEnd oanimationend",
                    msAnimation: "MSAnimationEnd",
                    animation: "animationend"
                };
            for (var n in t)
                if (t.hasOwnProperty(n) && void 0 !== e.style[n]) return t[n];
            return !1
        }(),
        O = function() {
            if (window.onkeydown = i.previousWindowKeyDown, i.previousActiveElement && i.previousActiveElement.focus) {
                var e = window.scrollX,
                    t = window.scrollY;
                i.previousActiveElement.focus(), e && t && window.scrollTo(e, t)
            }
        },
        H = function() {
            if ("ontouchstart" in window || navigator.msMaxTouchPoints) return 0;
            var e = document.createElement("div");
            e.style.width = "50px", e.style.height = "50px", e.style.overflow = "scroll", document.body.appendChild(e);
            var t = e.offsetWidth - e.clientWidth;
            return document.body.removeChild(e), t
        },
        I = function(e, t) {
            var n = void 0;
            return function() {
                var o = function() {
                    n = null, e()
                };
                clearTimeout(n), n = setTimeout(o, t)
            }
        },
        N = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function(e) {
            return typeof e
        } : function(e) {
            return e && "function" == typeof Symbol && e.constructor === Symbol && e !== Symbol.prototype ? "symbol" : typeof e
        },
        j = Object.assign || function(e) {
                for (var t = 1; t < arguments.length; t++) {
                    var n = arguments[t];
                    for (var o in n) Object.prototype.hasOwnProperty.call(n, o) && (e[o] = n[o])
                }
                return e
            },
        K = j({}, e),
        D = [],
        U = void 0,
        W = function(t) {
            var r = u() || a(t);
            for (var i in t) e.hasOwnProperty(i) || "extraParams" === i || console.warn('SweetAlert2: Unknown parameter "' + i + '"');
            r.style.width = "number" == typeof t.width ? t.width + "px" : t.width, r.style.padding = t.padding + "px", r.style.background = t.background;
            var l = p(),
                s = f(),
                d = g(),
                y = b(),
                C = w();
            if (t.titleText ? l.innerText = t.titleText : l.innerHTML = t.title.split("\n").join("<br>"), t.text || t.html) {
                if ("object" === N(t.html))
                    if (s.innerHTML = "", 0 in t.html)
                        for (var k = 0; k in t.html; k++) s.appendChild(t.html[k].cloneNode(!0));
                    else s.appendChild(t.html.cloneNode(!0));
                else t.html ? s.innerHTML = t.html : t.text && (s.textContent = t.text);
                A(s)
            } else P(s);
            t.showCloseButton ? A(C) : P(C), r.className = n.modal, t.customClass && S(r, t.customClass);
            var x = h(),
                B = parseInt(null === t.currentProgressStep ? $.getQueueStep() : t.currentProgressStep, 10);
            t.progressSteps.length ? (A(x), T(x), B >= t.progressSteps.length && console.warn("SweetAlert2: Invalid currentProgressStep parameter, it should be less than progressSteps.length (currentProgressStep like JS arrays starts from 0)"), t.progressSteps.forEach(function(e, o) {
                var r = document.createElement("li");
                if (S(r, n.progresscircle), r.innerHTML = e, o === B && S(r, n.activeprogressstep), x.appendChild(r), o !== t.progressSteps.length - 1) {
                    var i = document.createElement("li");
                    S(i, n.progressline), i.style.width = t.progressStepsDistance, x.appendChild(i)
                }
            })) : P(x);
            for (var L = c(), q = 0; q < L.length; q++) P(L[q]);
            if (t.type) {
                var V = !1;
                for (var O in o)
                    if (t.type === O) {
                        V = !0;
                        break
                    }
                if (!V) return console.error("SweetAlert2: Unknown alert type: " + t.type), !1;
                var H = r.querySelector("." + n.icon + "." + o[t.type]);
                switch (A(H), t.type) {
                    case "success":
                        S(H, "animate"), S(H.querySelector(".tip"), "animate-success-tip"), S(H.querySelector(".long"), "animate-success-long");
                        break;
                    case "error":
                        S(H, "animate-error-icon"), S(H.querySelector(".x-mark"), "animate-x-mark");
                        break;
                    case "warning":
                        S(H, "pulse-warning")
                }
            }
            var I = m();
            t.imageUrl ? (I.setAttribute("src", t.imageUrl), A(I), t.imageWidth ? I.setAttribute("width", t.imageWidth) : I.removeAttribute("width"), t.imageHeight ? I.setAttribute("height", t.imageHeight) : I.removeAttribute("height"), I.className = n.image, t.imageClass && S(I, t.imageClass)) : P(I), t.showCancelButton ? y.style.display = "inline-block" : P(y), t.showConfirmButton ? M(d, "display") : P(d);
            var j = v();
            t.showConfirmButton || t.showCancelButton ? A(j) : P(j), d.innerHTML = t.confirmButtonText, y.innerHTML = t.cancelButtonText, t.buttonsStyling && (d.style.backgroundColor = t.confirmButtonColor, y.style.backgroundColor = t.cancelButtonColor), d.className = n.confirm, S(d, t.confirmButtonClass), y.className = n.cancel, S(y, t.cancelButtonClass), t.buttonsStyling ? (S(d, n.styled), S(y, n.styled)) : (E(d, n.styled), E(y, n.styled), d.style.backgroundColor = d.style.borderLeftColor = d.style.borderRightColor = "", y.style.backgroundColor = y.style.borderLeftColor = y.style.borderRightColor = ""), t.animation === !0 ? E(r, n.noanimation) : S(r, n.noanimation)
        },
        R = function(e, t) {
            var o = s(),
                r = u();
            e ? (S(r, n.show), S(o, n.fade), E(r, n.hide)) : E(r, n.fade), A(r), o.style.overflowY = "hidden", V && !k(r, n.noanimation) ? r.addEventListener(V, function e() {
                r.removeEventListener(V, e), o.style.overflowY = "auto"
            }) : o.style.overflowY = "auto", S(document.documentElement, n.shown), S(document.body, n.shown), S(o, n.shown), z(), Y(), i.previousActiveElement = document.activeElement, null !== t && "function" == typeof t && setTimeout(function() {
                t(r)
            })
        },
        z = function() {
            null === i.previousBodyPadding && document.body.scrollHeight > window.innerHeight && (i.previousBodyPadding = document.body.style.paddingRight, document.body.style.paddingRight = H() + "px")
        },
        Q = function() {
            null !== i.previousBodyPadding && (document.body.style.paddingRight = i.previousBodyPadding, i.previousBodyPadding = null)
        },
        Y = function() {
            if (/iPad|iPhone|iPod/.test(navigator.userAgent) && !window.MSStream && !k(document.body, n.iosfix)) {
                var e = document.body.scrollTop;
                document.body.style.top = e * -1 + "px", S(document.body, n.iosfix)
            }
        },
        Z = function() {
            if (k(document.body, n.iosfix)) {
                var e = parseInt(document.body.style.top, 10);
                E(document.body, n.iosfix), document.body.style.top = "", document.body.scrollTop = e * -1
            }
        },
        $ = function e() {
            for (var t = arguments.length, o = Array(t), a = 0; a < t; a++) o[a] = arguments[a];
            if (void 0 === o[0]) return console.error("SweetAlert2 expects at least 1 attribute!"), !1;
            var l = j({}, K);
            switch (N(o[0])) {
                case "string":
                    l.title = o[0], l.html = o[1], l.type = o[2];
                    break;
                case "object":
                    j(l, o[0]), l.extraParams = o[0].extraParams, "email" === l.input && null === l.inputValidator && (l.inputValidator = function(e) {
                        return new Promise(function(t, n) {
                            /^[a-zA-Z0-9.+_-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,6}$/.test(e) ? t() : n("Invalid email address")
                        })
                    }), "url" === l.input && null === l.inputValidator && (l.inputValidator = function(e) {
                        return new Promise(function(t, n) {
                            /^(https?:\/\/)?([\da-z.-]+)\.([a-z.]{2,6})([\/\w .-]*)*\/?$/.test(e) ? t() : n("Invalid URL")
                        })
                    });
                    break;
                default:
                    return console.error('SweetAlert2: Unexpected type of argument! Expected "string" or "object", got ' + N(o[0])), !1
            }
            W(l);
            var c = s(),
                d = u();
            return new Promise(function(t, o) {
                l.timer && (d.timeout = setTimeout(function() {
                    e.closeModal(l.onClose), o("timer")
                }, l.timer));
                var a = function(e) {
                        if (!(e = e || l.input)) return null;
                        switch (e) {
                            case "select":
                            case "textarea":
                            case "file":
                                return B(d, n[e]);
                            case "checkbox":
                                return d.querySelector("." + n.checkbox + " input");
                            case "radio":
                                return d.querySelector("." + n.radio + " input:checked") || d.querySelector("." + n.radio + " input:first-child");
                            case "range":
                                return d.querySelector("." + n.range + " input");
                            default:
                                return B(d, n.input)
                        }
                    },
                    k = function() {
                        var e = a();
                        if (!e) return null;
                        switch (l.input) {
                            case "checkbox":
                                return e.checked ? 1 : 0;
                            case "radio":
                                return e.checked ? e.value : null;
                            case "file":
                                return e.files.length ? e.files[0] : null;
                            default:
                                return l.inputAutoTrim ? e.value.trim() : e.value
                        }
                    };
                l.input && setTimeout(function() {
                    var e = a();
                    e && x(e)
                }, 0);
                for (var T = function(n) {
                    l.showLoaderOnConfirm && e.showLoading(), l.preConfirm ? l.preConfirm(n, l.extraParams).then(function(o) {
                        e.closeModal(l.onClose), t(o || n)
                    }, function(t) {
                        e.hideLoading(), t && e.showValidationError(t)
                    }) : (e.closeModal(l.onClose), t(n))
                }, M = function(t) {
                    var n = t || window.event,
                        i = n.target || n.srcElement,
                        a = g(),
                        s = b(),
                        u = a === i || a.contains(i),
                        c = s === i || s.contains(i);
                    switch (n.type) {
                        case "mouseover":
                        case "mouseup":
                            l.buttonsStyling && (u ? a.style.backgroundColor = r(l.confirmButtonColor, -.1) : c && (s.style.backgroundColor = r(l.cancelButtonColor, -.1)));
                            break;
                        case "mouseout":
                            l.buttonsStyling && (u ? a.style.backgroundColor = l.confirmButtonColor : c && (s.style.backgroundColor = l.cancelButtonColor));
                            break;
                        case "mousedown":
                            l.buttonsStyling && (u ? a.style.backgroundColor = r(l.confirmButtonColor, -.2) : c && (s.style.backgroundColor = r(l.cancelButtonColor, -.2)));
                            break;
                        case "click":
                            if (u && e.isVisible())
                                if (e.disableButtons(), l.input) {
                                    var d = k();
                                    l.inputValidator ? (e.disableInput(), l.inputValidator(d, l.extraParams).then(function() {
                                        e.enableButtons(), e.enableInput(), T(d)
                                    }, function(t) {
                                        e.enableButtons(), e.enableInput(), t && e.showValidationError(t)
                                    })) : T(d)
                                } else T(!0);
                            else c && e.isVisible() && (e.disableButtons(), e.closeModal(l.onClose), o("cancel"))
                    }
                }, V = d.querySelectorAll("button"), O = 0; O < V.length; O++) V[O].onclick = M, V[O].onmouseover = M, V[O].onmouseout = M, V[O].onmousedown = M;
                w().onclick = function() {
                    e.closeModal(l.onClose), o("close")
                }, c.onclick = function(t) {
                    t.target === c && l.allowOutsideClick && (e.closeModal(l.onClose), o("overlay"))
                };
                var H = g(),
                    j = b();
                l.reverseButtons ? H.parentNode.insertBefore(j, H) : H.parentNode.insertBefore(H, j);
                var K = function(e, t) {
                        for (var n = C(l.focusCancel), o = 0; o < n.length; o++) {
                            e += t, e === n.length ? e = 0 : e === -1 && (e = n.length - 1);
                            var r = n[e];
                            if (L(r)) return r.focus()
                        }
                    },
                    D = function(t) {
                        var n = t || window.event,
                            r = n.keyCode || n.which;
                        if ([9, 13, 32, 27].indexOf(r) !== -1) {
                            for (var i = n.target || n.srcElement, a = C(l.focusCancel), s = -1, u = 0; u < a.length; u++)
                                if (i === a[u]) {
                                    s = u;
                                    break
                                }
                            9 === r ? (n.shiftKey ? K(s, -1) : K(s, 1), n.stopPropagation(), n.preventDefault()) : 13 === r || 32 === r ? s === -1 && l.allowEnterKey && q(l.focusCancel ? j : H) : 27 === r && l.allowEscapeKey === !0 && (e.closeModal(l.onClose), o("esc"))
                        }
                    };
                i.previousWindowKeyDown = window.onkeydown, window.onkeydown = D, l.buttonsStyling && (H.style.borderLeftColor = l.confirmButtonColor, H.style.borderRightColor = l.confirmButtonColor), e.showLoading = e.enableLoading = function() {
                    A(v()), A(H, "inline-block"), S(H, n.loading), S(d, n.loading), H.disabled = !0, j.disabled = !0
                }, e.hideLoading = e.disableLoading = function() {
                    l.showConfirmButton || (P(H), l.showCancelButton || P(v())), E(H, n.loading), E(d, n.loading), H.disabled = !1, j.disabled = !1
                }, e.getTitle = function() {
                    return p()
                }, e.getContent = function() {
                    return f()
                }, e.getInput = function() {
                    return a()
                }, e.getImage = function() {
                    return m()
                }, e.getConfirmButton = function() {
                    return g()
                }, e.getCancelButton = function() {
                    return b()
                }, e.enableButtons = function() {
                    H.disabled = !1, j.disabled = !1
                }, e.disableButtons = function() {
                    H.disabled = !0, j.disabled = !0
                }, e.enableConfirmButton = function() {
                    H.disabled = !1
                }, e.disableConfirmButton = function() {
                    H.disabled = !0
                }, e.enableInput = function() {
                    var e = a();
                    if (!e) return !1;
                    if ("radio" === e.type)
                        for (var t = e.parentNode.parentNode, n = t.querySelectorAll("input"), o = 0; o < n.length; o++) n[o].disabled = !1;
                    else e.disabled = !1
                }, e.disableInput = function() {
                    var e = a();
                    if (!e) return !1;
                    if (e && "radio" === e.type)
                        for (var t = e.parentNode.parentNode, n = t.querySelectorAll("input"), o = 0; o < n.length; o++) n[o].disabled = !0;
                    else e.disabled = !0
                }, e.recalculateHeight = I(function() {
                    var e = u();
                    if (e) {
                        var t = e.style.display;
                        e.style.minHeight = "", A(e), e.style.minHeight = e.scrollHeight + 1 + "px", e.style.display = t
                    }
                }, 50), e.showValidationError = function(e) {
                    var t = y();
                    t.innerHTML = e, A(t);
                    var o = a();
                    o && (x(o), S(o, n.inputerror))
                }, e.resetValidationError = function() {
                    P(y()), e.recalculateHeight();
                    var t = a();
                    t && E(t, n.inputerror)
                }, e.getProgressSteps = function() {
                    return l.progressSteps
                }, e.setProgressSteps = function(e) {
                    l.progressSteps = e, W(l)
                }, e.showProgressSteps = function() {
                    A(h())
                }, e.hideProgressSteps = function() {
                    P(h())
                }, e.enableButtons(), e.hideLoading(), e.resetValidationError();
                for (var z = ["input", "file", "range", "select", "radio", "checkbox", "textarea"], Q = void 0, Y = 0; Y < z.length; Y++) {
                    var Z = n[z[Y]],
                        $ = B(d, Z);
                    if (Q = a(z[Y])) {
                        for (var J in Q.attributes)
                            if (Q.attributes.hasOwnProperty(J)) {
                                var X = Q.attributes[J].name;
                                "type" !== X && "value" !== X && Q.removeAttribute(X)
                            }
                        for (var _ in l.inputAttributes) Q.setAttribute(_, l.inputAttributes[_])
                    }
                    $.className = Z, l.inputClass && S($, l.inputClass), P($)
                }
                var F = void 0;
                switch (l.input) {
                    case "text":
                    case "email":
                    case "password":
                    case "number":
                    case "tel":
                    case "url":
                        Q = B(d, n.input), Q.value = l.inputValue, Q.placeholder = l.inputPlaceholder, Q.type = l.input, A(Q);
                        break;
                    case "file":
                        Q = B(d, n.file), Q.placeholder = l.inputPlaceholder, Q.type = l.input, A(Q);
                        break;
                    case "range":
                        var G = B(d, n.range),
                            ee = G.querySelector("input"),
                            te = G.querySelector("output");
                        ee.value = l.inputValue, ee.type = l.input, te.value = l.inputValue, A(G);
                        break;
                    case "select":
                        var ne = B(d, n.select);
                        if (ne.innerHTML = "", l.inputPlaceholder) {
                            var oe = document.createElement("option");
                            oe.innerHTML = l.inputPlaceholder, oe.value = "", oe.disabled = !0, oe.selected = !0, ne.appendChild(oe)
                        }
                        F = function(e) {
                            for (var t in e) {
                                var n = document.createElement("option");
                                n.value = t, n.innerHTML = e[t], l.inputValue === t && (n.selected = !0), ne.appendChild(n)
                            }
                            A(ne), ne.focus()
                        };
                        break;
                    case "radio":
                        var re = B(d, n.radio);
                        re.innerHTML = "", F = function(e) {
                            for (var t in e) {
                                var o = document.createElement("input"),
                                    r = document.createElement("label"),
                                    i = document.createElement("span");
                                o.type = "radio", o.name = n.radio, o.value = t, l.inputValue === t && (o.checked = !0), i.innerHTML = e[t], r.appendChild(o), r.appendChild(i), r.for = o.id, re.appendChild(r)
                            }
                            A(re);
                            var a = re.querySelectorAll("input");
                            a.length && a[0].focus()
                        };
                        break;
                    case "checkbox":
                        var ie = B(d, n.checkbox),
                            ae = a("checkbox");
                        ae.type = "checkbox", ae.value = 1, ae.id = n.checkbox, ae.checked = Boolean(l.inputValue);
                        var le = ie.getElementsByTagName("span");
                        le.length && ie.removeChild(le[0]), le = document.createElement("span"), le.innerHTML = l.inputPlaceholder, ie.appendChild(le), A(ie);
                        break;
                    case "textarea":
                        var se = B(d, n.textarea);
                        se.value = l.inputValue, se.placeholder = l.inputPlaceholder, A(se);
                        break;
                    case null:
                        break;
                    default:
                        console.error('SweetAlert2: Unexpected type of input! Expected "text", "email", "password", "number", "tel", "select", "radio", "checkbox", "textarea", "file" or "url", got "' + l.input + '"')
                }
                "select" !== l.input && "radio" !== l.input || (l.inputOptions instanceof Promise ? (e.showLoading(), l.inputOptions.then(function(t) {
                    e.hideLoading(), F(t)
                })) : "object" === N(l.inputOptions) ? F(l.inputOptions) : console.error("SweetAlert2: Unexpected type of inputOptions! Expected object or Promise, got " + N(l.inputOptions))), R(l.animation, l.onOpen), l.allowEnterKey ? K(-1, 1) : document.activeElement && document.activeElement.blur(), s().scrollTop = 0, "undefined" == typeof MutationObserver || U || (U = new MutationObserver(e.recalculateHeight), U.observe(d, {
                    childList: !0,
                    characterData: !0,
                    subtree: !0
                }))
            })
        };
    return $.isVisible = function() {
        return !!u()
    }, $.queue = function(e) {
        D = e;
        var t = function() {
                D = [], document.body.removeAttribute("data-swal2-queue-step")
            },
            n = [];
        return new Promise(function(e, o) {
            ! function r(i, a) {
                i < D.length ? (document.body.setAttribute("data-swal2-queue-step", i), $(D[i]).then(function(e) {
                    n.push(e), r(i + 1, a)
                }, function(e) {
                    t(), o(e)
                })) : (t(), e(n))
            }(0)
        })
    }, $.getQueueStep = function() {
        return document.body.getAttribute("data-swal2-queue-step")
    }, $.insertQueueStep = function(e, t) {
        return t && t < D.length ? D.splice(t, 0, e) : D.push(e)
    }, $.deleteQueueStep = function(e) {
        void 0 !== D[e] && D.splice(e, 1)
    }, $.close = $.closeModal = function(e) {
        var t = s(),
            o = u();
        if (o) {
            E(o, n.show), S(o, n.hide), clearTimeout(o.timeout), O();
            var r = function() {
                t.parentNode.removeChild(t), E(document.documentElement, n.shown), E(document.body, n.shown), Q(), Z()
            };
            V && !k(o, n.noanimation) ? o.addEventListener(V, function e() {
                o.removeEventListener(V, e), k(o, n.hide) && r()
            }) : r(), null !== e && "function" == typeof e && setTimeout(function() {
                e(o)
            })
        }
    }, $.clickConfirm = function() {
        return g().click()
    }, $.clickCancel = function() {
        return b().click()
    }, $.setDefaults = function(t) {
        if (!t || "object" !== (void 0 === t ? "undefined" : N(t))) return console.error("SweetAlert2: the argument for setDefaults() is required and has to be a object");
        for (var n in t) e.hasOwnProperty(n) || "extraParams" === n || (console.warn('SweetAlert2: Unknown parameter "' + n + '"'), delete t[n]);
        j(K, t)
    }, $.resetDefaults = function() {
        K = j({}, e)
    }, $.noop = function() {}, $.version = "6.4.4", $.default = $, $
}), window.Sweetalert2 && (window.sweetAlert = window.swal = window.Sweetalert2);
/*!
 * Bootstrap-select v1.11.2 (http://silviomoreto.github.io/bootstrap-select)
 *
 * Copyright 2013-2016 bootstrap-select
 * Licensed under MIT (https://github.com/silviomoreto/bootstrap-select/blob/master/LICENSE)
 */
! function(a, b) {
    "function" == typeof define && define.amd ? define(["jquery"], function(a) {
        return b(a)
    }) : "object" == typeof exports ? module.exports = b(require("jquery")) : b(jQuery)
}(this, function(a) {
    ! function(a) {
        "use strict";

        function b(b) {
            var c = [{
                re: /[\xC0-\xC6]/g,
                ch: "A"
            }, {
                re: /[\xE0-\xE6]/g,
                ch: "a"
            }, {
                re: /[\xC8-\xCB]/g,
                ch: "E"
            }, {
                re: /[\xE8-\xEB]/g,
                ch: "e"
            }, {
                re: /[\xCC-\xCF]/g,
                ch: "I"
            }, {
                re: /[\xEC-\xEF]/g,
                ch: "i"
            }, {
                re: /[\xD2-\xD6]/g,
                ch: "O"
            }, {
                re: /[\xF2-\xF6]/g,
                ch: "o"
            }, {
                re: /[\xD9-\xDC]/g,
                ch: "U"
            }, {
                re: /[\xF9-\xFC]/g,
                ch: "u"
            }, {
                re: /[\xC7-\xE7]/g,
                ch: "c"
            }, {
                re: /[\xD1]/g,
                ch: "N"
            }, {
                re: /[\xF1]/g,
                ch: "n"
            }];
            return a.each(c, function() {
                b = b.replace(this.re, this.ch)
            }), b
        }

        function c(a) {
            var b = {
                    "&": "&amp;",
                    "<": "&lt;",
                    ">": "&gt;",
                    '"': "&quot;",
                    "'": "&#x27;",
                    "`": "&#x60;"
                },
                c = "(?:" + Object.keys(b).join("|") + ")",
                d = new RegExp(c),
                e = new RegExp(c, "g"),
                f = null == a ? "" : "" + a;
            return d.test(f) ? f.replace(e, function(a) {
                return b[a]
            }) : f
        }

        function d(b, c) {
            var d = arguments,
                e = b,
                f = c;
            [].shift.apply(d);
            var h, i = this.each(function() {
                var b = a(this);
                if (b.is("select")) {
                    var c = b.data("selectpicker"),
                        i = "object" == typeof e && e;
                    if (c) {
                        if (i)
                            for (var j in i) i.hasOwnProperty(j) && (c.options[j] = i[j])
                    } else {
                        var k = a.extend({}, g.DEFAULTS, a.fn.selectpicker.defaults || {}, b.data(), i);
                        k.template = a.extend({}, g.DEFAULTS.template, a.fn.selectpicker.defaults ? a.fn.selectpicker.defaults.template : {}, b.data().template, i.template), b.data("selectpicker", c = new g(this, k, f))
                    }
                    "string" == typeof e && (h = c[e] instanceof Function ? c[e].apply(c, d) : c.options[e])
                }
            });
            return "undefined" != typeof h ? h : i
        }
        String.prototype.includes || ! function() {
            var a = {}.toString,
                b = function() {
                    try {
                        var a = {},
                            b = Object.defineProperty,
                            c = b(a, a, a) && b
                    } catch (d) {}
                    return c
                }(),
                c = "".indexOf,
                d = function(b) {
                    if (null == this) throw new TypeError;
                    var d = String(this);
                    if (b && "[object RegExp]" == a.call(b)) throw new TypeError;
                    var e = d.length,
                        f = String(b),
                        g = f.length,
                        h = arguments.length > 1 ? arguments[1] : void 0,
                        i = h ? Number(h) : 0;
                    i != i && (i = 0);
                    var j = Math.min(Math.max(i, 0), e);
                    return g + j > e ? !1 : -1 != c.call(d, f, i)
                };
            b ? b(String.prototype, "includes", {
                value: d,
                configurable: !0,
                writable: !0
            }) : String.prototype.includes = d
        }(), String.prototype.startsWith || ! function() {
            var a = function() {
                    try {
                        var a = {},
                            b = Object.defineProperty,
                            c = b(a, a, a) && b
                    } catch (d) {}
                    return c
                }(),
                b = {}.toString,
                c = function(a) {
                    if (null == this) throw new TypeError;
                    var c = String(this);
                    if (a && "[object RegExp]" == b.call(a)) throw new TypeError;
                    var d = c.length,
                        e = String(a),
                        f = e.length,
                        g = arguments.length > 1 ? arguments[1] : void 0,
                        h = g ? Number(g) : 0;
                    h != h && (h = 0);
                    var i = Math.min(Math.max(h, 0), d);
                    if (f + i > d) return !1;
                    for (var j = -1; ++j < f;)
                        if (c.charCodeAt(i + j) != e.charCodeAt(j)) return !1;
                    return !0
                };
            a ? a(String.prototype, "startsWith", {
                value: c,
                configurable: !0,
                writable: !0
            }) : String.prototype.startsWith = c
        }(), Object.keys || (Object.keys = function(a, b, c) {
            c = [];
            for (b in a) c.hasOwnProperty.call(a, b) && c.push(b);
            return c
        });
        var e = {
            useDefault: !1,
            _set: a.valHooks.select.set
        };
        a.valHooks.select.set = function(b, c) {
            return c && !e.useDefault && a(b).data("selected", !0), e._set.apply(this, arguments)
        };
        var f = null;
        a.fn.triggerNative = function(a) {
            var b, c = this[0];
            c.dispatchEvent ? ("function" == typeof Event ? b = new Event(a, {
                bubbles: !0
            }) : (b = document.createEvent("Event"), b.initEvent(a, !0, !1)), c.dispatchEvent(b)) : c.fireEvent ? (b = document.createEventObject(), b.eventType = a, c.fireEvent("on" + a, b)) : this.trigger(a)
        }, a.expr.pseudos.icontains = function(b, c, d) {
            var e = a(b),
                f = (e.data("tokens") || e.text()).toString().toUpperCase();
            return f.includes(d[3].toUpperCase())
        }, a.expr.pseudos.ibegins = function(b, c, d) {
            var e = a(b),
                f = (e.data("tokens") || e.text()).toString().toUpperCase();
            return f.startsWith(d[3].toUpperCase())
        }, a.expr.pseudos.aicontains = function(b, c, d) {
            var e = a(b),
                f = (e.data("tokens") || e.data("normalizedText") || e.text()).toString().toUpperCase();
            return f.includes(d[3].toUpperCase())
        }, a.expr.pseudos.aibegins = function(b, c, d) {
            var e = a(b),
                f = (e.data("tokens") || e.data("normalizedText") || e.text()).toString().toUpperCase();
            return f.startsWith(d[3].toUpperCase())
        };
        var g = function(b, c, d) {
            e.useDefault || (a.valHooks.select.set = e._set, e.useDefault = !0), d && (d.stopPropagation(), d.preventDefault()), this.$element = a(b), this.$newElement = null, this.$button = null, this.$menu = null, this.$lis = null, this.options = c, null === this.options.title && (this.options.title = this.$element.attr("title")), this.val = g.prototype.val, this.render = g.prototype.render, this.refresh = g.prototype.refresh, this.setStyle = g.prototype.setStyle, this.selectAll = g.prototype.selectAll, this.deselectAll = g.prototype.deselectAll, this.destroy = g.prototype.destroy, this.remove = g.prototype.remove, this.show = g.prototype.show, this.hide = g.prototype.hide, this.init()
        };
        g.VERSION = "1.11.2", g.DEFAULTS = {
            noneSelectedText: "Nothing selected",
            noneResultsText: "No results matched {0}",
            countSelectedText: function(a, b) {
                return 1 == a ? "{0} item selected" : "{0} items selected"
            },
            maxOptionsText: function(a, b) {
                return [1 == a ? "Limit reached ({n} item max)" : "Limit reached ({n} items max)", 1 == b ? "Group limit reached ({n} item max)" : "Group limit reached ({n} items max)"]
            },
            selectAllText: "Select All",
            deselectAllText: "Deselect All",
            doneButton: !1,
            doneButtonText: "Close",
            multipleSeparator: ", ",
            styleBase: "btn",
            style: "btn-default",
            size: "auto",
            title: null,
            selectedTextFormat: "values",
            width: !1,
            container: !1,
            hideDisabled: !1,
            showSubtext: !1,
            showIcon: !0,
            showContent: !0,
            dropupAuto: !0,
            header: !1,
            liveSearch: !1,
            liveSearchPlaceholder: null,
            liveSearchNormalize: !1,
            liveSearchStyle: "contains",
            actionsBox: !1,
            iconBase: "glyphicon",
            tickIcon: "glyphicon-ok",
            showTick: !1,
            template: {
                caret: '<span class="caret"></span>'
            },
            maxOptions: !1,
            mobile: !1,
            selectOnTab: !1,
            dropdownAlignRight: !1
        }, g.prototype = {
            constructor: g,
            init: function() {
                var b = this,
                    c = this.$element.attr("id");
                this.$element.addClass("bs-select-hidden"), this.liObj = {}, this.multiple = this.$element.prop("multiple"), this.autofocus = this.$element.prop("autofocus"), this.$newElement = this.createView(), this.$element.after(this.$newElement).appendTo(this.$newElement), this.$button = this.$newElement.children("button"), this.$menu = this.$newElement.children(".dropdown-menu"), this.$menuInner = this.$menu.children(".inner"), this.$searchbox = this.$menu.find("input"), this.$element.removeClass("bs-select-hidden"), this.options.dropdownAlignRight === !0 && this.$menu.addClass("dropdown-menu-right"), "undefined" != typeof c && (this.$button.attr("data-id", c), a('label[for="' + c + '"]').click(function(a) {
                    a.preventDefault(), b.$button.focus()
                })), this.checkDisabled(), this.clickListener(), this.options.liveSearch && this.liveSearchListener(), this.render(), this.setStyle(), this.setWidth(), this.options.container && this.selectPosition(), this.$menu.data("this", this), this.$newElement.data("this", this), this.options.mobile && this.mobile(), this.$newElement.on({
                    "hide.bs.dropdown": function(a) {
                        b.$menuInner.attr("aria-expanded", !1), b.$element.trigger("hide.bs.select", a)
                    },
                    "hidden.bs.dropdown": function(a) {
                        b.$element.trigger("hidden.bs.select", a)
                    },
                    "show.bs.dropdown": function(a) {
                        b.$menuInner.attr("aria-expanded", !0), b.$element.trigger("show.bs.select", a)
                    },
                    "shown.bs.dropdown": function(a) {
                        b.$element.trigger("shown.bs.select", a)
                    }
                }), b.$element[0].hasAttribute("required") && this.$element.on("invalid", function() {
                    b.$button.addClass("bs-invalid").focus(), b.$element.on({
                        "focus.bs.select": function() {
                            b.$button.focus(), b.$element.off("focus.bs.select")
                        },
                        "shown.bs.select": function() {
                            b.$element.val(b.$element.val()).off("shown.bs.select")
                        },
                        "rendered.bs.select": function() {
                            this.validity.valid && b.$button.removeClass("bs-invalid"), b.$element.off("rendered.bs.select")
                        }
                    })
                }), setTimeout(function() {
                    b.$element.trigger("loaded.bs.select")
                })
            },
            createDropdown: function() {
                var b = this.multiple || this.options.showTick ? " show-tick" : "",
                    d = this.$element.parent().hasClass("input-group") ? " input-group-btn" : "",
                    e = this.autofocus ? " autofocus" : "",
                    f = this.options.header ? '<div class="popover-title"><button type="button" class="close" aria-hidden="true">&times;</button>' + this.options.header + "</div>" : "",
                    g = this.options.liveSearch ? '<div class="bs-searchbox"><input type="text" class="form-control" autocomplete="off"' + (null === this.options.liveSearchPlaceholder ? "" : ' placeholder="' + c(this.options.liveSearchPlaceholder) + '"') + ' role="textbox" aria-label="Search"></div>' : "",
                    h = this.multiple && this.options.actionsBox ? '<div class="bs-actionsbox"><div class="btn-group btn-group-sm btn-block"><button type="button" class="actions-btn bs-select-all btn btn-default">' + this.options.selectAllText + '</button><button type="button" class="actions-btn bs-deselect-all btn btn-default">' + this.options.deselectAllText + "</button></div></div>" : "",
                    i = this.multiple && this.options.doneButton ? '<div class="bs-donebutton"><div class="btn-group btn-block"><button type="button" class="btn btn-sm btn-default">' + this.options.doneButtonText + "</button></div></div>" : "",
                    j = '<div class="btn-group bootstrap-select' + b + d + '"><button type="button" class="' + this.options.styleBase + ' dropdown-toggle" data-toggle="dropdown"' + e + ' role="button"><span class="filter-option pull-left"></span>&nbsp;<span class="bs-caret">' + this.options.template.caret + '</span></button><div class="dropdown-menu open" role="combobox">' + f + g + h + '<ul class="dropdown-menu inner" role="listbox" aria-expanded="false"></ul>' + i + "</div></div>";
                return a(j)
            },
            createView: function() {
                var a = this.createDropdown(),
                    b = this.createLi();
                return a.find("ul")[0].innerHTML = b, a
            },
            reloadLi: function() {
                this.destroyLi();
                var a = this.createLi();
                this.$menuInner[0].innerHTML = a
            },
            destroyLi: function() {
                this.$menu.find("li").remove()
            },
            createLi: function() {
                var d = this,
                    e = [],
                    f = 0,
                    g = document.createElement("option"),
                    h = -1,
                    i = function(a, b, c, d) {
                        return "<li" + ("undefined" != typeof c & "" !== c ? ' class="' + c + '"' : "") + ("undefined" != typeof b & null !== b ? ' data-original-index="' + b + '"' : "") + ("undefined" != typeof d & null !== d ? 'data-optgroup="' + d + '"' : "") + ">" + a + "</li>"
                    },
                    j = function(a, e, f, g) {
                        return '<a tabindex="0"' + ("undefined" != typeof e ? ' class="' + e + '"' : "") + ("undefined" != typeof f ? ' style="' + f + '"' : "") + (d.options.liveSearchNormalize ? ' data-normalized-text="' + b(c(a)) + '"' : "") + ("undefined" != typeof g || null !== g ? ' data-tokens="' + g + '"' : "") + ' role="option">' + a + '<span class="' + d.options.iconBase + " " + d.options.tickIcon + ' check-mark"></span></a>'
                    };
                if (this.options.title && !this.multiple && (h--, !this.$element.find(".bs-title-option").length)) {
                    var k = this.$element[0];
                    g.className = "bs-title-option", g.appendChild(document.createTextNode(this.options.title)), g.value = "", k.insertBefore(g, k.firstChild);
                    var l = a(k.options[k.selectedIndex]);
                    void 0 === l.attr("selected") && void 0 === this.$element.data("selected") && (g.selected = !0)
                }
                return this.$element.find("option").each(function(b) {
                    var c = a(this);
                    if (h++, !c.hasClass("bs-title-option")) {
                        var g = this.className || "",
                            k = this.style.cssText,
                            l = c.data("content") ? c.data("content") : c.html(),
                            m = c.data("tokens") ? c.data("tokens") : null,
                            n = "undefined" != typeof c.data("subtext") ? '<small class="text-muted">' + c.data("subtext") + "</small>" : "",
                            o = "undefined" != typeof c.data("icon") ? '<span class="' + d.options.iconBase + " " + c.data("icon") + '"></span> ' : "",
                            p = c.parent(),
                            q = "OPTGROUP" === p[0].tagName,
                            r = q && p[0].disabled,
                            s = this.disabled || r;
                        if ("" !== o && s && (o = "<span>" + o + "</span>"), d.options.hideDisabled && (s && !q || r)) return void h--;
                        if (c.data("content") || (l = o + '<span class="text">' + l + n + "</span>"), q && c.data("divider") !== !0) {
                            if (d.options.hideDisabled && s) {
                                if (void 0 === p.data("allOptionsDisabled")) {
                                    var t = p.children();
                                    p.data("allOptionsDisabled", t.filter(":disabled").length === t.length)
                                }
                                if (p.data("allOptionsDisabled")) return void h--
                            }
                            var u = " " + p[0].className || "";
                            if (0 === c.index()) {
                                f += 1;
                                var v = p[0].label,
                                    w = "undefined" != typeof p.data("subtext") ? '<small class="text-muted">' + p.data("subtext") + "</small>" : "",
                                    x = p.data("icon") ? '<span class="' + d.options.iconBase + " " + p.data("icon") + '"></span> ' : "";
                                v = x + '<span class="text">' + v + w + "</span>", 0 !== b && e.length > 0 && (h++, e.push(i("", null, "divider", f + "div"))), h++, e.push(i(v, null, "dropdown-header" + u, f))
                            }
                            if (d.options.hideDisabled && s) return void h--;
                            e.push(i(j(l, "opt " + g + u, k, m), b, "", f))
                        } else if (c.data("divider") === !0) e.push(i("", b, "divider"));
                        else if (c.data("hidden") === !0) e.push(i(j(l, g, k, m), b, "hidden is-hidden"));
                        else {
                            var y = this.previousElementSibling && "OPTGROUP" === this.previousElementSibling.tagName;
                            if (!y && d.options.hideDisabled)
                                for (var z = a(this).prevAll(), A = 0; A < z.length; A++)
                                    if ("OPTGROUP" === z[A].tagName) {
                                        for (var B = 0, C = 0; A > C; C++) {
                                            var D = z[C];
                                            (D.disabled || a(D).data("hidden") === !0) && B++
                                        }
                                        B === A && (y = !0);
                                        break
                                    }
                            y && (h++, e.push(i("", null, "divider", f + "div"))), e.push(i(j(l, g, k, m), b))
                        }
                        d.liObj[b] = h
                    }
                }), this.multiple || 0 !== this.$element.find("option:selected").length || this.options.title || this.$element.find("option").eq(0).prop("selected", !0).attr("selected", "selected"), e.join("")
            },
            findLis: function() {
                return null == this.$lis && (this.$lis = this.$menu.find("li")), this.$lis
            },
            render: function(b) {
                var c, d = this;
                b !== !1 && this.$element.find("option").each(function(a) {
                    var b = d.findLis().eq(d.liObj[a]);
                    d.setDisabled(a, this.disabled || "OPTGROUP" === this.parentNode.tagName && this.parentNode.disabled, b), d.setSelected(a, this.selected, b)
                }), this.togglePlaceholder(), this.tabIndex();
                var e = this.$element.find("option").map(function() {
                        if (this.selected) {
                            if (d.options.hideDisabled && (this.disabled || "OPTGROUP" === this.parentNode.tagName && this.parentNode.disabled)) return;
                            var b, c = a(this),
                                e = c.data("icon") && d.options.showIcon ? '<i class="' + d.options.iconBase + " " + c.data("icon") + '"></i> ' : "";
                            return b = d.options.showSubtext && c.data("subtext") && !d.multiple ? ' <small class="text-muted">' + c.data("subtext") + "</small>" : "", "undefined" != typeof c.attr("title") ? c.attr("title") : c.data("content") && d.options.showContent ? c.data("content") : e + c.html() + b
                        }
                    }).toArray(),
                    f = this.multiple ? e.join(this.options.multipleSeparator) : e[0];
                if (this.multiple && this.options.selectedTextFormat.indexOf("count") > -1) {
                    var g = this.options.selectedTextFormat.split(">");
                    if (g.length > 1 && e.length > g[1] || 1 == g.length && e.length >= 2) {
                        c = this.options.hideDisabled ? ", [disabled]" : "";
                        var h = this.$element.find("option").not('[data-divider="true"], [data-hidden="true"]' + c).length,
                            i = "function" == typeof this.options.countSelectedText ? this.options.countSelectedText(e.length, h) : this.options.countSelectedText;
                        f = i.replace("{0}", e.length.toString()).replace("{1}", h.toString())
                    }
                }
                void 0 == this.options.title && (this.options.title = this.$element.attr("title")), "static" == this.options.selectedTextFormat && (f = this.options.title), f || (f = "undefined" != typeof this.options.title ? this.options.title : this.options.noneSelectedText), this.$button.attr("title", a.trim(f.replace(/<[^>]*>?/g, ""))), this.$button.children(".filter-option").html(f), this.$element.trigger("rendered.bs.select")
            },
            setStyle: function(a, b) {
                this.$element.attr("class") && this.$newElement.addClass(this.$element.attr("class").replace(/selectpicker|mobile-device|bs-select-hidden|validate\[.*\]/gi, ""));
                var c = a ? a : this.options.style;
                "add" == b ? this.$button.addClass(c) : "remove" == b ? this.$button.removeClass(c) : (this.$button.removeClass(this.options.style), this.$button.addClass(c))
            },
            liHeight: function(b) {
                if (b || this.options.size !== !1 && !this.sizeInfo) {
                    var c = document.createElement("div"),
                        d = document.createElement("div"),
                        e = document.createElement("ul"),
                        f = document.createElement("li"),
                        g = document.createElement("li"),
                        h = document.createElement("a"),
                        i = document.createElement("span"),
                        j = this.options.header && this.$menu.find(".popover-title").length > 0 ? this.$menu.find(".popover-title")[0].cloneNode(!0) : null,
                        k = this.options.liveSearch ? document.createElement("div") : null,
                        l = this.options.actionsBox && this.multiple && this.$menu.find(".bs-actionsbox").length > 0 ? this.$menu.find(".bs-actionsbox")[0].cloneNode(!0) : null,
                        m = this.options.doneButton && this.multiple && this.$menu.find(".bs-donebutton").length > 0 ? this.$menu.find(".bs-donebutton")[0].cloneNode(!0) : null;
                    if (i.className = "text", c.className = this.$menu[0].parentNode.className + " open", d.className = "dropdown-menu open", e.className = "dropdown-menu inner", f.className = "divider", i.appendChild(document.createTextNode("Inner text")), h.appendChild(i), g.appendChild(h), e.appendChild(g), e.appendChild(f), j && d.appendChild(j), k) {
                        var n = document.createElement("span");
                        k.className = "bs-searchbox", n.className = "form-control", k.appendChild(n), d.appendChild(k)
                    }
                    l && d.appendChild(l), d.appendChild(e), m && d.appendChild(m), c.appendChild(d), document.body.appendChild(c);
                    var o = h.offsetHeight,
                        p = j ? j.offsetHeight : 0,
                        q = k ? k.offsetHeight : 0,
                        r = l ? l.offsetHeight : 0,
                        s = m ? m.offsetHeight : 0,
                        t = a(f).outerHeight(!0),
                        u = "function" == typeof getComputedStyle ? getComputedStyle(d) : !1,
                        v = u ? null : a(d),
                        w = {
                            vert: parseInt(u ? u.paddingTop : v.css("paddingTop")) + parseInt(u ? u.paddingBottom : v.css("paddingBottom")) + parseInt(u ? u.borderTopWidth : v.css("borderTopWidth")) + parseInt(u ? u.borderBottomWidth : v.css("borderBottomWidth")),
                            horiz: parseInt(u ? u.paddingLeft : v.css("paddingLeft")) + parseInt(u ? u.paddingRight : v.css("paddingRight")) + parseInt(u ? u.borderLeftWidth : v.css("borderLeftWidth")) + parseInt(u ? u.borderRightWidth : v.css("borderRightWidth"))
                        },
                        x = {
                            vert: w.vert + parseInt(u ? u.marginTop : v.css("marginTop")) + parseInt(u ? u.marginBottom : v.css("marginBottom")) + 2,
                            horiz: w.horiz + parseInt(u ? u.marginLeft : v.css("marginLeft")) + parseInt(u ? u.marginRight : v.css("marginRight")) + 2
                        };
                    document.body.removeChild(c), this.sizeInfo = {
                        liHeight: o,
                        headerHeight: p,
                        searchHeight: q,
                        actionsHeight: r,
                        doneButtonHeight: s,
                        dividerHeight: t,
                        menuPadding: w,
                        menuExtras: x
                    }
                }
            },
            setSize: function() {
                if (this.findLis(), this.liHeight(), this.options.header && this.$menu.css("padding-top", 0), this.options.size !== !1) {
                    var b, c, d, e, f, g, h, i, j = this,
                        k = this.$menu,
                        l = this.$menuInner,
                        m = a(window),
                        n = this.$newElement[0].offsetHeight,
                        o = this.$newElement[0].offsetWidth,
                        p = this.sizeInfo.liHeight,
                        q = this.sizeInfo.headerHeight,
                        r = this.sizeInfo.searchHeight,
                        s = this.sizeInfo.actionsHeight,
                        t = this.sizeInfo.doneButtonHeight,
                        u = this.sizeInfo.dividerHeight,
                        v = this.sizeInfo.menuPadding,
                        w = this.sizeInfo.menuExtras,
                        x = this.options.hideDisabled ? ".disabled" : "",
                        y = function() {
                            var b, c = j.$newElement.offset(),
                                d = a(j.options.container);
                            j.options.container && !d.is("body") ? (b = d.offset(), b.top += parseInt(d.css("borderTopWidth")), b.left += parseInt(d.css("borderLeftWidth"))) : b = {
                                top: 0,
                                left: 0
                            }, f = c.top - b.top - m.scrollTop(), g = m.height() - f - n - b.top, h = c.left - b.left - m.scrollLeft(), i = m.width() - h - o - b.left
                        };
                    if (y(), "auto" === this.options.size) {
                        var z = function() {
                            var m, n = function(b, c) {
                                    return function(d) {
                                        return c ? d.classList ? d.classList.contains(b) : a(d).hasClass(b) : !(d.classList ? d.classList.contains(b) : a(d).hasClass(b))
                                    }
                                },
                                u = j.$menuInner[0].getElementsByTagName("li"),
                                x = Array.prototype.filter ? Array.prototype.filter.call(u, n("hidden", !1)) : j.$lis.not(".hidden"),
                                z = Array.prototype.filter ? Array.prototype.filter.call(x, n("dropdown-header", !0)) : x.filter(".dropdown-header");
                            y(), b = g - w.vert, c = i - w.horiz, j.options.container ? (k.data("height") || k.data("height", k.height()), d = k.data("height"), k.data("width") || k.data("width", k.width()), e = k.data("width")) : (d = k.height(), e = k.width()), j.options.dropupAuto && j.$newElement.toggleClass("dropup", f > g && b - w.vert < d), j.$newElement.hasClass("dropup") && (b = f - w.vert), "auto" === j.options.dropdownAlignRight && k.toggleClass("dropdown-menu-right", h > i && c - w.horiz < e - o), m = x.length + z.length > 3 ? 3 * p + w.vert - 2 : 0, k.css({
                                "max-height": b + "px",
                                overflow: "hidden",
                                "min-height": m + q + r + s + t + "px"
                            }), l.css({
                                "max-height": b - q - r - s - t - v.vert + "px",
                                "overflow-y": "auto",
                                "min-height": Math.max(m - v.vert, 0) + "px"
                            })
                        };
                        z(), this.$searchbox.off("input.getSize propertychange.getSize").on("input.getSize propertychange.getSize", z), m.off("resize.getSize scroll.getSize").on("resize.getSize scroll.getSize", z)
                    } else if (this.options.size && "auto" != this.options.size && this.$lis.not(x).length > this.options.size) {
                        var A = this.$lis.not(".divider").not(x).children().slice(0, this.options.size).last().parent().index(),
                            B = this.$lis.slice(0, A + 1).filter(".divider").length;
                        b = p * this.options.size + B * u + v.vert, j.options.container ? (k.data("height") || k.data("height", k.height()), d = k.data("height")) : d = k.height(), j.options.dropupAuto && this.$newElement.toggleClass("dropup", f > g && b - w.vert < d), k.css({
                            "max-height": b + q + r + s + t + "px",
                            overflow: "hidden",
                            "min-height": ""
                        }), l.css({
                            "max-height": b - v.vert + "px",
                            "overflow-y": "auto",
                            "min-height": ""
                        })
                    }
                }
            },
            setWidth: function() {
                if ("auto" === this.options.width) {
                    this.$menu.css("min-width", "0");
                    var a = this.$menu.parent().clone().appendTo("body"),
                        b = this.options.container ? this.$newElement.clone().appendTo("body") : a,
                        c = a.children(".dropdown-menu").outerWidth(),
                        d = b.css("width", "auto").children("button").outerWidth();
                    a.remove(), b.remove(), this.$newElement.css("width", Math.max(c, d) + "px")
                } else "fit" === this.options.width ? (this.$menu.css("min-width", ""), this.$newElement.css("width", "").addClass("fit-width")) : this.options.width ? (this.$menu.css("min-width", ""), this.$newElement.css("width", this.options.width)) : (this.$menu.css("min-width", ""), this.$newElement.css("width", ""));
                this.$newElement.hasClass("fit-width") && "fit" !== this.options.width && this.$newElement.removeClass("fit-width")
            },
            selectPosition: function() {
                this.$bsContainer = a('<div class="bs-container" />');
                var b, c, d, e = this,
                    f = a(this.options.container),
                    g = function(a) {
                        e.$bsContainer.addClass(a.attr("class").replace(/form-control|fit-width/gi, "")).toggleClass("dropup", a.hasClass("dropup")), b = a.offset(), f.is("body") ? c = {
                            top: 0,
                            left: 0
                        } : (c = f.offset(), c.top += parseInt(f.css("borderTopWidth")) - f.scrollTop(), c.left += parseInt(f.css("borderLeftWidth")) - f.scrollLeft()), d = a.hasClass("dropup") ? 0 : a[0].offsetHeight, e.$bsContainer.css({
                            top: b.top - c.top + d,
                            left: b.left - c.left,
                            width: a[0].offsetWidth
                        })
                    };
                this.$button.on("click", function() {
                    var b = a(this);
                    e.isDisabled() || (g(e.$newElement), e.$bsContainer.appendTo(e.options.container).toggleClass("open", !b.hasClass("open")).append(e.$menu))
                }), a(window).on("resize scroll", function() {
                    g(e.$newElement)
                }), this.$element.on("hide.bs.select", function() {
                    e.$menu.data("height", e.$menu.height()), e.$bsContainer.detach()
                })
            },
            setSelected: function(a, b, c) {
                c || (this.togglePlaceholder(), c = this.findLis().eq(this.liObj[a])), c.toggleClass("selected", b).find("a").attr("aria-selected", b)
            },
            setDisabled: function(a, b, c) {
                c || (c = this.findLis().eq(this.liObj[a])), b ? c.addClass("disabled").children("a").attr("href", "#").attr("tabindex", -1).attr("aria-disabled", !0) : c.removeClass("disabled").children("a").removeAttr("href").attr("tabindex", 0).attr("aria-disabled", !1)
            },
            isDisabled: function() {
                return this.$element[0].disabled
            },
            checkDisabled: function() {
                var a = this;
                this.isDisabled() ? (this.$newElement.addClass("disabled"), this.$button.addClass("disabled").attr("tabindex", -1)) : (this.$button.hasClass("disabled") && (this.$newElement.removeClass("disabled"), this.$button.removeClass("disabled")), -1 != this.$button.attr("tabindex") || this.$element.data("tabindex") || this.$button.removeAttr("tabindex")), this.$button.click(function() {
                    return !a.isDisabled()
                })
            },
            togglePlaceholder: function() {
                var a = this.$element.val();
                this.$button.toggleClass("bs-placeholder", null === a || "" === a)
            },
            tabIndex: function() {
                this.$element.data("tabindex") !== this.$element.attr("tabindex") && -98 !== this.$element.attr("tabindex") && "-98" !== this.$element.attr("tabindex") && (this.$element.data("tabindex", this.$element.attr("tabindex")), this.$button.attr("tabindex", this.$element.data("tabindex"))), this.$element.attr("tabindex", -98)
            },
            clickListener: function() {
                var b = this,
                    c = a(document);
                this.$newElement.on("touchstart.dropdown", ".dropdown-menu", function(a) {
                    a.stopPropagation()
                }), c.data("spaceSelect", !1), this.$button.on("keyup", function(a) {
                    /(32)/.test(a.keyCode.toString(10)) && c.data("spaceSelect") && (a.preventDefault(), c.data("spaceSelect", !1))
                }), this.$button.on("click", function() {
                    b.setSize()
                }), this.$element.on("shown.bs.select", function() {
                    if (b.options.liveSearch || b.multiple) {
                        if (!b.multiple) {
                            var a = b.liObj[b.$element[0].selectedIndex];
                            if ("number" != typeof a || b.options.size === !1) return;
                            var c = b.$lis.eq(a)[0].offsetTop - b.$menuInner[0].offsetTop;
                            c = c - b.$menuInner[0].offsetHeight / 2 + b.sizeInfo.liHeight / 2, b.$menuInner[0].scrollTop = c
                        }
                    } else b.$menuInner.find(".selected a").focus()
                }), this.$menuInner.on("click", "li a", function(c) {
                    var d = a(this),
                        e = d.parent().data("originalIndex"),
                        g = b.$element.val(),
                        h = b.$element.prop("selectedIndex"),
                        i = !0;
                    if (b.multiple && 1 !== b.options.maxOptions && c.stopPropagation(), c.preventDefault(), !b.isDisabled() && !d.parent().hasClass("disabled")) {
                        var j = b.$element.find("option"),
                            k = j.eq(e),
                            l = k.prop("selected"),
                            m = k.parent("optgroup"),
                            n = b.options.maxOptions,
                            o = m.data("maxOptions") || !1;
                        if (b.multiple) {
                            if (k.prop("selected", !l), b.setSelected(e, !l), d.blur(), n !== !1 || o !== !1) {
                                var p = n < j.filter(":selected").length,
                                    q = o < m.find("option:selected").length;
                                if (n && p || o && q)
                                    if (n && 1 == n) j.prop("selected", !1), k.prop("selected", !0), b.$menuInner.find(".selected").removeClass("selected"), b.setSelected(e, !0);
                                    else if (o && 1 == o) {
                                        m.find("option:selected").prop("selected", !1), k.prop("selected", !0);
                                        var r = d.parent().data("optgroup");
                                        b.$menuInner.find('[data-optgroup="' + r + '"]').removeClass("selected"), b.setSelected(e, !0)
                                    } else {
                                        var s = "string" == typeof b.options.maxOptionsText ? [b.options.maxOptionsText, b.options.maxOptionsText] : b.options.maxOptionsText,
                                            t = "function" == typeof s ? s(n, o) : s,
                                            u = t[0].replace("{n}", n),
                                            v = t[1].replace("{n}", o),
                                            w = a('<div class="notify"></div>');
                                        t[2] && (u = u.replace("{var}", t[2][n > 1 ? 0 : 1]), v = v.replace("{var}", t[2][o > 1 ? 0 : 1])), k.prop("selected", !1), b.$menu.append(w), n && p && (w.append(a("<div>" + u + "</div>")), i = !1, b.$element.trigger("maxReached.bs.select")), o && q && (w.append(a("<div>" + v + "</div>")), i = !1, b.$element.trigger("maxReachedGrp.bs.select")), setTimeout(function() {
                                            b.setSelected(e, !1)
                                        }, 10), w.delay(750).fadeOut(300, function() {
                                            a(this).remove()
                                        })
                                    }
                            }
                        } else j.prop("selected", !1), k.prop("selected", !0), b.$menuInner.find(".selected").removeClass("selected").find("a").attr("aria-selected", !1), b.setSelected(e, !0);
                        !b.multiple || b.multiple && 1 === b.options.maxOptions ? b.$button.focus() : b.options.liveSearch && b.$searchbox.focus(), i && (g != b.$element.val() && b.multiple || h != b.$element.prop("selectedIndex") && !b.multiple) && (f = [e, k.prop("selected"), l], b.$element.triggerNative("change"))
                    }
                }), this.$menu.on("click", "li.disabled a, .popover-title, .popover-title :not(.close)", function(c) {
                    c.currentTarget == this && (c.preventDefault(), c.stopPropagation(), b.options.liveSearch && !a(c.target).hasClass("close") ? b.$searchbox.focus() : b.$button.focus())
                }), this.$menuInner.on("click", ".divider, .dropdown-header", function(a) {
                    a.preventDefault(), a.stopPropagation(), b.options.liveSearch ? b.$searchbox.focus() : b.$button.focus()
                }), this.$menu.on("click", ".popover-title .close", function() {
                    b.$button.click()
                }), this.$searchbox.on("click", function(a) {
                    a.stopPropagation()
                }), this.$menu.on("click", ".actions-btn", function(c) {
                    b.options.liveSearch ? b.$searchbox.focus() : b.$button.focus(), c.preventDefault(), c.stopPropagation(), a(this).hasClass("bs-select-all") ? b.selectAll() : b.deselectAll()
                }), this.$element.change(function() {
                    b.render(!1), b.$element.trigger("changed.bs.select", f), f = null
                })
            },
            liveSearchListener: function() {
                var d = this,
                    e = a('<li class="no-results"></li>');
                this.$button.on("click.dropdown.data-api touchstart.dropdown.data-api", function() {
                    d.$menuInner.find(".active").removeClass("active"), d.$searchbox.val() && (d.$searchbox.val(""), d.$lis.not(".is-hidden").removeClass("hidden"), e.parent().length && e.remove()), d.multiple || d.$menuInner.find(".selected").addClass("active"), setTimeout(function() {
                        d.$searchbox.focus()
                    }, 10)
                }), this.$searchbox.on("click.dropdown.data-api focus.dropdown.data-api touchend.dropdown.data-api", function(a) {
                    a.stopPropagation()
                }), this.$searchbox.on("input propertychange", function() {
                    if (d.$searchbox.val()) {
                        var f = d.$lis.not(".is-hidden").removeClass("hidden").children("a");
                        f = d.options.liveSearchNormalize ? f.not(":a" + d._searchStyle() + '("' + b(d.$searchbox.val()) + '")') : f.not(":" + d._searchStyle() + '("' + d.$searchbox.val() + '")'), f.parent().addClass("hidden"), d.$lis.filter(".dropdown-header").each(function() {
                            var b = a(this),
                                c = b.data("optgroup");
                            0 === d.$lis.filter("[data-optgroup=" + c + "]").not(b).not(".hidden").length && (b.addClass("hidden"), d.$lis.filter("[data-optgroup=" + c + "div]").addClass("hidden"))
                        });
                        var g = d.$lis.not(".hidden");
                        g.each(function(b) {
                            var c = a(this);
                            c.hasClass("divider") && (c.index() === g.first().index() || c.index() === g.last().index() || g.eq(b + 1).hasClass("divider")) && c.addClass("hidden")
                        }), d.$lis.not(".hidden, .no-results").length ? e.parent().length && e.remove() : (e.parent().length && e.remove(), e.html(d.options.noneResultsText.replace("{0}", '"' + c(d.$searchbox.val()) + '"')).show(), d.$menuInner.append(e))
                    } else d.$lis.not(".is-hidden").removeClass("hidden"), e.parent().length && e.remove();
                    d.$lis.filter(".active").removeClass("active"), d.$searchbox.val() && d.$lis.not(".hidden, .divider, .dropdown-header").eq(0).addClass("active").children("a").focus(), a(this).focus()
                })
            },
            _searchStyle: function() {
                var a = {
                    begins: "ibegins",
                    startsWith: "ibegins"
                };
                return a[this.options.liveSearchStyle] || "icontains"
            },
            val: function(a) {
                return "undefined" != typeof a ? (this.$element.val(a), this.render(), this.$element) : this.$element.val()
            },
            changeAll: function(b) {
                if (this.multiple) {
                    "undefined" == typeof b && (b = !0), this.findLis();
                    var c = this.$element.find("option"),
                        d = this.$lis.not(".divider, .dropdown-header, .disabled, .hidden"),
                        e = d.length,
                        f = [];
                    if (b) {
                        if (d.filter(".selected").length === d.length) return
                    } else if (0 === d.filter(".selected").length) return;
                    d.toggleClass("selected", b);
                    for (var g = 0; e > g; g++) {
                        var h = d[g].getAttribute("data-original-index");
                        f[f.length] = c.eq(h)[0]
                    }
                    a(f).prop("selected", b), this.render(!1), this.togglePlaceholder(), this.$element.triggerNative("change")
                }
            },
            selectAll: function() {
                return this.changeAll(!0)
            },
            deselectAll: function() {
                return this.changeAll(!1)
            },
            toggle: function(a) {
                a = a || window.event, a && a.stopPropagation(), this.$button.trigger("click")
            },
            keydown: function(c) {
                var d, e, f, g, h, i, j, k, l, m = a(this),
                    n = m.is("input") ? m.parent().parent() : m.parent(),
                    o = n.data("this"),
                    p = ":not(.disabled, .hidden, .dropdown-header, .divider)",
                    q = {
                        32: " ",
                        48: "0",
                        49: "1",
                        50: "2",
                        51: "3",
                        52: "4",
                        53: "5",
                        54: "6",
                        55: "7",
                        56: "8",
                        57: "9",
                        59: ";",
                        65: "a",
                        66: "b",
                        67: "c",
                        68: "d",
                        69: "e",
                        70: "f",
                        71: "g",
                        72: "h",
                        73: "i",
                        74: "j",
                        75: "k",
                        76: "l",
                        77: "m",
                        78: "n",
                        79: "o",
                        80: "p",
                        81: "q",
                        82: "r",
                        83: "s",
                        84: "t",
                        85: "u",
                        86: "v",
                        87: "w",
                        88: "x",
                        89: "y",
                        90: "z",
                        96: "0",
                        97: "1",
                        98: "2",
                        99: "3",
                        100: "4",
                        101: "5",
                        102: "6",
                        103: "7",
                        104: "8",
                        105: "9"
                    };
                if (o.options.liveSearch && (n = m.parent().parent()), o.options.container && (n = o.$menu), d = a('[role="listbox"] li', n), l = o.$newElement.hasClass("open"), !l && (c.keyCode >= 48 && c.keyCode <= 57 || c.keyCode >= 96 && c.keyCode <= 105 || c.keyCode >= 65 && c.keyCode <= 90)) return o.options.container ? o.$button.trigger("click") : (o.setSize(), o.$menu.parent().addClass("open"), l = !0), void o.$searchbox.focus();
                if (o.options.liveSearch && (/(^9$|27)/.test(c.keyCode.toString(10)) && l && (c.preventDefault(), c.stopPropagation(), o.$button.click().focus()), d = a('[role="listbox"] li' + p, n), m.val() || /(38|40)/.test(c.keyCode.toString(10)) || 0 === d.filter(".active").length && (d = o.$menuInner.find("li"), d = o.options.liveSearchNormalize ? d.filter(":a" + o._searchStyle() + "(" + b(q[c.keyCode]) + ")") : d.filter(":" + o._searchStyle() + "(" + q[c.keyCode] + ")"))), d.length) {
                    if (/(38|40)/.test(c.keyCode.toString(10))) e = d.index(d.find("a").filter(":focus").parent()), g = d.filter(p).first().index(), h = d.filter(p).last().index(), f = d.eq(e).nextAll(p).eq(0).index(), i = d.eq(e).prevAll(p).eq(0).index(), j = d.eq(f).prevAll(p).eq(0).index(), o.options.liveSearch && (d.each(function(b) {
                        a(this).hasClass("disabled") || a(this).data("index", b)
                    }), e = d.index(d.filter(".active")), g = d.first().data("index"), h = d.last().data("index"), f = d.eq(e).nextAll().eq(0).data("index"), i = d.eq(e).prevAll().eq(0).data("index"), j = d.eq(f).prevAll().eq(0).data("index")), k = m.data("prevIndex"), 38 == c.keyCode ? (o.options.liveSearch && e--, e != j && e > i && (e = i), g > e && (e = g), e == k && (e = h)) : 40 == c.keyCode && (o.options.liveSearch && e++, -1 == e && (e = 0), e != j && f > e && (e = f), e > h && (e = h), e == k && (e = g)), m.data("prevIndex", e), o.options.liveSearch ? (c.preventDefault(), m.hasClass("dropdown-toggle") || (d.removeClass("active").eq(e).addClass("active").children("a").focus(), m.focus())) : d.eq(e).children("a").focus();
                    else if (!m.is("input")) {
                        var r, s, t = [];
                        d.each(function() {
                            a(this).hasClass("disabled") || a.trim(a(this).children("a").text().toLowerCase()).substring(0, 1) == q[c.keyCode] && t.push(a(this).index())
                        }), r = a(document).data("keycount"), r++, a(document).data("keycount", r), s = a.trim(a(":focus").text().toLowerCase()).substring(0, 1), s != q[c.keyCode] ? (r = 1, a(document).data("keycount", r)) : r >= t.length && (a(document).data("keycount", 0), r > t.length && (r = 1)), d.eq(t[r - 1]).children("a").focus()
                    }
                    if ((/(13|32)/.test(c.keyCode.toString(10)) || /(^9$)/.test(c.keyCode.toString(10)) && o.options.selectOnTab) && l) {
                        if (/(32)/.test(c.keyCode.toString(10)) || c.preventDefault(), o.options.liveSearch) /(32)/.test(c.keyCode.toString(10)) || (o.$menuInner.find(".active a").click(), m.focus());
                        else {
                            var u = a(":focus");
                            u.click(), u.focus(), c.preventDefault(), a(document).data("spaceSelect", !0)
                        }
                        a(document).data("keycount", 0)
                    }(/(^9$|27)/.test(c.keyCode.toString(10)) && l && (o.multiple || o.options.liveSearch) || /(27)/.test(c.keyCode.toString(10)) && !l) && (o.$menu.parent().removeClass("open"), o.options.container && o.$newElement.removeClass("open"), o.$button.focus())
                }
            },
            mobile: function() {
                this.$element.addClass("mobile-device")
            },
            refresh: function() {
                this.$lis = null, this.liObj = {}, this.reloadLi(), this.render(), this.checkDisabled(), this.liHeight(!0), this.setStyle(), this.setWidth(), this.$lis && this.$searchbox.trigger("propertychange"), this.$element.trigger("refreshed.bs.select")
            },
            hide: function() {
                this.$newElement.hide()
            },
            show: function() {
                this.$newElement.show()
            },
            remove: function() {
                this.$newElement.remove(), this.$element.remove()
            },
            destroy: function() {
                this.$newElement.before(this.$element).remove(), this.$bsContainer ? this.$bsContainer.remove() : this.$menu.remove(), this.$element.off(".bs.select").removeData("selectpicker").removeClass("bs-select-hidden selectpicker")
            }
        };
        var h = a.fn.selectpicker;
        a.fn.selectpicker = d, a.fn.selectpicker.Constructor = g, a.fn.selectpicker.noConflict = function() {
            return a.fn.selectpicker = h, this
        }, a(document).data("keycount", 0).on("keydown.bs.select", '.bootstrap-select [data-toggle=dropdown], .bootstrap-select [role="listbox"], .bs-searchbox input', g.prototype.keydown).on("focusin.modal", '.bootstrap-select [data-toggle=dropdown], .bootstrap-select [role="listbox"], .bs-searchbox input', function(a) {
            a.stopPropagation()
        }), a(window).on("load.bs.select.data-api", function() {
            a(".selectpicker").each(function() {
                var b = a(this);
                d.call(b, b.data())
            })
        })
    }(a)
});

/*!
 * FormValidation (http://formvalidation.io)
 * The best jQuery plugin to validate form fields. Support Bootstrap, Foundation, Pure, SemanticUI, UIKit and custom frameworks
 *
 * This is a custom build that does NOT consist of all validators. Only popular validators are included:
 * - between
 * - callback
 * - choice
 * - color
 * - creditCard
 * - date
 * - different
 * - digits
 * - emailAddress
 * - file
 * - greaterThan
 * - identical
 * - integer
 * - lessThan
 * - notEmpty
 * - numeric
 * - promise
 * - regexp
 * - remote
 * - stringLength
 * - uri
 *
 * Use formValidation(.min).js file if you want to have all validators.
 *
 * @version     v0.8.1, built on 2016-07-29 1:10:54 AM
 * @author      https://twitter.com/formvalidation
 * @copyright   (c) 2013 - 2016 Nguyen Huu Phuoc
 * @license     http://formvalidation.io/license/
 */
if (window.FormValidation = {
        AddOn: {},
        Framework: {},
        I18n: {},
        Validator: {}
    }, "undefined" == typeof jQuery) throw new Error("FormValidation requires jQuery");
! function(a) {
    var b = a.fn.jquery.split(" ")[0].split(".");
    if (+b[0] < 2 && +b[1] < 9 || 1 === +b[0] && 9 === +b[1] && +b[2] < 1) throw new Error("FormValidation requires jQuery version 1.9.1 or higher")
}(jQuery),
    function(a) {
        FormValidation.Base = function(b, c, d) {
            this.$form = a(b), this.options = a.extend({}, a.fn.formValidation.DEFAULT_OPTIONS, c), this._namespace = d || "fv", this.$invalidFields = a([]), this.$submitButton = null, this.$hiddenButton = null, this.STATUS_NOT_VALIDATED = "NOT_VALIDATED", this.STATUS_VALIDATING = "VALIDATING", this.STATUS_INVALID = "INVALID", this.STATUS_VALID = "VALID", this.STATUS_IGNORED = "IGNORED", this.DEFAULT_MESSAGE = a.fn.formValidation.DEFAULT_MESSAGE, this._ieVersion = function() {
                for (var a = 3, b = document.createElement("div"), c = b.all || []; b.innerHTML = "<!--[if gt IE " + ++a + "]><br><![endif]-->", c[0];);
                return a > 4 ? a : document.documentMode
            }();
            var e = document.createElement("div");
            this._changeEvent = 9 !== this._ieVersion && "oninput" in e ? "input" : "keyup", this._submitIfValid = null, this._cacheFields = {}, this._init()
        }, FormValidation.Base.prototype = {
            constructor: FormValidation.Base,
            _exceedThreshold: function(b) {
                var c = this._namespace,
                    d = b.attr("data-" + c + "-field"),
                    e = this.options.fields[d].threshold || this.options.threshold;
                if (!e) return !0;
                var f = -1 !== a.inArray(b.attr("type"), ["button", "checkbox", "file", "hidden", "image", "radio", "reset", "submit"]);
                return f || b.val().length >= e
            },
            _init: function() {
                var b = this,
                    c = this._namespace,
                    d = {
                        addOns: {},
                        autoFocus: this.$form.attr("data-" + c + "-autofocus"),
                        button: {
                            selector: this.$form.attr("data-" + c + "-button-selector") || this.$form.attr("data-" + c + "-submitbuttons"),
                            disabled: this.$form.attr("data-" + c + "-button-disabled")
                        },
                        control: {
                            valid: this.$form.attr("data-" + c + "-control-valid"),
                            invalid: this.$form.attr("data-" + c + "-control-invalid")
                        },
                        err: {
                            clazz: this.$form.attr("data-" + c + "-err-clazz"),
                            container: this.$form.attr("data-" + c + "-err-container") || this.$form.attr("data-" + c + "-container"),
                            parent: this.$form.attr("data-" + c + "-err-parent")
                        },
                        events: {
                            formInit: this.$form.attr("data-" + c + "-events-form-init"),
                            formPreValidate: this.$form.attr("data-" + c + "-events-form-prevalidate"),
                            formError: this.$form.attr("data-" + c + "-events-form-error"),
                            formReset: this.$form.attr("data-" + c + "-events-form-reset"),
                            formSuccess: this.$form.attr("data-" + c + "-events-form-success"),
                            fieldAdded: this.$form.attr("data-" + c + "-events-field-added"),
                            fieldRemoved: this.$form.attr("data-" + c + "-events-field-removed"),
                            fieldInit: this.$form.attr("data-" + c + "-events-field-init"),
                            fieldError: this.$form.attr("data-" + c + "-events-field-error"),
                            fieldReset: this.$form.attr("data-" + c + "-events-field-reset"),
                            fieldSuccess: this.$form.attr("data-" + c + "-events-field-success"),
                            fieldStatus: this.$form.attr("data-" + c + "-events-field-status"),
                            localeChanged: this.$form.attr("data-" + c + "-events-locale-changed"),
                            validatorError: this.$form.attr("data-" + c + "-events-validator-error"),
                            validatorSuccess: this.$form.attr("data-" + c + "-events-validator-success"),
                            validatorIgnored: this.$form.attr("data-" + c + "-events-validator-ignored")
                        },
                        excluded: this.$form.attr("data-" + c + "-excluded"),
                        icon: {
                            valid: this.$form.attr("data-" + c + "-icon-valid") || this.$form.attr("data-" + c + "-feedbackicons-valid"),
                            invalid: this.$form.attr("data-" + c + "-icon-invalid") || this.$form.attr("data-" + c + "-feedbackicons-invalid"),
                            validating: this.$form.attr("data-" + c + "-icon-validating") || this.$form.attr("data-" + c + "-feedbackicons-validating"),
                            feedback: this.$form.attr("data-" + c + "-icon-feedback")
                        },
                        live: this.$form.attr("data-" + c + "-live"),
                        locale: this.$form.attr("data-" + c + "-locale"),
                        message: this.$form.attr("data-" + c + "-message"),
                        onPreValidate: this.$form.attr("data-" + c + "-onprevalidate"),
                        onError: this.$form.attr("data-" + c + "-onerror"),
                        onReset: this.$form.attr("data-" + c + "-onreset"),
                        onSuccess: this.$form.attr("data-" + c + "-onsuccess"),
                        row: {
                            selector: this.$form.attr("data-" + c + "-row-selector") || this.$form.attr("data-" + c + "-group"),
                            valid: this.$form.attr("data-" + c + "-row-valid"),
                            invalid: this.$form.attr("data-" + c + "-row-invalid"),
                            feedback: this.$form.attr("data-" + c + "-row-feedback")
                        },
                        threshold: this.$form.attr("data-" + c + "-threshold"),
                        trigger: this.$form.attr("data-" + c + "-trigger"),
                        verbose: this.$form.attr("data-" + c + "-verbose"),
                        fields: {}
                    };
                this.$form.attr("novalidate", "novalidate").addClass(this.options.elementClass).on("submit." + c, function(a) {
                    a.preventDefault(), b.validate()
                }).on("click." + c, this.options.button.selector, function() {
                    b.$submitButton = a(this), b._submitIfValid = !0
                }), (this.options.declarative === !0 || "true" === this.options.declarative) && this.$form.find("[name], [data-" + c + "-field]").each(function() {
                    var e = a(this),
                        f = e.attr("name") || e.attr("data-" + c + "-field"),
                        g = b._parseOptions(e);
                    g && (e.attr("data-" + c + "-field", f), d.fields[f] = a.extend({}, g, d.fields[f]))
                }), this.options = a.extend(!0, this.options, d), "string" == typeof this.options.err.parent && (this.options.err.parent = new RegExp(this.options.err.parent)), this.options.container && (this.options.err.container = this.options.container, delete this.options.container), this.options.feedbackIcons && (this.options.icon = a.extend(!0, this.options.icon, this.options.feedbackIcons), delete this.options.feedbackIcons), this.options.group && (this.options.row.selector = this.options.group, delete this.options.group), this.options.submitButtons && (this.options.button.selector = this.options.submitButtons, delete this.options.submitButtons), FormValidation.I18n[this.options.locale] || (this.options.locale = a.fn.formValidation.DEFAULT_OPTIONS.locale), (this.options.declarative === !0 || "true" === this.options.declarative) && (this.options = a.extend(!0, this.options, {
                    addOns: this._parseAddOnOptions()
                })), this.$hiddenButton = a("<button/>").attr("type", "submit").prependTo(this.$form).addClass("fv-hidden-submit").css({
                    display: "none",
                    width: 0,
                    height: 0
                }), this.$form.on("click." + this._namespace, '[type="submit"]', function(c) {
                    if (!c.isDefaultPrevented()) {
                        var d = a(c.target),
                            e = d.is('[type="submit"]') ? d.eq(0) : d.parent('[type="submit"]').eq(0);
                        if (b.options.button.selector && !e.is(b.options.button.selector) && !e.is(b.$hiddenButton)) return b.$form.off("submit." + b._namespace).submit(), !1
                    }
                });
                for (var e in this.options.fields) this._initField(e);
                for (var f in this.options.addOns) "function" == typeof FormValidation.AddOn[f].init && FormValidation.AddOn[f].init(this, this.options.addOns[f]);
                this.$form.trigger(a.Event(this.options.events.formInit), {
                    bv: this,
                    fv: this,
                    options: this.options
                }), this.options.onPreValidate && this.$form.on(this.options.events.formPreValidate, function(a) {
                    FormValidation.Helper.call(b.options.onPreValidate, [a])
                }), this.options.onSuccess && this.$form.on(this.options.events.formSuccess, function(a) {
                    FormValidation.Helper.call(b.options.onSuccess, [a])
                }), this.options.onError && this.$form.on(this.options.events.formError, function(a) {
                    FormValidation.Helper.call(b.options.onError, [a])
                }), this.options.onReset && this.$form.on(this.options.events.formReset, function(a) {
                    FormValidation.Helper.call(b.options.onReset, [a])
                })
            },
            _initField: function(b) {
                var c = this._namespace,
                    d = a([]);
                switch (typeof b) {
                    case "object":
                        d = b, b = b.attr("data-" + c + "-field");
                        break;
                    case "string":
                        d = this.getFieldElements(b), d.attr("data-" + c + "-field", b)
                }
                if (0 !== d.length && null !== this.options.fields[b] && null !== this.options.fields[b].validators) {
                    var e, f, g = this.options.fields[b].validators;
                    for (e in g) f = g[e].alias || e, FormValidation.Validator[f] || delete this.options.fields[b].validators[e];
                    null === this.options.fields[b].enabled && (this.options.fields[b].enabled = !0);
                    for (var h = this, i = d.length, j = d.attr("type"), k = 1 === i || "radio" === j || "checkbox" === j, l = this._getFieldTrigger(d.eq(0)), m = this.options.err.clazz.split(" ").join("."), n = a.map(l, function(a) {
                        return a + ".update." + c
                    }).join(" "), o = 0; i > o; o++) {
                        var p = d.eq(o),
                            q = this.options.fields[b].row || this.options.row.selector,
                            r = p.closest(q),
                            s = "function" == typeof(this.options.fields[b].container || this.options.fields[b].err || this.options.err.container) ? (this.options.fields[b].container || this.options.fields[b].err || this.options.err.container).call(this, p, this) : this.options.fields[b].container || this.options.fields[b].err || this.options.err.container,
                            t = s && "tooltip" !== s && "popover" !== s ? a(s) : this._getMessageContainer(p, q);
                        s && "tooltip" !== s && "popover" !== s && t.addClass(this.options.err.clazz), t.find("." + m + "[data-" + c + "-validator][data-" + c + '-for="' + b + '"]').remove(), r.find("i[data-" + c + '-icon-for="' + b + '"]').remove(), p.off(n).on(n, function() {
                            h.updateStatus(a(this), h.STATUS_NOT_VALIDATED)
                        }), p.data(c + ".messages", t);
                        for (e in g) p.data(c + ".result." + e, this.STATUS_NOT_VALIDATED), k && o !== i - 1 || a("<small/>").css("display", "none").addClass(this.options.err.clazz).attr("data-" + c + "-validator", e).attr("data-" + c + "-for", b).attr("data-" + c + "-result", this.STATUS_NOT_VALIDATED).html(this._getMessage(b, e)).appendTo(t), f = g[e].alias || e, "function" == typeof FormValidation.Validator[f].init && FormValidation.Validator[f].init(this, p, this.options.fields[b].validators[e], e);
                        if (this.options.fields[b].icon !== !1 && "false" !== this.options.fields[b].icon && this.options.icon && this.options.icon.valid && this.options.icon.invalid && this.options.icon.validating && (!k || o === i - 1)) {
                            r.addClass(this.options.row.feedback);
                            var u = a("<i/>").css("display", "none").addClass(this.options.icon.feedback).attr("data-" + c + "-icon-for", b).insertAfter(p);
                            (k ? d : p).data(c + ".icon", u), ("tooltip" === s || "popover" === s) && ((k ? d : p).on(this.options.events.fieldError, function() {
                                r.addClass("fv-has-tooltip")
                            }).on(this.options.events.fieldSuccess, function() {
                                r.removeClass("fv-has-tooltip")
                            }), p.off("focus.container." + c).on("focus.container." + c, function() {
                                h._showTooltip(a(this), s)
                            }).off("blur.container." + c).on("blur.container." + c, function() {
                                h._hideTooltip(a(this), s)
                            })), "string" == typeof this.options.fields[b].icon && "true" !== this.options.fields[b].icon ? u.appendTo(a(this.options.fields[b].icon)) : this._fixIcon(p, u)
                        }
                    }
                    var v = [];
                    for (e in g) f = g[e].alias || e, g[e].priority = parseInt(g[e].priority || FormValidation.Validator[f].priority || 1, 10), v.push({
                        validator: e,
                        priority: g[e].priority
                    });
                    v = v.sort(function(a, b) {
                        return a.priority - b.priority
                    }), d.data(c + ".validators", v).on(this.options.events.fieldSuccess, function(a, b) {
                        var c = h.getOptions(b.field, null, "onSuccess");
                        c && FormValidation.Helper.call(c, [a, b])
                    }).on(this.options.events.fieldError, function(a, b) {
                        var c = h.getOptions(b.field, null, "onError");
                        c && FormValidation.Helper.call(c, [a, b])
                    }).on(this.options.events.fieldReset, function(a, b) {
                        var c = h.getOptions(b.field, null, "onReset");
                        c && FormValidation.Helper.call(c, [a, b])
                    }).on(this.options.events.fieldStatus, function(a, b) {
                        var c = h.getOptions(b.field, null, "onStatus");
                        c && FormValidation.Helper.call(c, [a, b])
                    }).on(this.options.events.validatorError, function(a, b) {
                        var c = h.getOptions(b.field, b.validator, "onError");
                        c && FormValidation.Helper.call(c, [a, b])
                    }).on(this.options.events.validatorIgnored, function(a, b) {
                        var c = h.getOptions(b.field, b.validator, "onIgnored");
                        c && FormValidation.Helper.call(c, [a, b])
                    }).on(this.options.events.validatorSuccess, function(a, b) {
                        var c = h.getOptions(b.field, b.validator, "onSuccess");
                        c && FormValidation.Helper.call(c, [a, b])
                    }), this.onLiveChange(d, "live", function() {
                        h._exceedThreshold(a(this)) && h.validateField(a(this))
                    }), d.trigger(a.Event(this.options.events.fieldInit), {
                        bv: this,
                        fv: this,
                        field: b,
                        element: d
                    })
                }
            },
            _isExcluded: function(b) {
                var c = this._namespace,
                    d = b.attr("data-" + c + "-excluded"),
                    e = b.attr("data-" + c + "-field") || b.attr("name");
                switch (!0) {
                    case !!e && this.options.fields && this.options.fields[e] && ("true" === this.options.fields[e].excluded || this.options.fields[e].excluded === !0):
                    case "true" === d:
                    case "" === d:
                        return !0;
                    case !!e && this.options.fields && this.options.fields[e] && ("false" === this.options.fields[e].excluded || this.options.fields[e].excluded === !1):
                    case "false" === d:
                        return !1;
                    case !!e && this.options.fields && this.options.fields[e] && "function" == typeof this.options.fields[e].excluded:
                        return this.options.fields[e].excluded.call(this, b, this);
                    case !!e && this.options.fields && this.options.fields[e] && "string" == typeof this.options.fields[e].excluded:
                    case d:
                        return FormValidation.Helper.call(this.options.fields[e].excluded, [b, this]);
                    default:
                        if (this.options.excluded) {
                            "string" == typeof this.options.excluded && (this.options.excluded = a.map(this.options.excluded.split(","), function(b) {
                                return a.trim(b)
                            }));
                            for (var f = this.options.excluded.length, g = 0; f > g; g++)
                                if ("string" == typeof this.options.excluded[g] && b.is(this.options.excluded[g]) || "function" == typeof this.options.excluded[g] && this.options.excluded[g].call(this, b, this) === !0) return !0
                        }
                        return !1
                }
            },
            _getFieldTrigger: function(a) {
                var b = this._namespace,
                    c = a.data(b + ".trigger");
                if (c) return c;
                var d = a.attr("type"),
                    e = a.attr("data-" + b + "-field"),
                    f = "radio" === d || "checkbox" === d || "file" === d || "SELECT" === a.get(0).tagName ? "change" : this._ieVersion >= 10 && a.attr("placeholder") ? "keyup" : this._changeEvent;
                return c = ((this.options.fields[e] ? this.options.fields[e].trigger : null) || this.options.trigger || f).split(" "), a.data(b + ".trigger", c), c
            },
            _getMessage: function(a, b) {
                if (!this.options.fields[a] || !this.options.fields[a].validators) return "";
                var c = this.options.fields[a].validators,
                    d = c[b] && c[b].alias ? c[b].alias : b;
                if (!FormValidation.Validator[d]) return "";
                switch (!0) {
                    case !!c[b].message:
                        return c[b].message;
                    case !!this.options.fields[a].message:
                        return this.options.fields[a].message;
                    case !!this.options.message:
                        return this.options.message;
                    case !!FormValidation.I18n[this.options.locale] && !!FormValidation.I18n[this.options.locale][d] && !!FormValidation.I18n[this.options.locale][d]["default"]:
                        return FormValidation.I18n[this.options.locale][d]["default"];
                    default:
                        return this.DEFAULT_MESSAGE
                }
            },
            _getMessageContainer: function(a, b) {
                if (!this.options.err.parent) throw new Error("The err.parent option is not defined");
                var c = a.parent();
                if (c.is(b)) return c;
                var d = c.attr("class");
                return d && this.options.err.parent.test(d) ? c : this._getMessageContainer(c, b)
            },
            _parseAddOnOptions: function() {
                var a = this._namespace,
                    b = this.$form.attr("data-" + a + "-addons"),
                    c = this.options.addOns || {};
                if (b) {
                    b = b.replace(/\s/g, "").split(",");
                    for (var d = 0; d < b.length; d++) c[b[d]] || (c[b[d]] = {})
                }
                var e, f, g, h;
                for (e in c)
                    if (FormValidation.AddOn[e]) {
                        if (f = FormValidation.AddOn[e].html5Attributes)
                            for (g in f) h = this.$form.attr("data-" + a + "-addons-" + e.toLowerCase() + "-" + g.toLowerCase()), h && (c[e][f[g]] = h)
                    } else delete c[e];
                return c
            },
            _parseOptions: function(b) {
                var c, d, e, f, g, h, i, j, k, l = this._namespace,
                    m = b.attr("name") || b.attr("data-" + l + "-field"),
                    n = {},
                    o = new RegExp("^data-" + l + "-([a-z]+)-alias$"),
                    p = a.extend({}, FormValidation.Validator);
                a.each(b.get(0).attributes, function(a, b) {
                    b.value && o.test(b.name) && (d = b.name.split("-")[2], p[b.value] && (p[d] = p[b.value], p[d].alias = b.value))
                });
                for (d in p)
                    if (c = p[d], e = "data-" + l + "-" + d.toLowerCase(), f = b.attr(e) + "", k = "function" == typeof c.enableByHtml5 ? c.enableByHtml5(b) : null, k && "false" !== f || k !== !0 && ("" === f || "true" === f || e === f.toLowerCase())) {
                        c.html5Attributes = a.extend({}, {
                            message: "message",
                            onerror: "onError",
                            onreset: "onReset",
                            onsuccess: "onSuccess",
                            priority: "priority",
                            transformer: "transformer"
                        }, c.html5Attributes), n[d] = a.extend({}, k === !0 ? {} : k, n[d]), c.alias && (n[d].alias = c.alias);
                        for (j in c.html5Attributes) g = c.html5Attributes[j], h = "data-" + l + "-" + d.toLowerCase() + "-" + j, i = b.attr(h), i && ("true" === i || h === i.toLowerCase() ? i = !0 : "false" === i && (i = !1), n[d][g] = i)
                    }
                var q = {
                        autoFocus: b.attr("data-" + l + "-autofocus"),
                        err: b.attr("data-" + l + "-err-container") || b.attr("data-" + l + "-container"),
                        enabled: b.attr("data-" + l + "-enabled"),
                        excluded: b.attr("data-" + l + "-excluded"),
                        icon: b.attr("data-" + l + "-icon") || b.attr("data-" + l + "-feedbackicons") || (this.options.fields && this.options.fields[m] ? this.options.fields[m].feedbackIcons : null),
                        message: b.attr("data-" + l + "-message"),
                        onError: b.attr("data-" + l + "-onerror"),
                        onReset: b.attr("data-" + l + "-onreset"),
                        onStatus: b.attr("data-" + l + "-onstatus"),
                        onSuccess: b.attr("data-" + l + "-onsuccess"),
                        row: b.attr("data-" + l + "-row") || b.attr("data-" + l + "-group") || (this.options.fields && this.options.fields[m] ? this.options.fields[m].group : null),
                        selector: b.attr("data-" + l + "-selector"),
                        threshold: b.attr("data-" + l + "-threshold"),
                        transformer: b.attr("data-" + l + "-transformer"),
                        trigger: b.attr("data-" + l + "-trigger"),
                        verbose: b.attr("data-" + l + "-verbose"),
                        validators: n
                    },
                    r = a.isEmptyObject(q),
                    s = a.isEmptyObject(n);
                return !s || !r && this.options.fields && this.options.fields[m] ? q : null
            },
            _submit: function() {
                var b = this.isValid();
                if (null !== b) {
                    var c = b ? this.options.events.formSuccess : this.options.events.formError,
                        d = a.Event(c);
                    this.$form.trigger(d), this.$submitButton && (b ? this._onSuccess(d) : this._onError(d))
                }
            },
            _onError: function(b) {
                if (!b.isDefaultPrevented()) {
                    if ("submitted" === this.options.live) {
                        this.options.live = "enabled";
                        var c = this;
                        for (var d in this.options.fields) ! function(b) {
                            var d = c.getFieldElements(b);
                            d.length && c.onLiveChange(d, "live", function() {
                                c._exceedThreshold(a(this)) && c.validateField(a(this))
                            })
                        }(d)
                    }
                    for (var e = this._namespace, f = 0; f < this.$invalidFields.length; f++) {
                        var g = this.$invalidFields.eq(f),
                            h = this.isOptionEnabled(g.attr("data-" + e + "-field"), "autoFocus");
                        if (h) {
                            g.focus();
                            break
                        }
                    }
                }
            },
            _onFieldValidated: function(b, c) {
                var d = this._namespace,
                    e = b.attr("data-" + d + "-field"),
                    f = this.options.fields[e].validators,
                    g = {},
                    h = 0,
                    i = {
                        bv: this,
                        fv: this,
                        field: e,
                        element: b,
                        validator: c,
                        result: b.data(d + ".response." + c)
                    };
                if (c) switch (b.data(d + ".result." + c)) {
                    case this.STATUS_INVALID:
                        b.trigger(a.Event(this.options.events.validatorError), i);
                        break;
                    case this.STATUS_VALID:
                        b.trigger(a.Event(this.options.events.validatorSuccess), i);
                        break;
                    case this.STATUS_IGNORED:
                        b.trigger(a.Event(this.options.events.validatorIgnored), i)
                }
                g[this.STATUS_NOT_VALIDATED] = 0, g[this.STATUS_VALIDATING] = 0, g[this.STATUS_INVALID] = 0, g[this.STATUS_VALID] = 0, g[this.STATUS_IGNORED] = 0;
                for (var j in f)
                    if (f[j].enabled !== !1) {
                        h++;
                        var k = b.data(d + ".result." + j);
                        k && g[k]++
                    }
                g[this.STATUS_VALID] + g[this.STATUS_IGNORED] === h ? (this.$invalidFields = this.$invalidFields.not(b), b.trigger(a.Event(this.options.events.fieldSuccess), i)) : (0 === g[this.STATUS_NOT_VALIDATED] || !this.isOptionEnabled(e, "verbose")) && 0 === g[this.STATUS_VALIDATING] && g[this.STATUS_INVALID] > 0 && (this.$invalidFields = this.$invalidFields.add(b), b.trigger(a.Event(this.options.events.fieldError), i))
            },
            _onSuccess: function(a) {
                a.isDefaultPrevented() || this.disableSubmitButtons(!0).defaultSubmit()
            },
            _fixIcon: function(a, b) {},
            _createTooltip: function(a, b, c) {},
            _destroyTooltip: function(a, b) {},
            _hideTooltip: function(a, b) {},
            _showTooltip: function(a, b) {},
            defaultSubmit: function() {
                var b = this._namespace;
                this.$submitButton && a("<input/>").attr({
                    type: "hidden",
                    name: this.$submitButton.attr("name")
                }).attr("data-" + b + "-submit-hidden", "").val(this.$submitButton.val()).appendTo(this.$form), this.$form.off("submit." + b).submit()
            },
            disableSubmitButtons: function(a) {
                return a ? "disabled" !== this.options.live && this.$form.find(this.options.button.selector).attr("disabled", "disabled").addClass(this.options.button.disabled) : this.$form.find(this.options.button.selector).removeAttr("disabled").removeClass(this.options.button.disabled), this
            },
            getFieldElements: function(b) {
                if (!this._cacheFields[b])
                    if (this.options.fields[b] && this.options.fields[b].selector) {
                        var c = this.$form.find(this.options.fields[b].selector);
                        this._cacheFields[b] = c.length ? c : a(this.options.fields[b].selector)
                    } else this._cacheFields[b] = this.$form.find('[name="' + b + '"]');
                return this._cacheFields[b]
            },
            getFieldValue: function(a, b) {
                var c, d = this._namespace;
                if ("string" == typeof a) {
                    if (c = this.getFieldElements(a), 0 === c.length) return null
                } else c = a, a = c.attr("data-" + d + "-field");
                if (!a || !this.options.fields[a]) return c.val();
                var e = (this.options.fields[a].validators && this.options.fields[a].validators[b] ? this.options.fields[a].validators[b].transformer : null) || this.options.fields[a].transformer;
                return e ? FormValidation.Helper.call(e, [c, b, this]) : c.val()
            },
            getNamespace: function() {
                return this._namespace
            },
            getOptions: function(a, b, c) {
                var d = this._namespace;
                if (!a) return c ? this.options[c] : this.options;
                if ("object" == typeof a && (a = a.attr("data-" + d + "-field")), !this.options.fields[a]) return null;
                var e = this.options.fields[a];
                return b ? e.validators && e.validators[b] ? c ? e.validators[b][c] : e.validators[b] : null : c ? e[c] : e
            },
            getStatus: function(a, b) {
                var c = this._namespace;
                switch (typeof a) {
                    case "object":
                        return a.data(c + ".result." + b);
                    case "string":
                    default:
                        return this.getFieldElements(a).eq(0).data(c + ".result." + b)
                }
            },
            isOptionEnabled: function(a, b) {
                return !this.options.fields[a] || "true" !== this.options.fields[a][b] && this.options.fields[a][b] !== !0 ? !this.options.fields[a] || "false" !== this.options.fields[a][b] && this.options.fields[a][b] !== !1 ? "true" === this.options[b] || this.options[b] === !0 : !1 : !0
            },
            isValid: function() {
                for (var a in this.options.fields) {
                    var b = this.isValidField(a);
                    if (null === b) return null;
                    if (b === !1) return !1
                }
                return !0
            },
            isValidContainer: function(b) {
                var c = this,
                    d = this._namespace,
                    e = [],
                    f = "string" == typeof b ? a(b) : b;
                if (0 === f.length) return !0;
                f.find("[data-" + d + "-field]").each(function() {
                    var b = a(this);
                    c._isExcluded(b) || e.push(b)
                });
                for (var g = e.length, h = this.options.err.clazz.split(" ").join("."), i = 0; g > i; i++) {
                    var j = e[i],
                        k = j.attr("data-" + d + "-field"),
                        l = j.data(d + ".messages").find("." + h + "[data-" + d + "-validator][data-" + d + '-for="' + k + '"]');
                    if (!this.options.fields || !this.options.fields[k] || "false" !== this.options.fields[k].enabled && this.options.fields[k].enabled !== !1) {
                        if (l.filter("[data-" + d + '-result="' + this.STATUS_INVALID + '"]').length > 0) return !1;
                        if (l.filter("[data-" + d + '-result="' + this.STATUS_NOT_VALIDATED + '"]').length > 0 || l.filter("[data-" + d + '-result="' + this.STATUS_VALIDATING + '"]').length > 0) return null
                    }
                }
                return !0
            },
            isValidField: function(b) {
                var c = this._namespace,
                    d = a([]);
                switch (typeof b) {
                    case "object":
                        d = b, b = b.attr("data-" + c + "-field");
                        break;
                    case "string":
                        d = this.getFieldElements(b)
                }
                if (0 === d.length || !this.options.fields[b] || "false" === this.options.fields[b].enabled || this.options.fields[b].enabled === !1) return !0;
                for (var e, f, g, h = d.attr("type"), i = "radio" === h || "checkbox" === h ? 1 : d.length, j = 0; i > j; j++)
                    if (e = d.eq(j), !this._isExcluded(e))
                        for (f in this.options.fields[b].validators)
                            if (this.options.fields[b].validators[f].enabled !== !1) {
                                if (g = e.data(c + ".result." + f), g === this.STATUS_VALIDATING || g === this.STATUS_NOT_VALIDATED) return null;
                                if (g === this.STATUS_INVALID) return !1
                            }
                return !0
            },
            offLiveChange: function(b, c) {
                if (null === b || 0 === b.length) return this;
                var d = this._namespace,
                    e = this._getFieldTrigger(b.eq(0)),
                    f = a.map(e, function(a) {
                        return a + "." + c + "." + d
                    }).join(" ");
                return b.off(f), this
            },
            onLiveChange: function(b, c, d) {
                if (null === b || 0 === b.length) return this;
                var e = this._namespace,
                    f = this._getFieldTrigger(b.eq(0)),
                    g = a.map(f, function(a) {
                        return a + "." + c + "." + e
                    }).join(" ");
                switch (this.options.live) {
                    case "submitted":
                        break;
                    case "disabled":
                        b.off(g);
                        break;
                    case "enabled":
                    default:
                        b.off(g).on(g, function(a) {
                            d.apply(this, arguments)
                        })
                }
                return this
            },
            updateMessage: function(b, c, d) {
                var e = this._namespace,
                    f = a([]);
                switch (typeof b) {
                    case "object":
                        f = b, b = b.attr("data-" + e + "-field");
                        break;
                    case "string":
                        f = this.getFieldElements(b)
                }
                var g = this.options.err.clazz.split(" ").join(".");
                return f.each(function() {
                    a(this).data(e + ".messages").find("." + g + "[data-" + e + '-validator="' + c + '"][data-' + e + '-for="' + b + '"]').html(d)
                }), this
            },
            updateStatus: function(b, c, d) {
                var e = this._namespace,
                    f = a([]);
                switch (typeof b) {
                    case "object":
                        f = b, b = b.attr("data-" + e + "-field");
                        break;
                    case "string":
                        f = this.getFieldElements(b)
                }
                if (!b || !this.options.fields[b]) return this;
                c === this.STATUS_NOT_VALIDATED && (this._submitIfValid = !1);
                for (var g = this, h = f.attr("type"), i = this.options.fields[b].row || this.options.row.selector, j = "radio" === h || "checkbox" === h ? 1 : f.length, k = this.options.err.clazz.split(" ").join("."), l = 0; j > l; l++) {
                    var m = f.eq(l);
                    if (!this._isExcluded(m)) {
                        var n, o, p = m.closest(i),
                            q = m.data(e + ".messages"),
                            r = q.find("." + k + "[data-" + e + "-validator][data-" + e + '-for="' + b + '"]'),
                            s = d ? r.filter("[data-" + e + '-validator="' + d + '"]') : r,
                            t = m.data(e + ".icon"),
                            u = "function" == typeof(this.options.fields[b].container || this.options.fields[b].err || this.options.err.container) ? (this.options.fields[b].container || this.options.fields[b].err || this.options.err.container).call(this, m, this) : this.options.fields[b].container || this.options.fields[b].err || this.options.err.container,
                            v = null;
                        if (d) m.data(e + ".result." + d, c);
                        else
                            for (var w in this.options.fields[b].validators) m.data(e + ".result." + w, c);
                        switch (s.attr("data-" + e + "-result", c), c) {
                            case this.STATUS_VALIDATING:
                                v = null, this.disableSubmitButtons(!0), m.removeClass(this.options.control.valid).removeClass(this.options.control.invalid), p.removeClass(this.options.row.valid).removeClass(this.options.row.invalid), t && t.removeClass(this.options.icon.valid).removeClass(this.options.icon.invalid).addClass(this.options.icon.validating).show();
                                break;
                            case this.STATUS_INVALID:
                                v = !1, this.disableSubmitButtons(!0), m.removeClass(this.options.control.valid).addClass(this.options.control.invalid), p.removeClass(this.options.row.valid).addClass(this.options.row.invalid), t && t.removeClass(this.options.icon.valid).removeClass(this.options.icon.validating).addClass(this.options.icon.invalid).show();
                                break;
                            case this.STATUS_IGNORED:
                            case this.STATUS_VALID:
                                n = r.filter("[data-" + e + '-result="' + this.STATUS_VALIDATING + '"]').length > 0, o = r.filter("[data-" + e + '-result="' + this.STATUS_NOT_VALIDATED + '"]').length > 0;
                                var x = r.filter("[data-" + e + '-result="' + this.STATUS_IGNORED + '"]').length;
                                v = n || o ? null : r.filter("[data-" + e + '-result="' + this.STATUS_VALID + '"]').length + x === r.length, m.removeClass(this.options.control.valid).removeClass(this.options.control.invalid), v === !0 ? (this.disableSubmitButtons(this.isValid() === !1), c === this.STATUS_VALID && m.addClass(this.options.control.valid)) : v === !1 && (this.disableSubmitButtons(!0), c === this.STATUS_VALID && m.addClass(this.options.control.invalid)), t && (t.removeClass(this.options.icon.invalid).removeClass(this.options.icon.validating).removeClass(this.options.icon.valid), (c === this.STATUS_VALID || x !== r.length) && t.addClass(n ? this.options.icon.validating : null === v ? "" : v ? this.options.icon.valid : this.options.icon.invalid).show());
                                var y = this.isValidContainer(p);
                                null !== y && (p.removeClass(this.options.row.valid).removeClass(this.options.row.invalid), (c === this.STATUS_VALID || x !== r.length) && p.addClass(y ? this.options.row.valid : this.options.row.invalid));
                                break;
                            case this.STATUS_NOT_VALIDATED:
                            default:
                                v = null, this.disableSubmitButtons(!1), m.removeClass(this.options.control.valid).removeClass(this.options.control.invalid), p.removeClass(this.options.row.valid).removeClass(this.options.row.invalid), t && t.removeClass(this.options.icon.valid).removeClass(this.options.icon.invalid).removeClass(this.options.icon.validating).hide()
                        }!t || "tooltip" !== u && "popover" !== u ? c === this.STATUS_INVALID ? s.show() : s.hide() : v === !1 ? this._createTooltip(m, r.filter("[data-" + e + '-result="' + g.STATUS_INVALID + '"]').eq(0).html(), u) : this._destroyTooltip(m, u), m.trigger(a.Event(this.options.events.fieldStatus), {
                            bv: this,
                            fv: this,
                            field: b,
                            element: m,
                            status: c,
                            validator: d
                        }), this._onFieldValidated(m, d)
                    }
                }
                return this
            },
            validate: function() {
                if (a.isEmptyObject(this.options.fields)) return this._submit(), this;
                this.$form.trigger(a.Event(this.options.events.formPreValidate)), this.disableSubmitButtons(!0), this._submitIfValid = !1;
                for (var b in this.options.fields) this.validateField(b);
                return this._submit(), this._submitIfValid = !0, this
            },
            validateField: function(b) {
                var c = this._namespace,
                    d = a([]);
                switch (typeof b) {
                    case "object":
                        d = b, b = b.attr("data-" + c + "-field");
                        break;
                    case "string":
                        d = this.getFieldElements(b)
                }
                if (0 === d.length || !this.options.fields[b] || "false" === this.options.fields[b].enabled || this.options.fields[b].enabled === !1) return this;
                for (var e, f, g, h = this, i = d.attr("type"), j = "radio" !== i && "checkbox" !== i || "disabled" === this.options.live ? d.length : 1, k = "radio" === i || "checkbox" === i, l = this.options.fields[b].validators, m = this.isOptionEnabled(b, "verbose"), n = 0; j > n; n++) {
                    var o = d.eq(n);
                    if (!this._isExcluded(o))
                        for (var p = !1, q = o.data(c + ".validators"), r = q.length, s = 0; r > s && (e = q[s].validator, o.data(c + ".dfs." + e) && o.data(c + ".dfs." + e).reject(), !p); s++) {
                            var t = o.data(c + ".result." + e);
                            if (t !== this.STATUS_VALID && t !== this.STATUS_INVALID)
                                if (l[e].enabled !== !1)
                                    if (o.data(c + ".result." + e, this.STATUS_VALIDATING), f = l[e].alias || e, g = FormValidation.Validator[f].validate(this, o, l[e], e), "object" == typeof g && g.resolve) this.updateStatus(k ? b : o, this.STATUS_VALIDATING, e), o.data(c + ".dfs." + e, g), g.done(function(a, b, d) {
                                        a.removeData(c + ".dfs." + b).data(c + ".response." + b, d), d.message && h.updateMessage(a, b, d.message), h.updateStatus(k ? a.attr("data-" + c + "-field") : a, d.valid === !0 ? h.STATUS_VALID : d.valid === !1 ? h.STATUS_INVALID : h.STATUS_IGNORED, b), d.valid && h._submitIfValid === !0 ? h._submit() : d.valid !== !1 || m || (p = !0)
                                    });
                                    else if ("object" == typeof g && void 0 !== g.valid) {
                                        if (o.data(c + ".response." + e, g), g.message && this.updateMessage(k ? b : o, e, g.message), this.updateStatus(k ? b : o, g.valid === !0 ? this.STATUS_VALID : g.valid === !1 ? this.STATUS_INVALID : this.STATUS_IGNORED, e), g.valid === !1 && !m) break
                                    } else if ("boolean" == typeof g) {
                                        if (o.data(c + ".response." + e, g), this.updateStatus(k ? b : o, g ? this.STATUS_VALID : this.STATUS_INVALID, e), !g && !m) break
                                    } else null === g && (o.data(c + ".response." + e, g), this.updateStatus(k ? b : o, this.STATUS_IGNORED, e));
                                else this.updateStatus(k ? b : o, this.STATUS_IGNORED, e);
                            else this._onFieldValidated(o, e)
                        }
                }
                return this
            },
            addField: function(b, c) {
                var d = this._namespace,
                    e = a([]);
                switch (typeof b) {
                    case "object":
                        e = b, b = b.attr("data-" + d + "-field") || b.attr("name");
                        break;
                    case "string":
                        delete this._cacheFields[b], e = this.getFieldElements(b)
                }
                e.attr("data-" + d + "-field", b);
                for (var f = e.attr("type"), g = "radio" === f || "checkbox" === f ? 1 : e.length, h = 0; g > h; h++) {
                    var i = e.eq(h),
                        j = this._parseOptions(i);
                    j = null === j ? c : a.extend(!0, j, c), this.options.fields[b] = a.extend(!0, this.options.fields[b], j), this._cacheFields[b] = this._cacheFields[b] ? this._cacheFields[b].add(i) : i, this._initField("checkbox" === f || "radio" === f ? b : i)
                }
                return this.disableSubmitButtons(!1), this.$form.trigger(a.Event(this.options.events.fieldAdded), {
                    field: b,
                    element: e,
                    options: this.options.fields[b]
                }), this
            },
            destroy: function() {
                var a, b, c, d, e, f, g, h, i = this._namespace;
                for (b in this.options.fields)
                    for (c = this.getFieldElements(b), a = 0; a < c.length; a++) {
                        d = c.eq(a);
                        for (e in this.options.fields[b].validators) d.data(i + ".dfs." + e) && d.data(i + ".dfs." + e).reject(), d.removeData(i + ".result." + e).removeData(i + ".response." + e).removeData(i + ".dfs." + e), h = this.options.fields[b].validators[e].alias || e, "function" == typeof FormValidation.Validator[h].destroy && FormValidation.Validator[h].destroy(this, d, this.options.fields[b].validators[e], e)
                    }
                var j = this.options.err.clazz.split(" ").join(".");
                for (b in this.options.fields)
                    for (c = this.getFieldElements(b), g = this.options.fields[b].row || this.options.row.selector, a = 0; a < c.length; a++) {
                        d = c.eq(a);
                        var k = d.data(i + ".messages");
                        k && k.find("." + j + "[data-" + i + "-validator][data-" + i + '-for="' + b + '"]').remove(), d.removeData(i + ".messages").removeData(i + ".validators").closest(g).removeClass(this.options.row.valid).removeClass(this.options.row.invalid).removeClass(this.options.row.feedback).end().off("." + i).removeAttr("data-" + i + "-field");
                        var l = "function" == typeof(this.options.fields[b].container || this.options.fields[b].err || this.options.err.container) ? (this.options.fields[b].container || this.options.fields[b].err || this.options.err.container).call(this, d, this) : this.options.fields[b].container || this.options.fields[b].err || this.options.err.container;
                        ("tooltip" === l || "popover" === l) && this._destroyTooltip(d, l), f = d.data(i + ".icon"), f && f.remove(), d.removeData(i + ".icon").removeData(i + ".trigger")
                    }
                for (var m in this.options.addOns) "function" == typeof FormValidation.AddOn[m].destroy && FormValidation.AddOn[m].destroy(this, this.options.addOns[m]);
                this.disableSubmitButtons(!1), this.$hiddenButton.remove(), this.$form.removeClass(this.options.elementClass).off("." + i).removeData("bootstrapValidator").removeData("formValidation").find("[data-" + i + "-submit-hidden]").remove().end().find('[type="submit"]').off("click." + i)
            },
            enableFieldValidators: function(a, b, c) {
                var d = this.options.fields[a].validators;
                if (c && d && d[c] && d[c].enabled !== b) this.options.fields[a].validators[c].enabled = b, this.updateStatus(a, this.STATUS_NOT_VALIDATED, c);
                else if (!c && this.options.fields[a].enabled !== b) {
                    this.options.fields[a].enabled = b;
                    for (var e in d) this.enableFieldValidators(a, b, e)
                }
                return this
            },
            getDynamicOption: function(a, b) {
                var c = "string" == typeof a ? this.getFieldElements(a) : a,
                    d = c.val();
                if ("function" == typeof b) return FormValidation.Helper.call(b, [d, this, c]);
                if ("string" == typeof b) {
                    var e = this.getFieldElements(b);
                    return e.length ? e.val() : FormValidation.Helper.call(b, [d, this, c]) || b
                }
                return null
            },
            getForm: function() {
                return this.$form
            },
            getInvalidFields: function() {
                return this.$invalidFields
            },
            getLocale: function() {
                return this.options.locale
            },
            getMessages: function(b, c) {
                var d = this,
                    e = this._namespace,
                    f = [],
                    g = a([]);
                switch (!0) {
                    case b && "object" == typeof b:
                        g = b;
                        break;
                    case b && "string" == typeof b:
                        var h = this.getFieldElements(b);
                        if (h.length > 0) {
                            var i = h.attr("type");
                            g = "radio" === i || "checkbox" === i ? h.eq(0) : h
                        }
                        break;
                    default:
                        g = this.$invalidFields
                }
                var j = c ? "[data-" + e + '-validator="' + c + '"]' : "",
                    k = this.options.err.clazz.split(" ").join(".");
                return g.each(function() {
                    f = f.concat(a(this).data(e + ".messages").find("." + k + "[data-" + e + '-for="' + a(this).attr("data-" + e + "-field") + '"][data-' + e + '-result="' + d.STATUS_INVALID + '"]' + j).map(function() {
                        var b = a(this).attr("data-" + e + "-validator"),
                            c = a(this).attr("data-" + e + "-for");
                        return d.options.fields[c].validators[b].enabled === !1 ? "" : a(this).html()
                    }).get())
                }), f
            },
            getSubmitButton: function() {
                return this.$submitButton
            },
            removeField: function(b) {
                var c = this._namespace,
                    d = a([]);
                switch (typeof b) {
                    case "object":
                        d = b, b = b.attr("data-" + c + "-field") || b.attr("name"), d.attr("data-" + c + "-field", b);
                        break;
                    case "string":
                        d = this.getFieldElements(b)
                }
                if (0 === d.length) return this;
                for (var e = d.attr("type"), f = "radio" === e || "checkbox" === e ? 1 : d.length, g = 0; f > g; g++) {
                    var h = d.eq(g);
                    this.$invalidFields = this.$invalidFields.not(h), this._cacheFields[b] = this._cacheFields[b].not(h)
                }
                return this._cacheFields[b] && 0 !== this._cacheFields[b].length || delete this.options.fields[b], ("checkbox" === e || "radio" === e) && this._initField(b), this.disableSubmitButtons(!1), this.$form.trigger(a.Event(this.options.events.fieldRemoved), {
                    field: b,
                    element: d
                }), this
            },
            resetField: function(b, c) {
                var d = this._namespace,
                    e = a([]);
                switch (typeof b) {
                    case "object":
                        e = b, b = b.attr("data-" + d + "-field");
                        break;
                    case "string":
                        e = this.getFieldElements(b)
                }
                var f = 0,
                    g = e.length;
                if (this.options.fields[b])
                    for (f = 0; g > f; f++)
                        for (var h in this.options.fields[b].validators) e.eq(f).removeData(d + ".dfs." + h);
                if (c) {
                    var i = e.attr("type");
                    "radio" === i || "checkbox" === i ? e.prop("checked", !1).removeAttr("selected") : e.val("")
                }
                for (this.updateStatus(b, this.STATUS_NOT_VALIDATED), f = 0; g > f; f++) e.eq(f).trigger(a.Event(this.options.events.fieldReset), {
                    fv: this,
                    field: b,
                    element: e.eq(f),
                    resetValue: c
                });
                return this
            },
            resetForm: function(b) {
                for (var c in this.options.fields) this.resetField(c, b);
                return this.$invalidFields = a([]), this.$submitButton = null, this.disableSubmitButtons(!1), this.$form.trigger(a.Event(this.options.events.formReset), {
                    fv: this,
                    resetValue: b
                }), this
            },
            revalidateField: function(a) {
                return this.updateStatus(a, this.STATUS_NOT_VALIDATED).validateField(a), this
            },
            setLocale: function(b) {
                return this.options.locale = b, this.$form.trigger(a.Event(this.options.events.localeChanged), {
                    locale: b,
                    bv: this,
                    fv: this
                }), this
            },
            updateOption: function(a, b, c, d) {
                var e = this._namespace;
                return "object" == typeof a && (a = a.attr("data-" + e + "-field")), this.options.fields[a] && this.options.fields[a].validators[b] && (this.options.fields[a].validators[b][c] = d, this.updateStatus(a, this.STATUS_NOT_VALIDATED, b)), this
            },
            validateContainer: function(b) {
                var c = this,
                    d = this._namespace,
                    e = [],
                    f = "string" == typeof b ? a(b) : b;
                if (0 === f.length) return this;
                f.find("[data-" + d + "-field]").each(function() {
                    var b = a(this);
                    c._isExcluded(b) || e.push(b)
                });
                for (var g = e.length, h = 0; g > h; h++) this.validateField(e[h]);
                return this
            }
        }, a.fn.formValidation = function(b) {
            var c = arguments;
            return this.each(function() {
                var d = a(this),
                    e = d.data("formValidation"),
                    f = "object" == typeof b && b;
                if (!e) {
                    var g = (f.framework || d.attr("data-fv-framework") || "bootstrap").toLowerCase(),
                        h = g.substr(0, 1).toUpperCase() + g.substr(1);
                    if ("undefined" == typeof FormValidation.Framework[h]) throw new Error("The class FormValidation.Framework." + h + " is not implemented");
                    e = new FormValidation.Framework[h](this, f), d.addClass("fv-form-" + g).data("formValidation", e)
                }
                "string" == typeof b && e[b].apply(e, Array.prototype.slice.call(c, 1))
            })
        }, a.fn.formValidation.Constructor = FormValidation.Base, a.fn.formValidation.DEFAULT_MESSAGE = "This value is not valid", a.fn.formValidation.DEFAULT_OPTIONS = {
            autoFocus: !0,
            declarative: !0,
            elementClass: "fv-form",
            events: {
                formInit: "init.form.fv",
                formPreValidate: "prevalidate.form.fv",
                formError: "err.form.fv",
                formReset: "rst.form.fv",
                formSuccess: "success.form.fv",
                fieldAdded: "added.field.fv",
                fieldRemoved: "removed.field.fv",
                fieldInit: "init.field.fv",
                fieldError: "err.field.fv",
                fieldReset: "rst.field.fv",
                fieldSuccess: "success.field.fv",
                fieldStatus: "status.field.fv",
                localeChanged: "changed.locale.fv",
                validatorError: "err.validator.fv",
                validatorSuccess: "success.validator.fv",
                validatorIgnored: "ignored.validator.fv"
            },
            excluded: [":disabled", ":hidden", ":not(:visible)"],
            fields: null,
            live: "enabled",
            locale: "en_US",
            message: null,
            threshold: null,
            verbose: !0,
            button: {
                selector: '[type="submit"]:not([formnovalidate])',
                disabled: ""
            },
            control: {
                valid: "",
                invalid: ""
            },
            err: {
                clazz: "",
                container: null,
                parent: null
            },
            icon: {
                valid: null,
                invalid: null,
                validating: null,
                feedback: ""
            },
            row: {
                selector: null,
                valid: "",
                invalid: "",
                feedback: ""
            }
        }
    }(jQuery),
    function(a) {
        FormValidation.Helper = {
            call: function(a, b) {
                if ("function" == typeof a) return a.apply(this, b);
                if ("string" == typeof a) {
                    "()" === a.substring(a.length - 2) && (a = a.substring(0, a.length - 2));
                    for (var c = a.split("."), d = c.pop(), e = window, f = 0; f < c.length; f++) e = e[c[f]];
                    return "undefined" == typeof e[d] ? null : e[d].apply(this, b)
                }
            },
            date: function(a, b, c, d) {
                if (isNaN(a) || isNaN(b) || isNaN(c)) return !1;
                if (c.length > 2 || b.length > 2 || a.length > 4) return !1;
                if (c = parseInt(c, 10), b = parseInt(b, 10), a = parseInt(a, 10), 1e3 > a || a > 9999 || 0 >= b || b > 12) return !1;
                var e = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];
                if ((a % 400 === 0 || a % 100 !== 0 && a % 4 === 0) && (e[1] = 29), 0 >= c || c > e[b - 1]) return !1;
                if (d === !0) {
                    var f = new Date,
                        g = f.getFullYear(),
                        h = f.getMonth(),
                        i = f.getDate();
                    return g > a || a === g && h > b - 1 || a === g && b - 1 === h && i > c
                }
                return !0
            },
            format: function(b, c) {
                a.isArray(c) || (c = [c]);
                for (var d in c) b = b.replace("%s", c[d]);
                return b
            },
            luhn: function(a) {
                for (var b = a.length, c = 0, d = [
                    [0, 1, 2, 3, 4, 5, 6, 7, 8, 9],
                    [0, 2, 4, 6, 8, 1, 3, 5, 7, 9]
                ], e = 0; b--;) e += d[c][parseInt(a.charAt(b), 10)], c ^= 1;
                return e % 10 === 0 && e > 0
            },
            mod11And10: function(a) {
                for (var b = 5, c = a.length, d = 0; c > d; d++) b = (2 * (b || 10) % 11 + parseInt(a.charAt(d), 10)) % 10;
                return 1 === b
            },
            mod37And36: function(a, b) {
                b = b || "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
                for (var c = b.length, d = a.length, e = Math.floor(c / 2), f = 0; d > f; f++) e = (2 * (e || c) % (c + 1) + b.indexOf(a.charAt(f))) % c;
                return 1 === e
            }
        }
    }(jQuery),
    function(a) {
        FormValidation.I18n = a.extend(!0, FormValidation.I18n || {}, {
            en_US: {
                between: {
                    "default": "Please enter a value between %s and %s",
                    notInclusive: "Please enter a value between %s and %s strictly"
                }
            }
        }), FormValidation.Validator.between = {
            html5Attributes: {
                message: "message",
                min: "min",
                max: "max",
                inclusive: "inclusive"
            },
            enableByHtml5: function(a) {
                return "range" === a.attr("type") ? {
                    min: a.attr("min"),
                    max: a.attr("max")
                } : !1
            },
            validate: function(b, c, d, e) {
                var f = b.getFieldValue(c, e);
                if ("" === f) return !0;
                f = this._format(f);
                var g = b.getLocale(),
                    h = a.isNumeric(d.min) ? d.min : b.getDynamicOption(c, d.min),
                    i = a.isNumeric(d.max) ? d.max : b.getDynamicOption(c, d.max),
                    j = this._format(h),
                    k = this._format(i);
                return d.inclusive === !0 || void 0 === d.inclusive ? {
                    valid: a.isNumeric(f) && parseFloat(f) >= j && parseFloat(f) <= k,
                    message: FormValidation.Helper.format(d.message || FormValidation.I18n[g].between["default"], [h, i])
                } : {
                    valid: a.isNumeric(f) && parseFloat(f) > j && parseFloat(f) < k,
                    message: FormValidation.Helper.format(d.message || FormValidation.I18n[g].between.notInclusive, [h, i])
                }
            },
            _format: function(a) {
                return (a + "").replace(",", ".")
            }
        }
    }(jQuery),
    function(a) {
        FormValidation.I18n = a.extend(!0, FormValidation.I18n || {}, {
            en_US: {
                callback: {
                    "default": "Please enter a valid value"
                }
            }
        }), FormValidation.Validator.callback = {
            priority: 999,
            html5Attributes: {
                message: "message",
                callback: "callback"
            },
            validate: function(b, c, d, e) {
                var f = b.getFieldValue(c, e),
                    g = new a.Deferred,
                    h = {
                        valid: !0
                    };
                if (d.callback) {
                    var i = FormValidation.Helper.call(d.callback, [f, b, c]);
                    h = "boolean" == typeof i || null === i ? {
                        valid: i
                    } : i
                }
                return g.resolve(c, e, h), g
            }
        }
    }(jQuery),
    function(a) {
        FormValidation.I18n = a.extend(!0, FormValidation.I18n || {}, {
            en_US: {
                choice: {
                    "default": "Please enter a valid value",
                    less: "Please choose %s options at minimum",
                    more: "Please choose %s options at maximum",
                    between: "Please choose %s - %s options"
                }
            }
        }), FormValidation.Validator.choice = {
            html5Attributes: {
                message: "message",
                min: "min",
                max: "max"
            },
            validate: function(b, c, d, e) {
                var f = b.getLocale(),
                    g = b.getNamespace(),
                    h = c.is("select") ? b.getFieldElements(c.attr("data-" + g + "-field")).find("option").filter(":selected").length : b.getFieldElements(c.attr("data-" + g + "-field")).filter(":checked").length,
                    i = d.min ? a.isNumeric(d.min) ? d.min : b.getDynamicOption(c, d.min) : null,
                    j = d.max ? a.isNumeric(d.max) ? d.max : b.getDynamicOption(c, d.max) : null,
                    k = !0,
                    l = d.message || FormValidation.I18n[f].choice["default"];
                switch ((i && h < parseInt(i, 10) || j && h > parseInt(j, 10)) && (k = !1), !0) {
                    case !!i && !!j:
                        l = FormValidation.Helper.format(d.message || FormValidation.I18n[f].choice.between, [parseInt(i, 10), parseInt(j, 10)]);
                        break;
                    case !!i:
                        l = FormValidation.Helper.format(d.message || FormValidation.I18n[f].choice.less, parseInt(i, 10));
                        break;
                    case !!j:
                        l = FormValidation.Helper.format(d.message || FormValidation.I18n[f].choice.more, parseInt(j, 10))
                }
                return {
                    valid: k,
                    message: l
                }
            }
        }
    }(jQuery),
    function(a) {
        FormValidation.I18n = a.extend(!0, FormValidation.I18n || {}, {
            en_US: {
                color: {
                    "default": "Please enter a valid color"
                }
            }
        }), FormValidation.Validator.color = {
            html5Attributes: {
                message: "message",
                type: "type"
            },
            enableByHtml5: function(a) {
                return "color" === a.attr("type")
            },
            SUPPORTED_TYPES: ["hex", "rgb", "rgba", "hsl", "hsla", "keyword"],
            KEYWORD_COLORS: ["aliceblue", "antiquewhite", "aqua", "aquamarine", "azure", "beige", "bisque", "black", "blanchedalmond", "blue", "blueviolet", "brown", "burlywood", "cadetblue", "chartreuse", "chocolate", "coral", "cornflowerblue", "cornsilk", "crimson", "cyan", "darkblue", "darkcyan", "darkgoldenrod", "darkgray", "darkgreen", "darkgrey", "darkkhaki", "darkmagenta", "darkolivegreen", "darkorange", "darkorchid", "darkred", "darksalmon", "darkseagreen", "darkslateblue", "darkslategray", "darkslategrey", "darkturquoise", "darkviolet", "deeppink", "deepskyblue", "dimgray", "dimgrey", "dodgerblue", "firebrick", "floralwhite", "forestgreen", "fuchsia", "gainsboro", "ghostwhite", "gold", "goldenrod", "gray", "green", "greenyellow", "grey", "honeydew", "hotpink", "indianred", "indigo", "ivory", "khaki", "lavender", "lavenderblush", "lawngreen", "lemonchiffon", "lightblue", "lightcoral", "lightcyan", "lightgoldenrodyellow", "lightgray", "lightgreen", "lightgrey", "lightpink", "lightsalmon", "lightseagreen", "lightskyblue", "lightslategray", "lightslategrey", "lightsteelblue", "lightyellow", "lime", "limegreen", "linen", "magenta", "maroon", "mediumaquamarine", "mediumblue", "mediumorchid", "mediumpurple", "mediumseagreen", "mediumslateblue", "mediumspringgreen", "mediumturquoise", "mediumvioletred", "midnightblue", "mintcream", "mistyrose", "moccasin", "navajowhite", "navy", "oldlace", "olive", "olivedrab", "orange", "orangered", "orchid", "palegoldenrod", "palegreen", "paleturquoise", "palevioletred", "papayawhip", "peachpuff", "peru", "pink", "plum", "powderblue", "purple", "red", "rosybrown", "royalblue", "saddlebrown", "salmon", "sandybrown", "seagreen", "seashell", "sienna", "silver", "skyblue", "slateblue", "slategray", "slategrey", "snow", "springgreen", "steelblue", "tan", "teal", "thistle", "tomato", "transparent", "turquoise", "violet", "wheat", "white", "whitesmoke", "yellow", "yellowgreen"],
            validate: function(b, c, d, e) {
                var f = b.getFieldValue(c, e);
                if ("" === f) return !0;
                if (this.enableByHtml5(c)) return /^#[0-9A-F]{6}$/i.test(f);
                var g = d.type || this.SUPPORTED_TYPES;
                a.isArray(g) || (g = g.replace(/s/g, "").split(","));
                for (var h, i, j = !1, k = 0; k < g.length; k++)
                    if (i = g[k], h = "_" + i.toLowerCase(), j = j || this[h](f)) return !0;
                return !1
            },
            _hex: function(a) {
                return /(^#[0-9A-F]{6}$)|(^#[0-9A-F]{3}$)/i.test(a)
            },
            _hsl: function(a) {
                return /^hsl\((\s*(-?\d+)\s*,)(\s*(\b(0?\d{1,2}|100)\b%)\s*,)(\s*(\b(0?\d{1,2}|100)\b%)\s*)\)$/.test(a)
            },
            _hsla: function(a) {
                return /^hsla\((\s*(-?\d+)\s*,)(\s*(\b(0?\d{1,2}|100)\b%)\s*,){2}(\s*(0?(\.\d+)?|1(\.0+)?)\s*)\)$/.test(a)
            },
            _keyword: function(b) {
                return a.inArray(b, this.KEYWORD_COLORS) >= 0
            },
            _rgb: function(a) {
                var b = /^rgb\((\s*(\b([01]?\d{1,2}|2[0-4]\d|25[0-5])\b)\s*,){2}(\s*(\b([01]?\d{1,2}|2[0-4]\d|25[0-5])\b)\s*)\)$/,
                    c = /^rgb\((\s*(\b(0?\d{1,2}|100)\b%)\s*,){2}(\s*(\b(0?\d{1,2}|100)\b%)\s*)\)$/;
                return b.test(a) || c.test(a)
            },
            _rgba: function(a) {
                var b = /^rgba\((\s*(\b([01]?\d{1,2}|2[0-4]\d|25[0-5])\b)\s*,){3}(\s*(0?(\.\d+)?|1(\.0+)?)\s*)\)$/,
                    c = /^rgba\((\s*(\b(0?\d{1,2}|100)\b%)\s*,){3}(\s*(0?(\.\d+)?|1(\.0+)?)\s*)\)$/;
                return b.test(a) || c.test(a)
            }
        }
    }(jQuery),
    function(a) {
        FormValidation.I18n = a.extend(!0, FormValidation.I18n || {}, {
            en_US: {
                creditCard: {
                    "default": "Please enter a valid credit card number"
                }
            }
        }), FormValidation.Validator.creditCard = {
            validate: function(b, c, d, e) {
                var f = b.getFieldValue(c, e);
                if ("" === f) return !0;
                if (/[^0-9-\s]+/.test(f)) return !1;
                if (f = f.replace(/\D/g, ""), !FormValidation.Helper.luhn(f)) return !1;
                var g, h, i = {
                    AMERICAN_EXPRESS: {
                        length: [15],
                        prefix: ["34", "37"]
                    },
                    DANKORT: {
                        length: [16],
                        prefix: ["5019"]
                    },
                    DINERS_CLUB: {
                        length: [14],
                        prefix: ["300", "301", "302", "303", "304", "305", "36"]
                    },
                    DINERS_CLUB_US: {
                        length: [16],
                        prefix: ["54", "55"]
                    },
                    DISCOVER: {
                        length: [16],
                        prefix: ["6011", "622126", "622127", "622128", "622129", "62213", "62214", "62215", "62216", "62217", "62218", "62219", "6222", "6223", "6224", "6225", "6226", "6227", "6228", "62290", "62291", "622920", "622921", "622922", "622923", "622924", "622925", "644", "645", "646", "647", "648", "649", "65"]
                    },
                    ELO: {
                        length: [16],
                        prefix: ["4011", "4312", "4389", "4514", "4573", "4576", "5041", "5066", "5067", "509", "6277", "6362", "6363", "650", "6516", "6550"]
                    },
                    FORBRUGSFORENINGEN: {
                        length: [16],
                        prefix: ["600722"]
                    },
                    JCB: {
                        length: [16],
                        prefix: ["3528", "3529", "353", "354", "355", "356", "357", "358"]
                    },
                    LASER: {
                        length: [16, 17, 18, 19],
                        prefix: ["6304", "6706", "6771", "6709"]
                    },
                    MAESTRO: {
                        length: [12, 13, 14, 15, 16, 17, 18, 19],
                        prefix: ["5018", "5020", "5038", "5868", "6304", "6759", "6761", "6762", "6763", "6764", "6765", "6766"]
                    },
                    MASTERCARD: {
                        length: [16],
                        prefix: ["51", "52", "53", "54", "55"]
                    },
                    SOLO: {
                        length: [16, 18, 19],
                        prefix: ["6334", "6767"]
                    },
                    UNIONPAY: {
                        length: [16, 17, 18, 19],
                        prefix: ["622126", "622127", "622128", "622129", "62213", "62214", "62215", "62216", "62217", "62218", "62219", "6222", "6223", "6224", "6225", "6226", "6227", "6228", "62290", "62291", "622920", "622921", "622922", "622923", "622924", "622925"]
                    },
                    VISA_ELECTRON: {
                        length: [16],
                        prefix: ["4026", "417500", "4405", "4508", "4844", "4913", "4917"]
                    },
                    VISA: {
                        length: [16],
                        prefix: ["4"]
                    }
                };
                for (g in i)
                    for (h in i[g].prefix)
                        if (f.substr(0, i[g].prefix[h].length) === i[g].prefix[h] && -1 !== a.inArray(f.length, i[g].length)) return {
                            valid: !0,
                            type: g
                        };
                return !1
            }
        }
    }(jQuery),
    function(a) {
        FormValidation.I18n = a.extend(!0, FormValidation.I18n || {}, {
            en_US: {
                date: {
                    "default": "Please enter a valid date",
                    min: "Please enter a date after %s",
                    max: "Please enter a date before %s",
                    range: "Please enter a date in the range %s - %s"
                }
            }
        }), FormValidation.Validator.date = {
            html5Attributes: {
                message: "message",
                format: "format",
                min: "min",
                max: "max",
                separator: "separator"
            },
            validate: function(b, c, d, e) {
                var f = b.getFieldValue(c, e);
                if ("" === f) return !0;
                d.format = d.format || "MM/DD/YYYY", "date" === c.attr("type") && (d.format = "YYYY-MM-DD");
                var g = b.getLocale(),
                    h = d.message || FormValidation.I18n[g].date["default"],
                    i = d.format.split(" "),
                    j = i[0],
                    k = i.length > 1 ? i[1] : null,
                    l = i.length > 2 ? i[2] : null,
                    m = f.split(" "),
                    n = m[0],
                    o = m.length > 1 ? m[1] : null;
                if (i.length !== m.length) return {
                    valid: !1,
                    message: h
                };
                var p = d.separator;
                if (p || (p = -1 !== n.indexOf("/") ? "/" : -1 !== n.indexOf("-") ? "-" : -1 !== n.indexOf(".") ? "." : null), null === p || -1 === n.indexOf(p)) return {
                    valid: !1,
                    message: h
                };
                if (n = n.split(p), j = j.split(p), n.length !== j.length) return {
                    valid: !1,
                    message: h
                };
                var q = n[a.inArray("YYYY", j)],
                    r = n[a.inArray("MM", j)],
                    s = n[a.inArray("DD", j)];
                if (!q || !r || !s || 4 !== q.length) return {
                    valid: !1,
                    message: h
                };
                var t = null,
                    u = null,
                    v = null;
                if (k) {
                    if (k = k.split(":"), o = o.split(":"), k.length !== o.length) return {
                        valid: !1,
                        message: h
                    };
                    if (u = o.length > 0 ? o[0] : null, t = o.length > 1 ? o[1] : null, v = o.length > 2 ? o[2] : null, "" === u || "" === t || "" === v) return {
                        valid: !1,
                        message: h
                    };
                    if (v) {
                        if (isNaN(v) || v.length > 2) return {
                            valid: !1,
                            message: h
                        };
                        if (v = parseInt(v, 10), 0 > v || v > 60) return {
                            valid: !1,
                            message: h
                        }
                    }
                    if (u) {
                        if (isNaN(u) || u.length > 2) return {
                            valid: !1,
                            message: h
                        };
                        if (u = parseInt(u, 10), 0 > u || u >= 24 || l && u > 12) return {
                            valid: !1,
                            message: h
                        }
                    }
                    if (t) {
                        if (isNaN(t) || t.length > 2) return {
                            valid: !1,
                            message: h
                        };
                        if (t = parseInt(t, 10), 0 > t || t > 59) return {
                            valid: !1,
                            message: h
                        }
                    }
                }
                var w = FormValidation.Helper.date(q, r, s),
                    x = null,
                    y = null,
                    z = d.min,
                    A = d.max;
                switch (z && (x = z instanceof Date ? z : this._parseDate(z, j, p) || this._parseDate(b.getDynamicOption(c, z), j, p), z = this._formatDate(x, d.format)), A && (y = A instanceof Date ? A : this._parseDate(A, j, p) || this._parseDate(b.getDynamicOption(c, A), j, p), A = this._formatDate(y, d.format)), n = new Date(q, r - 1, s, u, t, v), !0) {
                    case z && !A && w:
                        w = n.getTime() >= x.getTime(), h = d.message || FormValidation.Helper.format(FormValidation.I18n[g].date.min, z);
                        break;
                    case A && !z && w:
                        w = n.getTime() <= y.getTime(), h = d.message || FormValidation.Helper.format(FormValidation.I18n[g].date.max, A);
                        break;
                    case A && z && w:
                        w = n.getTime() <= y.getTime() && n.getTime() >= x.getTime(), h = d.message || FormValidation.Helper.format(FormValidation.I18n[g].date.range, [z, A])
                }
                return {
                    valid: w,
                    date: n,
                    message: h
                }
            },
            _parseDate: function(b, c, d) {
                if (b instanceof Date) return b;
                if ("string" != typeof b) return null;
                var e = a.inArray("YYYY", c),
                    f = a.inArray("MM", c),
                    g = a.inArray("DD", c);
                if (-1 === e || -1 === f || -1 === g) return null;
                var h = 0,
                    i = 0,
                    j = 0,
                    k = b.split(" "),
                    l = k[0].split(d);
                if (l.length < 3) return null;
                if (k.length > 1) {
                    var m = k[1].split(":");
                    i = m.length > 0 ? m[0] : null, h = m.length > 1 ? m[1] : null, j = m.length > 2 ? m[2] : null
                }
                return new Date(l[e], l[f] - 1, l[g], i, h, j)
            },
            _formatDate: function(a, b) {
                b = b.replace(/Y/g, "y").replace(/M/g, "m").replace(/D/g, "d").replace(/:m/g, ":M").replace(/:mm/g, ":MM").replace(/:S/, ":s").replace(/:SS/, ":ss");
                var c = {
                    d: function(a) {
                        return a.getDate()
                    },
                    dd: function(a) {
                        var b = a.getDate();
                        return 10 > b ? "0" + b : b
                    },
                    m: function(a) {
                        return a.getMonth() + 1
                    },
                    mm: function(a) {
                        var b = a.getMonth() + 1;
                        return 10 > b ? "0" + b : b
                    },
                    yy: function(a) {
                        return ("" + a.getFullYear()).substr(2)
                    },
                    yyyy: function(a) {
                        return a.getFullYear()
                    },
                    h: function(a) {
                        return a.getHours() % 12 || 12
                    },
                    hh: function(a) {
                        var b = a.getHours() % 12 || 12;
                        return 10 > b ? "0" + b : b
                    },
                    H: function(a) {
                        return a.getHours()
                    },
                    HH: function(a) {
                        var b = a.getHours();
                        return 10 > b ? "0" + b : b
                    },
                    M: function(a) {
                        return a.getMinutes()
                    },
                    MM: function(a) {
                        var b = a.getMinutes();
                        return 10 > b ? "0" + b : b
                    },
                    s: function(a) {
                        return a.getSeconds()
                    },
                    ss: function(a) {
                        var b = a.getSeconds();
                        return 10 > b ? "0" + b : b
                    }
                };
                return b.replace(/d{1,4}|m{1,4}|yy(?:yy)?|([HhMs])\1?|"[^"]*"|'[^']*'/g, function(b) {
                    return c[b] ? c[b](a) : b.slice(1, b.length - 1)
                })
            }
        }
    }(jQuery),
    function(a) {
        FormValidation.I18n = a.extend(!0, FormValidation.I18n || {}, {
            en_US: {
                different: {
                    "default": "Please enter a different value"
                }
            }
        }), FormValidation.Validator.different = {
            html5Attributes: {
                message: "message",
                field: "field"
            },
            init: function(b, c, d, e) {
                for (var f = d.field.split(","), g = 0; g < f.length; g++) {
                    var h = b.getFieldElements(a.trim(f[g]));
                    b.onLiveChange(h, "live_" + e, function() {
                        var a = b.getStatus(c, e);
                        a !== b.STATUS_NOT_VALIDATED && b.revalidateField(c)
                    })
                }
            },
            destroy: function(b, c, d, e) {
                for (var f = d.field.split(","), g = 0; g < f.length; g++) {
                    var h = b.getFieldElements(a.trim(f[g]));
                    b.offLiveChange(h, "live_" + e)
                }
            },
            validate: function(b, c, d, e) {
                var f = b.getFieldValue(c, e);
                if ("" === f) return !0;
                for (var g = d.field.split(","), h = !0, i = 0; i < g.length; i++) {
                    var j = b.getFieldElements(a.trim(g[i]));
                    if (null != j && 0 !== j.length) {
                        var k = b.getFieldValue(j, e);
                        f === k ? h = !1 : "" !== k && b.updateStatus(j, b.STATUS_VALID, e)
                    }
                }
                return h
            }
        }
    }(jQuery),
    function(a) {
        FormValidation.I18n = a.extend(!0, FormValidation.I18n || {}, {
            en_US: {
                digits: {
                    "default": "Please enter only digits"
                }
            }
        }), FormValidation.Validator.digits = {
            validate: function(a, b, c, d) {
                var e = a.getFieldValue(b, d);
                return "" === e ? !0 : /^\d+$/.test(e)
            }
        }
    }(jQuery),
    function(a) {
        FormValidation.I18n = a.extend(!0, FormValidation.I18n || {}, {
            en_US: {
                emailAddress: {
                    "default": "Please enter a valid email address"
                }
            }
        }), FormValidation.Validator.emailAddress = {
            html5Attributes: {
                message: "message",
                multiple: "multiple",
                separator: "separator"
            },
            enableByHtml5: function(a) {
                return "email" === a.attr("type")
            },
            validate: function(a, b, c, d) {
                var e = a.getFieldValue(b, d);
                if ("" === e) return !0;
                var f = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/,
                    g = c.multiple === !0 || "true" === c.multiple;
                if (g) {
                    for (var h = c.separator || /[,;]/, i = this._splitEmailAddresses(e, h), j = 0; j < i.length; j++)
                        if (!f.test(i[j])) return !1;
                    return !0
                }
                return f.test(e)
            },
            _splitEmailAddresses: function(a, b) {
                for (var c = a.split(/"/), d = c.length, e = [], f = "", g = 0; d > g; g++)
                    if (g % 2 === 0) {
                        var h = c[g].split(b),
                            i = h.length;
                        if (1 === i) f += h[0];
                        else {
                            e.push(f + h[0]);
                            for (var j = 1; i - 1 > j; j++) e.push(h[j]);
                            f = h[i - 1]
                        }
                    } else f += '"' + c[g], d - 1 > g && (f += '"');
                return e.push(f), e
            }
        }
    }(jQuery),
    function(a) {
        FormValidation.I18n = a.extend(!0, FormValidation.I18n || {}, {
            en_US: {
                file: {
                    "default": "Please choose a valid file"
                }
            }
        }), FormValidation.Validator.file = {
            Error: {
                EXTENSION: "EXTENSION",
                MAX_FILES: "MAX_FILES",
                MAX_SIZE: "MAX_SIZE",
                MAX_TOTAL_SIZE: "MAX_TOTAL_SIZE",
                MIN_FILES: "MIN_FILES",
                MIN_SIZE: "MIN_SIZE",
                MIN_TOTAL_SIZE: "MIN_TOTAL_SIZE",
                TYPE: "TYPE"
            },
            html5Attributes: {
                extension: "extension",
                maxfiles: "maxFiles",
                minfiles: "minFiles",
                maxsize: "maxSize",
                minsize: "minSize",
                maxtotalsize: "maxTotalSize",
                mintotalsize: "minTotalSize",
                message: "message",
                type: "type"
            },
            validate: function(b, c, d, e) {
                var f = b.getFieldValue(c, e);
                if ("" === f) return !0;
                var g, h = d.extension ? d.extension.toLowerCase().split(",") : null,
                    i = d.type ? d.type.toLowerCase().split(",") : null,
                    j = window.File && window.FileList && window.FileReader;
                if (j) {
                    var k = c.get(0).files,
                        l = k.length,
                        m = 0;
                    if (d.maxFiles && l > parseInt(d.maxFiles, 10)) return {
                        valid: !1,
                        error: this.Error.MAX_FILES
                    };
                    if (d.minFiles && l < parseInt(d.minFiles, 10)) return {
                        valid: !1,
                        error: this.Error.MIN_FILES
                    };
                    for (var n = {}, o = 0; l > o; o++) {
                        if (m += k[o].size, g = k[o].name.substr(k[o].name.lastIndexOf(".") + 1), n = {
                                file: k[o],
                                size: k[o].size,
                                ext: g,
                                type: k[o].type
                            }, d.minSize && k[o].size < parseInt(d.minSize, 10)) return {
                            valid: !1,
                            error: this.Error.MIN_SIZE,
                            metaData: n
                        };
                        if (d.maxSize && k[o].size > parseInt(d.maxSize, 10)) return {
                            valid: !1,
                            error: this.Error.MAX_SIZE,
                            metaData: n
                        };
                        if (h && -1 === a.inArray(g.toLowerCase(), h)) return {
                            valid: !1,
                            error: this.Error.EXTENSION,
                            metaData: n
                        };
                        if (k[o].type && i && -1 === a.inArray(k[o].type.toLowerCase(), i)) return {
                            valid: !1,
                            error: this.Error.TYPE,
                            metaData: n
                        }
                    }
                    if (d.maxTotalSize && m > parseInt(d.maxTotalSize, 10)) return {
                        valid: !1,
                        error: this.Error.MAX_TOTAL_SIZE,
                        metaData: {
                            totalSize: m
                        }
                    };
                    if (d.minTotalSize && m < parseInt(d.minTotalSize, 10)) return {
                        valid: !1,
                        error: this.Error.MIN_TOTAL_SIZE,
                        metaData: {
                            totalSize: m
                        }
                    }
                } else if (g = f.substr(f.lastIndexOf(".") + 1), h && -1 === a.inArray(g.toLowerCase(), h)) return {
                    valid: !1,
                    error: this.Error.EXTENSION,
                    metaData: {
                        ext: g
                    }
                };
                return !0
            }
        }
    }(jQuery),
    function(a) {
        FormValidation.I18n = a.extend(!0, FormValidation.I18n || {}, {
            en_US: {
                greaterThan: {
                    "default": "Please enter a value greater than or equal to %s",
                    notInclusive: "Please enter a value greater than %s"
                }
            }
        }), FormValidation.Validator.greaterThan = {
            html5Attributes: {
                message: "message",
                value: "value",
                inclusive: "inclusive"
            },
            enableByHtml5: function(a) {
                var b = a.attr("type"),
                    c = a.attr("min");
                return c && "date" !== b ? {
                    value: c
                } : !1
            },
            validate: function(b, c, d, e) {
                var f = b.getFieldValue(c, e);
                if ("" === f) return !0;
                f = this._format(f);
                var g = b.getLocale(),
                    h = a.isNumeric(d.value) ? d.value : b.getDynamicOption(c, d.value),
                    i = this._format(h);
                return d.inclusive === !0 || void 0 === d.inclusive ? {
                    valid: a.isNumeric(f) && parseFloat(f) >= i,
                    message: FormValidation.Helper.format(d.message || FormValidation.I18n[g].greaterThan["default"], h)
                } : {
                    valid: a.isNumeric(f) && parseFloat(f) > i,
                    message: FormValidation.Helper.format(d.message || FormValidation.I18n[g].greaterThan.notInclusive, h)
                }
            },
            _format: function(a) {
                return (a + "").replace(",", ".")
            }
        }
    }(jQuery),
    function(a) {
        FormValidation.I18n = a.extend(!0, FormValidation.I18n || {}, {
            en_US: {
                identical: {
                    "default": "Please enter the same value"
                }
            }
        }), FormValidation.Validator.identical = {
            html5Attributes: {
                message: "message",
                field: "field"
            },
            init: function(a, b, c, d) {
                var e = a.getFieldElements(c.field);
                a.onLiveChange(e, "live_" + d, function() {
                    var c = a.getStatus(b, d);
                    c !== a.STATUS_NOT_VALIDATED && a.revalidateField(b)
                })
            },
            destroy: function(a, b, c, d) {
                var e = a.getFieldElements(c.field);
                a.offLiveChange(e, "live_" + d)
            },
            validate: function(a, b, c, d) {
                var e = a.getFieldValue(b, d),
                    f = a.getFieldElements(c.field);
                if (null === f || 0 === f.length) return !0;
                var g = a.getFieldValue(f, d);
                return e === g ? (a.updateStatus(f, a.STATUS_VALID, d), !0) : !1
            }
        }
    }(jQuery),
    function(a) {
        FormValidation.I18n = a.extend(!0, FormValidation.I18n || {}, {
            en_US: {
                integer: {
                    "default": "Please enter a valid number"
                }
            }
        }), FormValidation.Validator.integer = {
            html5Attributes: {
                message: "message",
                thousandsseparator: "thousandsSeparator",
                decimalseparator: "decimalSeparator"
            },
            enableByHtml5: function(a) {
                return "number" === a.attr("type") && (void 0 === a.attr("step") || a.attr("step") % 1 === 0)
            },
            validate: function(a, b, c, d) {
                if (this.enableByHtml5(b) && b.get(0).validity && b.get(0).validity.badInput === !0) return !1;
                var e = a.getFieldValue(b, d);
                if ("" === e) return !0;
                var f = c.decimalSeparator || ".",
                    g = c.thousandsSeparator || "";
                f = "." === f ? "\\." : f, g = "." === g ? "\\." : g;
                var h = new RegExp("^-?[0-9]{1,3}(" + g + "[0-9]{3})*(" + f + "[0-9]+)?$"),
                    i = new RegExp(g, "g");
                return h.test(e) ? (g && (e = e.replace(i, "")), f && (e = e.replace(f, ".")), isNaN(e) || !isFinite(e) ? !1 : (e = parseFloat(e), Math.floor(e) === e)) : !1
            }
        }
    }(jQuery),
    function(a) {
        FormValidation.I18n = a.extend(!0, FormValidation.I18n || {}, {
            en_US: {
                lessThan: {
                    "default": "Please enter a value less than or equal to %s",
                    notInclusive: "Please enter a value less than %s"
                }
            }
        }), FormValidation.Validator.lessThan = {
            html5Attributes: {
                message: "message",
                value: "value",
                inclusive: "inclusive"
            },
            enableByHtml5: function(a) {
                var b = a.attr("type"),
                    c = a.attr("max");
                return c && "date" !== b ? {
                    value: c
                } : !1
            },
            validate: function(b, c, d, e) {
                var f = b.getFieldValue(c, e);
                if ("" === f) return !0;
                f = this._format(f);
                var g = b.getLocale(),
                    h = a.isNumeric(d.value) ? d.value : b.getDynamicOption(c, d.value),
                    i = this._format(h);
                return d.inclusive === !0 || void 0 === d.inclusive ? {
                    valid: a.isNumeric(f) && parseFloat(f) <= i,
                    message: FormValidation.Helper.format(d.message || FormValidation.I18n[g].lessThan["default"], h)
                } : {
                    valid: a.isNumeric(f) && parseFloat(f) < i,
                    message: FormValidation.Helper.format(d.message || FormValidation.I18n[g].lessThan.notInclusive, h)
                }
            },
            _format: function(a) {
                return (a + "").replace(",", ".")
            }
        }
    }(jQuery),
    function(a) {
        FormValidation.I18n = a.extend(!0, FormValidation.I18n || {}, {
            en_US: {
                notEmpty: {
                    "default": "Please enter a value"
                }
            }
        }), FormValidation.Validator.notEmpty = {
            enableByHtml5: function(a) {
                var b = a.attr("required") + "";
                return "required" === b || "true" === b
            },
            validate: function(b, c, d, e) {
                var f = c.attr("type");
                if ("radio" === f || "checkbox" === f) {
                    var g = b.getNamespace();
                    return b.getFieldElements(c.attr("data-" + g + "-field")).filter(":checked").length > 0
                }
                if ("number" === f && c.get(0).validity && c.get(0).validity.badInput === !0) return !0;
                var h = b.getFieldValue(c, e);
                return "" !== a.trim(h)
            }
        }
    }(jQuery),
    function(a) {
        FormValidation.I18n = a.extend(!0, FormValidation.I18n || {}, {
            en_US: {
                numeric: {
                    "default": "Please enter a valid float number"
                }
            }
        }), FormValidation.Validator.numeric = {
            html5Attributes: {
                message: "message",
                separator: "separator",
                thousandsseparator: "thousandsSeparator",
                decimalseparator: "decimalSeparator"
            },
            enableByHtml5: function(a) {
                return "number" === a.attr("type") && void 0 !== a.attr("step") && a.attr("step") % 1 !== 0
            },
            validate: function(a, b, c, d) {
                if (this.enableByHtml5(b) && b.get(0).validity && b.get(0).validity.badInput === !0) return !1;
                var e = a.getFieldValue(b, d);
                if ("" === e) return !0;
                var f = c.separator || c.decimalSeparator || ".",
                    g = c.thousandsSeparator || "";
                e.substr(0, 1) === f ? e = "0" + f + e.substr(1) : e.substr(0, 2) === "-" + f && (e = "-0" + f + e.substr(2)), f = "." === f ? "\\." : f, g = "." === g ? "\\." : g;
                var h = new RegExp("^-?[0-9]{1,3}(" + g + "[0-9]{3})*(" + f + "[0-9]+)?$"),
                    i = new RegExp(g, "g");
                return h.test(e) ? (g && (e = e.replace(i, "")), f && (e = e.replace(f, ".")), !isNaN(parseFloat(e)) && isFinite(e)) : !1
            }
        }
    }(jQuery),
    function(a) {
        FormValidation.I18n = a.extend(!0, FormValidation.I18n || {}, {
            en_US: {
                promise: {
                    "default": "Please enter a valid value"
                }
            }
        }), FormValidation.Validator.promise = {
            priority: 999,
            html5Attributes: {
                message: "message",
                promise: "promise"
            },
            validate: function(b, c, d, e) {
                var f = b.getFieldValue(c, e),
                    g = new a.Deferred,
                    h = FormValidation.Helper.call(d.promise, [f, b, c]);
                return h.done(function(a) {
                    g.resolve(c, e, a)
                }).fail(function(a) {
                    a = a || {}, a.valid = !1, g.resolve(c, e, a)
                }), g
            }
        }
    }(jQuery),
    function(a) {
        FormValidation.I18n = a.extend(!0, FormValidation.I18n || {}, {
            en_US: {
                regexp: {
                    "default": "Please enter a value matching the pattern"
                }
            }
        }), FormValidation.Validator.regexp = {
            html5Attributes: {
                message: "message",
                flags: "flags",
                regexp: "regexp"
            },
            enableByHtml5: function(a) {
                var b = a.attr("pattern");
                return b ? {
                    regexp: b
                } : !1
            },
            validate: function(a, b, c, d) {
                var e = a.getFieldValue(b, d);
                if ("" === e) return !0;
                var f = "string" == typeof c.regexp ? c.flags ? new RegExp(c.regexp, c.flags) : new RegExp(c.regexp) : c.regexp;
                return f.test(e)
            }
        }
    }(jQuery),
    function(a) {
        FormValidation.I18n = a.extend(!0, FormValidation.I18n || {}, {
            en_US: {
                remote: {
                    "default": "Please enter a valid value"
                }
            }
        }), FormValidation.Validator.remote = {
            priority: 1e3,
            html5Attributes: {
                async: "async",
                crossdomain: "crossDomain",
                data: "data",
                datatype: "dataType",
                delay: "delay",
                message: "message",
                name: "name",
                type: "type",
                url: "url",
                validkey: "validKey"
            },
            destroy: function(a, b, c, d) {
                var e = a.getNamespace(),
                    f = b.data(e + "." + d + ".timer");
                f && (clearTimeout(f), b.removeData(e + "." + d + ".timer"))
            },
            validate: function(b, c, d, e) {
                function f() {
                    var b = a.ajax(n);
                    return b.success(function(a) {
                        a.valid = a[m] === !0 || "true" === a[m] ? !0 : a[m] === !1 || "false" === a[m] ? !1 : null, i.resolve(c, e, a)
                    }).error(function(a) {
                        i.resolve(c, e, {
                            valid: !1
                        })
                    }), i.fail(function() {
                        b.abort()
                    }), i
                }
                var g = b.getNamespace(),
                    h = b.getFieldValue(c, e),
                    i = new a.Deferred;
                if ("" === h) return i.resolve(c, e, {
                    valid: !0
                }), i;
                var j = c.attr("data-" + g + "-field"),
                    k = d.data || {},
                    l = d.url,
                    m = d.validKey || "valid";
                "function" == typeof k && (k = k.call(this, b, c, h)), "string" == typeof k && (k = JSON.parse(k)), "function" == typeof l && (l = l.call(this, b, c, h)), k[d.name || j] = h;
                var n = {
                    async: null === d.async || d.async === !0 || "true" === d.async,
                    data: k,
                    dataType: d.dataType || "json",
                    headers: d.headers || {},
                    type: d.type || "GET",
                    url: l
                };
                return null !== d.crossDomain && (n.crossDomain = d.crossDomain === !0 || "true" === d.crossDomain), d.delay ? (c.data(g + "." + e + ".timer") && clearTimeout(c.data(g + "." + e + ".timer")), c.data(g + "." + e + ".timer", setTimeout(f, d.delay)), i) : f()
            }
        }
    }(jQuery),
    function(a) {
        FormValidation.I18n = a.extend(!0, FormValidation.I18n || {}, {
            en_US: {
                stringLength: {
                    "default": "Please enter a value with valid length",
                    less: "Please enter less than %s characters",
                    more: "Please enter more than %s characters",
                    between: "Please enter value between %s and %s characters long"
                }
            }
        }), FormValidation.Validator.stringLength = {
            html5Attributes: {
                message: "message",
                min: "min",
                max: "max",
                trim: "trim",
                utf8bytes: "utf8Bytes"
            },
            enableByHtml5: function(b) {
                var c = {},
                    d = b.attr("maxlength"),
                    e = b.attr("minlength");
                return d && (c.max = parseInt(d, 10)), e && (c.min = parseInt(e, 10)), a.isEmptyObject(c) ? !1 : c
            },
            validate: function(b, c, d, e) {
                var f = b.getFieldValue(c, e);
                if ((d.trim === !0 || "true" === d.trim) && (f = a.trim(f)), "" === f) return !0;
                var g = b.getLocale(),
                    h = a.isNumeric(d.min) ? d.min : b.getDynamicOption(c, d.min),
                    i = a.isNumeric(d.max) ? d.max : b.getDynamicOption(c, d.max),
                    j = function(a) {
                        for (var b = a.length, c = a.length - 1; c >= 0; c--) {
                            var d = a.charCodeAt(c);
                            d > 127 && 2047 >= d ? b++ : d > 2047 && 65535 >= d && (b += 2), d >= 56320 && 57343 >= d && c--
                        }
                        return b
                    },
                    k = d.utf8Bytes ? j(f) : f.length,
                    l = !0,
                    m = d.message || FormValidation.I18n[g].stringLength["default"];
                switch ((h && k < parseInt(h, 10) || i && k > parseInt(i, 10)) && (l = !1), !0) {
                    case !!h && !!i:
                        m = FormValidation.Helper.format(d.message || FormValidation.I18n[g].stringLength.between, [parseInt(h, 10), parseInt(i, 10)]);
                        break;
                    case !!h:
                        m = FormValidation.Helper.format(d.message || FormValidation.I18n[g].stringLength.more, parseInt(h, 10) - 1);
                        break;
                    case !!i:
                        m = FormValidation.Helper.format(d.message || FormValidation.I18n[g].stringLength.less, parseInt(i, 10) + 1)
                }
                return {
                    valid: l,
                    message: m
                }
            }
        }
    }(jQuery),
    function(a) {
        FormValidation.I18n = a.extend(!0, FormValidation.I18n || {}, {
            en_US: {
                uri: {
                    "default": "Please enter a valid URI"
                }
            }
        }), FormValidation.Validator.uri = {
            html5Attributes: {
                message: "message",
                allowlocal: "allowLocal",
                allowemptyprotocol: "allowEmptyProtocol",
                protocol: "protocol"
            },
            enableByHtml5: function(a) {
                return "url" === a.attr("type")
            },
            validate: function(a, b, c, d) {
                var e = a.getFieldValue(b, d);
                if ("" === e) return !0;
                var f = c.allowLocal === !0 || "true" === c.allowLocal,
                    g = c.allowEmptyProtocol === !0 || "true" === c.allowEmptyProtocol,
                    h = (c.protocol || "http, https, ftp").split(",").join("|").replace(/\s/g, ""),
                    i = new RegExp("^(?:(?:" + h + ")://)" + (g ? "?" : "") + "(?:\\S+(?::\\S*)?@)?(?:" + (f ? "" : "(?!(?:10|127)(?:\\.\\d{1,3}){3})(?!(?:169\\.254|192\\.168)(?:\\.\\d{1,3}){2})(?!172\\.(?:1[6-9]|2\\d|3[0-1])(?:\\.\\d{1,3}){2})") + "(?:[1-9]\\d?|1\\d\\d|2[01]\\d|22[0-3])(?:\\.(?:1?\\d{1,2}|2[0-4]\\d|25[0-5])){2}(?:\\.(?:[1-9]\\d?|1\\d\\d|2[0-4]\\d|25[0-4]))|(?:(?:[a-z\\u00a1-\\uffff0-9]-?)*[a-z\\u00a1-\\uffff0-9]+)(?:\\.(?:[a-z\\u00a1-\\uffff0-9]-?)*[a-z\\u00a1-\\uffff0-9])*(?:\\.(?:[a-z\\u00a1-\\uffff]{2,}))" + (f ? "?" : "") + ")(?::\\d{2,5})?(?:/[^\\s]*)?$", "i");
                return i.test(e)
            }
        }
    }(jQuery);
/*!
 * FormValidation (http://formvalidation.io)
 * The best jQuery plugin to validate form fields. Support Bootstrap, Foundation, Pure, SemanticUI, UIKit and custom frameworks
 *
 * @version     v0.8.1, built on 2016-07-29 1:10:56 AM
 * @author      https://twitter.com/formvalidation
 * @copyright   (c) 2013 - 2016 Nguyen Huu Phuoc
 * @license     http://formvalidation.io/license/
 */
! function(a) {
    FormValidation.Framework.Bootstrap = function(b, c, d) {
        c = a.extend(!0, {
            button: {
                selector: '[type="submit"]:not([formnovalidate])',
                disabled: "disabled"
            },
            err: {
                clazz: "help-block",
                parent: "^(.*)col-(xs|sm|md|lg)-(offset-){0,1}[0-9]+(.*)$"
            },
            icon: {
                valid: null,
                invalid: null,
                validating: null,
                feedback: "form-control-feedback"
            },
            row: {
                selector: ".form-group",
                valid: "has-success",
                invalid: "has-error",
                feedback: "has-feedback"
            }
        }, c), FormValidation.Base.apply(this, [b, c, d])
    }, FormValidation.Framework.Bootstrap.prototype = a.extend({}, FormValidation.Base.prototype, {
        _fixIcon: function(a, b) {
            var c = this._namespace,
                d = a.attr("type"),
                e = a.attr("data-" + c + "-field"),
                f = this.options.fields[e].row || this.options.row.selector,
                g = a.closest(f);
            if ("checkbox" === d || "radio" === d) {
                var h = a.parent();
                h.hasClass(d) ? b.insertAfter(h) : h.parent().hasClass(d) && b.insertAfter(h.parent())
            }
            0 !== g.find(".input-group").length && b.addClass("fv-bootstrap-icon-input-group").insertAfter(g.find(".input-group").eq(0))
        },
        _createTooltip: function(a, b, c) {
            var d = this._namespace,
                e = a.data(d + ".icon");
            if (e) switch (c) {
                case "popover":
                    e.css({
                        cursor: "pointer",
                        "pointer-events": "auto"
                    }).popover("destroy").popover({
                        container: "body",
                        content: b,
                        html: !0,
                        placement: "auto top",
                        trigger: "hover click"
                    });
                    break;
                case "tooltip":
                default:
                    e.css({
                        cursor: "pointer",
                        "pointer-events": "auto"
                    }).tooltip("destroy").tooltip({
                        container: "body",
                        html: !0,
                        placement: "auto top",
                        title: b
                    })
            }
        },
        _destroyTooltip: function(a, b) {
            var c = this._namespace,
                d = a.data(c + ".icon");
            if (d) switch (b) {
                case "popover":
                    d.css({
                        cursor: "",
                        "pointer-events": "none"
                    }).popover("destroy");
                    break;
                case "tooltip":
                default:
                    d.css({
                        cursor: "",
                        "pointer-events": "none"
                    }).tooltip("destroy")
            }
        },
        _hideTooltip: function(a, b) {
            var c = this._namespace,
                d = a.data(c + ".icon");
            if (d) switch (b) {
                case "popover":
                    d.popover("hide");
                    break;
                case "tooltip":
                default:
                    d.tooltip("hide")
            }
        },
        _showTooltip: function(a, b) {
            var c = this._namespace,
                d = a.data(c + ".icon");
            if (d) switch (b) {
                case "popover":
                    d.popover("show");
                    break;
                case "tooltip":
                default:
                    d.tooltip("show")
            }
        }
    }), a.fn.bootstrapValidator = function(b) {
        var c = arguments;
        return this.each(function() {
            var d = a(this),
                e = d.data("formValidation") || d.data("bootstrapValidator"),
                f = "object" == typeof b && b;
            e || (e = new FormValidation.Framework.Bootstrap(this, a.extend({}, {
                events: {
                    formInit: "init.form.bv",
                    formPreValidate: "prevalidate.form.bv",
                    formError: "error.form.bv",
                    formSuccess: "success.form.bv",
                    fieldAdded: "added.field.bv",
                    fieldRemoved: "removed.field.bv",
                    fieldInit: "init.field.bv",
                    fieldError: "error.field.bv",
                    fieldSuccess: "success.field.bv",
                    fieldStatus: "status.field.bv",
                    localeChanged: "changed.locale.bv",
                    validatorError: "error.validator.bv",
                    validatorSuccess: "success.validator.bv"
                }
            }, f), "bv"), d.addClass("fv-form-bootstrap").data("formValidation", e).data("bootstrapValidator", e)), "string" == typeof b && e[b].apply(e, Array.prototype.slice.call(c, 1))
        })
    }, a.fn.bootstrapValidator.Constructor = FormValidation.Framework.Bootstrap
}(jQuery);
"use strict";

function sa_img(t, e) {
    swal({
        title: t,
        html: e,
        type: "success",
        confirmButtonText: "Okay"
    }).catch(swal.noop)
}

function update_cart_item(t, e) {
    $.ajax({
        url: t,
        type: "POST",
        data: e,
        success: function (t) {
            t.cart && (cart = t.cart, update_mini_cart(cart), update_cart(cart)), sa_alert(t.status, t.message)
        },
        error: function () {
            sa_alert("Error!", "Ajax call failed, please try again or contact site owner.", "error", !0)
        }
    })
}
function update_cart_shipment(t, e) {
    $.ajax({
        url: t,
        type: "POST",
        data: e,
        success: function (t) {
            console.log(t.cart);
            t.cart && (cart = t.cart, update_cart_shipping(cart))
        },
        error: function () {
            sa_alert("Error!", "Ajax call failed, please try again or contact site owner.", "error", !0)
        }
    })
}
function sticky_con() {
    get_width() > 767 ? ($("#sticky-con").stick_in_parent({
        parent: $(".container")
    }), $("#sticky-con").on("sticky_kit:bottom", function (t) {
        $(this).parent().css("position", "static")
    }).on("sticky_kit:unbottom", function (t) {
        $(this).parent().css("position", "relative")
    })) : $("#sticky-con").trigger("sticky_kit:detach")
}

function sticky_footer() {
    $("body").css("padding-bottom", $(".footer").height())
}

function get_width() {
    return $(window).width()
}

function loading(t) {
    $("#loading").show(), setTimeout(function () {
        $("#loading").hide()
    }, t)
}

function get(t) {
    return "undefined" != typeof Storage ? localStorage.getItem(t) : void alert("Please use a modern browser as this site needs localstroage!")
}

function store(t, e) {
    "undefined" != typeof Storage ? localStorage.setItem(t, e) : alert("Please use a modern browser as this site needs localstroage!")
}

function remove(t) {
    "undefined" != typeof Storage ? localStorage.removeItem(t) : alert("Please use a modern browser as this site needs localstroage!")
}

function gen_html_old(t) {
    var e = "";
    if (get_width() > 992) var a = get("shop_grid"),
        s = ".three-col" == a ? 4 : 6;
    else var a = ".two-col",
        s = 4;
    var i = a && ".three-col" == a ? "col-md-12 list" : "featured-products col-sm-6 col-md-4 col-xs-6 grid",
        o = a && ".three-col" == a ? "" : "alt";
    t || (e += "<h4>No product found, please try to search or filter again.</h4>"), $.each(t, function (a, r) {
        if (r.image.length > 1) {
            r.image = r.image
        } else {
            r.image = 'no_image.png'
        }
        if (r.free_shipment == 1) {
            r.free_ship_tag = 'FREE Shipping'
        } else {
            r.free_ship_tag = ''
        }

        var p_n = r.promo_price;
        var p_status = r.promo_status;
        var calcluated_promotion_price = r.calcluated_promotion_price;
        var calcluated_different_product_promotion_price = r.calcluated_different_product_promotion_price;
        var n = " AED " + r.price;
        if (p_status > 0 && p_n > 0) {
            n = "<del class='text-red was normal_custom_font'>" + n + "</del><br>" + " AED" + p_n;
        } else if (calcluated_promotion_price > 0) {
            n = "<del class='text-red was normal_custom_font'>" + n + "</del><br>" + " AED" + calcluated_promotion_price;
        } else if (calcluated_different_product_promotion_price > 0) {
            n = "<del class='text-red was normal_custom_font'>" + n + "</del><br>" + " AED" + calcluated_different_product_promotion_price;
        }

        0 === a && (e += '<div class="row">'), a % s === 0 && (e += '</div><div class="row">'), e += '<div class="product-container ' + i + '">\n <div class="label-product"><span>'+r.promo_text+'</span></div>  <a class="btn-wishlist1  add-to-wishlist" data-id="' + r.id + '" data-toggle="tooltip" title="" data-original-title="Add to Wish List" ><i class="pe-7s-like"></i></a>\n      <div class="ribbon_parent product cus_pro_listing ' + o + '">\n  ' + r.buy1get1_other_offer + ' ' + r.buy1get1_cat_offer + ' ' + r.buy1get1_offer + '  ' + r.popular_offer + ' ' + r.buyCategoryWithPercentage + ' ' + r.buyProductsWithPercentage + ' <div class="product-top">\n        <div class="product-image product-image_' + r.id + ' custom_pro_list_img_bucket " id="' + r.id + '">\n        <a class="pro_list_img" href="javascript:void(0)">\n        <img class="img-responsive cus_img_size" style=" max-height: 100%; width: 100%;" src="' + site.base_url + "ucloud/plugins/filepreviewer/site/resize_image.php?f=" + r.image+"&w=1100&h=900&m=medium&uh=&o=jpg" + '" alt=""/>\n   </a> <a href="' + site.site_url + "product/" + r.slug + '" class="moredeatil"><div class="btn btn-primary quickview quickview_pop cus_add_to_cart hide" id="' + r.id + '" data-id="' + r.id + '" style="opacity: 1;">MORE DETAIL</div> </a> <a href="' + site.site_url + "product/" + r.slug + '" class="moredeatil"><div class="btn btn-primary quickview cus_add_to_cart hide" style="top:40%;" id="' + r.id + '" data-id="' + r.id + '" style="opacity: 1;">ADD TO CART</div>  </a>     \n        </div>\n        <div class="product-desc custom_pro_list_desc_bucket" style="text-align:  justify">\n        <a class="blackcolor" href="' + site.site_url + "product/" + r.slug + '">\n        <p class=" product-des">' + r.details + '</p>\n        </a>\n <div class="free_shipment hide">' + r.free_ship_tag + '</div> <div class="product-ration-king hide"><img src="' + site.base_url + "assets/uploads/four-stars.png" + '" style="width: 74px; height: 14px; padding: 0px;">  </div>\n  <div class="price cus_height is_souq_price_color" style="text-align: center;">\n        ' + n + '  </div>    <p class="cus_pro_details hide">' + r.details + '</p>\n <br/>         <button class="btn btn-info add-to-wishlist hidden" data-id="' + r.id + '"><i class="fa fa-heart-o"></i></button>\n                        <button class="btn btn-default add-to-cart" style="float: left; border-radius: 0px;" data-id="' + r.id + '"><i class="pe-7s-cart padding-right-md"></i> Add to Cart</button>\n      <div class="product-rating hide">\n   <div class="form-group" style="margin-bottom:0;">\n        <div class="input-group">\n        <span class="input-group-addon pointer btn-minus"><span class="fa fa-minus"></span></span>\n        <input type="text" name="quantity" class="form-control text-center quantity-input" value="1" required="required">\n        <span class="input-group-addon pointer btn-plus"><span class="fa fa-plus"></span></span>\n        </div>\n        </div>\n        </div>\n  <br/>  </div>\n        </div>\n        <div class="clearfix"></div>\n        <div class="product-bottom">\n   <div class="product-cart-button">\n        <div class="btn-group" role="group" aria-label="...">\n                       </div>\n        </div>\n        </div>\n        <div class="clearfix"></div>\n        </div>\n        </div>', a + 1 === t.length && (e += "</div>")
    }), $("#results").empty(), $("<div>" + e + "</div>").appendTo($("#results"))
}
function gen_html_list(t) {
    var currency_default = 'AED ';
    var e = "";
    if (get_width() > 992) var a = get("shop_grid"),
        s = ".three-col" == a ? 4 : 6;
    else var a = ".two-col",
        s = 4;
    var i = a && ".three-col" == a ? "col-md-12 list" : " col-sm-6 col-md-4 col-xs-6 small_list_products",
        o = a && ".three-col" == a ? "" : "alt";
    t || (e += "<h4>No product found, please try to search or filter again.</h4>"), $.each(t, function (a, r) {
        if (r.image.length > 0) {
            r.image = r.image
        } else {
            r.image = 'no_image.png'
        }
        if (r.free_shipment == 1) {
            r.free_ship_tag = 'FREE Shipping'
        } else {
            r.free_ship_tag = ''
        }

        var pro_name = r.name;
        if(pro_name.length > 23 ){
            pro_name = pro_name.substring(0,23);
        }

        var p_n = r.promo_price;
        var p_status = r.promo_status;
        var calcluated_promotion_price = r.calcluated_promotion_price;
        var calcluated_different_product_promotion_price = r.calcluated_different_product_promotion_price;
        var n = " AED " + r.price;
        if (p_status > 0 && p_n > 0) {
            n = "<del class='text-red was normal_custom_font'>" + n + "</del><br>" + " AED" + p_n;
        } else if (calcluated_promotion_price > 0) {
            n = "<del class='text-red was normal_custom_font'>" + n + "</del><br>" + " AED" + calcluated_promotion_price;
        } else if (calcluated_different_product_promotion_price > 0) {
            n = "<del class='text-red was normal_custom_font'>" + n + "</del><br>" + " AED" + calcluated_different_product_promotion_price;
        }
        //  0 === a && (e += '<div class="row">'), a % s === 0 && (e += '</div><div class="row">'), e += '<div class=" ' + i + '">\n <div class="label-product"><span>new</span></div>  <a class="btn-wishlist1  add-to-wishlist" data-id="' + r.id + '" data-toggle="tooltip" title="" data-original-title="Add to Wish List" ><i class="pe-7s-like"></i></a>\n      <div class="ribbon_parent product cus_pro_listing ' + o + '">\n  ' + r.buy1get1_other_offer + ' ' + r.buy1get1_cat_offer + ' ' + r.buy1get1_offer + '  ' + r.popular_offer + ' ' + r.buyCategoryWithPercentage + ' ' + r.buyProductsWithPercentage + ' <div class="product-top">\n        <div class="product-image product-image_' + r.id + ' custom_pro_list_img_bucket " id="' + r.id + '">\n        <a class="pro_list_img" href="javascript:void(0)">\n        <img class="img-responsive cus_img_size" style=" max-height: 100%; width: 100%;" src="' + site.base_url + "assets/uploads/" + r.image + '" alt=""/>\n   </a> <a href="' + site.site_url + "product/" + r.slug + '" class="moredeatil"><div class="btn btn-primary quickview quickview_pop cus_add_to_cart hide" id="' + r.id + '" data-id="' + r.id + '" style="opacity: 1;">MORE DETAIL</div> </a> <a href="' + site.site_url + "product/" + r.slug + '" class="moredeatil"><div class="btn btn-primary quickview cus_add_to_cart hide" style="top:40%;" id="' + r.id + '" data-id="' + r.id + '" style="opacity: 1;">ADD TO CART</div>  </a>     \n        </div>\n        <div class="product-desc custom_pro_list_desc_bucket" style="text-align:  justify">\n        <a class="blackcolor" href="' + site.site_url + "product/" + r.slug + '">\n        <p class=" product-des">' + r.details + '</p>\n        </a>\n <div class="free_shipment hide">' + r.free_ship_tag + '</div> <div class="product-ration-king hide"><img src="' + site.base_url + "assets/uploads/four-stars.png" + '" style="width: 74px; height: 14px; padding: 0px;">  </div>\n  <div class="price cus_height is_souq_price_color" style="text-align: center;">\n        ' + n + '  </div>    <p class="cus_pro_details hide">' + r.details + '</p>\n <br/>         <button class="btn btn-info add-to-wishlist hidden" data-id="' + r.id + '"><i class="fa fa-heart-o"></i></button>\n                        <button class="btn btn-default add-to-cart" style="float: left; border-radius: 0px;" data-id="' + r.id + '"><i class="pe-7s-cart padding-right-md"></i> Add to Cart</button>\n      <div class="product-rating hide">\n   <div class="form-group" style="margin-bottom:0;">\n        <div class="input-group">\n        <span class="input-group-addon pointer btn-minus"><span class="fa fa-minus"></span></span>\n        <input type="text" name="quantity" class="form-control text-center quantity-input" value="1" required="required">\n        <span class="input-group-addon pointer btn-plus"><span class="fa fa-plus"></span></span>\n        </div>\n        </div>\n        </div>\n  <br/>  </div>\n        </div>\n        <div class="clearfix"></div>\n        <div class="product-bottom">\n   <div class="product-cart-button">\n        <div class="btn-group" role="group" aria-label="...">\n                       </div>\n        </div>\n        </div>\n        <div class="clearfix"></div>\n        </div>\n        </div>', a + 1 === t.length && (e += "</div>")
        0 === a && (e += '<div class="row">'), a % s === 0 && (e += '</div><div class="row">'), e += '<div class=" ' + i + '">\n<div class="main-single-product fix"><div class="col-sm-4"><div class="single-product"> '+r.promotext_2+'<div class="pro-img"><a href="' + site.site_url + "product/" + r.slug + '"><img class="primary-img homeproduct_slider_img" src="' + site.base_url + "ucloud/plugins/filepreviewer/site/resize_image.php?f=" + r.image+"&w=1100&h=900&m=medium&uh=&o=jpg" + '" alt="single-product"></a><span class="sticker-new">'+r.promo_text+'</span> </div> </div> </div> <div class="col-sm-8"> <div class="thubnail-desc fix"><h4 class="product-header"><a href="product-page.html">' + pro_name + '</a></h4><div class="pro-price mb-15"> <ul class="pro-price-list"> <li class="price">'+ r.price +'</li> <li class="mtb-50"><p>'+ r.details +'</p></li></ul></div> <div class="product-button-actions"> <a href="' + site.site_url + "product/" + r.slug + '"><button   class="add-to-cart customaddtocart" data-toggle="tooltip" title="Add to Cart">add to cart</button></a><a href="#" data-toggle="tooltip" title="Add to Wishlist" data-id="' + r.id + '" class="same-btn mr-15 add-to-wishlist"><i class="pe-7s-like"></i></a> </div></div> </div> </div></div> ', a + 1 === t.length && (e += "</div>")
        //  0 === a && (e += '<div class="row">'), a % s === 0 && (e += '</div><div class="row">'), e += '<div class=" custom_listing_height ' + i + '">\n<div class="single-product"> <div class="pro-img"><a href="' + site.site_url + "product/" + r.slug + '">  <img class="primary-img homeproduct_slider_img" src="' + site.base_url + "assets/uploads/" + r.image + '" alt="single-product"> <img class="secondary-img homeproduct_slider_img" src="' + site.base_url + "assets/uploads/" + r.image + '" alt="single-product"></a><div class="quick-view">  <a href="#" class="quickview_pop"  data-id="' + r.id + '"><i class="pe-7s-look " ></i>quick view</a></div><span class="sticker-new">new</span></div><div class="pro-content text-center"><h4><a href="' + site.site_url + "product/" + r.slug + '">'+ pro_name +'</a></h4><p class="price"><span>'+ r.price +'</span></p><div class="action-links2"><a data-toggle="tooltip" title="Add to Cart" href="cart.html">add to cart</a></div></div></div></div>', a + 1 === t.length && (e += "</div>")
    }), $("#results").empty(), $("<div>" + e + "</div>").appendTo($("#results")),$(".preloader").hide()
}
function gen_html(t) {
    if (get("shop_grid") == '.three-col') {
        gen_html_list(t);

    } else {
        var currency_default = 'AED ';
        var e = "";
        if (get_width() > 992) var a = get("shop_grid"),
            s = ".three-col" == a ? 4 : 6;
        else var a = ".two-col",
            s = 4;
        var i = a && ".three-col" == a ? "col-md-12 list" : " col-sm-6 col-md-4 col-xs-6",
            o = a && ".three-col" == a ? "" : "alt";
        t || (e += "<h4>No product found, please try to search or filter again.</h4>"), $.each(t, function (a, r) {
            if (r.image.length > 0) {
                r.image = r.image
            } else {
                r.image = 'no_image.png'
            }
            if (r.free_shipment == 1) {
                r.free_ship_tag = 'FREE Shipping'
            } else {
                r.free_ship_tag = ''
            }

            var pro_name = r.name;
            if (pro_name.length > 23) {
                pro_name = pro_name.substring(0, 23);
            }

            var p_n = r.promo_price;
            var p_status = r.promo_status;
            var calcluated_promotion_price = r.calcluated_promotion_price;
            var calcluated_different_product_promotion_price = r.calcluated_different_product_promotion_price;
            var n = " AED " + r.price;
            if (p_status > 0 && p_n > 0) {
                n = "<del class='text-red was normal_custom_font'>" + n + "</del><br>" + " AED" + p_n;
            } else if (calcluated_promotion_price > 0) {
                n = "<del class='text-red was normal_custom_font'>" + n + "</del><br>" + " AED" + calcluated_promotion_price;
            } else if (calcluated_different_product_promotion_price > 0) {
                n = "<del class='text-red was normal_custom_font'>" + n + "</del><br>" + " AED" + calcluated_different_product_promotion_price;
            }
            //  0 === a && (e += '<div class="row">'), a % s === 0 && (e += '</div><div class="row">'), e += '<div class=" ' + i + '">\n <div class="label-product"><span>new</span></div>  <a class="btn-wishlist1  add-to-wishlist" data-id="' + r.id + '" data-toggle="tooltip" title="" data-original-title="Add to Wish List" ><i class="pe-7s-like"></i></a>\n      <div class="ribbon_parent product cus_pro_listing ' + o + '">\n  ' + r.buy1get1_other_offer + ' ' + r.buy1get1_cat_offer + ' ' + r.buy1get1_offer + '  ' + r.popular_offer + ' ' + r.buyCategoryWithPercentage + ' ' + r.buyProductsWithPercentage + ' <div class="product-top">\n        <div class="product-image product-image_' + r.id + ' custom_pro_list_img_bucket " id="' + r.id + '">\n        <a class="pro_list_img" href="javascript:void(0)">\n        <img class="img-responsive cus_img_size" style=" max-height: 100%; width: 100%;" src="' + site.base_url + "assets/uploads/" + r.image + '" alt=""/>\n   </a> <a href="' + site.site_url + "product/" + r.slug + '" class="moredeatil"><div class="btn btn-primary quickview quickview_pop cus_add_to_cart hide" id="' + r.id + '" data-id="' + r.id + '" style="opacity: 1;">MORE DETAIL</div> </a> <a href="' + site.site_url + "product/" + r.slug + '" class="moredeatil"><div class="btn btn-primary quickview cus_add_to_cart hide" style="top:40%;" id="' + r.id + '" data-id="' + r.id + '" style="opacity: 1;">ADD TO CART</div>  </a>     \n        </div>\n        <div class="product-desc custom_pro_list_desc_bucket" style="text-align:  justify">\n        <a class="blackcolor" href="' + site.site_url + "product/" + r.slug + '">\n        <p class=" product-des">' + r.details + '</p>\n        </a>\n <div class="free_shipment hide">' + r.free_ship_tag + '</div> <div class="product-ration-king hide"><img src="' + site.base_url + "assets/uploads/four-stars.png" + '" style="width: 74px; height: 14px; padding: 0px;">  </div>\n  <div class="price cus_height is_souq_price_color" style="text-align: center;">\n        ' + n + '  </div>    <p class="cus_pro_details hide">' + r.details + '</p>\n <br/>         <button class="btn btn-info add-to-wishlist hidden" data-id="' + r.id + '"><i class="fa fa-heart-o"></i></button>\n                        <button class="btn btn-default add-to-cart" style="float: left; border-radius: 0px;" data-id="' + r.id + '"><i class="pe-7s-cart padding-right-md"></i> Add to Cart</button>\n      <div class="product-rating hide">\n   <div class="form-group" style="margin-bottom:0;">\n        <div class="input-group">\n        <span class="input-group-addon pointer btn-minus"><span class="fa fa-minus"></span></span>\n        <input type="text" name="quantity" class="form-control text-center quantity-input" value="1" required="required">\n        <span class="input-group-addon pointer btn-plus"><span class="fa fa-plus"></span></span>\n        </div>\n        </div>\n        </div>\n  <br/>  </div>\n        </div>\n        <div class="clearfix"></div>\n        <div class="product-bottom">\n   <div class="product-cart-button">\n        <div class="btn-group" role="group" aria-label="...">\n                       </div>\n        </div>\n        </div>\n        <div class="clearfix"></div>\n        </div>\n        </div>', a + 1 === t.length && (e += "</div>")
            0 === a && (e += '<div class="row">'), a % s === 0 && (e += '</div><div class="row">'), e += '<div class=" custom_listing_height ' + i + '">\n<div class="single-product"> '+r.promotext_2+' <div class="pro-img"><a href="' + site.site_url + "product/" + r.slug + '">  <img class="primary-img homeproduct_slider_img" src="' + site.base_url + "ucloud/plugins/filepreviewer/site/resize_image.php?f=" + r.image +"&w=1100&h=900&m=medium&uh=&o=jpg" + '" alt="single-product"><img class="secondary-img homeproduct_slider_img" src="' + site.base_url + "ucloud/plugins/filepreviewer/site/resize_image.php?f=" + r.image +"&w=1100&h=900&m=medium&uh=&o=jpg" + '" alt="single-product"></a><span class="sticker-new">'+r.promo_text+'</span></div><div class="pro-content text-center"><h4><a href="' + site.site_url + "product/" + r.slug + '">' + pro_name + '</a></h4><p class="price"><span>' + r.price + '</span></p><div class="action-links2"><a data-toggle="tooltip" title="Add to Cart" href="' + site.site_url + "product/" + r.slug + '">add to cart</a></div></div></div></div>', a + 1 === t.length && (e += "</div>")
        }), $("#results").empty(), $("<div>" + e + "</div>").appendTo($("#results")), $(".preloader").hide()
    }
}

function searchProducts() {
    $("#loading").show();
    var t = {};
    t[site.csrf_token] = site.csrf_token_value, t.filters = get_filters(), t.format = "json",
        $.ajax({
            url: site.shop_url + "search?page=" + filters.page,
            type: "POST",
            data: t,
            dataType: "json"
        }).done(function (t) {
            products = t.products, t.products ? (t.pagination && $("#pagination").empty().html(t.pagination), t.info && $(".page-info").empty().text(lang.page_info.replace("_page_", t.info.page).replace("_total_", t.info.total))) : ($(".page-info").empty(), $("#pagination").empty()), gen_html(products)
        }).always(function () {
            $("#loading").hide()
        })
}

function get_filters() {
    return filters.category = $("#product-category").val() ? $("#product-category").val() : filters.category, filters.min_price = $("#min-price").val(), filters.max_price = $("#max-price").val(), filters.in_stock = $("#in-stock").is(":checked") ? 1 : 0, filters.sorting = get("sorting"), filters
}

function update_mini_cart1(t) {
    if (t.total_items && t.total_items > 0) {
        $(".cart-total-items").text(t.total_items), $("#cart-items").empty(), $.each(t.contents, function () {
            if (this.image.length > 0) {
                this.image = this.image
            } else {
                this.image = 'no_image.png'
            }
            var t = '<td><span class="cart-item-image"><img src="' + site.base_url + "ucloud/plugins/filepreviewer/site/resize_image.php?f=" + this.image+"&w=1100&h=900&m=medium&uh=&o=jpg" + '" alt=""></span></td><td>' + this.name.substr(0, 16)+ "..." + "<br>" + this.qty + " x " + this.price + '</td><td class="text-right text-bold">' + this.subtotal + "</td>";
            $("<tr>" + t + "</tr>").appendTo("#cart-items")
        });
        var e = '\n        <tr class="text-bold"><td colspan="2">' + lang.total_items + '</td><td class="text-right">' + t.total_items + '</td></tr>\n        <tr class="text-bold"><td colspan="2">' + lang.total + '</td><td class="text-right">' + t.total + "</td></tr>\n        ";
        console.log(t);
        $("<tfoot>" + e + "</tfoot>").appendTo("#cart-items"), $("#cart-empty").hide(), $("#cart-contents").show()
    } else $(".cart-total-items").text(0), $("#cart-contents").hide(), $("#cart-empty").show()
}

function update_mini_cart(t) {

    if (t.total_items && t.total_items > 0) {
        $(".cart-total-items").text(t.total_items), $("#cart-items").empty(), $.each(t.contents, function () {
            if (this.image.length > 0) {
                this.image = this.image
            } else {
                this.image = 'no_image.png'
            }
            var t = '<div class="single-cart-box"><div class="cart-img "><img src="' + site.base_url + "ucloud/plugins/filepreviewer/site/resize_image.php?f=" + this.image +"&w=1100&h=900&m=medium&uh=&o=jpg" + '" alt=""></div><div class="cart-content"><h6><a href="' + site.base_url +'cart'+'">' + this.name.substr(0, 16)+ "..." + "</a></h6><span>" + this.qty + " x " + this.price + '</span></div></div>';
            // var tc = '<div class="single-cart-box single-cart-box-cust"><div class="cart-img cart-img-cust"><img src="' + site.base_url + "assets/uploads/thumbs/" + this.image + '" alt=""></div><div class="cart-content  cart-content-cust"><h6><a href="' + site.base_url +'cart'+'">' + this.name+ "..." + "</a></h6><span class='pull-right'>" + this.qty + " x " + this.price + '</span></div></div>';
            $(t).appendTo("#cart-items");
            // $(tc).appendTo("#cart-items1");
        });
        if (t.cartPromotionPercentage > 0) {
            var e = '\n         <div class="cart-footer fix"><h5 style="padding: 7px 0px; border-bottom: hidden; margin-bottom: 1px;">total :<span class="f-right">(<del >' +  t.total_old + '</del>) </span></h5> <h5 style="padding: 7px 0px; border-bottom: hidden; margin-bottom: 1px;">DISCOUNT :<span class="f-right">(' +  t.overall_permotion_discount + ') </span></h5> <h5 style="padding: 7px 0px; border-bottom: hidden; margin-bottom: 1px;">Payable :<span class="f-right">' +  t.total + ' </span></h5><div class="cart-actions"><a class="checkout" style="margin-bottom: 5px;" href="' + site.base_url +'cart'+'">View Cart</a> <a class="checkout" href="' + site.base_url +'cart/checkout'+'">Checkout</a></div></div>';
        }else {
            var e = '\n         <div class="cart-footer fix"><h5>total :<span class="f-right">' +  t.total + '</span></h5><div class="cart-actions"><a class="checkout" style="margin-bottom: 5px;" href="' + site.base_url +'cart'+'">View Cart</a> <a class="checkout" href="' + site.base_url +'cart/checkout'+'">Checkout</a></div></div>';
        }
        console.log(t);
        $(e).appendTo("#cart-items"),
            /* var e = '\n        <tr class="text-bold"><td colspan="2">' + lang.total_items + '</td><td class="text-right">' + t.total_items + '</td></tr>\n        <tr class="text-bold"><td colspan="2">' + lang.total + '</td><td class="text-right">' + t.total + "</td></tr>\n        ";
             console.log(t);
             $("<tfoot>" + e + "</tfoot>").appendTo("#cart-items"), */
            $("#cart-empty").hide(), $("#cart-contents").show()
    } else $(".cart-total-items").text(0), $("#cart-contents").hide(), $("#cart-empty").show()
}

function update_cart(t) {
    var gift_cart = $('#giftcart_amount').val();
    var hide_shipping_amount = $('#hide_shipping_amount').val();
    if (t.total_items && t.total_items > 0) {
        $("#cart-table tbody").empty();
        var e = 1;
        var opt='';
        var currency_default = 'AED ';
        $.each(t.contents, function () {

            if(this.option_name){
                opt = '(' + this.option_name+ ')';
                // opt = '('+ this.option['name'] +')';
            }
            var t = this,

                a = '<td class="product-thumbnail"><a href="#"><img src="' + site.base_url + "ucloud/plugins/filepreviewer/site/resize_image.php?f=" + this.image +"&w=1100&h=900&m=medium&uh=&o=jpg" + '" alt=""></a></td>    <td class="product-name"><a href="#">' + this.name+opt + '</a></td>';
            a += '<td class="product-price"><span class="amount">'+this.price+'</span></td> <td class="product-quantity"><input type="number" name="' + e + '[qty]" class=" input-qty cart-item-qty" value="' + this.qty + '"> </td> <td class="product-subtotal">' + this.subtotal + '</td> </td> <span style="float: left" class="input-group-btn"> <td class="product-remove"> <a href="#"> <i class="fa fa-times remove-item" aria-hidden="true" data-rowid="' + this.rowid + '"></i> </a> </td>', e++, $('<tr id="' + this.rowid + '">' + a + "</tr>").appendTo("#cart-table tbody");
            var tc = '<div class="single-cart-box single-cart-box-cust"><div class="cart-img cart-img-cust"><img src="' + site.base_url + "ucloud/plugins/filepreviewer/site/resize_image.php?f=" + this.image +"&w=1100&h=900&m=medium&uh=&o=jpg" + '" alt=""></div><div class="cart-content  cart-content-cust"><h6><a href="' + site.base_url +'cart'+'">' + this.name+opt+ "..." + "</a></h6><span class='pull-right'>" + this.qty + " x " + this.price + '</span></div></div>';
            $(tc).appendTo("#cart-items1");

        }), $("#cart-totals").empty();
        /*  var a = "<tr><td>" + lang.total_w_o_tax + '</td><td class="text-right">' + t.subtotal + "</td></tr>";*/
        var a = '<tr class="cart-subtotal"><th>' + lang.total_w_o_tax + '</th><td><span class="amount">' + t.subtotal + "</td></tr>";
        if (t.cartPromotionPercentage > 0) {
            a += '<tr class="cart-subtotal"><th>Discount(' + t.cartPromotionPercentage + ')%</th><td><span class="amount">' + t.permotion_discount + "</td></tr>";
            a += '<tr class="cart-subtotal"><th>Total After ' + t.cartPromotionPercentage + '% Promotion</th><td><span class="amount">' + t.tax_permotion_discount + site.settings.new_country_symbl+ "</td></tr>";
            a += '<tr><tr class="cart-subtotal"><th>' + lang.product_tax + '</th><td><span class="amount">' + t.tax_permotion_total + "</span></td></tr >";

            if (hide_shipping_amount > 0) {
                site.settings.tax2 !== !1 && (a += "<tr class='cart-subtotal hide'><th>" + lang.order_tax + '</th><td><span class="amount">' + t.order_tax + "</span></td></tr>"), a += '';
            } else {
                site.settings.tax2 !== !1 && (a += "<tr class='cart-subtotal hide'><th>" + lang.order_tax + '</th><td><span class="amount">' + t.order_tax + "</span></td></tr>"), a += "<tr class='cart-subtotal'><th>" + lang.shipping + ' *</th> <td><span class="amount">' + t.shipping + "</span></td></tr>", a += '';
            }

        }else {
            a += '<tr><tr class="cart-subtotal"><th>' + lang.product_tax + '</th><td><span class="amount">' + t.total_item_tax + "</span></td></tr >";
            if (hide_shipping_amount > 0) {
                a += '<tr class="cart-subtotal"><th>' + lang.total + '</th><td><span class="amount">' + t.total + "</span></td></tr>", site.settings.tax2 !== !1 && (a += "<tr class='cart-subtotal hide'><th>" + lang.order_tax + '</th><td><span class="amount">' + t.order_tax + "</span></td></tr>"), a += "<tr class='cart-subtotal'></tr>", a += '';
            } else {
                a += '<tr class="cart-subtotal"><th>' + lang.total + '</th><td><span class="amount">' + t.total + "</span></td></tr>", site.settings.tax2 !== !1 && (a += "<tr class='cart-subtotal hide'><th>" + lang.order_tax + '</th><td><span class="amount">' + t.order_tax + "</span></td></tr>"), a += "<tr class='cart-subtotal'><th>" + lang.shipping + ' *</th> <td><span class="amount">' + t.shipping + "</span></td></tr>", a += '';
            }
        }
        if (gift_cart > 0) {

            a += '<tr class="cart-subtotal"><th>Gift Card</th>', a += ' <td><span class="amount">' + gift_cart + site.settings.new_country_symbl+ "</span></td></tr>";

            var grandtotal = t.grand_total;
            grandtotal = grandtotal.replace(/,/g, "");
            var newtotal = parseInt(grandtotal) - parseInt(gift_cart);
            if (newtotal < 0) {
                newtotal = 0.00;
            }
            console.log(site.settings.new_country_symbl);
            a += '<tr class="order-total"><th>' + lang.grand_total + '</th>', a += '<td class="cart_grand_total"> <strong><span class="amount">' + (newtotal)+ '.00' + site.settings.new_country_symbl+"</span></strong></td></tr>";
        } else {
            if (t.cartPromotionPercentage > 0) {
                a += '<tr class="old_grand_total order-total"><th>' + lang.grand_total + '</th>', a += '<td class="cart_grand_total"><strong><span class="amount">' + t.grand_total + "</span></strong></td></tr>";
            } else {
                a += '<tr class="old_grand_total order-total"><th>' + lang.grand_total + '</th>', a += '<td class="cart_grand_total"><strong><span class="amount">' + t.grand_total + "</span></strong></td></tr>";
            }
        }
        $("<tbody>" + a + "</tbody>").appendTo("#cart-totals"), $("#total-items").text(t.total_items + "(" + t.total_unique_items + ")"), $(".cart-item-option").selectpicker("refresh"), $(".cart-empty-msg").hide(), $(".cart-contents").show()
    } else $("#total-items").text(t.total_items), $(".cart-contents").hide(), $(".cart-empty-msg").show()
}


function update_cart_shipping(t) {

    var gift_cart = $('#giftcart_amount').val();
    if (t.total_items && t.total_items > 0) {
        $("#cart-table tbody").empty();
        var e = 1;
        var opt='';
        var currency_default = 'AED ';
        $.each(t.contents, function () {
            if(this.option_name){
                opt = '(' + this.option_name+ ')';
                // opt = '('+ this.option['name'] +')';
            }


        }), $("#cart-totals").empty();
        /*  var a = "<tr><td>" + lang.total_w_o_tax + '</td><td class="text-right">' + t.subtotal + "</td></tr>";*/
        /*        var a = '<tr class="cart-subtotal"><th>' + lang.total_w_o_tax + '</th><td><span class="amount">' + t.subtotal + "</td></tr>";

         a += '<tr><tr class="cart-subtotal"><th>' + lang.product_tax + '</th><td><span class="amount">' + t.total_item_tax + "</span></td></tr >", a += '<tr class="cart-subtotal"><th>' + lang.total + '</th><td><span class="amount">' + t.total + "</span></td></tr>", site.settings.tax2 !== !1 && (a += "<tr class='cart-subtotal hide'><th>" + lang.order_tax + '</th><td><span class="amount">' + t.order_tax + "</span></td></tr>"), a += "<tr class='cart-subtotal'><th>" + lang.shipping + ' *</th> <td><span class="amount">' + t.shipping + "</span></td></tr>", a += '';*/

        var a = '<tr class="cart-subtotal"><th>' + lang.total_w_o_tax + '</th><td><span class="amount">' + t.subtotal + "</td></tr>";
        if (t.cartPromotionPercentage > 0) {
            a += '<tr class="cart-subtotal"><th>Discount(' + t.cartPromotionPercentage + ')%</th><td><span class="amount">' + t.permotion_discount + "</td></tr>";
            a += '<tr class="cart-subtotal"><th>Total After ' + t.cartPromotionPercentage + '% Promotion</th><td><span class="amount">' + t.tax_permotion_discount + site.settings.new_country_symbl+ "</td></tr>";
            a += '<tr><tr class="cart-subtotal"><th>' + lang.product_tax + '</th><td><span class="amount">' + t.tax_permotion_total + "</span></td></tr >";

            // if (hide_shipping_amount > 0) {
            //  site.settings.tax2 !== !1 && (a += "<tr class='cart-subtotal hide'><th>" + lang.order_tax + '</th><td><span class="amount">' + t.order_tax + "</span></td></tr>"), a += '';
            // } else {
            site.settings.tax2 !== !1 && (a += "<tr class='cart-subtotal hide'><th>" + lang.order_tax + '</th><td><span class="amount">' + t.order_tax + "</span></td></tr>"), a += "<tr class='cart-subtotal'><th>" + lang.shipping + ' *</th> <td><span class="amount">' + t.shipping + "</span></td></tr>", a += '';
            //}

        }else {
            a += '<tr><tr class="cart-subtotal"><th>' + lang.product_tax + '</th><td><span class="amount">' + t.total_item_tax + "</span></td></tr >";
            // if (hide_shipping_amount > 0) {
            //     a += '<tr class="cart-subtotal"><th>' + lang.total + '</th><td><span class="amount">' + t.total_real + "</span></td></tr>", site.settings.tax2 !== !1 && (a += "<tr class='cart-subtotal hide'><th>" + lang.order_tax + '</th><td><span class="amount">' + t.order_tax + "</span></td></tr>"), a += "<tr class='cart-subtotal'></tr>", a += '';
            //   } else {
            a += '<tr class="cart-subtotal"><th>' + lang.total + '</th><td><span class="amount">' + t.total + "</span></td></tr>", site.settings.tax2 !== !1 && (a += "<tr class='cart-subtotal hide'><th>" + lang.order_tax + '</th><td><span class="amount">' + t.order_tax + "</span></td></tr>"), a += "<tr class='cart-subtotal'><th>" + lang.shipping + ' *</th> <td><span class="amount">' + t.shipping + "</span></td></tr>", a += '';
            //  }
        }


        if (gift_cart > 0) {

            a += '<tr class="cart-subtotal"><th>Gift Card</th>', a += ' <td><span class="amount">' + gift_cart + "</span></td></tr>";

            var grandtotal = t.grand_total;
            grandtotal = grandtotal.replace(/,/g, "");
            var newtotal = parseInt(grandtotal) - parseInt(gift_cart);
            if (newtotal < 0) {
                newtotal = 0.00;
            }
            a += '<tr class="order-total"><th>' + lang.grand_total + '</th>', a += '<td class="cart_grand_total"> <strong><span class="amount">' + newtotal + '.00' + site.settings.new_country_symbl + "</span></strong></td></tr>";
        } else {
            if (t.cartPromotionPercentage > 0) {
                a += '<tr class="old_grand_total order-total"><th>' + lang.grand_total + '</th>', a += '<td class="cart_grand_total"><strong><span class="amount">' + t.grand_total + "</span></strong></td></tr>";
            } else {
                a += '<tr class="old_grand_total order-total"><th>e' + lang.grand_total + '</th>', a += '<td class="cart_grand_total"><strong><span class="amount">' + t.grand_total + "</span></strong></td></tr>";
            }
            // a += '<tr class="old_grand_total order-total"><th>' + lang.grand_total + '</th>', a += '<td class="cart_grand_total"><strong><span class="amount">' + t.grand_total + "</span></strong></td></tr>";
        }
        $("<tbody>" + a + "</tbody>").appendTo("#cart-totals"), $("#total-items").text(t.total_items + "(" + t.total_unique_items + ")"), $(".cart-item-option").selectpicker("refresh"), $(".cart-empty-msg").hide(), $(".cart-contents").show()
    } else $("#total-items").text(t.total_items), $(".cart-contents").hide(), $(".cart-empty-msg").show()
}


function update_cart_old(t) {
    var gift_cart = $('#giftcart_amount').val();


    if (t.total_items && t.total_items > 0) {
        $("#cart-table tbody").empty();
        var e = 1;
        var currency_default = 'AED ';
        $.each(t.contents, function () {
            var t = this,
                a = '\n          <td>\n            <span class=""><img class="img-thumbnail" width="110px" src="' + site.base_url + "ucloud/plugins/filepreviewer/site/resize_image.php?f=" + this.image +"&w=1100&h=900&m=medium&uh=&o=jpg" + '" alt=""></span>\n            </td>    <td class="text-center">\n     ' + this.name + '                  </td> \n           ' + this.name + "            <td>";
            this.options && (a += '<select name="' + e + '[option]" class="selectpicker cart-item-option" data-width="100%" data-style="btn-default">', $.each(this.options, function () {
                a += '<option value="' + this.id + '" ' + (this.id == t.option ? "selected" : "") + ">" + this.name + " " + (0 != parseFloat(this.price) ? "(+" + this.price + ")" : "") + "</option>"
            }), a += "</select>"), a += '</td>\n  <td><input style="width:60%; float: left;" size="1" type="text" name="' + e + '[qty]" class="form-control text-center input-qty cart-item-qty" value="' + this.qty + '">  <span style="float: left" class="input-group-btn"> <button type="submit" data-toggle="tooltip" title="" class="btn btn-primary" data-original-title="Update"><i class="fa fa-refresh"></i></button> <button type="button" data-toggle="tooltip" title="" class="btn btn-danger remove-item"  data-rowid="' + this.rowid + '" data-original-title="Remove"><i class="fa fa-times-circle"></i> </button> </span> </td>\n            <td class="text-right">'  + this.price + '</td>\n            <td class="text-right">' + currency_default + this.subtotal + "</td>\n            ", e++, $('<tr id="' + this.rowid + '">' + a + "</tr>").appendTo("#cart-table tbody")
        }), $("#cart-totals").empty();
        var a = "<tr><td>" + lang.total_w_o_tax + '</td><td class="text-right">' + t.subtotal + "</td></tr>";


        a += "<tr><td>" + lang.product_tax + '</td><td class="text-right">' + t.total_item_tax + "</td></tr>", a += "<tr><td>" + lang.total + '</td><td class="text-right cart_total">' + t.total + "</td></tr>", site.settings.tax2 !== !1 && (a += "<tr class='hide'><td>" + lang.order_tax + '</td><td class="text-right">' + t.order_tax + "</td></tr>"), a += "<tr><td>" + lang.shipping + ' *</td><td class="text-right">' + t.shipping + "</td></tr>", a += '';
        if (gift_cart > 0) {
            a += '<tr class=""><td>Gift Card</td>', a += '<td class="text-right">' + gift_cart + "</td></tr>";

            var grandtotal = t.grand_total;
            grandtotal = grandtotal.replace(/,/g, "");
            var newtotal = parseInt(grandtotal) - parseInt(gift_cart);
            if (newtotal < 0) {
                newtotal = 0.00;
            }


            a += '<tr class=""><td>' + lang.grand_total + '</td>', a += '<td class="text-right cart_grand_total">' + (newtotal) + "</td></tr>";
        } else {
            a += '<tr class="old_grand_total"><td>' + lang.grand_total + '</td>', a += '<td class="text-right cart_grand_total">' + t.grand_total + "</td></tr>";
        }
        $("<tbody>" + a + "</tbody>").appendTo("#cart-totals"), $("#total-items").text(t.total_items + "(" + t.total_unique_items + ")"), $(".cart-item-option").selectpicker("refresh"), $(".cart-empty-msg").hide(), $(".cart-contents").show()
    } else $("#total-items").text(t.total_items), $(".cart-contents").hide(), $(".cart-empty-msg").show()
}
function update_cart_gift(gcv,cn) {
    var gift_html = '';
    if (gcv > 0) {
        gift_html += '<tr class="cart-subtotal"><th>Gift Card</th><td><span class="amount">' + gcv + "</span></td></tr>";
        $(gift_html).insertBefore(".old_grand_total");
        var grandtotal = $('.cart_grand_total span.amount').text();
        grandtotal = grandtotal.replace(/,/g, "");
        var newtotal = parseInt(grandtotal) - parseInt(gcv);
        if (newtotal < 0) {
            newtotal = 0.00;
        }
        $('.cart_grand_total').text(newtotal);

        $.ajax({
            url: site.base_url + "admin/sales/saveGiftToCart?gcv=" + gcv+'&cn='+cn,
            type: "GET",
            success: function (t) {
                return true;
            }
        })


    }
}


function formatMoney(t, e) {
    if (e || (e = site.settings.symbol), 1 == site.settings.sac) return (1 == site.settings.display_symbol ? e : "") + "" + formatSA(parseFloat(t).toFixed(site.settings.decimals)) + (2 == site.settings.display_symbol ? e : "");
    var a = accounting.formatMoney(t, e, site.settings.decimals, 0 == site.settings.thousands_sep ? " " : site.settings.thousands_sep, site.settings.decimals_sep, "%s%v");
    return (1 == site.settings.display_symbol ? e : "") + a + (2 == site.settings.display_symbol ? e : "")
}

function formatSA(t) {
    t = t.toString();
    var e = "";
    t.indexOf(".") > 0 && (e = t.substring(t.indexOf("."), t.length)), t = Math.floor(t), t = t.toString();
    var a = t.substring(t.length - 3),
        s = t.substring(0, t.length - 3);
    "" != s && (a = "," + a);
    var i = s.replace(/\B(?=(\d{2})+(?!\d))/g, ",") + a + e;
    return i
}

function sa_alert(t, e, a, s) {
    a = a || "success", s = s || !1, swal({
        title: t,
        html: e,
        type: a,
        timer: s ? 6e4 : 2e3,
        confirmButtonText: "Okay"
    }).catch(swal.noop)
}

function saa_alert(t, e, a, s) {
    a = a || "delete", e = e || "This action can't be reverted back.", s = s || {}, s._method = a, s[site.csrf_token] = site.csrf_token_value, swal({
        title: "Are you sure?",
        html: e,
        type: "question",
        showCancelButton: !0,
        allowOutsideClick: !1,
        showLoaderOnConfirm: !0,
        preConfirm: function () {
            return new Promise(function () {
                $.ajax({
                    url: t,
                    type: "POST",
                    data: s,
                    success: function (t) {
                        return t.redirect ? (window.location.href = t.redirect, !1) : (t.cart && (cart = t.cart, update_mini_cart(cart), update_cart(cart)), void sa_alert(t.status, t.message))
                    },
                    error: function () {
                        sa_alert("Error!", "Ajax call failed, please try again or contact site owner.", "error", !0)
                    }
                })
            })
        }
    }).catch(swal.noop)
}

function prompt(t, e, a) {
    t = t || "Reset Password", e = e || "Please type your email address", a = a || {}, a[site.csrf_token] = site.csrf_token_value, swal({
        title: t,
        html: e,
        input: "email",
        showCancelButton: !0,
        allowOutsideClick: !1,
        showLoaderOnConfirm: !0,
        confirmButtonText: lang.submit,
        preConfirm: function (t) {
            return a.email = t, new Promise(function (t, e) {
                $.ajax({
                    url: site.base_url + "forgot_password",
                    type: "POST",
                    data: a,
                    success: function (a) {
                        a.status ? t(a) : e(a)
                    },
                    error: function () {
                        sa_alert("Error!", "Ajax call failed, please try again or contact site owner.", "error", !0)
                    }
                })
            })
        }
    }).then(function (t) {
        sa_alert(t.status, t.message)
    })
}

function add_address(t) {
    $(".preloader_pro").show();
    var country_list;
    var zone_list = "<option value=''>Select State</option>";
    $.ajax({
        url: site.base_url+'main/country_list',
        type: 'POST',
        async:false,
        beforeSend: function () {
            $('.preloader_pro').show();
        },
        data: {country: 'country_list'},
        success: function(data) {
            if (data) {
                country_list = data;
            }

        },
        error: function () {
            sa_alert('Error!', 'Ajax call failed, please try again or contact site owner.', 'error', true);
        }
    });
    var resp_postal ='';
    var resp_postal_message ='';
    function ajaxPostalCodeValidation() {
        // $(document).on('change', '#address-postal-code', function() {
        $(".preloader_pro").show();
        var postal_code_val = $("#address-postal-code").val();
        var address_city_val = $("#address-city").val();
        var address_country_iso_code_val = $('#address-country option:selected').attr('iso_code');
        $.ajax({
            url: site.base_url+'main/postal_code_validation',
            type: 'POST',
            async:false,
            beforeSend: function () {
                $('.preloader_pro').show();
            },
            data: {postal_code_val: postal_code_val, address_city_val: address_city_val, address_country_iso_code_val: address_country_iso_code_val },
            success: function(data) {
                $(".preloader_pro").hide();
                if (data) {
                    if(data == 1){
                        resp_postal =1;
                    }else{
                        // alert( data);
                        resp_postal_message = data;
                    }
                }
            },
            error: function () {
                sa_alert('Error!', 'Ajax call failed, please try again or contact site owner.', 'error', true);
            }
        });

        //});

    }

    $(document).on('change', '#address-country', function() {
        var selected_country = $("#address-country").val();

        $.ajax({
            url: site.base_url+'main/region_list',
            type: 'POST',
            async:false,
            data: {country: selected_country},
            success: function(data) {
                if (data) {
                    $("#address-state").html(data);
                }
            },
            error: function () {
                sa_alert('Error!', 'Ajax call failed, please try again or contact site owner.', 'error', true);
            }
        });
    });
    console.log(t);


    t = t || {}, swal({
        title: t.id ? lang.update_address : lang.add_address,
        html: '<span class="text-bold padding-bottom-md">' + lang.fill_form + '</span><hr class="swal2-spacer padding-bottom-xs" style="display: block;"><form action="' + site.shop_url + 'address" id="address-form" class="padding-bottom-md"><input type="hidden" name="' + site.csrf_token + '" value="' + site.csrf_token_value + '"><div class="row"><div class="form-group col-sm-12"><label class="pull-left" for="Address1">Address 1 *</label><input name="line1" id="address-line-1" value="' + (t.line1 ? t.line1 : "") + '" class="form-control" placeholder="Address 1"></div></div><div class="row"><div class="form-group col-sm-12"><label class="pull-left" for="Address2">Address 2 *</label><input name="line2" id="address-line-2" value="' + (t.line2 ? t.line2 : "") + '" class="form-control" placeholder="Address 2"></div></div><div class="row"><div class="form-group col-sm-6"><label class="pull-left" for="country">Country *</label><select  name="country" id="address-country" class="form-control" >'+country_list+'</select></div><div class="form-group col-sm-6"><label class="pull-left" for="state">State *</label><select id="address-state" name="state" class="form-control"> '+zone_list+' </select></div> <div class="form-group col-sm-6"><label class="pull-left" for="city">City *</label><input name="city" value="' + (t.city ? t.city : "") + '" id="address-city" class="form-control" placeholder="City"></div><div class="form-group col-sm-6"><label class="pull-left" for="zipcode">ZipCode *</label><input name="postal_code" value="' + (t.postal_code ? t.postal_code : "") + '" id="address-postal-code" class="form-control" placeholder="Postal Code"></div><div class="form-group col-sm-12 margin-bottom-no"><label class="pull-left" for="phonenumber">Phone Number *</label><input name="phone" value="' + (t.phone ? t.phone : "") + '" id="address-phone" class="form-control" placeholder="Phone"> <input name="old_address_id" value="" id="old_address_id" type="hidden"></div></form></div>',
        showCancelButton: !0,
        allowOutsideClick: !1,
        confirmButtonText: lang.submit,
        preConfirm: function () {
            $(".preloader_pro").show();
            return new Promise(function (t, e) {
                ajaxPostalCodeValidation();
                $(".preloader_pro").hide();
                $("#address-line-1").val() || e("Address 1 is required"), $("#address-line-2").val() || e("Address 2 is required"), $("#address-city").val() || e("City is required"), $("#address-state").val() || e("State is required"), $("#address-postal-code").val() || e("Postal code is required"), $("#address-country").val() || e("Country is required"), $("#address-phone").val() || e("Phone is required"), resp_postal || e(resp_postal_message), t()
            })
        },
        onOpen: function () {
            $(".preloader_pro").hide();
            var address_id_old = $('input[name=address]:checked').attr('address_id');
            if(address_id_old) {
                $('#old_address_id').val(address_id_old);
            }else{
                $('#old_address_id').val(0);
            }
            $("#address-line-1").val(t.line1 ? t.line1 : "").focus()
            $("#address-country").val(t.country ? t.country : "")
            if(t.country){
                var selected_country = t.country;

                $.ajax({
                    url: site.base_url+'main/region_list',
                    type: 'POST',
                    async:false,
                    beforeSend: function () {
                        $('.preloader_pro').show();
                    },
                    data: {country: selected_country},
                    success: function(data) {
                        if (data) {
                            $("#address-state").html(data);
                            $("#address-state").val(t.state ? t.state : "")
                        }
                        $('.preloader_pro').hide();
                    },
                    error: function () {
                        sa_alert('Error!', 'Ajax call failed, please try again or contact site owner.', 'error', true);
                    }
                });
            }



        }
    }).then(function (e) {
        var a = $("#address-form");
        $('.preloader_pro').show();
        $.ajax({
            url: a.attr("action") + (t.id ? "/" + t.id : ""),
            type: "POST",
            data: a.serialize(),
            success: function (t) {
                $('.preloader_pro').hide();
                return t.redirect ? (window.location.href = t.redirect, !1) : void sa_alert(t.status, t.message, t.level)
            },
            error: function () {
                sa_alert("Error!", "Ajax call failed, please try again or contact site owner.", "error", !0)
            }
        })
    }).catch(swal.noop)
}

function email_form() {
    swal({
        title: lang.send_email_title,
        html: '<div><span class="text-bold padding-bottom-md">' + lang.fill_form + '</span><hr class="swal2-spacer padding-bottom-xs" style="display: block;"><form action="' + site.shop_url + 'send_message" id="message-form" class="padding-bottom-md"><input type="hidden" name="' + site.csrf_token + '" value="' + site.csrf_token_value + '"><div class="row"><div class="form-group col-sm-12"><input type="text" name="name" id="form-name" value="" class="form-control" placeholder="Full Name"></div></div><div class="row"><div class="form-group col-sm-12"><input type="email" name="email" id="form-email" value="" class="form-control" placeholder="Email"></div></div><div class="row"><div class="form-group col-sm-12"><input type="text" name="subject" id="form-subject" value="" class="form-control" placeholder="Subject"></div></div><div class="row"><div class="col-sm-12"><textarea name="message" id="form-message" class="form-control" placeholder="Message" style="height:100px;"></textarea></div></div></form></div>',
        showCancelButton: !0,
        allowOutsideClick: !1,
        confirmButtonText: lang.submit,
        preConfirm: function () {
            return new Promise(function (t, e) {
                $("#form-name").val() || e("Name is required"), $("#form-email").val() || e("Email is required"), $("#form-subject").val() || e("Subject is required"), $("#form-message").val() || e("Message is required"), validateEmail($("#form-email").val()) || e("Email is invalid"), t()
            })
        },
        onOpen: function () {
            $("#form-name").focus()
        }
    }).then(function (t) {
        var e = $("#message-form");
        $.ajax({
            url: e.attr("action"),
            type: "POST",
            data: e.serialize(),
            success: function (t) {
                return t.redirect ? (window.location.href = t.redirect, !1) : void sa_alert(t.status, t.message, t.level, !0)
            },
            error: function () {
                sa_alert("Error!", "Ajax call failed, please try again or contact site owner.", "error", !0)
            }
        })
    }).catch(swal.noop)
}

function validateEmail(t) {
    var e = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return e.test(t)
}
$(function () {
    if ($(document).on("click", ".add-to-cart", function (t) {
            t.preventDefault();
            var e = $(this).attr("data-id"),
                a = $(".shopping-cart:visible"),
                s = $(this).parents(".product-bottom").find(".quantity-input"),
                i = $(this).parents(".product").find("img").eq(0);
            if (i) {
                var o = i.clone().offset({
                    top: i.offset().top,
                    left: i.offset().left
                }).css({
                    opacity: "0.5",
                    position: "absolute",
                    height: "150px",
                    width: "150px",
                    "z-index": "1000"
                }).appendTo($("body")).animate({
                    /* top: a.offset().top + 10,*/
                    /* left: a.offset().left + 10,*/
                    width: "50px",
                    height: "50px"
                }, 400);
                o.animate({
                    width: 0,
                    height: 0
                }, function () {
                    $(this).detach()
                })
            }
            $.ajax({
                url: site.site_url + "cart/add/" + e,
                type: "GET",
                dataType: "json",
                data: {
                    qty: s.val()
                }
            }).done(function (t) {
                a = t, update_mini_cart(t)

                var is_cat_on_prom = JSON.stringify(t.cat_on_promotion);
                is_cat_on_prom = is_cat_on_prom.replace('"', '');
                is_cat_on_prom = is_cat_on_prom.replace('"', '');
                if (is_cat_on_prom != '' && is_cat_on_prom != 'null') {
                    swal("",
                        is_cat_on_prom,
                        "success");
                }

            })
        }), $(document).on("click", ".btn-minus", function (t) {
            var e = $(this).parent().find("input");
            parseInt(e.val()) > 1 && e.val(parseInt(e.val()) - 1)
        }), $(document).on("click", ".btn-plus", function (t) {
            var e = $(this).parent().find("input");
            e.val(parseInt(e.val()) + 1)
        }), $(document).on("click", ".add-to-wishlist", function (t) {
            t.preventDefault();
            var e = $(this).attr("data-id");
            $.ajax({
                url: site.site_url + "cart/add_wishlist/" + e,
                type: "GET",
                dataType: "json"
            }).done(function (t) {
                if (t.total) $("#total-wishlist").text(t.total);
                else if (t.redirect) return window.location.href = t.redirect, !1;
                sa_alert(t.status, t.message, t.level)
            })
        }), $(document).on("click", ".remove-wishlist", function (t) {
            t.preventDefault();
            var e = $(this),
                a = $(this).attr("data-id");
            $.ajax({
                url: site.site_url + "cart/remove_wishlist/" + a,
                type: "GET",
                dataType: "json"
            }).done(function (t) {
                if (0 == t.total) setTimeout(function () {
                    location.reload()
                }, 1e3);
                else if (t.redirect) return window.location.href = t.redirect, !1;
                t.status != lang.error && e.closest("tr").remove(), $("#total-wishlist").text(t.total), sa_alert(t.status, t.message, t.level)
            })
        }), update_mini_cart(cart), $("#dropdown-cart").click(function () {
            $(this).next(".dropdown-menu").animate({
                scrollTop: $(this).next(".dropdown-menu").height() + 400
            }, 100)
        }), $("#add-address").click(function (t) {
            t.preventDefault(), add_address()
        }), $(".edit-address").click(function (t) {
            t.preventDefault();
            var e = $(this).attr("data-id");
            addresses && $.each(addresses, function () {
                this.id == e && add_address(this)
            })
        }), $(document).on("click", ".forgot-password", function (t) {
            t.preventDefault(), prompt(lang.reset_pw, lang.type_email)
        }), $("ul.nav li.dropdown").hover(function () {
            get_width() >= 767 && $(this).addClass("open")
        }, function () {
            get_width() >= 767 && $(this).removeClass("open")
        }), $("ul.dropdown-menu [data-toggle=dropdown]").on("click", function (t) {
            t.preventDefault(), t.stopPropagation(), $(this).parent().siblings().removeClass("open"), $(this).parent().toggleClass("open")
        }), $(".tip").tooltip({
            container: "body"
        }), $(".validate").formValidation({
            framework: "bootstrap",
            message: "This value is required or not valid"
        }), $(window).scroll(function () {
            $(this).scrollTop() > 70 ? $(".back-to-top").fadeIn() : $(".back-to-top").fadeOut()
        }), sticky_footer(), $(window).resize(sticky_footer), sticky_con(), $(window).resize(sticky_con), $(".theme-color").click(function (t) {
            return store("shop_color", $(this).attr("data-color")), $("#wrapper").removeAttr("class").addClass($(this).attr("data-color")), !1
        }), (shop_color = get("shop_color")) && $("#wrapper").removeAttr("class").addClass(shop_color), "products" == v && ($(window).resize(function () {
            gen_html(products)
        }),
            $(".sorting").click(function (t) {
                return store("sorting", $(this).attr("id")), $(".sorting").removeClass("active"), $(this).addClass("active"), searchProducts(), !1
            }),
            $("#grid-sort").change(function (t) {
                return store("sorting", $(this).val()), searchProducts(), !1
            }),
            $(".two-col").click(function () {
                return store("shop_grid", ".two-col"), $(this).addClass("active"), $(".three-col").removeClass("active"), gen_html(products), !1
            }), $(".three-col").click(function () {
            return store("shop_grid", ".three-col"), $(this).addClass("active"), $(".two-col").removeClass("active"), gen_html_list(products), !1
        }), $("#product-search-form").submit(function (t) {
            return t.preventDefault(), filters.query = $("#product-search").val(), searchProducts(), !1
        }), $("#product-search").blur(function (t) {
            return t.preventDefault(), filters.query = $(this).val(), searchProducts(), !1
        }), $(".reset_filters_brand").click(function (t) {
            filters.brand = null, searchProducts(), $(this).closest("li").remove()
        }), $(".reset_filters_category").click(function (t) {
            filters.category = null, searchProducts(), $(this).closest("li").remove()
        }), $("#min-price, #max-price, #in-stock").change(function () {
            searchProducts()
        }), $(document).on("click", "#pagination a", function (t) {
            t.preventDefault();
            var e = $(this).attr("href"),
                a = e.split("page=");
            if (a[1]) {
                var s = a[1].split("&");
                filters.page = s[0]
            } else filters.page = 1;
            return searchProducts(), !1
        }), (shop_grid = get("shop_grid")) && $(shop_grid).click(), (sorting = get("sorting")) ? ($(".sorting").removeClass("active"), $("#" + sorting).addClass("active")) : store("sorting", "name-asc"), filters.query && $("#product-search").val(filters.query), searchProducts()), $(".product").each(function (t, e) {
            $(e).find(".details").hover(function () {
                $(this).parent().css("z-index", "20"), $(this).addClass("animate")
            }, function () {
                $(this).removeClass("animate"), $(this).parent().css("z-index", "1")
            })
        }), ("cart_ajax" == m && "index" == v) || ("cart_ajax" == m && "checkout" == v)) update_mini_cart(cart), update_cart(cart), $(document).on("click", ".remove-item", function (t) {
        t.preventDefault();
        var e = {};
        e.rowid = $(this).attr("data-rowid");
        var a = site.site_url + "cart/remove";
        saa_alert(a, !1, "post", e)
    }), $(document).on("change", ".cart-item-option, .cart-item-qty", function (t) {
        t.preventDefault();
        var e = $(this).closest("tr"),
            a = e.attr("id"),
            s = site.site_url + "cart/update",
            i = {};
        i[site.csrf_token] = site.csrf_token_value, i.rowid = a, i.qty = e.find(".cart-item-qty").val(), i.option = e.find(".cart-item-option").children("option:selected").val(), update_cart_item(s, i)

    }),
        $("#cart-table .cart-item-option").click(function (t) {
            //btn-group bootstrap-select cart-item-option
            $("#cart-table .bootstrap-select.cart-item-option").toggleClass(" open");
            setTimeout(function() {
                location.reload();
            }, 4000);
        }),
        $(document).on('click', 'input[name=delivery_by]:checked', function(t) {
            console.log($(this).attr('data_shipment'));
            var e = $(this).attr('data_shipment'),
                s = site.site_url + "cart/update_shipment",
                i = {};
            i[site.csrf_token] = site.csrf_token_value, i.ship = e, update_cart_shipment(s, i)
        }),
        $("#empty-cart").click(function (t) {
            t.preventDefault();
            var e = $(this).attr("href");
            saa_alert(e)
        });
    else if ("shop" == m && "product" == v) {
        var t = $("#lightbox");
        $('[data-target="#lightbox"]').on("click", function (e) {
            var a = $(this).find("img"),
                s = a.attr("src"),
                i = a.attr("alt"),
                o = {
                    maxWidth: $(window).width() - 10,
                    maxHeight: $(window).height() - 10
                };
            t.find(".close").addClass("hidden"), t.find("img").attr("src", s), t.find("img").attr("alt", i), t.find("img").css(o), t.find(".modal-content").removeClass("swal2-hide").addClass("swal2-show")
        }), t.on("shown.bs.modal", function (e) {
            var a = t.find("img");
            t.find(".modal-dialog").css({
                width: a.width()
            }), t.find(".close").removeClass("hidden"), t.addClass("fade"), $(".modal-backdrop").addClass("fade")
        }), t.on("hide.bs.modal", function () {
            t.find(".modal-content").removeClass("swal2-show").addClass("swal2-hide")
        }), t.on("hidden.bs.modal", function () {
            t.removeClass("fade"), $(".modal-backdrop").removeClass("fade")
        })
    }
    var e = document.location.toString();
    e.match("#") && $('.nav-tabs a[href="#' + e.split("#")[1] + '"]').tab("show"), $(document).on("click", ".show-tab", function (t) {
        t.preventDefault(), $('.nav-tabs a[href="#' + $(this).attr("href") + '"]').tab("show")
    }), $(".history-tabs a").on("shown.bs.tab", function (t) {
        history.pushState ? history.pushState(null, null, t.target.hash) : window.location.hash = t.target.hash
    }), $(".email-modal").click(function (t) {
        t.preventDefault(), email_form()
    })
});
var inputs = document.querySelectorAll(".file"),
    submit_btn = document.querySelector("#submit-container");
submit_btn && (submit_btn.style.display = "none"), Array.prototype.forEach.call(inputs, function (t) {
    var e = t.nextElementSibling,
        a = e.innerHTML;
    t.addEventListener("change", function (t) {
        var s = "";
        this.files && this.files.length > 1 ? (s = (this.getAttribute("data-multiple-caption") || "").replace("{count}", this.files.length), submit_btn && (submit_btn.style.display = "inline-block")) : (s = t.target.value.split("\\").pop(), submit_btn && (submit_btn.style.display = "none")), s ? (e.querySelector("span").innerHTML = s, submit_btn && (submit_btn.style.display = "inline-block")) : (e.innerHTML = a, submit_btn && (submit_btn.style.display = "none"))
    })
});

$("#same_as_billing").change(function(t) {
    var hidden_same_as_billing_val = $("#hidden_same_as_billing").val();
    if(hidden_same_as_billing_val == 0){
        $("#hidden_same_as_billing").val(1);
    }else{
        $("#hidden_same_as_billing").val(0);
    }

    $(this).is(":checked") && ($("#shipping_line1").val($("#billing_line1").val()).change(), $("#shipping_line2").val($("#billing_line2").val()).change(), $("#shipping_city").val($("#billing_city").val()).change(), $("#shipping_state").val($("#billing_state").val()).change(), $("#shipping_postal_code").val($("#billing_postal_code").val()).change(), $("#shipping_country").val($("#billing_country").val()).change(), $("#shipping_phone").val($("#phone").val()).change(), $("#form-guest-checkout").data("formValidation").resetForm())
});


"function"!=typeof Object.create&&(Object.create=function(o){function e(){}return e.prototype=o,new e}),function(d,o,e,i){var t={init:function(o,e){var i=this;i.elem=e,i.$elem=d(e),i.imageSrc=i.$elem.data("zoom-image")?i.$elem.data("zoom-image"):i.$elem.attr("src"),i.options=d.extend({},d.fn.elevateZoom.options,o),i.options.tint&&(i.options.lensColour="none",i.options.lensOpacity="1"),"inner"==i.options.zoomType&&(i.options.showLens=!1),i.$elem.parent().removeAttr("title").removeAttr("alt"),i.zoomImage=i.imageSrc,i.refresh(1),d("#"+i.options.gallery+" a").click(function(o){return i.options.galleryActiveClass&&(d("#"+i.options.gallery+" a").removeClass(i.options.galleryActiveClass),d(this).addClass(i.options.galleryActiveClass)),o.preventDefault(),d(this).data("zoom-image")?i.zoomImagePre=d(this).data("zoom-image"):i.zoomImagePre=d(this).data("image"),i.swaptheimage(d(this).data("image"),i.zoomImagePre),!1})},refresh:function(o){var e=this;setTimeout(function(){e.fetch(e.imageSrc)},o||e.options.refresh)},fetch:function(o){var e=this,i=new Image;i.onload=function(){e.largeWidth=i.width,e.largeHeight=i.height,e.startZoom(),e.currentImage=e.imageSrc,e.options.onZoomedImageLoaded(e.$elem)},i.src=o},startZoom:function(){var i=this;if(i.nzWidth=i.$elem.width(),i.nzHeight=i.$elem.height(),i.isWindowActive=!1,i.isLensActive=!1,i.isTintActive=!1,i.overWindow=!1,i.options.imageCrossfade&&(i.zoomWrap=i.$elem.wrap('<div style="height:'+i.nzHeight+"px;width:"+i.nzWidth+'px;" class="zoomWrapper" />'),i.$elem.css("position","absolute")),i.zoomLock=1,i.scrollingLock=!1,i.changeBgSize=!1,i.currentZoomLevel=i.options.zoomLevel,i.nzOffset=i.$elem.offset(),i.widthRatio=i.largeWidth/i.currentZoomLevel/i.nzWidth,i.heightRatio=i.largeHeight/i.currentZoomLevel/i.nzHeight,"window"==i.options.zoomType&&(i.zoomWindowStyle="overflow: hidden;background-position: 0px 0px;text-align:center;background-color: "+String(i.options.zoomWindowBgColour)+";width: "+String(i.options.zoomWindowWidth)+"px;height: "+String(i.options.zoomWindowHeight)+"px;float: left;background-size: "+i.largeWidth/i.currentZoomLevel+"px "+i.largeHeight/i.currentZoomLevel+"px;display: none;z-index:100;border: "+String(i.options.borderSize)+"px solid "+i.options.borderColour+";background-repeat: no-repeat;position: absolute;"),"inner"==i.options.zoomType){var o=i.$elem.css("border-left-width");i.zoomWindowStyle="overflow: hidden;margin-left: "+String(o)+";margin-top: "+String(o)+";background-position: 0px 0px;width: "+String(i.nzWidth)+"px;height: "+String(i.nzHeight)+"px;px;float: left;display: none;cursor:"+i.options.cursor+";px solid "+i.options.borderColour+";background-repeat: no-repeat;position: absolute;"}"window"==i.options.zoomType&&(i.nzHeight<i.options.zoomWindowWidth/i.widthRatio?lensHeight=i.nzHeight:lensHeight=String(i.options.zoomWindowHeight/i.heightRatio),i.largeWidth<i.options.zoomWindowWidth?lensWidth=i.nzWidth:lensWidth=i.options.zoomWindowWidth/i.widthRatio,i.lensStyle="background-position: 0px 0px;width: "+String(i.options.zoomWindowWidth/i.widthRatio)+"px;height: "+String(i.options.zoomWindowHeight/i.heightRatio)+"px;float: right;display: none;overflow: hidden;z-index: 999;-webkit-transform: translateZ(0);opacity:"+i.options.lensOpacity+";filter: alpha(opacity = "+100*i.options.lensOpacity+"); zoom:1;width:"+lensWidth+"px;height:"+lensHeight+"px;background-color:"+i.options.lensColour+";cursor:"+i.options.cursor+";border: "+i.options.lensBorderSize+"px solid "+i.options.lensBorderColour+";background-repeat: no-repeat;position: absolute;"),i.tintStyle="display: block;position: absolute;background-color: "+i.options.tintColour+";filter:alpha(opacity=0);opacity: 0;width: "+i.nzWidth+"px;height: "+i.nzHeight+"px;",i.lensRound="","lens"==i.options.zoomType&&(i.lensStyle="background-position: 0px 0px;float: left;display: none;border: "+String(i.options.borderSize)+"px solid "+i.options.borderColour+";width:"+String(i.options.lensSize)+"px;height:"+String(i.options.lensSize)+"px;background-repeat: no-repeat;position: absolute;"),"round"==i.options.lensShape&&(i.lensRound="border-top-left-radius: "+String(i.options.lensSize/2+i.options.borderSize)+"px;border-top-right-radius: "+String(i.options.lensSize/2+i.options.borderSize)+"px;border-bottom-left-radius: "+String(i.options.lensSize/2+i.options.borderSize)+"px;border-bottom-right-radius: "+String(i.options.lensSize/2+i.options.borderSize)+"px;"),i.zoomContainer=d('<div class="zoomContainer" style="-webkit-transform: translateZ(0);position:absolute;left:'+i.nzOffset.left+"px;top:"+i.nzOffset.top+"px;height:"+i.nzHeight+"px;width:"+i.nzWidth+'px;"></div>'),d("body").append(i.zoomContainer),i.options.containLensZoom&&"lens"==i.options.zoomType&&i.zoomContainer.css("overflow","hidden"),"inner"!=i.options.zoomType&&(i.zoomLens=d("<div class='zoomLens' style='"+i.lensStyle+i.lensRound+"'>&nbsp;</div>").appendTo(i.zoomContainer).click(function(){i.$elem.trigger("click")}),i.options.tint&&(i.tintContainer=d("<div/>").addClass("tintContainer"),i.zoomTint=d("<div class='zoomTint' style='"+i.tintStyle+"'></div>"),i.zoomLens.wrap(i.tintContainer),i.zoomTintcss=i.zoomLens.after(i.zoomTint),i.zoomTintImage=d('<img style="position: absolute; left: 0px; top: 0px; max-width: none; width: '+i.nzWidth+"px; height: "+i.nzHeight+'px;" src="'+i.imageSrc+'">').appendTo(i.zoomLens).click(function(){i.$elem.trigger("click")}))),isNaN(i.options.zoomWindowPosition)?i.zoomWindow=d("<div style='z-index:999;left:"+i.windowOffsetLeft+"px;top:"+i.windowOffsetTop+"px;"+i.zoomWindowStyle+"' class='zoomWindow'>&nbsp;</div>").appendTo("body").click(function(){i.$elem.trigger("click")}):i.zoomWindow=d("<div style='z-index:999;left:"+i.windowOffsetLeft+"px;top:"+i.windowOffsetTop+"px;"+i.zoomWindowStyle+"' class='zoomWindow'>&nbsp;</div>").appendTo(i.zoomContainer).click(function(){i.$elem.trigger("click")}),i.zoomWindowContainer=d("<div/>").addClass("zoomWindowContainer").css("width",i.options.zoomWindowWidth),i.zoomWindow.wrap(i.zoomWindowContainer),"lens"==i.options.zoomType&&i.zoomLens.css({backgroundImage:"url('"+i.imageSrc+"')"}),"window"==i.options.zoomType&&i.zoomWindow.css({backgroundImage:"url('"+i.imageSrc+"')"}),"inner"==i.options.zoomType&&i.zoomWindow.css({backgroundImage:"url('"+i.imageSrc+"')"}),i.$elem.bind("touchmove",function(o){o.preventDefault();var e=o.originalEvent.touches[0]||o.originalEvent.changedTouches[0];i.setPosition(e)}),i.zoomContainer.bind("touchmove",function(o){"inner"==i.options.zoomType&&i.showHideWindow("show"),o.preventDefault();var e=o.originalEvent.touches[0]||o.originalEvent.changedTouches[0];i.setPosition(e)}),i.zoomContainer.bind("touchend",function(o){i.showHideWindow("hide"),i.options.showLens&&i.showHideLens("hide"),i.options.tint&&"inner"!=i.options.zoomType&&i.showHideTint("hide")}),i.$elem.bind("touchend",function(o){i.showHideWindow("hide"),i.options.showLens&&i.showHideLens("hide"),i.options.tint&&"inner"!=i.options.zoomType&&i.showHideTint("hide")}),i.options.showLens&&(i.zoomLens.bind("touchmove",function(o){o.preventDefault();var e=o.originalEvent.touches[0]||o.originalEvent.changedTouches[0];i.setPosition(e)}),i.zoomLens.bind("touchend",function(o){i.showHideWindow("hide"),i.options.showLens&&i.showHideLens("hide"),i.options.tint&&"inner"!=i.options.zoomType&&i.showHideTint("hide")})),i.$elem.bind("mousemove",function(o){0==i.overWindow&&i.setElements("show"),i.lastX===o.clientX&&i.lastY===o.clientY||(i.setPosition(o),i.currentLoc=o),i.lastX=o.clientX,i.lastY=o.clientY}),i.zoomContainer.bind("mousemove",function(o){0==i.overWindow&&i.setElements("show"),i.lastX===o.clientX&&i.lastY===o.clientY||(i.setPosition(o),i.currentLoc=o),i.lastX=o.clientX,i.lastY=o.clientY}),"inner"!=i.options.zoomType&&i.zoomLens.bind("mousemove",function(o){i.lastX===o.clientX&&i.lastY===o.clientY||(i.setPosition(o),i.currentLoc=o),i.lastX=o.clientX,i.lastY=o.clientY}),i.options.tint&&"inner"!=i.options.zoomType&&i.zoomTint.bind("mousemove",function(o){i.lastX===o.clientX&&i.lastY===o.clientY||(i.setPosition(o),i.currentLoc=o),i.lastX=o.clientX,i.lastY=o.clientY}),"inner"==i.options.zoomType&&i.zoomWindow.bind("mousemove",function(o){i.lastX===o.clientX&&i.lastY===o.clientY||(i.setPosition(o),i.currentLoc=o),i.lastX=o.clientX,i.lastY=o.clientY}),i.zoomContainer.add(i.$elem).mouseenter(function(){0==i.overWindow&&i.setElements("show")}).mouseleave(function(){i.scrollLock||(i.setElements("hide"),i.options.onDestroy(i.$elem))}),"inner"!=i.options.zoomType&&i.zoomWindow.mouseenter(function(){i.overWindow=!0,i.setElements("hide")}).mouseleave(function(){i.overWindow=!1}),i.options.zoomLevel,i.options.minZoomLevel?i.minZoomLevel=i.options.minZoomLevel:i.minZoomLevel=2*i.options.scrollZoomIncrement,i.options.scrollZoom&&i.zoomContainer.add(i.$elem).bind("mousewheel DOMMouseScroll MozMousePixelScroll",function(o){i.scrollLock=!0,clearTimeout(d.data(this,"timer")),d.data(this,"timer",setTimeout(function(){i.scrollLock=!1},250));var e=o.originalEvent.wheelDelta||-1*o.originalEvent.detail;return o.stopImmediatePropagation(),o.stopPropagation(),o.preventDefault(),0<e/120?i.currentZoomLevel>=i.minZoomLevel&&i.changeZoomLevel(i.currentZoomLevel-i.options.scrollZoomIncrement):i.options.maxZoomLevel?i.currentZoomLevel<=i.options.maxZoomLevel&&i.changeZoomLevel(parseFloat(i.currentZoomLevel)+i.options.scrollZoomIncrement):i.changeZoomLevel(parseFloat(i.currentZoomLevel)+i.options.scrollZoomIncrement),!1})},setElements:function(o){var e=this;if(!e.options.zoomEnabled)return!1;"show"==o&&e.isWindowSet&&("inner"==e.options.zoomType&&e.showHideWindow("show"),"window"==e.options.zoomType&&e.showHideWindow("show"),e.options.showLens&&e.showHideLens("show"),e.options.tint&&"inner"!=e.options.zoomType&&e.showHideTint("show")),"hide"==o&&("window"==e.options.zoomType&&e.showHideWindow("hide"),e.options.tint||e.showHideWindow("hide"),e.options.showLens&&e.showHideLens("hide"),e.options.tint&&e.showHideTint("hide"))},setPosition:function(o){var e=this;if(!e.options.zoomEnabled)return!1;e.nzHeight=e.$elem.height(),e.nzWidth=e.$elem.width(),e.nzOffset=e.$elem.offset(),e.options.tint&&"inner"!=e.options.zoomType&&(e.zoomTint.css({top:0}),e.zoomTint.css({left:0})),e.options.responsive&&!e.options.scrollZoom&&e.options.showLens&&(e.nzHeight<e.options.zoomWindowWidth/e.widthRatio?lensHeight=e.nzHeight:lensHeight=String(e.options.zoomWindowHeight/e.heightRatio),e.largeWidth<e.options.zoomWindowWidth?lensWidth=e.nzWidth:lensWidth=e.options.zoomWindowWidth/e.widthRatio,e.widthRatio=e.largeWidth/e.nzWidth,e.heightRatio=e.largeHeight/e.nzHeight,"lens"!=e.options.zoomType&&(e.nzHeight<e.options.zoomWindowWidth/e.widthRatio?lensHeight=e.nzHeight:lensHeight=String(e.options.zoomWindowHeight/e.heightRatio),e.nzWidth<e.options.zoomWindowHeight/e.heightRatio?lensWidth=e.nzWidth:lensWidth=String(e.options.zoomWindowWidth/e.widthRatio),e.zoomLens.css("width",lensWidth),e.zoomLens.css("height",lensHeight),e.options.tint&&(e.zoomTintImage.css("width",e.nzWidth),e.zoomTintImage.css("height",e.nzHeight))),"lens"==e.options.zoomType&&e.zoomLens.css({width:String(e.options.lensSize)+"px",height:String(e.options.lensSize)+"px"})),e.zoomContainer.css({top:e.nzOffset.top}),e.zoomContainer.css({left:e.nzOffset.left}),e.mouseLeft=parseInt(o.pageX-e.nzOffset.left),e.mouseTop=parseInt(o.pageY-e.nzOffset.top),"window"==e.options.zoomType&&(e.Etoppos=e.mouseTop<e.zoomLens.height()/2,e.Eboppos=e.mouseTop>e.nzHeight-e.zoomLens.height()/2-2*e.options.lensBorderSize,e.Eloppos=e.mouseLeft<0+e.zoomLens.width()/2,e.Eroppos=e.mouseLeft>e.nzWidth-e.zoomLens.width()/2-2*e.options.lensBorderSize),"inner"==e.options.zoomType&&(e.Etoppos=e.mouseTop<e.nzHeight/2/e.heightRatio,e.Eboppos=e.mouseTop>e.nzHeight-e.nzHeight/2/e.heightRatio,e.Eloppos=e.mouseLeft<0+e.nzWidth/2/e.widthRatio,e.Eroppos=e.mouseLeft>e.nzWidth-e.nzWidth/2/e.widthRatio-2*e.options.lensBorderSize),e.mouseLeft<0||e.mouseTop<0||e.mouseLeft>e.nzWidth||e.mouseTop>e.nzHeight?e.setElements("hide"):(e.options.showLens&&(e.lensLeftPos=String(Math.floor(e.mouseLeft-e.zoomLens.width()/2)),e.lensTopPos=String(Math.floor(e.mouseTop-e.zoomLens.height()/2))),e.Etoppos&&(e.lensTopPos=0),e.Eloppos&&(e.windowLeftPos=0,e.lensLeftPos=0,e.tintpos=0),"window"==e.options.zoomType&&(e.Eboppos&&(e.lensTopPos=Math.max(e.nzHeight-e.zoomLens.height()-2*e.options.lensBorderSize,0)),e.Eroppos&&(e.lensLeftPos=e.nzWidth-e.zoomLens.width()-2*e.options.lensBorderSize)),"inner"==e.options.zoomType&&(e.Eboppos&&(e.lensTopPos=Math.max(e.nzHeight-2*e.options.lensBorderSize,0)),e.Eroppos&&(e.lensLeftPos=e.nzWidth-e.nzWidth-2*e.options.lensBorderSize)),"lens"==e.options.zoomType&&(e.windowLeftPos=String(-1*((o.pageX-e.nzOffset.left)*e.widthRatio-e.zoomLens.width()/2)),e.windowTopPos=String(-1*((o.pageY-e.nzOffset.top)*e.heightRatio-e.zoomLens.height()/2)),e.zoomLens.css({backgroundPosition:e.windowLeftPos+"px "+e.windowTopPos+"px"}),e.changeBgSize&&(e.nzHeight>e.nzWidth?("lens"==e.options.zoomType&&e.zoomLens.css({"background-size":e.largeWidth/e.newvalueheight+"px "+e.largeHeight/e.newvalueheight+"px"}),e.zoomWindow.css({"background-size":e.largeWidth/e.newvalueheight+"px "+e.largeHeight/e.newvalueheight+"px"})):("lens"==e.options.zoomType&&e.zoomLens.css({"background-size":e.largeWidth/e.newvaluewidth+"px "+e.largeHeight/e.newvaluewidth+"px"}),e.zoomWindow.css({"background-size":e.largeWidth/e.newvaluewidth+"px "+e.largeHeight/e.newvaluewidth+"px"})),e.changeBgSize=!1),e.setWindowPostition(o)),e.options.tint&&"inner"!=e.options.zoomType&&e.setTintPosition(o),"window"==e.options.zoomType&&e.setWindowPostition(o),"inner"==e.options.zoomType&&e.setWindowPostition(o),e.options.showLens&&(e.fullwidth&&"lens"!=e.options.zoomType&&(e.lensLeftPos=0),e.zoomLens.css({left:e.lensLeftPos+"px",top:e.lensTopPos+"px"})))},showHideWindow:function(o){var e=this;"show"==o&&(e.isWindowActive||(e.options.zoomWindowFadeIn?e.zoomWindow.stop(!0,!0,!1).fadeIn(e.options.zoomWindowFadeIn):e.zoomWindow.show(),e.isWindowActive=!0)),"hide"==o&&e.isWindowActive&&(e.options.zoomWindowFadeOut?e.zoomWindow.stop(!0,!0).fadeOut(e.options.zoomWindowFadeOut,function(){e.loop&&(clearInterval(e.loop),e.loop=!1)}):e.zoomWindow.hide(),e.isWindowActive=!1)},showHideLens:function(o){var e=this;"show"==o&&(e.isLensActive||(e.options.lensFadeIn?e.zoomLens.stop(!0,!0,!1).fadeIn(e.options.lensFadeIn):e.zoomLens.show(),e.isLensActive=!0)),"hide"==o&&e.isLensActive&&(e.options.lensFadeOut?e.zoomLens.stop(!0,!0).fadeOut(e.options.lensFadeOut):e.zoomLens.hide(),e.isLensActive=!1)},showHideTint:function(o){var e=this;"show"==o&&(e.isTintActive||(e.options.zoomTintFadeIn?e.zoomTint.css({opacity:e.options.tintOpacity}).animate().stop(!0,!0).fadeIn("slow"):(e.zoomTint.css({opacity:e.options.tintOpacity}).animate(),e.zoomTint.show()),e.isTintActive=!0)),"hide"==o&&e.isTintActive&&(e.options.zoomTintFadeOut?e.zoomTint.stop(!0,!0).fadeOut(e.options.zoomTintFadeOut):e.zoomTint.hide(),e.isTintActive=!1)},setLensPostition:function(o){},setWindowPostition:function(o){var e=this;if(isNaN(e.options.zoomWindowPosition))e.externalContainer=d("#"+e.options.zoomWindowPosition),e.externalContainerWidth=e.externalContainer.width(),e.externalContainerHeight=e.externalContainer.height(),e.externalContainerOffset=e.externalContainer.offset(),e.windowOffsetTop=e.externalContainerOffset.top,e.windowOffsetLeft=e.externalContainerOffset.left;else switch(e.options.zoomWindowPosition){case 1:e.windowOffsetTop=e.options.zoomWindowOffety,e.windowOffsetLeft=+e.nzWidth;break;case 2:e.options.zoomWindowHeight>e.nzHeight&&(e.windowOffsetTop=-1*(e.options.zoomWindowHeight/2-e.nzHeight/2),e.windowOffsetLeft=e.nzWidth);break;case 3:e.windowOffsetTop=e.nzHeight-e.zoomWindow.height()-2*e.options.borderSize,e.windowOffsetLeft=e.nzWidth;break;case 4:e.windowOffsetTop=e.nzHeight,e.windowOffsetLeft=e.nzWidth;break;case 5:e.windowOffsetTop=e.nzHeight,e.windowOffsetLeft=e.nzWidth-e.zoomWindow.width()-2*e.options.borderSize;break;case 6:e.options.zoomWindowHeight>e.nzHeight&&(e.windowOffsetTop=e.nzHeight,e.windowOffsetLeft=-1*(e.options.zoomWindowWidth/2-e.nzWidth/2+2*e.options.borderSize));break;case 7:e.windowOffsetTop=e.nzHeight,e.windowOffsetLeft=0;break;case 8:e.windowOffsetTop=e.nzHeight,e.windowOffsetLeft=-1*(e.zoomWindow.width()+2*e.options.borderSize);break;case 9:e.windowOffsetTop=e.nzHeight-e.zoomWindow.height()-2*e.options.borderSize,e.windowOffsetLeft=-1*(e.zoomWindow.width()+2*e.options.borderSize);break;case 10:e.options.zoomWindowHeight>e.nzHeight&&(e.windowOffsetTop=-1*(e.options.zoomWindowHeight/2-e.nzHeight/2),e.windowOffsetLeft=-1*(e.zoomWindow.width()+2*e.options.borderSize));break;case 11:e.windowOffsetTop=e.options.zoomWindowOffety,e.windowOffsetLeft=-1*(e.zoomWindow.width()+2*e.options.borderSize);break;case 12:e.windowOffsetTop=-1*(e.zoomWindow.height()+2*e.options.borderSize),e.windowOffsetLeft=-1*(e.zoomWindow.width()+2*e.options.borderSize);break;case 13:e.windowOffsetTop=-1*(e.zoomWindow.height()+2*e.options.borderSize),e.windowOffsetLeft=0;break;case 14:e.options.zoomWindowHeight>e.nzHeight&&(e.windowOffsetTop=-1*(e.zoomWindow.height()+2*e.options.borderSize),e.windowOffsetLeft=-1*(e.options.zoomWindowWidth/2-e.nzWidth/2+2*e.options.borderSize));break;case 15:e.windowOffsetTop=-1*(e.zoomWindow.height()+2*e.options.borderSize),e.windowOffsetLeft=e.nzWidth-e.zoomWindow.width()-2*e.options.borderSize;break;case 16:e.windowOffsetTop=-1*(e.zoomWindow.height()+2*e.options.borderSize),e.windowOffsetLeft=e.nzWidth;break;default:e.windowOffsetTop=e.options.zoomWindowOffety,e.windowOffsetLeft=e.nzWidth}e.isWindowSet=!0,e.windowOffsetTop=e.windowOffsetTop+e.options.zoomWindowOffety,e.windowOffsetLeft=e.windowOffsetLeft+e.options.zoomWindowOffetx,e.zoomWindow.css({top:e.windowOffsetTop}),e.zoomWindow.css({left:e.windowOffsetLeft}),"inner"==e.options.zoomType&&(e.zoomWindow.css({top:0}),e.zoomWindow.css({left:0})),e.windowLeftPos=String(-1*((o.pageX-e.nzOffset.left)*e.widthRatio-e.zoomWindow.width()/2)),e.windowTopPos=String(-1*((o.pageY-e.nzOffset.top)*e.heightRatio-e.zoomWindow.height()/2)),e.Etoppos&&(e.windowTopPos=0),e.Eloppos&&(e.windowLeftPos=0),e.Eboppos&&(e.windowTopPos=-1*(e.largeHeight/e.currentZoomLevel-e.zoomWindow.height())),e.Eroppos&&(e.windowLeftPos=-1*(e.largeWidth/e.currentZoomLevel-e.zoomWindow.width())),e.fullheight&&(e.windowTopPos=0),e.fullwidth&&(e.windowLeftPos=0),"window"!=e.options.zoomType&&"inner"!=e.options.zoomType||(1==e.zoomLock&&(e.widthRatio<=1&&(e.windowLeftPos=0),e.heightRatio<=1&&(e.windowTopPos=0)),"window"==e.options.zoomType&&(e.largeHeight<e.options.zoomWindowHeight&&(e.windowTopPos=0),e.largeWidth<e.options.zoomWindowWidth&&(e.windowLeftPos=0)),e.options.easing?(e.xp||(e.xp=0),e.yp||(e.yp=0),e.loop||(e.loop=setInterval(function(){e.xp+=(e.windowLeftPos-e.xp)/e.options.easingAmount,e.yp+=(e.windowTopPos-e.yp)/e.options.easingAmount,e.scrollingLock?(clearInterval(e.loop),e.xp=e.windowLeftPos,e.yp=e.windowTopPos,e.xp=-1*((o.pageX-e.nzOffset.left)*e.widthRatio-e.zoomWindow.width()/2),e.yp=-1*((o.pageY-e.nzOffset.top)*e.heightRatio-e.zoomWindow.height()/2),e.changeBgSize&&(e.nzHeight>e.nzWidth?("lens"==e.options.zoomType&&e.zoomLens.css({"background-size":e.largeWidth/e.newvalueheight+"px "+e.largeHeight/e.newvalueheight+"px"}),e.zoomWindow.css({"background-size":e.largeWidth/e.newvalueheight+"px "+e.largeHeight/e.newvalueheight+"px"})):("lens"!=e.options.zoomType&&e.zoomLens.css({"background-size":e.largeWidth/e.newvaluewidth+"px "+e.largeHeight/e.newvalueheight+"px"}),e.zoomWindow.css({"background-size":e.largeWidth/e.newvaluewidth+"px "+e.largeHeight/e.newvaluewidth+"px"})),e.changeBgSize=!1),e.zoomWindow.css({backgroundPosition:e.windowLeftPos+"px "+e.windowTopPos+"px"}),e.scrollingLock=!1,e.loop=!1):Math.round(Math.abs(e.xp-e.windowLeftPos)+Math.abs(e.yp-e.windowTopPos))<1?(clearInterval(e.loop),e.zoomWindow.css({backgroundPosition:e.windowLeftPos+"px "+e.windowTopPos+"px"}),e.loop=!1):(e.changeBgSize&&(e.nzHeight>e.nzWidth?("lens"==e.options.zoomType&&e.zoomLens.css({"background-size":e.largeWidth/e.newvalueheight+"px "+e.largeHeight/e.newvalueheight+"px"}),e.zoomWindow.css({"background-size":e.largeWidth/e.newvalueheight+"px "+e.largeHeight/e.newvalueheight+"px"})):("lens"!=e.options.zoomType&&e.zoomLens.css({"background-size":e.largeWidth/e.newvaluewidth+"px "+e.largeHeight/e.newvaluewidth+"px"}),e.zoomWindow.css({"background-size":e.largeWidth/e.newvaluewidth+"px "+e.largeHeight/e.newvaluewidth+"px"})),e.changeBgSize=!1),e.zoomWindow.css({backgroundPosition:e.xp+"px "+e.yp+"px"}))},16))):(e.changeBgSize&&(e.nzHeight>e.nzWidth?("lens"==e.options.zoomType&&e.zoomLens.css({"background-size":e.largeWidth/e.newvalueheight+"px "+e.largeHeight/e.newvalueheight+"px"}),e.zoomWindow.css({"background-size":e.largeWidth/e.newvalueheight+"px "+e.largeHeight/e.newvalueheight+"px"})):("lens"==e.options.zoomType&&e.zoomLens.css({"background-size":e.largeWidth/e.newvaluewidth+"px "+e.largeHeight/e.newvaluewidth+"px"}),e.largeHeight/e.newvaluewidth<e.options.zoomWindowHeight?e.zoomWindow.css({"background-size":e.largeWidth/e.newvaluewidth+"px "+e.largeHeight/e.newvaluewidth+"px"}):e.zoomWindow.css({"background-size":e.largeWidth/e.newvalueheight+"px "+e.largeHeight/e.newvalueheight+"px"})),e.changeBgSize=!1),e.zoomWindow.css({backgroundPosition:e.windowLeftPos+"px "+e.windowTopPos+"px"})))},setTintPosition:function(o){var e=this;e.nzOffset=e.$elem.offset(),e.tintpos=String(-1*(o.pageX-e.nzOffset.left-e.zoomLens.width()/2)),e.tintposy=String(-1*(o.pageY-e.nzOffset.top-e.zoomLens.height()/2)),e.Etoppos&&(e.tintposy=0),e.Eloppos&&(e.tintpos=0),e.Eboppos&&(e.tintposy=-1*(e.nzHeight-e.zoomLens.height()-2*e.options.lensBorderSize)),e.Eroppos&&(e.tintpos=-1*(e.nzWidth-e.zoomLens.width()-2*e.options.lensBorderSize)),e.options.tint&&(e.fullheight&&(e.tintposy=0),e.fullwidth&&(e.tintpos=0),e.zoomTintImage.css({left:e.tintpos+"px"}),e.zoomTintImage.css({top:e.tintposy+"px"}))},swaptheimage:function(o,e){var i=this,t=new Image;i.options.loadingIcon&&(i.spinner=d("<div style=\"background: url('"+i.options.loadingIcon+"') no-repeat center;height:"+i.nzHeight+"px;width:"+i.nzWidth+'px;z-index: 2000;position: absolute; background-position: center center;"></div>'),i.$elem.after(i.spinner)),i.options.onImageSwap(i.$elem),t.onload=function(){i.largeWidth=t.width,i.largeHeight=t.height,i.zoomImage=e,i.zoomWindow.css({"background-size":i.largeWidth+"px "+i.largeHeight+"px"}),i.swapAction(o,e)},t.src=e},swapAction:function(o,e){var i=this,t=new Image;if(t.onload=function(){i.nzHeight=t.height,i.nzWidth=t.width,i.options.onImageSwapComplete(i.$elem),i.doneCallback()},t.src=o,i.currentZoomLevel=i.options.zoomLevel,i.options.maxZoomLevel=!1,"lens"==i.options.zoomType&&i.zoomLens.css({backgroundImage:"url('"+e+"')"}),"window"==i.options.zoomType&&i.zoomWindow.css({backgroundImage:"url('"+e+"')"}),"inner"==i.options.zoomType&&i.zoomWindow.css({backgroundImage:"url('"+e+"')"}),i.currentImage=e,i.options.imageCrossfade){var n=i.$elem,s=n.clone();if(i.$elem.attr("src",o),i.$elem.after(s),s.stop(!0).fadeOut(i.options.imageCrossfade,function(){d(this).remove()}),i.$elem.width("auto").removeAttr("width"),i.$elem.height("auto").removeAttr("height"),n.fadeIn(i.options.imageCrossfade),i.options.tint&&"inner"!=i.options.zoomType){var h=i.zoomTintImage,a=h.clone();i.zoomTintImage.attr("src",e),i.zoomTintImage.after(a),a.stop(!0).fadeOut(i.options.imageCrossfade,function(){d(this).remove()}),h.fadeIn(i.options.imageCrossfade),i.zoomTint.css({height:i.$elem.height()}),i.zoomTint.css({width:i.$elem.width()})}i.zoomContainer.css("height",i.$elem.height()),i.zoomContainer.css("width",i.$elem.width()),"inner"==i.options.zoomType&&(i.options.constrainType||(i.zoomWrap.parent().css("height",i.$elem.height()),i.zoomWrap.parent().css("width",i.$elem.width()),i.zoomWindow.css("height",i.$elem.height()),i.zoomWindow.css("width",i.$elem.width()))),i.options.imageCrossfade&&(i.zoomWrap.css("height",i.$elem.height()),i.zoomWrap.css("width",i.$elem.width()))}else i.$elem.attr("src",o),i.options.tint&&(i.zoomTintImage.attr("src",e),i.zoomTintImage.attr("height",i.$elem.height()),i.zoomTintImage.css({height:i.$elem.height()}),i.zoomTint.css({height:i.$elem.height()})),i.zoomContainer.css("height",i.$elem.height()),i.zoomContainer.css("width",i.$elem.width()),i.options.imageCrossfade&&(i.zoomWrap.css("height",i.$elem.height()),i.zoomWrap.css("width",i.$elem.width()));i.options.constrainType&&("height"==i.options.constrainType&&(i.zoomContainer.css("height",i.options.constrainSize),i.zoomContainer.css("width","auto"),i.options.imageCrossfade?(i.zoomWrap.css("height",i.options.constrainSize),i.zoomWrap.css("width","auto"),i.constwidth=i.zoomWrap.width()):(i.$elem.css("height",i.options.constrainSize),i.$elem.css("width","auto"),i.constwidth=i.$elem.width()),"inner"==i.options.zoomType&&(i.zoomWrap.parent().css("height",i.options.constrainSize),i.zoomWrap.parent().css("width",i.constwidth),i.zoomWindow.css("height",i.options.constrainSize),i.zoomWindow.css("width",i.constwidth)),i.options.tint&&(i.tintContainer.css("height",i.options.constrainSize),i.tintContainer.css("width",i.constwidth),i.zoomTint.css("height",i.options.constrainSize),i.zoomTint.css("width",i.constwidth),i.zoomTintImage.css("height",i.options.constrainSize),i.zoomTintImage.css("width",i.constwidth))),"width"==i.options.constrainType&&(i.zoomContainer.css("height","auto"),i.zoomContainer.css("width",i.options.constrainSize),i.options.imageCrossfade?(i.zoomWrap.css("height","auto"),i.zoomWrap.css("width",i.options.constrainSize),i.constheight=i.zoomWrap.height()):(i.$elem.css("height","auto"),i.$elem.css("width",i.options.constrainSize),i.constheight=i.$elem.height()),"inner"==i.options.zoomType&&(i.zoomWrap.parent().css("height",i.constheight),i.zoomWrap.parent().css("width",i.options.constrainSize),i.zoomWindow.css("height",i.constheight),i.zoomWindow.css("width",i.options.constrainSize)),i.options.tint&&(i.tintContainer.css("height",i.constheight),i.tintContainer.css("width",i.options.constrainSize),i.zoomTint.css("height",i.constheight),i.zoomTint.css("width",i.options.constrainSize),i.zoomTintImage.css("height",i.constheight),i.zoomTintImage.css("width",i.options.constrainSize))))},doneCallback:function(){var o=this;o.options.loadingIcon&&o.spinner.hide(),o.nzOffset=o.$elem.offset(),o.nzWidth=o.$elem.width(),o.nzHeight=o.$elem.height(),o.currentZoomLevel=o.options.zoomLevel,o.widthRatio=o.largeWidth/o.nzWidth,o.heightRatio=o.largeHeight/o.nzHeight,"window"==o.options.zoomType&&(o.nzHeight<o.options.zoomWindowWidth/o.widthRatio?lensHeight=o.nzHeight:lensHeight=String(o.options.zoomWindowHeight/o.heightRatio),o.options.zoomWindowWidth<o.options.zoomWindowWidth?lensWidth=o.nzWidth:lensWidth=o.options.zoomWindowWidth/o.widthRatio,o.zoomLens&&(o.zoomLens.css("width",lensWidth),o.zoomLens.css("height",lensHeight)))},getCurrentImage:function(){return this.zoomImage},getGalleryList:function(){var e=this;return e.gallerylist=[],e.options.gallery?d("#"+e.options.gallery+" a").each(function(){var o="";d(this).data("zoom-image")?o=d(this).data("zoom-image"):d(this).data("image")&&(o=d(this).data("image")),o==e.zoomImage?e.gallerylist.unshift({href:""+o,title:d(this).find("img").attr("title")}):e.gallerylist.push({href:""+o,title:d(this).find("img").attr("title")})}):e.gallerylist.push({href:""+e.zoomImage,title:d(this).find("img").attr("title")}),e.gallerylist},changeZoomLevel:function(o){var e=this;e.scrollingLock=!0,e.newvalue=parseFloat(o).toFixed(2),newvalue=parseFloat(o).toFixed(2),maxheightnewvalue=e.largeHeight/(e.options.zoomWindowHeight/e.nzHeight*e.nzHeight),maxwidthtnewvalue=e.largeWidth/(e.options.zoomWindowWidth/e.nzWidth*e.nzWidth),"inner"!=e.options.zoomType&&(maxheightnewvalue<=newvalue?(e.heightRatio=e.largeHeight/maxheightnewvalue/e.nzHeight,e.newvalueheight=maxheightnewvalue,e.fullheight=!0):(e.heightRatio=e.largeHeight/newvalue/e.nzHeight,e.newvalueheight=newvalue,e.fullheight=!1),maxwidthtnewvalue<=newvalue?(e.widthRatio=e.largeWidth/maxwidthtnewvalue/e.nzWidth,e.newvaluewidth=maxwidthtnewvalue,e.fullwidth=!0):(e.widthRatio=e.largeWidth/newvalue/e.nzWidth,e.newvaluewidth=newvalue,e.fullwidth=!1),"lens"==e.options.zoomType&&(maxheightnewvalue<=newvalue?(e.fullwidth=!0,e.newvaluewidth=maxheightnewvalue):(e.widthRatio=e.largeWidth/newvalue/e.nzWidth,e.newvaluewidth=newvalue,e.fullwidth=!1))),"inner"==e.options.zoomType&&(maxheightnewvalue=parseFloat(e.largeHeight/e.nzHeight).toFixed(2),maxwidthtnewvalue=parseFloat(e.largeWidth/e.nzWidth).toFixed(2),newvalue>maxheightnewvalue&&(newvalue=maxheightnewvalue),newvalue>maxwidthtnewvalue&&(newvalue=maxwidthtnewvalue),maxheightnewvalue<=newvalue?(e.heightRatio=e.largeHeight/newvalue/e.nzHeight,newvalue>maxheightnewvalue?e.newvalueheight=maxheightnewvalue:e.newvalueheight=newvalue,e.fullheight=!0):(e.heightRatio=e.largeHeight/newvalue/e.nzHeight,newvalue>maxheightnewvalue?e.newvalueheight=maxheightnewvalue:e.newvalueheight=newvalue,e.fullheight=!1),maxwidthtnewvalue<=newvalue?(e.widthRatio=e.largeWidth/newvalue/e.nzWidth,newvalue>maxwidthtnewvalue?e.newvaluewidth=maxwidthtnewvalue:e.newvaluewidth=newvalue,e.fullwidth=!0):(e.widthRatio=e.largeWidth/newvalue/e.nzWidth,e.newvaluewidth=newvalue,e.fullwidth=!1)),scrcontinue=!1,"inner"==e.options.zoomType&&(e.nzWidth>=e.nzHeight&&(e.newvaluewidth<=maxwidthtnewvalue?scrcontinue=!0:(scrcontinue=!1,e.fullheight=!0,e.fullwidth=!0)),e.nzHeight>e.nzWidth&&(e.newvaluewidth<=maxwidthtnewvalue?scrcontinue=!0:(scrcontinue=!1,e.fullheight=!0,e.fullwidth=!0))),"inner"!=e.options.zoomType&&(scrcontinue=!0),scrcontinue&&(e.zoomLock=0,e.changeZoom=!0,e.options.zoomWindowHeight/e.heightRatio<=e.nzHeight&&(e.currentZoomLevel=e.newvalueheight,"lens"!=e.options.zoomType&&"inner"!=e.options.zoomType&&(e.changeBgSize=!0,e.zoomLens.css({height:String(e.options.zoomWindowHeight/e.heightRatio)+"px"})),"lens"!=e.options.zoomType&&"inner"!=e.options.zoomType||(e.changeBgSize=!0)),e.options.zoomWindowWidth/e.widthRatio<=e.nzWidth&&("inner"!=e.options.zoomType&&e.newvaluewidth>e.newvalueheight&&(e.currentZoomLevel=e.newvaluewidth),"lens"!=e.options.zoomType&&"inner"!=e.options.zoomType&&(e.changeBgSize=!0,e.zoomLens.css({width:String(e.options.zoomWindowWidth/e.widthRatio)+"px"})),"lens"!=e.options.zoomType&&"inner"!=e.options.zoomType||(e.changeBgSize=!0)),"inner"==e.options.zoomType&&(e.changeBgSize=!0,e.nzWidth>e.nzHeight&&(e.currentZoomLevel=e.newvaluewidth),e.nzHeight>e.nzWidth&&(e.currentZoomLevel=e.newvaluewidth))),e.setPosition(e.currentLoc)},closeAll:function(){self.zoomWindow&&self.zoomWindow.hide(),self.zoomLens&&self.zoomLens.hide(),self.zoomTint&&self.zoomTint.hide()},changeState:function(o){"enable"==o&&(this.options.zoomEnabled=!0),"disable"==o&&(this.options.zoomEnabled=!1)}};d.fn.elevateZoom=function(e){return this.each(function(){var o=Object.create(t);o.init(e,this),d.data(this,"elevateZoom",o)})},d.fn.elevateZoom.options={zoomActivation:"hover",zoomEnabled:!0,preloading:1,zoomLevel:1,scrollZoom:!1,scrollZoomIncrement:.1,minZoomLevel:!1,maxZoomLevel:!1,easing:!1,easingAmount:12,lensSize:200,zoomWindowWidth:400,zoomWindowHeight:400,zoomWindowOffetx:0,zoomWindowOffety:0,zoomWindowPosition:1,zoomWindowBgColour:"#fff",lensFadeIn:!1,lensFadeOut:!1,debug:!1,zoomWindowFadeIn:!1,zoomWindowFadeOut:!1,zoomWindowAlwaysShow:!1,zoomTintFadeIn:!1,zoomTintFadeOut:!1,borderSize:4,showLens:!0,borderColour:"#888",lensBorderSize:1,lensBorderColour:"#000",lensShape:"square",zoomType:"window",containLensZoom:!1,lensColour:"white",lensOpacity:.4,lenszoom:!1,tint:!1,tintColour:"#333",tintOpacity:.4,gallery:!1,galleryActiveClass:"zoomGalleryActive",imageCrossfade:!1,constrainType:!1,constrainSize:!1,loadingIcon:!1,cursor:"default",responsive:!0,onComplete:d.noop,onDestroy:function(){},onZoomedImageLoaded:function(){},onImageSwap:d.noop,onImageSwapComplete:d.noop}}(jQuery,window,document);
!function(h,i,n,a){function l(t,e){this.settings=null,this.options=h.extend({},l.Defaults,e),this.$element=h(t),this._handlers={},this._plugins={},this._supress={},this._current=null,this._speed=null,this._coordinates=[],this._breakpoint=null,this._width=null,this._items=[],this._clones=[],this._mergers=[],this._widths=[],this._invalidated={},this._pipe=[],this._drag={time:null,target:null,pointer:null,stage:{start:null,current:null},direction:null},this._states={current:{},tags:{initializing:["busy"],animating:["busy"],dragging:["interacting"]}},h.each(["onResize","onThrottledResize"],h.proxy(function(t,e){this._handlers[e]=h.proxy(this[e],this)},this)),h.each(l.Plugins,h.proxy(function(t,e){this._plugins[t.charAt(0).toLowerCase()+t.slice(1)]=new e(this)},this)),h.each(l.Workers,h.proxy(function(t,e){this._pipe.push({filter:e.filter,run:h.proxy(e.run,this)})},this)),this.setup(),this.initialize()}l.Defaults={items:3,loop:!1,center:!1,rewind:!1,checkVisibility:!0,mouseDrag:!0,touchDrag:!0,pullDrag:!0,freeDrag:!1,margin:0,stagePadding:0,merge:!1,mergeFit:!0,autoWidth:!1,startPosition:0,rtl:!1,smartSpeed:250,fluidSpeed:!1,dragEndSpeed:!1,responsive:{},responsiveRefreshRate:200,responsiveBaseElement:i,fallbackEasing:"swing",info:!1,nestedItemSelector:!1,itemElement:"div",stageElement:"div",refreshClass:"owl-refresh",loadedClass:"owl-loaded",loadingClass:"owl-loading",rtlClass:"owl-rtl",responsiveClass:"owl-responsive",dragClass:"owl-drag",itemClass:"owl-item",stageClass:"owl-stage",stageOuterClass:"owl-stage-outer",grabClass:"owl-grab"},l.Width={Default:"default",Inner:"inner",Outer:"outer"},l.Type={Event:"event",State:"state"},l.Plugins={},l.Workers=[{filter:["width","settings"],run:function(){this._width=this.$element.width()}},{filter:["width","items","settings"],run:function(t){t.current=this._items&&this._items[this.relative(this._current)]}},{filter:["items","settings"],run:function(){this.$stage.children(".cloned").remove()}},{filter:["width","items","settings"],run:function(t){var e=this.settings.margin||"",i=!this.settings.autoWidth,s=this.settings.rtl,n={width:"auto","margin-left":s?e:"","margin-right":s?"":e};!i&&this.$stage.children().css(n),t.css=n}},{filter:["width","items","settings"],run:function(t){var e=(this.width()/this.settings.items).toFixed(3)-this.settings.margin,i=null,s=this._items.length,n=!this.settings.autoWidth,o=[];for(t.items={merge:!1,width:e};s--;)i=this._mergers[s],i=this.settings.mergeFit&&Math.min(i,this.settings.items)||i,t.items.merge=1<i||t.items.merge,o[s]=n?e*i:this._items[s].width();this._widths=o}},{filter:["items","settings"],run:function(){var t=[],e=this._items,i=this.settings,s=Math.max(2*i.items,4),n=2*Math.ceil(e.length/2),o=i.loop&&e.length?i.rewind?s:Math.max(s,n):0,r="",a="";for(o/=2;0<o;)t.push(this.normalize(t.length/2,!0)),r+=e[t[t.length-1]][0].outerHTML,t.push(this.normalize(e.length-1-(t.length-1)/2,!0)),a=e[t[t.length-1]][0].outerHTML+a,o-=1;this._clones=t,h(r).addClass("cloned").appendTo(this.$stage),h(a).addClass("cloned").prependTo(this.$stage)}},{filter:["width","items","settings"],run:function(){for(var t=this.settings.rtl?1:-1,e=this._clones.length+this._items.length,i=-1,s=0,n=0,o=[];++i<e;)s=o[i-1]||0,n=this._widths[this.relative(i)]+this.settings.margin,o.push(s+n*t);this._coordinates=o}},{filter:["width","items","settings"],run:function(){var t=this.settings.stagePadding,e=this._coordinates,i={width:Math.ceil(Math.abs(e[e.length-1]))+2*t,"padding-left":t||"","padding-right":t||""};this.$stage.css(i)}},{filter:["width","items","settings"],run:function(t){var e=this._coordinates.length,i=!this.settings.autoWidth,s=this.$stage.children();if(i&&t.items.merge)for(;e--;)t.css.width=this._widths[this.relative(e)],s.eq(e).css(t.css);else i&&(t.css.width=t.items.width,s.css(t.css))}},{filter:["items"],run:function(){this._coordinates.length<1&&this.$stage.removeAttr("style")}},{filter:["width","items","settings"],run:function(t){t.current=t.current?this.$stage.children().index(t.current):0,t.current=Math.max(this.minimum(),Math.min(this.maximum(),t.current)),this.reset(t.current)}},{filter:["position"],run:function(){this.animate(this.coordinates(this._current))}},{filter:["width","position","items","settings"],run:function(){var t,e,i,s,n=this.settings.rtl?1:-1,o=2*this.settings.stagePadding,r=this.coordinates(this.current())+o,a=r+this.width()*n,h=[];for(i=0,s=this._coordinates.length;i<s;i++)t=this._coordinates[i-1]||0,e=Math.abs(this._coordinates[i])+o*n,(this.op(t,"<=",r)&&this.op(t,">",a)||this.op(e,"<",r)&&this.op(e,">",a))&&h.push(i);this.$stage.children(".active").removeClass("active"),this.$stage.children(":eq("+h.join("), :eq(")+")").addClass("active"),this.$stage.children(".center").removeClass("center"),this.settings.center&&this.$stage.children().eq(this.current()).addClass("center")}}],l.prototype.initializeStage=function(){this.$stage=this.$element.find("."+this.settings.stageClass),this.$stage.length||(this.$element.addClass(this.options.loadingClass),this.$stage=h("<"+this.settings.stageElement+' class="'+this.settings.stageClass+'"/>').wrap('<div class="'+this.settings.stageOuterClass+'"/>'),this.$element.append(this.$stage.parent()))},l.prototype.initializeItems=function(){var t=this.$element.find(".owl-item");if(t.length)return this._items=t.get().map(function(t){return h(t)}),this._mergers=this._items.map(function(){return 1}),void this.refresh();this.replace(this.$element.children().not(this.$stage.parent())),this.isVisible()?this.refresh():this.invalidate("width"),this.$element.removeClass(this.options.loadingClass).addClass(this.options.loadedClass)},l.prototype.initialize=function(){var t,e,i;(this.enter("initializing"),this.trigger("initialize"),this.$element.toggleClass(this.settings.rtlClass,this.settings.rtl),this.settings.autoWidth&&!this.is("pre-loading"))&&(t=this.$element.find("img"),e=this.settings.nestedItemSelector?"."+this.settings.nestedItemSelector:a,i=this.$element.children(e).width(),t.length&&i<=0&&this.preloadAutoWidthImages(t));this.initializeStage(),this.initializeItems(),this.registerEventHandlers(),this.leave("initializing"),this.trigger("initialized")},l.prototype.isVisible=function(){return!this.settings.checkVisibility||this.$element.is(":visible")},l.prototype.setup=function(){var e=this.viewport(),t=this.options.responsive,i=-1,s=null;t?(h.each(t,function(t){t<=e&&i<t&&(i=Number(t))}),"function"==typeof(s=h.extend({},this.options,t[i])).stagePadding&&(s.stagePadding=s.stagePadding()),delete s.responsive,s.responsiveClass&&this.$element.attr("class",this.$element.attr("class").replace(new RegExp("("+this.options.responsiveClass+"-)\\S+\\s","g"),"$1"+i))):s=h.extend({},this.options),this.trigger("change",{property:{name:"settings",value:s}}),this._breakpoint=i,this.settings=s,this.invalidate("settings"),this.trigger("changed",{property:{name:"settings",value:this.settings}})},l.prototype.optionsLogic=function(){this.settings.autoWidth&&(this.settings.stagePadding=!1,this.settings.merge=!1)},l.prototype.prepare=function(t){var e=this.trigger("prepare",{content:t});return e.data||(e.data=h("<"+this.settings.itemElement+"/>").addClass(this.options.itemClass).append(t)),this.trigger("prepared",{content:e.data}),e.data},l.prototype.update=function(){for(var t=0,e=this._pipe.length,i=h.proxy(function(t){return this[t]},this._invalidated),s={};t<e;)(this._invalidated.all||0<h.grep(this._pipe[t].filter,i).length)&&this._pipe[t].run(s),t++;this._invalidated={},!this.is("valid")&&this.enter("valid")},l.prototype.width=function(t){switch(t=t||l.Width.Default){case l.Width.Inner:case l.Width.Outer:return this._width;default:return this._width-2*this.settings.stagePadding+this.settings.margin}},l.prototype.refresh=function(){this.enter("refreshing"),this.trigger("refresh"),this.setup(),this.optionsLogic(),this.$element.addClass(this.options.refreshClass),this.update(),this.$element.removeClass(this.options.refreshClass),this.leave("refreshing"),this.trigger("refreshed")},l.prototype.onThrottledResize=function(){i.clearTimeout(this.resizeTimer),this.resizeTimer=i.setTimeout(this._handlers.onResize,this.settings.responsiveRefreshRate)},l.prototype.onResize=function(){return!!this._items.length&&(this._width!==this.$element.width()&&(!!this.isVisible()&&(this.enter("resizing"),this.trigger("resize").isDefaultPrevented()?(this.leave("resizing"),!1):(this.invalidate("width"),this.refresh(),this.leave("resizing"),void this.trigger("resized")))))},l.prototype.registerEventHandlers=function(){h.support.transition&&this.$stage.on(h.support.transition.end+".owl.core",h.proxy(this.onTransitionEnd,this)),!1!==this.settings.responsive&&this.on(i,"resize",this._handlers.onThrottledResize),this.settings.mouseDrag&&(this.$element.addClass(this.options.dragClass),this.$stage.on("mousedown.owl.core",h.proxy(this.onDragStart,this)),this.$stage.on("dragstart.owl.core selectstart.owl.core",function(){return!1})),this.settings.touchDrag&&(this.$stage.on("touchstart.owl.core",h.proxy(this.onDragStart,this)),this.$stage.on("touchcancel.owl.core",h.proxy(this.onDragEnd,this)))},l.prototype.onDragStart=function(t){var e=null;3!==t.which&&(e=h.support.transform?{x:(e=this.$stage.css("transform").replace(/.*\(|\)| /g,"").split(","))[16===e.length?12:4],y:e[16===e.length?13:5]}:(e=this.$stage.position(),{x:this.settings.rtl?e.left+this.$stage.width()-this.width()+this.settings.margin:e.left,y:e.top}),this.is("animating")&&(h.support.transform?this.animate(e.x):this.$stage.stop(),this.invalidate("position")),this.$element.toggleClass(this.options.grabClass,"mousedown"===t.type),this.speed(0),this._drag.time=(new Date).getTime(),this._drag.target=h(t.target),this._drag.stage.start=e,this._drag.stage.current=e,this._drag.pointer=this.pointer(t),h(n).on("mouseup.owl.core touchend.owl.core",h.proxy(this.onDragEnd,this)),h(n).one("mousemove.owl.core touchmove.owl.core",h.proxy(function(t){var e=this.difference(this._drag.pointer,this.pointer(t));h(n).on("mousemove.owl.core touchmove.owl.core",h.proxy(this.onDragMove,this)),Math.abs(e.x)<Math.abs(e.y)&&this.is("valid")||(t.preventDefault(),this.enter("dragging"),this.trigger("drag"))},this)))},l.prototype.onDragMove=function(t){var e=null,i=null,s=null,n=this.difference(this._drag.pointer,this.pointer(t)),o=this.difference(this._drag.stage.start,n);this.is("dragging")&&(t.preventDefault(),this.settings.loop?(e=this.coordinates(this.minimum()),i=this.coordinates(this.maximum()+1)-e,o.x=((o.x-e)%i+i)%i+e):(e=this.settings.rtl?this.coordinates(this.maximum()):this.coordinates(this.minimum()),i=this.settings.rtl?this.coordinates(this.minimum()):this.coordinates(this.maximum()),s=this.settings.pullDrag?-1*n.x/5:0,o.x=Math.max(Math.min(o.x,e+s),i+s)),this._drag.stage.current=o,this.animate(o.x))},l.prototype.onDragEnd=function(t){var e=this.difference(this._drag.pointer,this.pointer(t)),i=this._drag.stage.current,s=0<e.x^this.settings.rtl?"left":"right";h(n).off(".owl.core"),this.$element.removeClass(this.options.grabClass),(0!==e.x&&this.is("dragging")||!this.is("valid"))&&(this.speed(this.settings.dragEndSpeed||this.settings.smartSpeed),this.current(this.closest(i.x,0!==e.x?s:this._drag.direction)),this.invalidate("position"),this.update(),this._drag.direction=s,(3<Math.abs(e.x)||300<(new Date).getTime()-this._drag.time)&&this._drag.target.one("click.owl.core",function(){return!1})),this.is("dragging")&&(this.leave("dragging"),this.trigger("dragged"))},l.prototype.closest=function(i,s){var n=-1,o=this.width(),r=this.coordinates();return this.settings.freeDrag||h.each(r,h.proxy(function(t,e){return"left"===s&&e-30<i&&i<e+30?n=t:"right"===s&&e-o-30<i&&i<e-o+30?n=t+1:this.op(i,"<",e)&&this.op(i,">",r[t+1]!==a?r[t+1]:e-o)&&(n="left"===s?t+1:t),-1===n},this)),this.settings.loop||(this.op(i,">",r[this.minimum()])?n=i=this.minimum():this.op(i,"<",r[this.maximum()])&&(n=i=this.maximum())),n},l.prototype.animate=function(t){var e=0<this.speed();this.is("animating")&&this.onTransitionEnd(),e&&(this.enter("animating"),this.trigger("translate")),h.support.transform3d&&h.support.transition?this.$stage.css({transform:"translate3d("+t+"px,0px,0px)",transition:this.speed()/1e3+"s"}):e?this.$stage.animate({left:t+"px"},this.speed(),this.settings.fallbackEasing,h.proxy(this.onTransitionEnd,this)):this.$stage.css({left:t+"px"})},l.prototype.is=function(t){return this._states.current[t]&&0<this._states.current[t]},l.prototype.current=function(t){if(t===a)return this._current;if(0===this._items.length)return a;if(t=this.normalize(t),this._current!==t){var e=this.trigger("change",{property:{name:"position",value:t}});e.data!==a&&(t=this.normalize(e.data)),this._current=t,this.invalidate("position"),this.trigger("changed",{property:{name:"position",value:this._current}})}return this._current},l.prototype.invalidate=function(t){return"string"===h.type(t)&&(this._invalidated[t]=!0,this.is("valid")&&this.leave("valid")),h.map(this._invalidated,function(t,e){return e})},l.prototype.reset=function(t){(t=this.normalize(t))!==a&&(this._speed=0,this._current=t,this.suppress(["translate","translated"]),this.animate(this.coordinates(t)),this.release(["translate","translated"]))},l.prototype.normalize=function(t,e){var i=this._items.length,s=e?0:this._clones.length;return!this.isNumeric(t)||i<1?t=a:(t<0||i+s<=t)&&(t=((t-s/2)%i+i)%i+s/2),t},l.prototype.relative=function(t){return t-=this._clones.length/2,this.normalize(t,!0)},l.prototype.maximum=function(t){var e,i,s,n=this.settings,o=this._coordinates.length;if(n.loop)o=this._clones.length/2+this._items.length-1;else if(n.autoWidth||n.merge){if(e=this._items.length)for(i=this._items[--e].width(),s=this.$element.width();e--&&!(s<(i+=this._items[e].width()+this.settings.margin)););o=e+1}else o=n.center?this._items.length-1:this._items.length-n.items;return t&&(o-=this._clones.length/2),Math.max(o,0)},l.prototype.minimum=function(t){return t?0:this._clones.length/2},l.prototype.items=function(t){return t===a?this._items.slice():(t=this.normalize(t,!0),this._items[t])},l.prototype.mergers=function(t){return t===a?this._mergers.slice():(t=this.normalize(t,!0),this._mergers[t])},l.prototype.clones=function(i){var e=this._clones.length/2,s=e+this._items.length,n=function(t){return t%2==0?s+t/2:e-(t+1)/2};return i===a?h.map(this._clones,function(t,e){return n(e)}):h.map(this._clones,function(t,e){return t===i?n(e):null})},l.prototype.speed=function(t){return t!==a&&(this._speed=t),this._speed},l.prototype.coordinates=function(t){var e,i=1,s=t-1;return t===a?h.map(this._coordinates,h.proxy(function(t,e){return this.coordinates(e)},this)):(this.settings.center?(this.settings.rtl&&(i=-1,s=t+1),e=this._coordinates[t],e+=(this.width()-e+(this._coordinates[s]||0))/2*i):e=this._coordinates[s]||0,e=Math.ceil(e))},l.prototype.duration=function(t,e,i){return 0===i?0:Math.min(Math.max(Math.abs(e-t),1),6)*Math.abs(i||this.settings.smartSpeed)},l.prototype.to=function(t,e){var i=this.current(),s=null,n=t-this.relative(i),o=(0<n)-(n<0),r=this._items.length,a=this.minimum(),h=this.maximum();this.settings.loop?(!this.settings.rewind&&Math.abs(n)>r/2&&(n+=-1*o*r),(s=(((t=i+n)-a)%r+r)%r+a)!==t&&s-n<=h&&0<s-n&&(i=s-n,t=s,this.reset(i))):t=this.settings.rewind?(t%(h+=1)+h)%h:Math.max(a,Math.min(h,t)),this.speed(this.duration(i,t,e)),this.current(t),this.isVisible()&&this.update()},l.prototype.next=function(t){t=t||!1,this.to(this.relative(this.current())+1,t)},l.prototype.prev=function(t){t=t||!1,this.to(this.relative(this.current())-1,t)},l.prototype.onTransitionEnd=function(t){if(t!==a&&(t.stopPropagation(),(t.target||t.srcElement||t.originalTarget)!==this.$stage.get(0)))return!1;this.leave("animating"),this.trigger("translated")},l.prototype.viewport=function(){var t;return this.options.responsiveBaseElement!==i?t=h(this.options.responsiveBaseElement).width():i.innerWidth?t=i.innerWidth:n.documentElement&&n.documentElement.clientWidth?t=n.documentElement.clientWidth:console.warn("Can not detect viewport width."),t},l.prototype.replace=function(t){this.$stage.empty(),this._items=[],t&&(t=t instanceof jQuery?t:h(t)),this.settings.nestedItemSelector&&(t=t.find("."+this.settings.nestedItemSelector)),t.filter(function(){return 1===this.nodeType}).each(h.proxy(function(t,e){e=this.prepare(e),this.$stage.append(e),this._items.push(e),this._mergers.push(1*e.find("[data-merge]").addBack("[data-merge]").attr("data-merge")||1)},this)),this.reset(this.isNumeric(this.settings.startPosition)?this.settings.startPosition:0),this.invalidate("items")},l.prototype.add=function(t,e){var i=this.relative(this._current);e=e===a?this._items.length:this.normalize(e,!0),t=t instanceof jQuery?t:h(t),this.trigger("add",{content:t,position:e}),t=this.prepare(t),0===this._items.length||e===this._items.length?(0===this._items.length&&this.$stage.append(t),0!==this._items.length&&this._items[e-1].after(t),this._items.push(t),this._mergers.push(1*t.find("[data-merge]").addBack("[data-merge]").attr("data-merge")||1)):(this._items[e].before(t),this._items.splice(e,0,t),this._mergers.splice(e,0,1*t.find("[data-merge]").addBack("[data-merge]").attr("data-merge")||1)),this._items[i]&&this.reset(this._items[i].index()),this.invalidate("items"),this.trigger("added",{content:t,position:e})},l.prototype.remove=function(t){(t=this.normalize(t,!0))!==a&&(this.trigger("remove",{content:this._items[t],position:t}),this._items[t].remove(),this._items.splice(t,1),this._mergers.splice(t,1),this.invalidate("items"),this.trigger("removed",{content:null,position:t}))},l.prototype.preloadAutoWidthImages=function(t){t.each(h.proxy(function(t,e){this.enter("pre-loading"),e=h(e),h(new Image).one("load",h.proxy(function(t){e.attr("src",t.target.src),e.css("opacity",1),this.leave("pre-loading"),!this.is("pre-loading")&&!this.is("initializing")&&this.refresh()},this)).attr("src",e.attr("src")||e.attr("data-src")||e.attr("data-src-retina"))},this))},l.prototype.destroy=function(){for(var t in this.$element.off(".owl.core"),this.$stage.off(".owl.core"),h(n).off(".owl.core"),!1!==this.settings.responsive&&(i.clearTimeout(this.resizeTimer),this.off(i,"resize",this._handlers.onThrottledResize)),this._plugins)this._plugins[t].destroy();this.$stage.children(".cloned").remove(),this.$stage.unwrap(),this.$stage.children().contents().unwrap(),this.$stage.children().unwrap(),this.$stage.remove(),this.$element.removeClass(this.options.refreshClass).removeClass(this.options.loadingClass).removeClass(this.options.loadedClass).removeClass(this.options.rtlClass).removeClass(this.options.dragClass).removeClass(this.options.grabClass).attr("class",this.$element.attr("class").replace(new RegExp(this.options.responsiveClass+"-\\S+\\s","g"),"")).removeData("owl.carousel")},l.prototype.op=function(t,e,i){var s=this.settings.rtl;switch(e){case"<":return s?i<t:t<i;case">":return s?t<i:i<t;case">=":return s?t<=i:i<=t;case"<=":return s?i<=t:t<=i}},l.prototype.on=function(t,e,i,s){t.addEventListener?t.addEventListener(e,i,s):t.attachEvent&&t.attachEvent("on"+e,i)},l.prototype.off=function(t,e,i,s){t.removeEventListener?t.removeEventListener(e,i,s):t.detachEvent&&t.detachEvent("on"+e,i)},l.prototype.trigger=function(t,e,i,s,n){var o={item:{count:this._items.length,index:this.current()}},r=h.camelCase(h.grep(["on",t,i],function(t){return t}).join("-").toLowerCase()),a=h.Event([t,"owl",i||"carousel"].join(".").toLowerCase(),h.extend({relatedTarget:this},o,e));return this._supress[t]||(h.each(this._plugins,function(t,e){e.onTrigger&&e.onTrigger(a)}),this.register({type:l.Type.Event,name:t}),this.$element.trigger(a),this.settings&&"function"==typeof this.settings[r]&&this.settings[r].call(this,a)),a},l.prototype.enter=function(t){h.each([t].concat(this._states.tags[t]||[]),h.proxy(function(t,e){this._states.current[e]===a&&(this._states.current[e]=0),this._states.current[e]++},this))},l.prototype.leave=function(t){h.each([t].concat(this._states.tags[t]||[]),h.proxy(function(t,e){this._states.current[e]--},this))},l.prototype.register=function(i){if(i.type===l.Type.Event){if(h.event.special[i.name]||(h.event.special[i.name]={}),!h.event.special[i.name].owl){var e=h.event.special[i.name]._default;h.event.special[i.name]._default=function(t){return!e||!e.apply||t.namespace&&-1!==t.namespace.indexOf("owl")?t.namespace&&-1<t.namespace.indexOf("owl"):e.apply(this,arguments)},h.event.special[i.name].owl=!0}}else i.type===l.Type.State&&(this._states.tags[i.name]?this._states.tags[i.name]=this._states.tags[i.name].concat(i.tags):this._states.tags[i.name]=i.tags,this._states.tags[i.name]=h.grep(this._states.tags[i.name],h.proxy(function(t,e){return h.inArray(t,this._states.tags[i.name])===e},this)))},l.prototype.suppress=function(t){h.each(t,h.proxy(function(t,e){this._supress[e]=!0},this))},l.prototype.release=function(t){h.each(t,h.proxy(function(t,e){delete this._supress[e]},this))},l.prototype.pointer=function(t){var e={x:null,y:null};return(t=(t=t.originalEvent||t||i.event).touches&&t.touches.length?t.touches[0]:t.changedTouches&&t.changedTouches.length?t.changedTouches[0]:t).pageX?(e.x=t.pageX,e.y=t.pageY):(e.x=t.clientX,e.y=t.clientY),e},l.prototype.isNumeric=function(t){return!isNaN(parseFloat(t))},l.prototype.difference=function(t,e){return{x:t.x-e.x,y:t.y-e.y}},h.fn.owlCarousel=function(e){var s=Array.prototype.slice.call(arguments,1);return this.each(function(){var t=h(this),i=t.data("owl.carousel");i||(i=new l(this,"object"==typeof e&&e),t.data("owl.carousel",i),h.each(["next","prev","to","destroy","refresh","replace","add","remove"],function(t,e){i.register({type:l.Type.Event,name:e}),i.$element.on(e+".owl.carousel.core",h.proxy(function(t){t.namespace&&t.relatedTarget!==this&&(this.suppress([e]),i[e].apply(this,[].slice.call(arguments,1)),this.release([e]))},i))})),"string"==typeof e&&"_"!==e.charAt(0)&&i[e].apply(i,s)})},h.fn.owlCarousel.Constructor=l}(window.Zepto||window.jQuery,window,document),function(e,i,t,s){var n=function(t){this._core=t,this._interval=null,this._visible=null,this._handlers={"initialized.owl.carousel":e.proxy(function(t){t.namespace&&this._core.settings.autoRefresh&&this.watch()},this)},this._core.options=e.extend({},n.Defaults,this._core.options),this._core.$element.on(this._handlers)};n.Defaults={autoRefresh:!0,autoRefreshInterval:500},n.prototype.watch=function(){this._interval||(this._visible=this._core.isVisible(),this._interval=i.setInterval(e.proxy(this.refresh,this),this._core.settings.autoRefreshInterval))},n.prototype.refresh=function(){this._core.isVisible()!==this._visible&&(this._visible=!this._visible,this._core.$element.toggleClass("owl-hidden",!this._visible),this._visible&&this._core.invalidate("width")&&this._core.refresh())},n.prototype.destroy=function(){var t,e;for(t in i.clearInterval(this._interval),this._handlers)this._core.$element.off(t,this._handlers[t]);for(e in Object.getOwnPropertyNames(this))"function"!=typeof this[e]&&(this[e]=null)},e.fn.owlCarousel.Constructor.Plugins.AutoRefresh=n}(window.Zepto||window.jQuery,window,document),function(a,o,t,e){var i=function(t){this._core=t,this._loaded=[],this._handlers={"initialized.owl.carousel change.owl.carousel resized.owl.carousel":a.proxy(function(t){if(t.namespace&&this._core.settings&&this._core.settings.lazyLoad&&(t.property&&"position"==t.property.name||"initialized"==t.type))for(var e=this._core.settings,i=e.center&&Math.ceil(e.items/2)||e.items,s=e.center&&-1*i||0,n=(t.property&&void 0!==t.property.value?t.property.value:this._core.current())+s,o=this._core.clones().length,r=a.proxy(function(t,e){this.load(e)},this);s++<i;)this.load(o/2+this._core.relative(n)),o&&a.each(this._core.clones(this._core.relative(n)),r),n++},this)},this._core.options=a.extend({},i.Defaults,this._core.options),this._core.$element.on(this._handlers)};i.Defaults={lazyLoad:!1},i.prototype.load=function(t){var e=this._core.$stage.children().eq(t),i=e&&e.find(".owl-lazy");!i||-1<a.inArray(e.get(0),this._loaded)||(i.each(a.proxy(function(t,e){var i,s=a(e),n=1<o.devicePixelRatio&&s.attr("data-src-retina")||s.attr("data-src")||s.attr("data-srcset");this._core.trigger("load",{element:s,url:n},"lazy"),s.is("img")?s.one("load.owl.lazy",a.proxy(function(){s.css("opacity",1),this._core.trigger("loaded",{element:s,url:n},"lazy")},this)).attr("src",n):s.is("source")?s.one("load.owl.lazy",a.proxy(function(){this._core.trigger("loaded",{element:s,url:n},"lazy")},this)).attr("srcset",n):((i=new Image).onload=a.proxy(function(){s.css({"background-image":'url("'+n+'")',opacity:"1"}),this._core.trigger("loaded",{element:s,url:n},"lazy")},this),i.src=n)},this)),this._loaded.push(e.get(0)))},i.prototype.destroy=function(){var t,e;for(t in this.handlers)this._core.$element.off(t,this.handlers[t]);for(e in Object.getOwnPropertyNames(this))"function"!=typeof this[e]&&(this[e]=null)},a.fn.owlCarousel.Constructor.Plugins.Lazy=i}(window.Zepto||window.jQuery,window,document),function(o,i,t,e){var s=function(t){this._core=t,this._handlers={"initialized.owl.carousel refreshed.owl.carousel":o.proxy(function(t){t.namespace&&this._core.settings.autoHeight&&this.update()},this),"changed.owl.carousel":o.proxy(function(t){t.namespace&&this._core.settings.autoHeight&&"position"===t.property.name&&(console.log("update called"),this.update())},this),"loaded.owl.lazy":o.proxy(function(t){t.namespace&&this._core.settings.autoHeight&&t.element.closest("."+this._core.settings.itemClass).index()===this._core.current()&&this.update()},this)},this._core.options=o.extend({},s.Defaults,this._core.options),this._core.$element.on(this._handlers),this._intervalId=null;var e=this;o(i).on("load",function(){e._core.settings.autoHeight&&e.update()}),o(i).resize(function(){e._core.settings.autoHeight&&(null!=e._intervalId&&clearTimeout(e._intervalId),e._intervalId=setTimeout(function(){e.update()},250))})};s.Defaults={autoHeight:!1,autoHeightClass:"owl-height"},s.prototype.update=function(){var t,e=this._core._current,i=e+this._core.settings.items,s=this._core.$stage.children().toArray().slice(e,i),n=[];o.each(s,function(t,e){n.push(o(e).height())}),t=Math.max.apply(null,n),this._core.$stage.parent().height(t).addClass(this._core.settings.autoHeightClass)},s.prototype.destroy=function(){var t,e;for(t in this._handlers)this._core.$element.off(t,this._handlers[t]);for(e in Object.getOwnPropertyNames(this))"function"!=typeof this[e]&&(this[e]=null)},o.fn.owlCarousel.Constructor.Plugins.AutoHeight=s}(window.Zepto||window.jQuery,window,document),function(c,t,e,i){var s=function(t){this._core=t,this._videos={},this._playing=null,this._handlers={"initialized.owl.carousel":c.proxy(function(t){t.namespace&&this._core.register({type:"state",name:"playing",tags:["interacting"]})},this),"resize.owl.carousel":c.proxy(function(t){t.namespace&&this._core.settings.video&&this.isInFullScreen()&&t.preventDefault()},this),"refreshed.owl.carousel":c.proxy(function(t){t.namespace&&this._core.is("resizing")&&this._core.$stage.find(".cloned .owl-video-frame").remove()},this),"changed.owl.carousel":c.proxy(function(t){t.namespace&&"position"===t.property.name&&this._playing&&this.stop()},this),"prepared.owl.carousel":c.proxy(function(t){if(t.namespace){var e=c(t.content).find(".owl-video");e.length&&(e.css("display","none"),this.fetch(e,c(t.content)))}},this)},this._core.options=c.extend({},s.Defaults,this._core.options),this._core.$element.on(this._handlers),this._core.$element.on("click.owl.video",".owl-video-play-icon",c.proxy(function(t){this.play(t)},this))};s.Defaults={video:!1,videoHeight:!1,videoWidth:!1},s.prototype.fetch=function(t,e){var i=t.attr("data-vimeo-id")?"vimeo":t.attr("data-vzaar-id")?"vzaar":"youtube",s=t.attr("data-vimeo-id")||t.attr("data-youtube-id")||t.attr("data-vzaar-id"),n=t.attr("data-width")||this._core.settings.videoWidth,o=t.attr("data-height")||this._core.settings.videoHeight,r=t.attr("href");if(!r)throw new Error("Missing video URL.");if(-1<(s=r.match(/(http:|https:|)\/\/(player.|www.|app.)?(vimeo\.com|youtu(be\.com|\.be|be\.googleapis\.com)|vzaar\.com)\/(video\/|videos\/|embed\/|channels\/.+\/|groups\/.+\/|watch\?v=|v\/)?([A-Za-z0-9._%-]*)(\&\S+)?/))[3].indexOf("youtu"))i="youtube";else if(-1<s[3].indexOf("vimeo"))i="vimeo";else{if(!(-1<s[3].indexOf("vzaar")))throw new Error("Video URL not supported.");i="vzaar"}s=s[6],this._videos[r]={type:i,id:s,width:n,height:o},e.attr("data-video",r),this.thumbnail(t,this._videos[r])},s.prototype.thumbnail=function(e,t){var i,s,n=t.width&&t.height?'style="width:'+t.width+"px;height:"+t.height+'px;"':"",o=e.find("img"),r="src",a="",h=this._core.settings,l=function(t){'<div class="owl-video-play-icon"></div>',i=h.lazyLoad?'<div class="owl-video-tn '+a+'" '+r+'="'+t+'"></div>':'<div class="owl-video-tn" style="opacity:1;background-image:url('+t+')"></div>',e.after(i),e.after('<div class="owl-video-play-icon"></div>')};if(e.wrap('<div class="owl-video-wrapper"'+n+"></div>"),this._core.settings.lazyLoad&&(r="data-src",a="owl-lazy"),o.length)return l(o.attr(r)),o.remove(),!1;"youtube"===t.type?(s="//img.youtube.com/vi/"+t.id+"/hqdefault.jpg",l(s)):"vimeo"===t.type?c.ajax({type:"GET",url:"//vimeo.com/api/v2/video/"+t.id+".json",jsonp:"callback",dataType:"jsonp",success:function(t){s=t[0].thumbnail_large,l(s)}}):"vzaar"===t.type&&c.ajax({type:"GET",url:"//vzaar.com/api/videos/"+t.id+".json",jsonp:"callback",dataType:"jsonp",success:function(t){s=t.framegrab_url,l(s)}})},s.prototype.stop=function(){this._core.trigger("stop",null,"video"),this._playing.find(".owl-video-frame").remove(),this._playing.removeClass("owl-video-playing"),this._playing=null,this._core.leave("playing"),this._core.trigger("stopped",null,"video")},s.prototype.play=function(t){var e,i=c(t.target).closest("."+this._core.settings.itemClass),s=this._videos[i.attr("data-video")],n=s.width||"100%",o=s.height||this._core.$stage.height();this._playing||(this._core.enter("playing"),this._core.trigger("play",null,"video"),i=this._core.items(this._core.relative(i.index())),this._core.reset(i.index()),"youtube"===s.type?e='<iframe width="'+n+'" height="'+o+'" src="//www.youtube.com/embed/'+s.id+"?autoplay=1&rel=0&v="+s.id+'" frameborder="0" allowfullscreen></iframe>':"vimeo"===s.type?e='<iframe src="//player.vimeo.com/video/'+s.id+'?autoplay=1" width="'+n+'" height="'+o+'" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>':"vzaar"===s.type&&(e='<iframe frameborder="0"height="'+o+'"width="'+n+'" allowfullscreen mozallowfullscreen webkitAllowFullScreen src="//view.vzaar.com/'+s.id+'/player?autoplay=true"></iframe>'),c('<div class="owl-video-frame">'+e+"</div>").insertAfter(i.find(".owl-video")),this._playing=i.addClass("owl-video-playing"))},s.prototype.isInFullScreen=function(){var t=e.fullscreenElement||e.mozFullScreenElement||e.webkitFullscreenElement;return t&&c(t).parent().hasClass("owl-video-frame")},s.prototype.destroy=function(){var t,e;for(t in this._core.$element.off("click.owl.video"),this._handlers)this._core.$element.off(t,this._handlers[t]);for(e in Object.getOwnPropertyNames(this))"function"!=typeof this[e]&&(this[e]=null)},c.fn.owlCarousel.Constructor.Plugins.Video=s}(window.Zepto||window.jQuery,window,document),function(r,t,e,i){var s=function(t){this.core=t,this.core.options=r.extend({},s.Defaults,this.core.options),this.swapping=!0,this.previous=void 0,this.next=void 0,this.handlers={"change.owl.carousel":r.proxy(function(t){t.namespace&&"position"==t.property.name&&(this.previous=this.core.current(),this.next=t.property.value)},this),"drag.owl.carousel dragged.owl.carousel translated.owl.carousel":r.proxy(function(t){t.namespace&&(this.swapping="translated"==t.type)},this),"translate.owl.carousel":r.proxy(function(t){t.namespace&&this.swapping&&(this.core.options.animateOut||this.core.options.animateIn)&&this.swap()},this)},this.core.$element.on(this.handlers)};s.Defaults={animateOut:!1,animateIn:!1},s.prototype.swap=function(){if(1===this.core.settings.items&&r.support.animation&&r.support.transition){this.core.speed(0);var t,e=r.proxy(this.clear,this),i=this.core.$stage.children().eq(this.previous),s=this.core.$stage.children().eq(this.next),n=this.core.settings.animateIn,o=this.core.settings.animateOut;this.core.current()!==this.previous&&(o&&(t=this.core.coordinates(this.previous)-this.core.coordinates(this.next),i.one(r.support.animation.end,e).css({left:t+"px"}).addClass("animated owl-animated-out").addClass(o)),n&&s.one(r.support.animation.end,e).addClass("animated owl-animated-in").addClass(n))}},s.prototype.clear=function(t){r(t.target).css({left:""}).removeClass("animated owl-animated-out owl-animated-in").removeClass(this.core.settings.animateIn).removeClass(this.core.settings.animateOut),this.core.onTransitionEnd()},s.prototype.destroy=function(){var t,e;for(t in this.handlers)this.core.$element.off(t,this.handlers[t]);for(e in Object.getOwnPropertyNames(this))"function"!=typeof this[e]&&(this[e]=null)},r.fn.owlCarousel.Constructor.Plugins.Animate=s}(window.Zepto||window.jQuery,window,document),function(s,n,e,t){var i=function(t){this._core=t,this._call=null,this._time=0,this._timeout=0,this._paused=!0,this._handlers={"changed.owl.carousel":s.proxy(function(t){t.namespace&&"settings"===t.property.name?this._core.settings.autoplay?this.play():this.stop():t.namespace&&"position"===t.property.name&&this._paused&&(this._time=0)},this),"initialized.owl.carousel":s.proxy(function(t){t.namespace&&this._core.settings.autoplay&&this.play()},this),"play.owl.autoplay":s.proxy(function(t,e,i){t.namespace&&this.play(e,i)},this),"stop.owl.autoplay":s.proxy(function(t){t.namespace&&this.stop()},this),"mouseover.owl.autoplay":s.proxy(function(){this._core.settings.autoplayHoverPause&&this._core.is("rotating")&&this.pause()},this),"mouseleave.owl.autoplay":s.proxy(function(){this._core.settings.autoplayHoverPause&&this._core.is("rotating")&&this.play()},this),"touchstart.owl.core":s.proxy(function(){this._core.settings.autoplayHoverPause&&this._core.is("rotating")&&this.pause()},this),"touchend.owl.core":s.proxy(function(){this._core.settings.autoplayHoverPause&&this.play()},this)},this._core.$element.on(this._handlers),this._core.options=s.extend({},i.Defaults,this._core.options)};i.Defaults={autoplay:!1,autoplayTimeout:5e3,autoplayHoverPause:!1,autoplaySpeed:!1},i.prototype._next=function(t){this._call=n.setTimeout(s.proxy(this._next,this,t),this._timeout*(Math.round(this.read()/this._timeout)+1)-this.read()),this._core.is("interacting")||e.hidden||this._core.next(t||this._core.settings.autoplaySpeed)},i.prototype.read=function(){return(new Date).getTime()-this._time},i.prototype.play=function(t,e){var i;this._core.is("rotating")||this._core.enter("rotating"),t=t||this._core.settings.autoplayTimeout,i=Math.min(this._time%(this._timeout||t),t),this._paused?(this._time=this.read(),this._paused=!1):n.clearTimeout(this._call),this._time+=this.read()%t-i,this._timeout=t,this._call=n.setTimeout(s.proxy(this._next,this,e),t-i)},i.prototype.stop=function(){this._core.is("rotating")&&(this._time=0,this._paused=!0,n.clearTimeout(this._call),this._core.leave("rotating"))},i.prototype.pause=function(){this._core.is("rotating")&&!this._paused&&(this._time=this.read(),this._paused=!0,n.clearTimeout(this._call))},i.prototype.destroy=function(){var t,e;for(t in this.stop(),this._handlers)this._core.$element.off(t,this._handlers[t]);for(e in Object.getOwnPropertyNames(this))"function"!=typeof this[e]&&(this[e]=null)},s.fn.owlCarousel.Constructor.Plugins.autoplay=i}(window.Zepto||window.jQuery,window,document),function(o,t,e,i){"use strict";var s=function(t){this._core=t,this._initialized=!1,this._pages=[],this._controls={},this._templates=[],this.$element=this._core.$element,this._overrides={next:this._core.next,prev:this._core.prev,to:this._core.to},this._handlers={"prepared.owl.carousel":o.proxy(function(t){t.namespace&&this._core.settings.dotsData&&this._templates.push('<div class="'+this._core.settings.dotClass+'">'+o(t.content).find("[data-dot]").addBack("[data-dot]").attr("data-dot")+"</div>")},this),"added.owl.carousel":o.proxy(function(t){t.namespace&&this._core.settings.dotsData&&this._templates.splice(t.position,0,this._templates.pop())},this),"remove.owl.carousel":o.proxy(function(t){t.namespace&&this._core.settings.dotsData&&this._templates.splice(t.position,1)},this),"changed.owl.carousel":o.proxy(function(t){t.namespace&&"position"==t.property.name&&this.draw()},this),"initialized.owl.carousel":o.proxy(function(t){t.namespace&&!this._initialized&&(this._core.trigger("initialize",null,"navigation"),this.initialize(),this.update(),this.draw(),this._initialized=!0,this._core.trigger("initialized",null,"navigation"))},this),"refreshed.owl.carousel":o.proxy(function(t){t.namespace&&this._initialized&&(this._core.trigger("refresh",null,"navigation"),this.update(),this.draw(),this._core.trigger("refreshed",null,"navigation"))},this)},this._core.options=o.extend({},s.Defaults,this._core.options),this.$element.on(this._handlers)};s.Defaults={nav:!1,navText:['<span aria-label="Previous">&#x2039;</span>','<span aria-label="Next">&#x203a;</span>'],navSpeed:!1,navElement:'button type="button" role="presentation"',navContainer:!1,navContainerClass:"owl-nav",navClass:["owl-prev","owl-next"],slideBy:1,dotClass:"owl-dot",dotsClass:"owl-dots",dots:!0,dotsEach:!1,dotsData:!1,dotsSpeed:!1,dotsContainer:!1},s.prototype.initialize=function(){var t,i=this._core.settings;for(t in this._controls.$relative=(i.navContainer?o(i.navContainer):o("<div>").addClass(i.navContainerClass).appendTo(this.$element)).addClass("disabled"),this._controls.$previous=o("<"+i.navElement+">").addClass(i.navClass[0]).html(i.navText[0]).prependTo(this._controls.$relative).on("click",o.proxy(function(t){this.prev(i.navSpeed)},this)),this._controls.$next=o("<"+i.navElement+">").addClass(i.navClass[1]).html(i.navText[1]).appendTo(this._controls.$relative).on("click",o.proxy(function(t){this.next(i.navSpeed)},this)),i.dotsData||(this._templates=[o('<button role="button">').addClass(i.dotClass).append(o("<span>")).prop("outerHTML")]),this._controls.$absolute=(i.dotsContainer?o(i.dotsContainer):o("<div>").addClass(i.dotsClass).appendTo(this.$element)).addClass("disabled"),this._controls.$absolute.on("click","button",o.proxy(function(t){var e=o(t.target).parent().is(this._controls.$absolute)?o(t.target).index():o(t.target).parent().index();t.preventDefault(),this.to(e,i.dotsSpeed)},this)),this._overrides)this._core[t]=o.proxy(this[t],this)},s.prototype.destroy=function(){var t,e,i,s,n;for(t in n=this._core.settings,this._handlers)this.$element.off(t,this._handlers[t]);for(e in this._controls)"$relative"===e&&n.navContainer?this._controls[e].html(""):this._controls[e].remove();for(s in this.overides)this._core[s]=this._overrides[s];for(i in Object.getOwnPropertyNames(this))"function"!=typeof this[i]&&(this[i]=null)},s.prototype.update=function(){var t,e,i=this._core.clones().length/2,s=i+this._core.items().length,n=this._core.maximum(!0),o=this._core.settings,r=o.center||o.autoWidth||o.dotsData?1:o.dotsEach||o.items;if("page"!==o.slideBy&&(o.slideBy=Math.min(o.slideBy,o.items)),o.dots||"page"==o.slideBy)for(this._pages=[],t=i,e=0;t<s;t++){if(r<=e||0===e){if(this._pages.push({start:Math.min(n,t-i),end:t-i+r-1}),Math.min(n,t-i)===n)break;e=0,0}e+=this._core.mergers(this._core.relative(t))}},s.prototype.draw=function(){var t,e=this._core.settings,i=this._core.items().length<=e.items,s=this._core.relative(this._core.current()),n=e.loop||e.rewind;this._controls.$relative.toggleClass("disabled",!e.nav||i),e.nav&&(this._controls.$previous.toggleClass("disabled",!n&&s<=this._core.minimum(!0)),this._controls.$next.toggleClass("disabled",!n&&s>=this._core.maximum(!0))),this._controls.$absolute.toggleClass("disabled",!e.dots||i),e.dots&&(t=this._pages.length-this._controls.$absolute.children().length,e.dotsData&&0!==t?this._controls.$absolute.html(this._templates.join("")):0<t?this._controls.$absolute.append(new Array(t+1).join(this._templates[0])):t<0&&this._controls.$absolute.children().slice(t).remove(),this._controls.$absolute.find(".active").removeClass("active"),this._controls.$absolute.children().eq(o.inArray(this.current(),this._pages)).addClass("active"))},s.prototype.onTrigger=function(t){var e=this._core.settings;t.page={index:o.inArray(this.current(),this._pages),count:this._pages.length,size:e&&(e.center||e.autoWidth||e.dotsData?1:e.dotsEach||e.items)}},s.prototype.current=function(){var i=this._core.relative(this._core.current());return o.grep(this._pages,o.proxy(function(t,e){return t.start<=i&&t.end>=i},this)).pop()},s.prototype.getPosition=function(t){var e,i,s=this._core.settings;return"page"==s.slideBy?(e=o.inArray(this.current(),this._pages),i=this._pages.length,t?++e:--e,e=this._pages[(e%i+i)%i].start):(e=this._core.relative(this._core.current()),i=this._core.items().length,t?e+=s.slideBy:e-=s.slideBy),e},s.prototype.next=function(t){o.proxy(this._overrides.to,this._core)(this.getPosition(!0),t)},s.prototype.prev=function(t){o.proxy(this._overrides.to,this._core)(this.getPosition(!1),t)},s.prototype.to=function(t,e,i){var s;!i&&this._pages.length?(s=this._pages.length,o.proxy(this._overrides.to,this._core)(this._pages[(t%s+s)%s].start,e)):o.proxy(this._overrides.to,this._core)(t,e)},o.fn.owlCarousel.Constructor.Plugins.Navigation=s}(window.Zepto||window.jQuery,window,document),function(s,n,t,e){"use strict";var i=function(t){this._core=t,this._hashes={},this.$element=this._core.$element,this._handlers={"initialized.owl.carousel":s.proxy(function(t){t.namespace&&"URLHash"===this._core.settings.startPosition&&s(n).trigger("hashchange.owl.navigation")},this),"prepared.owl.carousel":s.proxy(function(t){if(t.namespace){var e=s(t.content).find("[data-hash]").addBack("[data-hash]").attr("data-hash");if(!e)return;this._hashes[e]=t.content}},this),"changed.owl.carousel":s.proxy(function(t){if(t.namespace&&"position"===t.property.name){var i=this._core.items(this._core.relative(this._core.current())),e=s.map(this._hashes,function(t,e){return t===i?e:null}).join();if(!e||n.location.hash.slice(1)===e)return;n.location.hash=e}},this)},this._core.options=s.extend({},i.Defaults,this._core.options),this.$element.on(this._handlers),s(n).on("hashchange.owl.navigation",s.proxy(function(t){var e=n.location.hash.substring(1),i=this._core.$stage.children(),s=this._hashes[e]&&i.index(this._hashes[e]);void 0!==s&&s!==this._core.current()&&this._core.to(this._core.relative(s),!1,!0)},this))};i.Defaults={URLhashListener:!1},i.prototype.destroy=function(){var t,e;for(t in s(n).off("hashchange.owl.navigation"),this._handlers)this._core.$element.off(t,this._handlers[t]);for(e in Object.getOwnPropertyNames(this))"function"!=typeof this[e]&&(this[e]=null)},s.fn.owlCarousel.Constructor.Plugins.Hash=i}(window.Zepto||window.jQuery,window,document),function(n,t,e,o){var r=n("<support>").get(0).style,a="Webkit Moz O ms".split(" "),i={transition:{end:{WebkitTransition:"webkitTransitionEnd",MozTransition:"transitionend",OTransition:"oTransitionEnd",transition:"transitionend"}},animation:{end:{WebkitAnimation:"webkitAnimationEnd",MozAnimation:"animationend",OAnimation:"oAnimationEnd",animation:"animationend"}}},s=function(){return!!c("transform")},h=function(){return!!c("perspective")},l=function(){return!!c("animation")};function c(t,i){var s=!1,e=t.charAt(0).toUpperCase()+t.slice(1);return n.each((t+" "+a.join(e+" ")+e).split(" "),function(t,e){if(r[e]!==o)return s=!i||e,!1}),s}function p(t){return c(t,!0)}(function(){return!!c("transition")})()&&(n.support.transition=new String(p("transition")),n.support.transition.end=i.transition.end[n.support.transition]),l()&&(n.support.animation=new String(p("animation")),n.support.animation.end=i.animation.end[n.support.animation]),s()&&(n.support.transform=new String(p("transform")),n.support.transform3d=h())}(window.Zepto||window.jQuery,window,document);
