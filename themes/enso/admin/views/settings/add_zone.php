<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<style>
    .padlr0{

        padding-left: 0px;
    }
</style>

<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i>
            </button>
            <h4 class="modal-title" id="myModalLabel">Add Zone</h4>
        </div>
        <?php $attrib = array('data-toggle' => 'validator', 'role' => 'form');
        echo admin_form_open("system_settings/add_zone", $attrib); ?>
        <div class="modal-body">
            <p><?= lang('enter_info'); ?></p>

            <div class="form-group">
                <label>Name*</label>
                <?= form_input('name', set_value('name'), 'class="form-control tip" id="name" required="required"'); ?>
            </div>

            <div class="form-group">
                <label>Description </label>
                <textarea name="description" class="form-control" required placeholder="Description"></textarea>
            </div>


            <div class="" id="zonezhtml"></div>
            <div class="col-sm-12 padlr0">
            <a class="btn btn-success" id="add_more_zone"> Add More Zones</a>
            </div>
            <div class="clearfix"></div>


        </div>
        <div class="modal-footer">
            <?= form_submit('add_zone', 'Add Zone', 'class="btn btn-primary"'); ?>
        </div>
    </div>
    <?= form_close(); ?>
</div>
<?= $modal_js ?>
<script>
    var countries = <?=json_encode($countries);?>;
    var zones_options = <?=json_encode($zones_options);?>;
    console.log(countries);
    console.log(zones_options);
    var count =1;

    $('#add_more_zone').on('click', function () {

        var html = '';
        html += '<div id="zonez" class="zonez">';
        html += '<div class="form-group col-sm-5 padlr0">';
        html += '<label>Country</label>';
        html += '<select name="country_id[]" class="country_change country_id'+count+'" id="country_id" style="width: 100%">';
            $.each(countries, function (key, value) {

                html += '<option value="' + value.country_id + '">' + value.name + '</option>';

        });

        html += '</select>';
        html += '</div>';
        html += '<div class="form-group col-sm-5 padlr0">';
        html += '<label>Area</label>';
        html += '<select name="zone_id[]" class="zone_change zone_id'+count+'" id="zone_id" style="width: 100%">';
        html += '<option value="0">All Areas</option>';
        $.each(zones_options, function (key, value) {
            html += '<option value="' + value.zone_id + '">' + value.name + '</option>';

        });

        html += '</select>';
        html += '</div>';
        html += '<div class="form-group col-sm-2" style="padding-top: 30px">';
        html += '<a class="btn btn-success pull-right remVar" id="add_more_variant1"><i class="fa fa-trash"></i></a>';
        html += '</div>';
        html += '</div>';
        $('#zonezhtml').append(html);
        $(".country_id"+count).select2();
        $('.zone_id'+count).select2();
        count= count+1;
        $('.remVar').on('click', function () {
            $(this).parents('.zonez').remove();
            return false;
        });
        $('.country_change').on('change', function () {
            var country_id = $(this).val();
            var that = $(this);

            $.ajax({
                type: "get",
                //async: false,
                url: "<?= admin_url('system_settings/getAllZonesById') ?>/" + country_id,
                dataType: "HTML",
                success: function (data) {
                    that.parent().next().find('.zone_change').select2("destroy");
                    that.parent().next().find('.zone_change option').remove();
                    that.parent().next().find('.zone_change').append(data);
                    that.parent().next().find('.zone_change').select2();
                    // that.parent().next().find('.zone_change').on("change", function (e) {


                },
                error: function () {
                }
            });

        });

    });


    $('#country_id').on('change', function () {
        var country_id = $(this).val();
        $.ajax({
            type: "get",
            //async: false,
            url: "<?= admin_url('system_settings/getAllZonesById') ?>/" + country_id,
            dataType: "HTML",
            success: function (data) {
                $(".zone_id").select2("destroy");
                $(".zone_id option").remove();
                $(".zone_id").append(data);
                $(".zone_id").select2();
                // that.parent().next().find('.select-tagse').on("change", function (e) {


            },
            error: function () {
            }
        });
    });
</script>

