<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="box">
    <div class="box-header">
        <h2 class="blue"><i class="fa-fw fa fa-bullhorn"></i><?= lang('sizing_guide'); ?></h2>
        <div class="box-icon">
            <ul class="btn-tasks">
                <li class="dropdown">
                    <a data-toggle="dropdown" class="dropdown-toggle" href="#"><i class="icon fa fa-tasks tip" data-placement="left" title="<?= lang("actions") ?>"></i></a>
                    <ul class="dropdown-menu pull-right tasks-menus" role="menu" aria-labelledby="dLabel">
                        <li>
                            <a href="<?= admin_url('products/create_sizing_guide') ?>">
                                <i class="fa fa-plus-circle"></i> <?= lang('create_sizing_guide') ?>
                            </a>
                        </li>
                        <!--<li class="divider"></li>-->
                        <li class="hide"><a href="#" id="deletePromotions" data-action="delete"><i class="fa fa-trash-o"></i> <?= lang('delete_promotions') ?></a></li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
    <div class="box-content">
        <div class="row">
            <div class="col-lg-12">
                <p class="introtext"><?php echo $this->lang->line("All the sizing guide listed here."); ?></p>

                <div class="table-responsive">
                    <table id="GPData" class="table table-bordered table-hover table-striped">
                        <thead>
                        <tr>
                            <th>Title</th>
                            <th>Actions</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        foreach ($sizes as $size) {
                            ?>
                            <tr>
                                <td class="text-center"><?= $size->title; ?></td>
                                <td class="text-center">
                                    <?php echo '<a class="tip" title="' . $this->lang->line("edit_size") . '" href="' . admin_url('products/edit_sizing_guide/' . $size->id) . '"><i class="fa fa-edit"></i></a> <a href="#" class="tip po" title="' . $this->lang->line("delete_size") . '" data-content="<p>' . lang('r_u_sure') . '</p><a class=\'btn btn-danger\' href=\'' . admin_url('products/delete_sizing_guide/' . $size->id) . '\'>' . lang('i_m_sure') . '</a> <button class=\'btn po-close\'>' . lang('no') . '</button>"><i class="fa fa-trash-o"></i></a>'; ?>
                                </td>
                            </tr>
                        <?php
                        }
                        ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>