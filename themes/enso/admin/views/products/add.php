<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<?php
//Get product sizes(Extras)
$sizes_data = $this->site->getAllSizeGuide();

if (!empty($variants)) {
    foreach ($variants as $variant) {
        $vars[] = addslashes($variant->name);
    }
} else {
    $vars = array();
}

?>
<script type="text/javascript">
    $(document).ready(function () {
        $('.gen_slug').change(function (e) {
            getSlug($(this).val(), 'products');
        });
        $("#subcategory").select2("destroy").empty().attr("placeholder", "<?= lang('select_category_to_load') ?>").select2({
            placeholder: "<?= lang('select_category_to_load') ?>", minimumResultsForSearch: 7, data: [
                {id: '', text: '<?= lang('select_category_to_load') ?>'}
            ]
        });
        $('#category').change(function () {
            var v = $(this).val();
            $('#modal-loading').show();
            if (v) {
                $.ajax({
                    type: "get",
                    async: false,
                    url: "<?= admin_url('products/getSubCategories') ?>/" + v,
                    dataType: "json",
                    success: function (scdata) {
                        if (scdata != null) {
                            scdata.push({id: '', text: '<?= lang('select_subcategory') ?>'});
                            $("#subcategory").select2("destroy").empty().attr("placeholder", "<?= lang('select_subcategory') ?>").select2({
                                placeholder: "<?= lang('select_category_to_load') ?>",
                                minimumResultsForSearch: 7,
                                data: scdata
                            });
                        } else {
                            $("#subcategory").select2("destroy").empty().attr("placeholder", "<?= lang('no_subcategory') ?>").select2({
                                placeholder: "<?= lang('no_subcategory') ?>",
                                minimumResultsForSearch: 7,
                                data: [{id: '', text: '<?= lang('no_subcategory') ?>'}]
                            });
                        }
                    },
                    error: function () {
                        bootbox.alert('<?= lang('ajax_error') ?>');
                        $('#modal-loading').hide();
                    }
                });
            } else {
                $("#subcategory").select2("destroy").empty().attr("placeholder", "<?= lang('select_category_to_load') ?>").select2({
                    placeholder: "<?= lang('select_category_to_load') ?>",
                    minimumResultsForSearch: 7,
                    data: [{id: '', text: '<?= lang('select_category_to_load') ?>'}]
                });
            }
            $('#modal-loading').hide();
        });
        $('#code').bind('keypress', function (e) {
            if (e.keyCode == 13) {
                e.preventDefault();
                return false;
            }
        });
    });
</script>
<style>
    .form-control {
        border-radius: 3px !important;
    }

    .padding10 {
        padding: 10px !important;
    }

    .border_radius3 {
        border-radius: 3px !important;
    }

    .redactor_box, .redactor_toolbar {
        border-radius: 3px !important;
    }

    .grey_background {
        background: #FAFAF9 !important;
    }

    .paddingleftright0 {
        padding-right: 0px;
        padding-left: 0px;
    }

    .paddingtop15 {
        padding-top: 15px;
    }

    .paddingtop28 {
        padding-top: 28px;
    }

    .dz-progress {
        /* progress bar covers file name */
        /*display: none !important;*/
    }

    .dz-image img {
        width: 100% !important;
        height: 100% !important;
    }

    .dz-size {
        display: none !important;
    }

    .dropzone .dz-preview .dz-image {
        max-width: 115px !important;
        max-height: 115px !important;
    }

</style>
<style>
    .switch {
        position: relative;
        display: inline-block;
        width: 48px;
        height: 24px;
    }

    .switch input {display:none;}

    .slider {
        position: absolute;
        cursor: pointer;
        top: 0;
        left: 0;
        right: 0;
        bottom: 0;
        background-color: #ccc;
        -webkit-transition: .4s;
        transition: .4s;
    }

    .slider:before {
        position: absolute;
        content: "";
        height: 22px;
        width: 22px;
        left: 0px;
        bottom: 1px;
        background-color: white;
        -webkit-transition: .4s;
        transition: .4s;
    }

    input:checked + .slider {
        background-color: #00BA70;
    }

    input:focus + .slider {
        box-shadow: 0 0 1px #00BA70;
    }

    input:checked + .slider:before {
        -webkit-transform: translateX(26px);
        -ms-transform: translateX(26px);
        transform: translateX(26px);
    }

    /* Rounded sliders */
    .slider.round {
        border-radius: 34px;
    }

    .slider.round:before {
        border-radius: 50%;
    }
</style>
<div class="box">
    <div class="box-header">
        <h2 class="blue"><i class="fa-fw fa fa-plus"></i><?= lang('add_product'); ?></h2>

        <div class="box-icon">
            <ul class="btn-tasks">
                <li class="dropdown">
                    <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                        <i class="icon fa fa-tasks tip" data-placement="left" title="<?= lang("actions") ?>"></i>
                    </a>
                    <ul class="dropdown-menu pull-right tasks-menus" role="menu" aria-labelledby="dLabel">
                        <li>
                            <a href="<?= admin_url('products/import_csv'); ?>">
                                <i class="fa fa-plus-circle"></i> <?= lang('import_products') ?>
                            </a>
                        </li>
                        <li>
                            <a href="<?= admin_url('products/print_barcodes'); ?>">
                                <i class="fa fa-print"></i> <?= lang('print_barcode_label') ?>
                            </a>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
    <div class="box-content grey_background">
        <div class="row">
            <div class="col-lg-12">

                <p class="introtext"><?php echo lang('enter_info'); ?></p>

                <?php
                $attrib = array('data-toggle' => 'validator', 'role' => 'form');
                echo admin_form_open_multipart("products/add", $attrib)
                ?>

                <div class="col-md-7">
                    <div class="panel panel-default padding10 border_radius3">
                        <h2 class="blue">General Detail</h2>
                        <div class="form-group hidden">
                            <?= lang("product_type", "type"); ?>
                            <?php
                            $opts = array('standard' => lang('standard'), 'combo' => lang('combo'), 'digital' => lang('digital'), 'service' => lang('service'));
                            echo form_dropdown('type', $opts, (isset($_POST['type']) ? $_POST['type'] : ($product ? $product->type : '')), 'class="form-control " id="type" required="required"');
                            ?>
                        </div>

                        <div class="form-group all">
                            <?= lang("product_name", "name") ?>
                            <?= form_input('name', (isset($_POST['name']) ? $_POST['name'] : ($product ? $product->name : '')), 'class="form-control input-sm gen_slug" id="name" required="required"'); ?>
                        </div>
                        <div class="form-group all">
                            <?= lang('slug', 'slug'); ?>
                            <?= form_input('slug', set_value('slug'), 'class="form-control tip" id="slug" required="required"'); ?>
                        </div>

                        <div class="form-group all">
                            <?= lang("product_code", "code") ?>
                            <div class="input-group">
                                <?= form_input('code', (isset($_POST['code']) ? $_POST['code'] : ($product ? $product->code : '')), 'class="form-control" id="code"  required="required"') ?>
                                <span class="input-group-addon pointer" id="random_num" style="padding: 1px 10px;">
                                <i class="fa fa-random"></i>
                            </span>
                            </div>
                            <span class="help-block"><?= lang('you_scan_your_barcode_too') ?></span>
                        </div>


                        <div class="form-group all">
                            <?= lang("product_details", "product_details") ?>
                            <?= form_textarea('product_details', (isset($_POST['product_details']) ? $_POST['product_details'] : ($product ? $product->product_details : '')), 'class="form-control border_radius3" id="details"'); ?>
                        </div>

                    </div>

                    <div class="panel panel-default padding10 border_radius3">
                        <h2 class="blue">Images</h2>

                        <div class="form-group all">
                            <?= lang("product_front_image", "product_image") ?>
                            <div class="input-group">
                                <input type="hidden" id="pimage_id" name="image" readonly class="form-control mediaimage_id" placeholder="Select Image">
                                <input type="text" id="pimage"  readonly class="form-control mediaimage" placeholder="Select Image">

                                <span class="input-group-btn ">
                                    <button id="mediagallery" class="btn btn-primary mediagallery width120" type="button">Select Image</button>
                                </span>
                            </div>
                            <input class="alt_text" name="image_alt" value="" type="hidden">
                        </div>

                        <div class="form-group all text-writing">
                            <?= lang("product_back_image", "back_image") ?>
                            <div class="input-group">
                                <input type="hidden" id="pbimage_id" name="back_image" readonly class="form-control mediaimage_id" placeholder="Select Image">
                                <input type="text" id="pbimage"  readonly class="form-control mediaimage" placeholder="Select Image">
                                <span class="input-group-btn ">
                                    <button id="mediagallery" class="btn btn-primary mediagallery width120" type="button">Select Image</button>
                                </span>
                            </div>
                            <input class="alt_text" name="back_image_alt" value="" type="hidden">
                        </div>

                        <div class="form-group all text-writing">
                            <label for="product_image">Gallery Images</label>
                            <div class="input-group">
                                <input type="hidden" id="gimage1_id" name="gimage[]" readonly class="form-control mediaimage_id" placeholder="Select Image">
                                <input type="text" id="gimage1"  readonly class="form-control mediaimage" placeholder="Select Image">
                                <span class="input-group-btn ">
                                    <button id="mediagallery" class="btn btn-primary mediagallery width120" type="button">Select Image</button>
                                </span>
                            </div>
                            <input class="alt_text" name="gimage_alt[]" value="" type="hidden">
                        </div>




                        <div id="newgalleryimage"></div>

                        <a class="btn btn-success" id="add_more_gallery_image"> Add More Gallery Image</a>





                        <!-- <div id="myDropzone" class="dropzone "></div>
                        <input type="hidden" id="gallary_unique_code" name="gallary_unique_code"
                               value="<?php /*if (set_value('gallary_unique_code')) {
                                   echo set_value('gallary_unique_code');
                               } else {
                                   echo uniqid();
                               } */?>">-->

                        <!--  <div class="form-group all">
                            <? /*= lang("product_gallery_images", "images") */ ?>
                            <input id="images" type="file" data-browse-label="<? /*= lang('browse'); */ ?>" name="userfile[]"
                                   multiple="true" data-show-upload="false"
                                   data-show-preview="false" class="form-control file" accept="image/*">
                        </div>
                        <div id="img-details"></div>-->
                        <div class="clearfix"></div>
                    </div>

                    <div class="panel panel-default hidden padding10 border_radius3">
                        <h2 class="blue"></h2>


                        <div class="form-group all hidden">
                            <?= lang('second_name', 'second_name'); ?>
                            <?= form_input('second_name', set_value('second_name'), 'class="form-control tip" id="second_name"'); ?>
                        </div>


                        <div class="form-group all hidden">
                            <?= lang("barcode_symbology", "barcode_symbology") ?>
                            <?php
                            $bs = array('code25' => 'Code25', 'code39' => 'Code39', 'code128' => 'Code128', 'ean8' => 'EAN8', 'ean13' => 'EAN13', 'upca' => 'UPC-A', 'upce' => 'UPC-E');
                            echo form_dropdown('barcode_symbology', $bs, (isset($_POST['barcode_symbology']) ? $_POST['barcode_symbology'] : ($product ? $product->barcode_symbology : 'code128')), 'class="form-control select" id="barcode_symbology" required="required" style="width:100%;"');
                            ?>

                        </div>

                    </div>

                    <div class="panel panel-default padding10 border_radius3">
                        <h2 class="blue">Price & Tax Details </h2>
                        <?php if ($Owner || $Admin) { ?>
                            <div class="form-group standard">
                                <?= lang("product_cost", "cost") ?>
                                <?= form_input('cost', (isset($_POST['cost']) ? $_POST['cost'] : ($product ? $this->sma->formatDecimal($product->cost) : '')), 'class="form-control tip" required="required" id="cost"') ?>
                            </div>
                        <?php } ?>

                        <div class="form-group all">
                            <?= lang("product_price", "price") ?>
                            <?= form_input('price', (isset($_POST['price']) ? $_POST['price'] : ($product ? $this->sma->formatDecimal($product->price) : '')), 'class="form-control tip" id="price" required="required"') ?>
                        </div>


                        <div class="form-group">
                            <input type="checkbox" class="checkbox" value="1" name="promotion"
                                   id="promotion" <?= $this->input->post('promotion') ? 'checked="checked"' : ''; ?>>
                            <label for="promotion" class="padding05">
                                <?= lang('promotion'); ?>
                            </label>
                        </div>

                        <div id="promo" style="display:none;">
                            <div class="well well-sm border_radius3">
                                <div class="form-group">
                                    <?= lang('promo_price', 'promo_price'); ?>
                                    <?= form_input('promo_price', set_value('promo_price'), 'class="form-control tip" id="promo_price"'); ?>
                                </div>
                                <div class="form-group">
                                    <?= lang('start_date', 'start_date'); ?>
                                    <?= form_input('start_date', set_value('start_date'), 'class="form-control tip date" id="start_date"'); ?>
                                </div>
                                <div class="form-group">
                                    <?= lang('end_date', 'end_date'); ?>
                                    <?= form_input('end_date', set_value('end_date'), 'class="form-control tip date" id="end_date"'); ?>
                                </div>
                            </div>
                        </div>

                        <?php if ($Settings->invoice_view == 2) { ?>
                            <div class="form-group">
                                <?= lang('hsn_code', 'hsn_code'); ?>
                                <?= form_input('hsn_code', set_value('hsn_code', ($product ? $product->hsn_code : '')), 'class="form-control" id="hsn_code"'); ?>
                            </div>
                        <?php } ?>

                        <?php if ($Settings->tax1) { ?>
                            <div class="form-group all">
                                <?= lang('product_tax', "tax_rate") ?>
                                <?php
                                $tr[""] = "";
                                foreach ($tax_rates as $tax) {
                                    $tr[$tax->id] = $tax->name;
                                }
                                echo form_dropdown('tax_rate', $tr, (isset($_POST['tax_rate']) ? $_POST['tax_rate'] : ($product ? $product->tax_rate : $Settings->default_tax_rate)), 'class="form-control select" id="tax_rate" placeholder="' . lang("select") . ' ' . lang("product_tax") . '" style="width:100%"')
                                ?>
                            </div>
                            <div class="form-group all">
                                <?= lang("tax_method", "tax_method") ?>
                                <?php
                                $tm = array('1' => lang('exclusive'), '0' => lang('inclusive'));
                                echo form_dropdown('tax_method', $tm, (isset($_POST['tax_method']) ? $_POST['tax_method'] : ($product ? $product->tax_method : '')), 'class="form-control select" id="tax_method" placeholder="' . lang("select") . ' ' . lang("tax_method") . '" style="width:100%"');
                                ?>
                            </div>
                        <?php } ?>

                        <div class="form-group standard">
                            <?= lang("alert_quantity", "alert_quantity") ?>
                            <div class="input-group border_radius3"> <?= form_input('alert_quantity', (isset($_POST['alert_quantity']) ? $_POST['alert_quantity'] : ($product ? $this->sma->formatQuantityDecimal($product->alert_quantity) : '')), 'class="form-control tip" id="alert_quantity"') ?>
                                <span class="input-group-addon">
                            <input type="checkbox" name="track_quantity" id="track_quantity"
                                   value="1" <?= ($product ? (isset($product->track_quantity) ? 'checked="checked"' : '') : 'checked="checked"') ?>>
                        </span>
                            </div>
                        </div>


                    </div>


                    <div class="form-group all hidden">
                        <?= lang("translate_product_name", "traslate_name") ?>
                        <?= form_input('traslate_name', (isset($_POST['traslate_name']) ? $_POST['traslate_name'] : ($product ? $product->translate_name : '')), 'class="form-control" id="translate_name" '); ?>
                        <button type="button" id="translate_me" class="btn btn-primary hide">Translate Me!</button>
                    </div>

                </div>
                <div class="col-md-5">
                    <div class="panel panel-default padding10 border_radius3">
                        <h2 class="blue">Category Detail</h2>
                        <div class="form-group all">
                            <?= lang("brand", "brand") ?>
                            <?php
                            $br[''] = "";
                            foreach ($brands as $brand) {
                                $br[$brand->id] = $brand->name;
                            }
                            echo form_dropdown('brand', $br, (isset($_POST['brand']) ? $_POST['brand'] : ($product ? $product->brand : '1')), 'class="form-control select" id="language" placeholder="' . lang("select") . " " . lang("language") . '" required="required" style="width:100%"')
                            ?>
                        </div>
                        <div class="form-group all">
                            <?= lang("category", "category") ?>
                            <?php
                            $cat[''] = "";
                            foreach ($categories as $category) {
                                $cat[$category->id] = $category->name;
                            }
                            echo form_dropdown('category', $cat, (isset($_POST['category']) ? $_POST['category'] : ($product ? $product->category_id : '')), 'class="form-control select" id="category" placeholder="' . lang("select") . " " . lang("category") . '" required="required" style="width:100%"')
                            ?>
                        </div>
                        <div class="form-group all">
                            <?= lang("subcategory", "subcategory") ?>
                            <div class="controls" id="subcat_data"> <?php
                                echo form_input('subcategory', ($product ? $product->subcategory_id : ''), 'class="form-control" id="subcategory"  placeholder="' . lang("select_category_to_load") . '"');
                                ?>
                            </div>
                        </div>

                        <div class="form-group standard_combo">
                            <?= lang('weight', 'weight'); ?>
                            <?= form_input('weight', set_value('weight', ($product ? $product->weight : '')), 'class="form-control tip" id="weight"'); ?>
                        </div>

                        <div class="form-group col-md-5 paddingleftright0 standard_combo">
                            <?= lang('dimensions', 'dimensions'); ?>
                            <?= form_input('width', set_value('width', ($product ? $product->width : '')), 'class="form-control col-md-6 tip" id="width" placeholder="Width"'); ?>
                        </div>
                        <div class="col-md-2 text-center paddingleftright0 paddingtop15">
                            <h2>x</h2>
                        </div>
                        <div class="form-group col-md-5 paddingleftright0 paddingtop28 standard_combo">
                            <?= form_input('height', set_value('height', ($product ? $product->height : '')), 'class="form-control col-md-6 tip" id="height"  placeholder="Height"'); ?>
                        </div>
                        <div class="clearfix"></div>

                    </div>


                    <div class="panel panel-default hidden padding10 border_radius3">
                        <h2 class="blue">Units Detail</h2>
                        <div class="form-group standard">
                            <?= lang('product_unit', 'unit'); ?>
                            <?php
                            $pu[''] = lang('select') . ' ' . lang('unit');
                            foreach ($base_units as $bu) {
                                $pu[$bu->id] = $bu->name . ' (' . $bu->code . ')';
                            }
                            ?>
                            <?= form_dropdown('unit', $pu, set_value('unit', ($product ? $product->unit : '')), 'class="form-control tip" id="unit"  style="width:100%;"'); ?>
                        </div>
                        <div class="form-group standard">
                            <?= lang('default_sale_unit', 'default_sale_unit'); ?>
                            <?php $uopts[''] = lang('select_unit_first'); ?>
                            <?= form_dropdown('default_sale_unit', $uopts, ($product ? $product->sale_unit : ''), 'class="form-control" id="default_sale_unit" style="width:100%;"'); ?>
                        </div>
                        <div class="form-group standard">
                            <?= lang('default_purchase_unit', 'default_purchase_unit'); ?>
                            <?= form_dropdown('default_purchase_unit', $uopts, ($product ? $product->purchase_unit : ''), 'class="form-control" id="default_purchase_unit" style="width:100%;"'); ?>
                        </div>
                    </div>


                    <div class="panel panel-default padding10 border_radius3">
                        <h2 class="blue">Feature Options</h2>

                        <div class="form-group">
                            <input name="featured" type="checkbox" class="checkbox" id="featured"
                                   value="1" <?= isset($_POST['featured']) ? 'checked="checked"' : '' ?>/>
                            <label for="featured" class="padding05"><?= lang('featured') ?></label>
                        </div>
                        <div class="form-group">
                            <input name="hide" type="checkbox" class="checkbox" id="hide"
                                   value="1" <?= isset($_POST['hide']) ? 'checked="checked"' : '' ?>/>
                            <label for="hide" class="padding05"><?= lang('hide_in_shop') ?></label>
                        </div>
                        <div class="form-group">
                            <input name="product_freeship" type="checkbox" class="checkbox" id="product_freeship"
                                   value="1" <?= $product->free_shipment == 1 ? 'checked' : '' ?> /><label
                                    for="product_freeship" class="padding05">
                                <?= lang('Free Shipping') ?></label>
                        </div>

                        <div class="form-group">
                            <input name="show_variable_price_on_shop" type="checkbox" class="checkbox" id="show_variable_price_on_shop"
                                   value="1" <?= isset($_POST['show_variable_price_on_shop']) ? 'checked="checked"' : '' ?>/>
                            <label for="show_variable_price_on_shop" class="padding05">Show Variable Price on Shop</label>
                        </div>

                        <div class="form-group">
                            <input name="cf" type="checkbox" class="checkbox" id="extras"
                                   value="" <?= isset($_POST['cf']) ? 'checked="checked"' : '' ?>/>
                            <label for="extras" class="padding05"><?= lang('custom_fields') ?></label>
                        </div>
                        <div class=" well well-sm border_radius3" id="extras-con" style="display: none;">

                            <div class="col-md-6">
                                <div class="form-group all">
                                    <?= lang('pcf1', 'cf1') ?>
                                    <?= form_input('cf1', (isset($_POST['cf1']) ? $_POST['cf1'] : ($product ? $product->cf1 : '')), 'class="form-control tip" id="cf1"') ?>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group all">
                                    <?= lang('pcf2', 'cf2') ?>
                                    <?= form_input('cf2', (isset($_POST['cf2']) ? $_POST['cf2'] : ($product ? $product->cf2 : '')), 'class="form-control tip" id="cf2"') ?>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group all">
                                    <?= lang('pcf3', 'cf3') ?>
                                    <?= form_input('cf3', (isset($_POST['cf3']) ? $_POST['cf3'] : ($product ? $product->cf3 : '')), 'class="form-control tip" id="cf3"') ?>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group all">
                                    <?= lang('pcf4', 'cf4') ?>
                                    <?= form_input('cf4', (isset($_POST['cf4']) ? $_POST['cf4'] : ($product ? $product->cf4 : '')), 'class="form-control tip" id="cf4"') ?>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group all">
                                    <?= lang('pcf5', 'cf5') ?>
                                    <?= form_input('cf5', (isset($_POST['cf5']) ? $_POST['cf5'] : ($product ? $product->cf5 : '')), 'class="form-control tip" id="cf5"') ?>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group all">
                                    <?= lang('pcf6', 'cf6') ?>
                                    <?= form_input('cf6', (isset($_POST['cf6']) ? $_POST['cf6'] : ($product ? $product->cf6 : '')), 'class="form-control tip" id="cf6"') ?>
                                </div>
                            </div>
                            <div class="clearfix"></div>

                        </div>


                    </div>


                    <div class="standard">


                        <div class="<?= $product ? 'text-warning' : '' ?>">
                            <strong><?= lang("warehouse_quantity") ?></strong><br>
                            <?php
                            if (!empty($warehouses)) {
                                if ($product) {
                                    echo '<div class="row"><div class="col-md-12"><div class="Well border_radius3"><div id="show_wh_edit">';
                                    if (!empty($warehouses_products)) {
                                        echo '<div style="display:none;">';
                                        foreach ($warehouses_products as $wh_pr) {
                                            echo '<span class="bold text-info">' . $wh_pr->name . ': <span class="padding05" id="rwh_qty_' . $wh_pr->id . '">' . $this->sma->formatQuantity($wh_pr->quantity) . '</span>' . ($wh_pr->rack ? ' (<span class="padding05" id="rrack_' . $wh_pr->id . '">' . $wh_pr->rack . '</span>)' : '') . '</span><br>';
                                        }
                                        echo '</div>';
                                    }
                                    foreach ($warehouses as $warehouse) {
                                        //$whs[$warehouse->id] = $warehouse->name;
                                        echo '<div class="col-md-6 col-sm-6 col-xs-6" style="padding-bottom:15px;">' . $warehouse->name . '<br><div class="form-group">' . form_hidden('wh_' . $warehouse->id, $warehouse->id) . form_input('wh_qty_' . $warehouse->id, (isset($_POST['wh_qty_' . $warehouse->id]) ? $_POST['wh_qty_' . $warehouse->id] : (isset($warehouse->quantity) ? $warehouse->quantity : '')), 'class="form-control wh" id="wh_qty_' . $warehouse->id . '" placeholder="' . lang('quantity') . '"') . '</div>';
                                        if ($Settings->racks) {
                                            echo '<div class="form-group">' . form_input('rack_' . $warehouse->id, (isset($_POST['rack_' . $warehouse->id]) ? $_POST['rack_' . $warehouse->id] : (isset($warehouse->rack) ? $warehouse->rack : '')), 'class="form-control wh" id="rack_' . $warehouse->id . '" placeholder="' . lang('rack') . '"') . '</div>';
                                        }
                                        echo '</div>';
                                    }
                                    echo '</div><div class="clearfix"></div></div></div></div>';
                                } else {
                                    echo '<div class="row"><div class="col-md-12"><div class="well border_radius3">';
                                    foreach ($warehouses as $warehouse) {
                                        //$whs[$warehouse->id] = $warehouse->name;
                                        echo '<div class="col-md-6 col-sm-6 col-xs-6" style="padding-bottom:15px;">' . $warehouse->name . '<br><div class="form-group">' . form_hidden('wh_' . $warehouse->id, $warehouse->id) . form_input('wh_qty_' . $warehouse->id, (isset($_POST['wh_qty_' . $warehouse->id]) ? $_POST['wh_qty_' . $warehouse->id] : ''), 'class="form-control" id="wh_qty_' . $warehouse->id . '" placeholder="' . lang('quantity') . '"') . '</div>';
                                        if ($Settings->racks) {
                                            echo '<div class="form-group">' . form_input('rack_' . $warehouse->id, (isset($_POST['rack_' . $warehouse->id]) ? $_POST['rack_' . $warehouse->id] : ''), 'class="form-control" id="rack_' . $warehouse->id . '" placeholder="' . lang('rack') . '"') . '</div>';
                                        }
                                        echo '</div>';
                                    }
                                    echo '<div class="clearfix"></div></div></div></div>';
                                }
                            }
                            ?>
                        </div>
                        <div class="clearfix"></div>


                    </div>
                    <div class="combo " style="display:none;">

                        <div class="form-group">
                            <?= lang("add_product", "add_item") . ' (' . lang('not_with_variants') . ')'; ?>
                            <?php echo form_input('add_item', '', 'class="form-control ttip" id="add_item" data-placement="top" data-trigger="focus" data-bv-notEmpty-message="' . lang('please_add_items_below') . '" placeholder="' . $this->lang->line("add_item") . '"'); ?>
                        </div>
                        <div class="control-group table-group">
                            <label class="table-label" for="combo"><?= lang("combo_products"); ?></label>

                            <div class="controls table-controls">
                                <table id="prTable"
                                       class="table items table-striped table-bordered table-condensed table-hover">
                                    <thead>
                                    <tr>
                                        <th class="col-md-5 col-sm-5 col-xs-5"><?= lang('product') . ' (' . lang('code') . ' - ' . lang('name') . ')'; ?></th>
                                        <th class="col-md-2 col-sm-2 col-xs-2"><?= lang("quantity"); ?></th>
                                        <th class="col-md-3 col-sm-3 col-xs-3"><?= lang("unit_price"); ?></th>
                                        <th class="col-md-1 col-sm-1 col-xs-1 text-center">
                                            <i class="fa fa-trash-o" style="opacity:0.5; filter:alpha(opacity=50);"></i>
                                        </th>
                                    </tr>
                                    </thead>
                                    <tbody></tbody>
                                </table>
                            </div>
                        </div>

                    </div>

                    <div class="digital" style="display:none;">
                        <div class="form-group digital">
                            <?= lang("digital_file", "digital_file") ?>
                            <input id="digital_file" type="file" data-browse-label="<?= lang('browse'); ?>"
                                   name="digital_file" data-show-upload="false"
                                   data-show-preview="false" class="form-control file">
                        </div>
                        <div class="form-group">
                            <?= lang('file_link', 'file_link'); ?>
                            <?= form_input('file_link', set_value('file_link'), 'class="form-control" id="file_link"'); ?>
                        </div>
                    </div>

                    <div class="form-group standard">
                        <div class="form-group">
                            <?= lang("supplier", "supplier") ?>
                            <button type="button" class="btn btn-primary btn-xs" id="addSupplier"><i
                                        class="fa fa-plus"></i>
                            </button>
                        </div>
                        <div class="row" id="supplier-con">
                            <div class="col-xs-12">
                                <div class="form-group">
                                    <?php
                                    echo form_input('supplier', (isset($_POST['supplier']) ? $_POST['supplier'] : ''), 'class="form-control ' . ($product ? '' : 'suppliers') . '" id="' . ($product && !empty($product->supplier1) ? 'supplier1' : 'supplier') . '" placeholder="' . lang("select") . ' ' . lang("supplier") . '" style="width:100%;"');
                                    ?>
                                </div>
                            </div>
                            <div class="col-xs-6">
                                <div class="form-group">
                                    <?= form_input('supplier_part_no', (isset($_POST['supplier_part_no']) ? $_POST['supplier_part_no'] : ""), 'class="form-control tip" id="supplier_part_no" placeholder="' . lang('supplier_part_no') . '"'); ?>
                                </div>
                            </div>
                            <div class="col-xs-6">
                                <div class="form-group">
                                    <?= form_input('supplier_price', (isset($_POST['supplier_price']) ? $_POST['supplier_price'] : ""), 'class="form-control tip" id="supplier_price" placeholder="' . lang('supplier_price') . '"'); ?>
                                </div>
                            </div>
                        </div>
                        <div id="ex-suppliers"></div>
                    </div>

                </div>


                <div class="col-md-12">
                    <div class="panel panel-default padding10 border_radius3">
                        <h2 class="blue">Varients </h2>

                        <div id="attrs"></div>

                        <div class="form-group">
                            <input type="checkbox" class="checkbox" name="attributes"
                                   id="attributes" <?= $this->input->post('attributes') || $product_options ? 'checked="checked"' : ''; ?>><label
                                    for="attributes"
                                    class="padding05"><?= lang('product_has_attributes'); ?></label> <?= lang('eg_sizes_colors'); ?>
                        </div>

                        <div class="well well-sm border_radius3" id="attr-con"
                             style="<?= $this->input->post('attributes') || $product_options ? '' : 'display:block;'; ?>">
                            <div class="new_variant">
                                <div class="col-sm-4">
                                    <select  class="form-control variants_dropdown" name="variants[]"
                                            id="variants_dropdown1">
                                        <option value="">Select Varient</option>
                                        <?php foreach ($variants as $key => $varient) { ?>
                                            <option value="<?= $varient->name ?>"><?= $varient->name; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                                <div class="col-sm-4">
                                    <input id="attributesInput2" class="form-control select-tagse" name="attributesInput2[]"
                                           placeholder="<?= $this->lang->line("enter_attributes") ?>">
                                </div>

                                 <div class="col-sm-4">
                                    <select class="form-control" name="additional_guide[]">
                                        <option value="">Select Size Guide</option>
                                    <?php foreach($additional_guide as $guide){ ?>
                                            <option value="<?= $guide->id; ?>"><?= $guide->title; ?></option>
                                    <?php } ?>
                                    </select>
                                 </div>
                                
                            </div>
                            <div id="newvariants"></div>

                            <div class="col-sm-12" style="margin-top: 10px;">
                                <a class="btn btn-success" id="add_more_variant"> Add more Variant</a>
                                <a class="btn btn-success" id="gen_combinations">Generate combinations</a>
                            </div>


                            <div class="form-group" id="ui" style="margin-bottom: 0;">
                                <!--<div class="input-group">
                                    <?php /*// echo form_input('attributesInput', '', 'class="form-control select-tags" id="attributesInput" placeholder="' . $this->lang->line("enter_attributes") . '"'); */ ?>
                                    <div class="input-group-addon" style="padding: 2px 5px;">
                                        <a href="#" id="addAttributes">
                                            <i class="fa fa-2x fa-plus-circle" id="addIcon"></i>
                                        </a>
                                    </div>
                                </div>-->
                                <div style="clear:both;"></div>
                            </div>

                            <div class="table-responsive" style=" padding-left: 15px; padding-right: 15px;">
                                <table id="attrTable" class="table table-bordered table-condensed table-striped"
                                       style="<?= $this->input->post('attributes') || $product_options ? '' : 'display:none;'; ?>margin-bottom: 0; margin-top: 10px;">
                                    <thead>
                                    <tr class="active">
                                        <th>Variant Name</th>
                                        <th>Supplier Code</th>
                                        <th>Supplier Price</th>
                                        <th>Price Addition</th>
                                        <th>Weight (Kg)</th>
                                        <th>Website Display</th>
                                        <th><i class="fa fa-times attr-remove-all"></i></th>
                                    </tr>
                                    </thead>
                                    <tbody><?php
                                    if ($this->input->post('attributes')) {
                                        $a = sizeof($_POST['attr_name']);
                                        for ($r = 0; $r <= $a; $r++) {
                                            if (isset($_POST['attr_name'][$r]) && (isset($_POST['attr_warehouse'][$r]) || isset($_POST['attr_quantity'][$r]))) {
                                                echo '<tr class="attr"><td><input type="hidden" name="attr_name[]" value="' . $_POST['attr_name'][$r] . '"><span>' . $_POST['attr_name'][$r] . '</span></td><td class="code text-center"><input type="hidden" name="attr_warehouse[]" value="' . $_POST['attr_warehouse'][$r] . '"><input type="hidden" name="attr_wh_name[]" value="' . $_POST['attr_wh_name'][$r] . '"><span>' . $_POST['attr_wh_name'][$r] . '</span></td><td class="quantity text-center"><input type="hidden" name="attr_quantity[]" value="' . $this->sma->formatQuantityDecimal($_POST['attr_quantity'][$r]) . '"><span>' . $this->sma->formatQuantity($_POST['attr_quantity'][$r]) . '</span></td><td class="price text-right"><input type="hidden" name="attr_price[]" value="' . $_POST['attr_price'][$r] . '"><span>' . $_POST['attr_price'][$r] . '</span></span></td><td class="text-center"><i class="fa fa-times delAttr"></i></td></tr>';
                                            }
                                        }
                                    } elseif ($product_options) {
                                        foreach ($product_options as $option) {
                                            echo '<tr class="attr"><td><input type="hidden" name="attr_name[]" value="' . $option->name . '"><span>' . $option->name . '</span></td><td class="code text-center"><input type="hidden" name="attr_warehouse[]" value="' . $option->warehouse_id . '"><input type="hidden" name="attr_wh_name[]" value="' . $option->wh_name . '"><span>' . $option->wh_name . '</span></td><td class="quantity text-center"><input type="hidden" name="attr_quantity[]" value="' . $this->sma->formatQuantityDecimal($option->wh_qty) . '"><span>' . $this->sma->formatQuantity($option->wh_qty) . '</span></td><td class="price text-right"><input type="hidden" name="attr_price[]" value="' . $this->sma->formatMoney($option->price) . '"><span>' . $this->sma->formatMoney($option->price) . '</span></span></td><td class="text-center"><i class="fa fa-times delAttr"></i></td></tr>';
                                        }
                                    }
                                    ?></tbody>
                                </table>
                            </div>

                        </div>
                        <div class="clearfix"></div>


                    </div>
                </div>


                <div class="col-md-12">
                    <div class="form-group all hidden">
                        <?= lang("product_details_for_invoice", "details") ?>
                        <?= form_textarea('details', (isset($_POST['details']) ? $_POST['details'] : ($product ? $product->details : '')), 'class="form-control" id="details"'); ?>
                    </div>

                    <div class="form-group hidden">
                        <label for="extra">Extra (Product meaurement)</label>
                        <?php
                        $size[''] = 'Select product size';
                        foreach ($sizes_data as $sizes_data) {
                            $size[$sizes_data->id] = $sizes_data->title;
                        }
                        echo form_dropdown('extra_size', $size, (isset($_POST['extra_size']) ? $_POST['extra_size'] : ($product ? $product->extra_size : '')), 'class="form-control" id="extra_size"');
                        ?>
                    </div>


                    <div class="form-group">
                        <?php echo form_submit('add_product', $this->lang->line("add_product"), 'class="btn btn-primary"'); ?>
                    </div>

                </div>
                <?= form_close(); ?>

            </div>

        </div>
    </div>
</div>
<script src="<?= $assets ?>js/sweetalert.min.js"></script>
<script type="text/javascript">
    $(document).ready(function () {

        $('form[data-toggle="validator"]').bootstrapValidator({excluded: [':disabled']});
        var audio_success = new Audio('<?= $assets ?>sounds/sound2.mp3');
        var audio_error = new Audio('<?= $assets ?>sounds/sound3.mp3');
        var items = {};
        <?php
        if ($combo_items) {
            foreach ($combo_items as $item) {
                //echo 'ietms['.$item->id.'] = '.$item.';';
                if ($item->code) {
                    echo 'add_product_item(' . json_encode($item) . ');';
                }
            }
        }
        ?>
        <?=isset($_POST['cf']) ? '$("#extras").iCheck("check");' : '' ?>
        $('#extras').on('ifChecked', function () {
            $('#extras-con').slideDown();
        });
        $('#extras').on('ifUnchecked', function () {
            $('#extras-con').slideUp();
        });

        <?= isset($_POST['promotion']) ? '$("#promotion").iCheck("check");' : '' ?>
        $('#promotion').on('ifChecked', function (e) {
            $('#promo').slideDown();
        });
        $('#promotion').on('ifUnchecked', function (e) {
            $('#promo').slideUp();
        });

        $('.attributes').on('ifChecked', function (event) {
            $('#options_' + $(this).attr('id')).slideDown();
        });
        $('.attributes').on('ifUnchecked', function (event) {
            $('#options_' + $(this).attr('id')).slideUp();
        });
        //$('#cost').removeAttr('required');
        $('#digital_file').change(function () {
            if ($(this).val()) {
                $('#file_link').removeAttr('required');
                $('form[data-toggle="validator"]').bootstrapValidator('removeField', 'file_link');
            } else {
                $('#file_link').attr('required', 'required');
                $('form[data-toggle="validator"]').bootstrapValidator('addField', 'file_link');
            }
        });
        $('#type').change(function () {
            var t = $(this).val();
            if (t !== 'standard') {
                $('.standard').slideUp();
                $('#unit').attr('disabled', true);
                $('#cost').attr('disabled', true);
                $('#track_quantity').iCheck('uncheck');
            } else {
                $('.standard').slideDown();
                $('#track_quantity').iCheck('check');
                $('#unit').attr('disabled', false);
                $('#cost').attr('disabled', false);
            }
            if (t !== 'digital') {
                $('.digital').slideUp();
                $('#file_link').removeAttr('required');
                $('form[data-toggle="validator"]').bootstrapValidator('removeField', 'file_link');
            } else {
                $('.digital').slideDown();
                $('#file_link').attr('required', 'required');
                $('form[data-toggle="validator"]').bootstrapValidator('addField', 'file_link');
            }
            if (t !== 'combo') {
                $('.combo').slideUp();
            } else {
                $('.combo').slideDown();
            }
            if (t == 'standard' || t == 'combo') {
                $('.standard_combo').slideDown();
            } else {
                $('.standard_combo').slideUp();
            }
        });

        var t = $('#type').val();
        if (t !== 'standard') {
            $('.standard').slideUp();
            $('#unit').attr('disabled', true);
            $('#cost').attr('disabled', true);
            $('#track_quantity').iCheck('uncheck');
        } else {
            $('.standard').slideDown();
            $('#track_quantity').iCheck('check');
            $('#unit').attr('disabled', false);
            $('#cost').attr('disabled', false);
        }
        if (t !== 'digital') {
            $('.digital').slideUp();
            $('#file_link').removeAttr('required');
            $('form[data-toggle="validator"]').bootstrapValidator('removeField', 'file_link');
        } else {
            $('.digital').slideDown();
            $('#file_link').attr('required', 'required');
            $('form[data-toggle="validator"]').bootstrapValidator('addField', 'file_link');
        }
        if (t !== 'combo') {
            $('.combo').slideUp();
        } else {
            $('.combo').slideDown();
        }
        if (t == 'standard' || t == 'combo') {
            $('.standard_combo').slideDown();
        } else {
            $('.standard_combo').slideUp();
        }

        $("#add_item").autocomplete({
            source: '<?= admin_url('products/suggestions'); ?>',
            minLength: 1,
            autoFocus: false,
            delay: 250,
            response: function (event, ui) {
                if ($(this).val().length >= 16 && ui.content[0].id == 0) {
                    //audio_error.play();
                    bootbox.alert('<?= lang('no_product_found') ?>', function () {
                        $('#add_item').focus();
                    });
                    $(this).val('');
                }
                else if (ui.content.length == 1 && ui.content[0].id != 0) {
                    ui.item = ui.content[0];
                    $(this).data('ui-autocomplete')._trigger('select', 'autocompleteselect', ui);
                    $(this).autocomplete('close');
                    $(this).removeClass('ui-autocomplete-loading');
                }
                else if (ui.content.length == 1 && ui.content[0].id == 0) {
                    //audio_error.play();
                    bootbox.alert('<?= lang('no_product_found') ?>', function () {
                        $('#add_item').focus();
                    });
                    $(this).val('');

                }
            },
            select: function (event, ui) {
                event.preventDefault();
                if (ui.item.id !== 0) {
                    var row = add_product_item(ui.item);
                    if (row) {
                        $(this).val('');
                    }
                } else {
                    //audio_error.play();
                    bootbox.alert('<?= lang('no_product_found') ?>');
                }
            }
        });

        <?php
        if ($this->input->post('type') == 'combo') {
            $c = isset($_POST['combo_item_code']) ? sizeof($_POST['combo_item_code']) : 0;
            for ($r = 0; $r <= $c; $r++) {
                if (isset($_POST['combo_item_code'][$r]) && isset($_POST['combo_item_quantity'][$r]) && isset($_POST['combo_item_price'][$r])) {
                    $items[] = array('id' => $_POST['combo_item_id'][$r], 'name' => $_POST['combo_item_name'][$r], 'code' => $_POST['combo_item_code'][$r], 'qty' => $_POST['combo_item_quantity'][$r], 'price' => $_POST['combo_item_price'][$r]);
                }
            }
            echo '
            var ci = ' . (isset($items) ? json_encode($items) : "''") . ';
            $.each(ci, function() { add_product_item(this); });
            ';
        }
        ?>
        function add_product_item(item) {
            if (item == null) {
                return false;
            }
            item_id = item.id;
            if (items[item_id]) {
                items[item_id].qty = (parseFloat(items[item_id].qty) + 1).toFixed(2);
            } else {
                items[item_id] = item;
            }
            var pp = 0;
            $("#prTable tbody").empty();
            $.each(items, function () {
                var row_no = this.id;
                var newTr = $('<tr id="row_' + row_no + '" class="item_' + this.id + '" data-item-id="' + row_no + '"></tr>');
                tr_html = '<td><input name="combo_item_id[]" type="hidden" value="' + this.id + '"><input name="combo_item_name[]" type="hidden" value="' + this.name + '"><input name="combo_item_code[]" type="hidden" value="' + this.code + '"><span id="name_' + row_no + '">' + this.code + ' - ' + this.name + '</span></td>';
                tr_html += '<td><input class="form-control text-center rquantity" name="combo_item_quantity[]" type="text" value="' + formatDecimal(this.qty) + '" data-id="' + row_no + '" data-item="' + this.id + '" id="quantity_' + row_no + '" onClick="this.select();"></td>';
                tr_html += '<td><input class="form-control text-center rprice" name="combo_item_price[]" type="text" value="' + formatDecimal(this.price) + '" data-id="' + row_no + '" data-item="' + this.id + '" id="combo_item_price_' + row_no + '" onClick="this.select();"></td>';
                tr_html += '<td class="text-center"><i class="fa fa-times tip del" id="' + row_no + '" title="Remove" style="cursor:pointer;"></i></td>';
                newTr.html(tr_html);
                newTr.prependTo("#prTable");
                pp += formatDecimal(parseFloat(this.price) * parseFloat(this.qty));
            });
            $('.item_' + item_id).addClass('warning');
            $('#price').val(pp);
            return true;
        }

        function calculate_price() {
            var rows = $('#prTable').children('tbody').children('tr');
            var pp = 0;
            $.each(rows, function () {
                pp += formatDecimal(parseFloat($(this).find('.rprice').val()) * parseFloat($(this).find('.rquantity').val()));
            });
            $('#price').val(pp);
            return true;
        }

        $(document).on('change', '.rquantity, .rprice', function () {
            calculate_price();
        });

        $(document).on('click', '.del', function () {
            var id = $(this).attr('id');
            delete items[id];
            $(this).closest('#row_' + id).remove();
            calculate_price();
        });
        var su = 2;
        $('#addSupplier').click(function () {
            if (su <= 5) {
                $('#supplier_1').select2('destroy');
                var html = '<div style="clear:both;height:5px;"></div><div class="row"><div class="col-xs-12"><div class="form-group"><input type="hidden" name="supplier_' + su + '", class="form-control" id="supplier_' + su + '" placeholder="<?= lang("select") . ' ' . lang("supplier") ?>" style="width:100%;display: block !important;" /></div></div><div class="col-xs-6"><div class="form-group"><input type="text" name="supplier_' + su + '_part_no" class="form-control tip" id="supplier_' + su + '_part_no" placeholder="<?= lang('supplier_part_no') ?>" /></div></div><div class="col-xs-6"><div class="form-group"><input type="text" name="supplier_' + su + '_price" class="form-control tip" id="supplier_' + su + '_price" placeholder="<?= lang('supplier_price') ?>" /></div></div></div>';
                $('#ex-suppliers').append(html);
                var sup = $('#supplier_' + su);
                suppliers(sup);
                su++;
            } else {
                bootbox.alert('<?= lang('max_reached') ?>');
                return false;
            }
        });

        var _URL = window.URL || window.webkitURL;
        $("input#images").on('change.bs.fileinput', function () {
            var ele = document.getElementById($(this).attr('id'));
            var result = ele.files;
            $('#img-details').empty();
            for (var x = 0; x < result.length; x++) {
                var fle = result[x];
                for (var i = 0; i <= result.length; i++) {
                    var img = new Image();
                    img.onload = (function (value) {
                        return function () {
                            ctx[value].drawImage(result[value], 0, 0);
                        }
                    })(i);
                    img.src = 'images/' + result[i];
                }
            }
        });

        $('.variants_dropdown').on('change', function () {
            var variant = $(this).val();
            var that = $(this);
            // console.log(that.parent().next().find('.select-tagse'));
            if (variant) {
                $.ajax({
                    type: "get",
                    async: false,
                    url: "<?= admin_url('products/getVarientValuesById_add') ?>/" + variant,
                    dataType: "json",
                    success: function (data) {
                        console.log(data);
                        that.parent().next().find('.select-tagse').select2({
                            allowClear: true,
                            tags: true,
                            data: data
                        });

                        that.parent().next().find('.select-tagse').on("change", function (e) {
                            if (e.removed) {
                                genHtml();
                            }

                        });


                    },
                    error: function () {
                    }
                });

            } else {
                that.parent().next().find('.select-tagse').select2('destroy');
                that.parent().next().find('.select-tagse').val('');
            }
            //alert(variant);

        });


        var dynamic_id = 2;
        $('#add_more_gallery_image').on('click', function () {

            var html = '';
            html +='<div class="gallery_wrapper" style=" margin-top: 20px;">';
            html +='<div class="form-group all col-sm-11" style="padding-left: 0px; padding-right: 0px;">';
            html +='<div class="input-group">';
            html +='   <input type="hidden" id="gimage_id'+dynamic_id+'" name="gimage[]" readonly class="form-control mediaimage_id" placeholder="Select Image">';
            html +='   <input type="text" id="gimage'+dynamic_id+'"  readonly class="form-control mediaimage" placeholder="Select Image">';
            html +='   <span class="input-group-btn ">';
            html +='  <button id="mediagallery" class="btn btn-primary mediagallery width120" type="button">Select Image</button>';
            html +='</span>';
            html +='</div>';
            html +='<input class="alt_text" name="gimage_alt[]" value="" type="hidden">';
            html +='</div>';
            html += ' <div class="col-sm-1" style="padding-left: 0px; padding-right: 0px;">';
            html += '<a class="btn btn-success pull-right remVar" id="add_more_variant"><i class="fa fa-trash"></i></a>';
            html += '</div>';
            html +=' </div>';
            dynamic_id=dynamic_id+1;
            $('#newgalleryimage').append(html);

            $('.remVar').on('click', function () {
                $(this).parents('.gallery_wrapper').remove();
                return false;
            });
            mediaSettings();


        });

        $('#add_more_variant').on('click', function () {
            var selected_varients = [];
            var inputs = $(".variants_dropdown");
            for (var i = 0; i < inputs.length; i++) {
                selected_varients.push($(inputs[i]).val());
            }

            var variantsAll = <?=json_encode($variants);?>;
            var html = '';
            html += '<div class="new_variant" style="margin-top: 30px; padding-top: 20px;">';
            html += '<div class="col-sm-4">';
            html += '<select class="form-control variants_dropdown" name="variants[]" id="variants_dropdown1">';
            html += '<option value="">Select Varient</option>';
            $.each(variantsAll, function (key, value) {
                if ($.inArray(value.name, selected_varients) !== -1) {
                } else {
                    html += '<option value="' + value.name + '">' + value.name + '</option>';
                }
            });

            html += '</select>';
            html += '</div>';
            html += ' <div class="col-sm-4">';
            html += '<input id="attributesInput2" name="attributesInput2[]" class="form-control select-tagse" placeholder="Enter Attributes">';
            html += ' </div>';

            var allAditionalGuide = <?=json_encode($additional_guide);?>;
            html += ' <div class="col-sm-3">';
            html += ' <select class="form-control" name="additional_guide[]"> <option value="">Select Size Guide</option>';

            $.each(allAditionalGuide, function (key, value) {
                if ($.inArray(value.id, selected_varients) !== -1) {
                } else {
                    html += '<option value="' + value.id + '">' + value.title + '</option>';
                }
            });
            html += '</select>';
            html += ' </div>';

            html += ' <div class="col-sm-1">';
            html += '<a class="btn btn-success pull-right remVar" id="add_more_variant"><i class="fa fa-trash"></i></a>';
            html += '</div>';
            html += '</div>';

            $('#newvariants').append(html);

            $('.variants_dropdown').on('change', function () {
                var variant = $(this).val();
                var that = $(this);
                // console.log(that.parent().next().find('.select-tagse'));
                if (variant) {
                    $.ajax({
                        type: "get",
                        async: false,
                        url: "<?= admin_url('products/getVarientValuesById_add') ?>/" + variant,
                        dataType: "json",
                        success: function (data) {
                            console.log(data);
                            that.parent().next().find('.select-tagse').select2({
                                allowClear: true,
                                tags: true,
                                data: data
                            });
                            that.parent().next().find('.select-tagse').on("change", function (e) {
                                if (e.removed) {
                                    genHtml();
                                }

                            });
                        },
                        error: function () {
                        }
                    });


                } else {
                    that.parent().next().find('.select-tagse').select2('destroy');
                    that.parent().next().find('.select-tagse').val('');

                }
                //alert(variant);

            });

            $('.remVar').on('click', function () {
                // var tags = $("#attributesInput2").val();
                // console.log(tags+'sdsd');
                $(this).parents('.new_variant').remove();
                genHtml();

                return false;
            });


        });
        $('#gen_combinations').on('click', function () {

            genHtml();
            return false;
        });

        function genHtml() {
            var selected_varients = [];
            var selected_variant_values = [];
            var selected_variant_values_cm2 = [];
            var selected_variant_values_cm_all = [];
            var inputs = $(".variants_dropdown");
            for (var i = 0; i < inputs.length; i++) {
                selected_varients.push($(inputs[i]).val());
                selected_variant_values.push($(inputs[i]).parent().next().find('#attributesInput2').val());
            }
            for (var i = 1; i < selected_variant_values.length; i++) {
                var array_values = selected_variant_values[i].split(',');
                var selected_variant_values_cm = [];
                for (j = 0; j < array_values.length; j++) {
                    selected_variant_values_cm.push(array_values[j]);

                }
                selected_variant_values_cm_all.push(selected_variant_values_cm)
            }

            var final_results = $.combinations(selected_variant_values_cm_all);
            var html = '';
            for (var i = 0; i < final_results.length; i++) {
                var newdata = final_results[i];
                var name = '';
                for (j = 0; j < newdata.length; j++) {
                    if (j > 0) {
                        if (newdata[0] != '') {
                            name += ' / ' + newdata[j];
                        } else {
                            name += newdata[j];
                        }

                    } else {
                        name += newdata[j];
                    }
                }
                html += '<tr class="attr1"><td><input type="hidden" name="attr_name[]" value="' + name + '"><span>' + name + '</span></td><td class="code text-center form-group"><input type="text"  class="form-control" name="attr_supplier_code[]" value="0"></td><td class="supplier_price text-center form-group"><input type="number" min="0.00" class="form-control" name="attr_supplier_price[]" value="0"></td><td class="price text-center form-group"><input type="number" min="0.00" class="form-control" name="attr_price[]" value="0"></span></td> <td class="price text-center form-group"><input required type="number" min="0.00" class="form-control" name="attr_weight[]" value="0"></span></td> <td class="price text-center form-group"><label class="switch"> <input type="checkbox" class="slidet" checked > <span class="slider round"></span> </label>  <input id="website_status_val" type="hidden" value="1" name="website_status[]"> </td><td class="text-center"><i class="fa fa-times delAttr"></i></td></tr>';


            }
            $('#attrTable').show();
            $('#attrTable tbody').html(html);
            $('.slidet').on('change', function(){
                console.log($(this).parent().next().find('#website_status_val'));
                var that =$(this).parent().next();
                this.checked ? that.val(1) : that.val(0);
                // alert(this.value);
            });
        }


        /*  var inputs = $(".variants_dropdown");
         for(var i = 0; i < inputs.length; i++){
         //console.log(inputs[i]);
         // alert($(inputs[i]).val());

         // console.log(inputs[i].parent().next().find('.attributesInput2').val());
         // console.log($(inputs[i]).val());
         }*/
        // var tags = $(".select-tagse").val();
        // console.log(tags+'sdsd');


        var variants = <?=json_encode($vars);?>;
        console.log(variants);
        $(".select-tags").select2({
            tags: variants,
            tokenSeparators: [","],
            multiple: true
        });
        $(document).on('ifChecked', '#attributes', function (e) {
            $('#attr-con').slideDown();
        });
        $(document).on('ifUnchecked', '#attributes', function (e) {
            $(".select-tags").select2("val", "");
            $('.attr-remove-all').trigger('click');
            $('#attr-con').slideUp();
        });
        $('#addAttributes').click(function (e) {

            e.preventDefault();
            var attrs_val = $('#attributesInput').val(), attrs;
            attrs = attrs_val.split(',');
            for (var i in attrs) {
                if (attrs[i] !== '') {

                    $.ajax({
                        type: "get", async: false,
                        url: "<?= admin_url('products/getVarientIDByName') ?>/" + attrs[i],
                        dataType: "json",
                        success: function (scdata) {
                            if (scdata != null) {

                                $.each(scdata, function (key, value) {
                                    $('#attrTable').show().append('<tr class="attr"><td><input type="hidden" name="attr_name[]" value="' + attrs[i] + value.name + '"><span>' + attrs[i] + '.' + value.name + '</span></td><td class="code text-center"><input type="hidden" name="attr_supplier_code[]" value=""><span></span></td><td class="supplier_price text-center"><input type="hidden" name="attr_supplier_price[]" value="0"><span></span></td><td class="price text-right"><input type="hidden" name="attr_price[]" value="0"><span>0.0000</span></span></td><td class="text-center"><i class="fa fa-times delAttr"></i></td></tr>');
                                });

                            } else {


                            }
                        }
                    });





                    <?php /*if( ! empty($warehouses)) {*/
                    /*foreach ($warehouses as $warehouse) {
                        echo '$(\'#attrTable\').show().append(\'<tr class="attr"><td><input type="hidden" name="attr_name[]" value="\' + attrs[i] + \'"><span>\' + attrs[i] + \'</span></td><td class="code text-center"><input type="hidden" name="attr_warehouse[]" value="'.$warehouse->id.'"><span>'.$warehouse->name.'</span></td><td class="quantity text-center"><input type="hidden" name="attr_quantity[]" value="0"><span>0</span></td><td class="price text-right"><input type="hidden" name="attr_price[]" value="0"><span>0</span></span></td><td class="text-center"><i class="fa fa-times delAttr"></i></td></tr>\');';
                    }*/
                    /*} else { */?>
                    // $('#attrTable').show().append('<tr class="attr"><td><input type="hidden" name="attr_name[]" value="' + attrs[i] + '"><span>' + attrs[i] + '</span></td><td class="code text-center"><input type="hidden" name="attr_warehouse[]" value=""><span></span></td><td class="quantity text-center"><input type="hidden" name="attr_quantity[]" value="0"><span></span></td><td class="price text-right"><input type="hidden" name="attr_price[]" value="0"><span>0</span></span></td><td class="text-center"><i class="fa fa-times delAttr"></i></td></tr>');
                    <?php //} ?>


                }
            }
        });
//$('#attributesInput').on('select2-blur', function(){
//    $('#addAttributes').click();
//});
        $(document).on('click', '.delAttr', function () {
            $(this).closest("tr").remove();
        });
        $(document).on('click', '.attr-remove-all', function () {
            $('#attrTable tbody').empty();
            $('#attrTable').hide();
        });
        var row, warehouses = <?= json_encode($warehouses); ?>;
        $(document).on('click', '.attr td:not(:last-child)', function () {
            row = $(this).closest("tr");
            $('#aModalLabel').text(row.children().eq(0).find('span').text());
            //$('#awarehouse').select2("val", (row.children().eq(1).find('input').val()));
            $('#supplier_code').val(row.children().eq(1).find('input').val());
            $('#aquantity').val(row.children().eq(2).find('input').val());
            $('#aprice').val(row.children().eq(3).find('span').text());
            $('#aModal').appendTo('body').modal('show');
        });

        $('#aModal').on('shown.bs.modal', function () {
            $('#aquantity').focus();
            $(this).keypress(function (e) {
                if (e.which == 13) {
                    $('#updateAttr').click();
                }
            });
        });
        $(document).on('click', '#updateAttr', function () {
            var wh = $('#awarehouse').val(), wh_name;
            $.each(warehouses, function () {
                if (this.id == wh) {
                    wh_name = this.name;
                }
            });
            //row.children().eq(1).html('<input type="hidden" name="attr_warehouse[]" value="' + wh + '"><input type="hidden" name="attr_wh_name[]" value="' + wh_name + '"><span>' + wh_name + '</span>');
            row.children().eq(1).html('<input type="hidden" name="attr_warehouse[]" value="' + ($('#supplier_code').val() ? $('#supplier_code').val() : 0) + '"><span>' + $('#supplier_code').val() + '</span>');
            row.children().eq(2).html('<input type="hidden" name="attr_quantity[]" value="' + ($('#aquantity').val() ? $('#aquantity').val() : 0) + '"><span>' + decimalFormat($('#aquantity').val()) + '</span>');
            row.children().eq(3).html('<input type="hidden" name="attr_price[]" value="' + $('#aprice').val() + '"><span>' + currencyFormat($('#aprice').val()) + '</span>');
            $('#aModal').modal('hide');
        });
    });

    <?php if ($product) { ?>
    $(document).ready(function () {
        var t = "<?=$product->type?>";
        if (t !== 'standard') {
            $('.standard').slideUp();
            $('#cost').attr('required', 'required');
            $('#track_quantity').iCheck('uncheck');
            $('form[data-toggle="validator"]').bootstrapValidator('addField', 'cost');
        } else {
            $('.standard').slideDown();
            $('#track_quantity').iCheck('check');
            $('#cost').removeAttr('required');
            $('form[data-toggle="validator"]').bootstrapValidator('removeField', 'cost');
        }
        if (t !== 'digital') {
            $('.digital').slideUp();
            $('#file_link').removeAttr('required');
            $('form[data-toggle="validator"]').bootstrapValidator('removeField', 'file_link');
        } else {
            $('.digital').slideDown();
            $('#file_link').attr('required', 'required');
            $('form[data-toggle="validator"]').bootstrapValidator('addField', 'file_link');
        }
        if (t !== 'combo') {
            $('.combo').slideUp();
            //$('#add_item').removeAttr('required');
            //$('form[data-toggle="validator"]').bootstrapValidator('removeField', 'add_item');
        } else {
            $('.combo').slideDown();
            //$('#add_item').attr('required', 'required');
            //$('form[data-toggle="validator"]').bootstrapValidator('addField', 'add_item');
        }
        $("#code").parent('.form-group').addClass("has-error");
        $("#code").focus();
        $("#product_image").parent('.form-group').addClass("text-warning");
        $("#images").parent('.form-group').addClass("text-warning");
        $.ajax({
            type: "get", async: false,
            url: "<?= admin_url('products/getSubCategories') ?>/" + <?= $product->category_id ?>,
            dataType: "json",
            success: function (scdata) {
                if (scdata != null) {
                    $("#subcategory").select2("destroy").empty().attr("placeholder", "<?= lang('select_subcategory') ?>").select2({
                        placeholder: "<?= lang('select_category_to_load') ?>",
                        data: scdata
                    });
                } else {
                    $("#subcategory").select2("destroy").empty().attr("placeholder", "<?= lang('no_subcategory') ?>").select2({
                        placeholder: "<?= lang('no_subcategory') ?>",
                        data: [{id: '', text: '<?= lang('no_subcategory') ?>'}]
                    });
                }
            }
        });
        <?php if ($product->supplier1) { ?>
        select_supplier('supplier1', "<?= $product->supplier1; ?>");
        $('#supplier_price').val("<?= $product->supplier1price == 0 ? '' : $this->sma->formatDecimal($product->supplier1price); ?>");
        <?php } ?>
        <?php if ($product->supplier2) { ?>
        $('#addSupplier').click();
        select_supplier('supplier_2', "<?= $product->supplier2; ?>");
        $('#supplier_2_price').val("<?= $product->supplier2price == 0 ? '' : $this->sma->formatDecimal($product->supplier2price); ?>");
        <?php } ?>
        <?php if ($product->supplier3) { ?>
        $('#addSupplier').click();
        select_supplier('supplier_3', "<?= $product->supplier3; ?>");
        $('#supplier_3_price').val("<?= $product->supplier3price == 0 ? '' : $this->sma->formatDecimal($product->supplier3price); ?>");
        <?php } ?>
        <?php if ($product->supplier4) { ?>
        $('#addSupplier').click();
        select_supplier('supplier_4', "<?= $product->supplier4; ?>");
        $('#supplier_4_price').val("<?= $product->supplier4price == 0 ? '' : $this->sma->formatDecimal($product->supplier4price); ?>");
        <?php } ?>
        <?php if ($product->supplier5) { ?>
        $('#addSupplier').click();
        select_supplier('supplier_5', "<?= $product->supplier5; ?>");
        $('#supplier_5_price').val("<?= $product->supplier5price == 0 ? '' : $this->sma->formatDecimal($product->supplier5price); ?>");
        <?php } ?>
        function select_supplier(id, v) {
            $('#' + id).val(v).select2({
                minimumInputLength: 1,
                data: [],
                initSelection: function (element, callback) {
                    $.ajax({
                        type: "get", async: false,
                        url: "<?= admin_url('suppliers/getSupplier') ?>/" + $(element).val(),
                        dataType: "json",
                        success: function (data) {
                            callback(data[0]);
                        }
                    });
                },
                ajax: {
                    url: site.base_url + "suppliers/suggestions",
                    dataType: 'json',
                    quietMillis: 15,
                    data: function (term, page) {
                        return {
                            term: term,
                            limit: 10
                        };
                    },
                    results: function (data, page) {
                        if (data.results != null) {
                            return {results: data.results};
                        } else {
                            return {results: [{id: '', text: 'No Match Found'}]};
                        }
                    }
                }
            });//.select2("val", "<?= $product->supplier1; ?>");
        }

        var whs = $('.wh');
        $.each(whs, function () {
            $(this).val($('#r' + $(this).attr('id')).text());
        });
    });
    <?php } ?>
    $(document).ready(function () {
        $('#unit').change(function (e) {
            var v = $(this).val();
            if (v) {
                $.ajax({
                    type: "get",
                    async: false,
                    url: "<?= admin_url('products/getSubUnits') ?>/" + v,
                    dataType: "json",
                    success: function (data) {
                        $('#default_sale_unit').select2("destroy").empty().select2({minimumResultsForSearch: 7});
                        $('#default_purchase_unit').select2("destroy").empty().select2({minimumResultsForSearch: 7});
                        $.each(data, function () {
                            $("<option />", {
                                value: this.id,
                                text: this.name + ' (' + this.code + ')'
                            }).appendTo($('#default_sale_unit'));
                            $("<option />", {
                                value: this.id,
                                text: this.name + ' (' + this.code + ')'
                            }).appendTo($('#default_purchase_unit'));
                        });
                        $('#default_sale_unit').select2('val', v);
                        $('#default_purchase_unit').select2('val', v);
                    },
                    error: function () {
                        bootbox.alert('<?= lang('ajax_error') ?>');
                    }
                });
            } else {
                $('#default_sale_unit').select2("destroy").empty();
                $('#default_purchase_unit').select2("destroy").empty();
                $("<option />", {
                    value: '',
                    text: '<?= lang('select_unit_first') ?>'
                }).appendTo($('#default_sale_unit'));
                $("<option />", {
                    value: '',
                    text: '<?= lang('select_unit_first') ?>'
                }).appendTo($('#default_purchase_unit'));
                $('#default_sale_unit').select2({minimumResultsForSearch: 7}).select2('val', '');
                $('#default_purchase_unit').select2({minimumResultsForSearch: 7}).select2('val', '');
            }
        });

        var gallary_unique_code = $('#gallary_unique_code').val();
        var file_up_names = new Array;


    });
</script>

<div class="modal" id="aModal" tabindex="-1" role="dialog" aria-labelledby="aModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">
                    <iclass="fa fa-2x">&times;</i></span><span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title" id="aModalLabel"><?= lang('add_product_manually') ?></h4>
            </div>
            <div class="modal-body" id="pr_popover_content">
                <form class="form-horizontal" role="form">
                    <div class="form-group">
                        <label for="awarehouse" class="col-sm-4 control-label">Supplier Code</label>
                        <div class="col-sm-8">
                            <?php
                            /*$wh[''] = '';
                            foreach ($warehouses as $warehouse) {
                                $wh[$warehouse->id] = $warehouse->name;
                            }
                            echo form_dropdown('warehouse', $wh, '', 'id="awarehouse" class="form-control"');*/
                            ?>

                            <input type="text" class="form-control" id="supplier_code">

                        </div>
                    </div>
                    <div class="form-group">
                        <label for="aquantity" class="col-sm-4 control-label">Supplier Price</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="aquantity">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="aprice" class="col-sm-4 control-label"><?= lang('price_addition') ?></label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="aprice">
                        </div>
                    </div>

                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" id="updateAttr"><?= lang('submit') ?></button>
            </div>
        </div>
    </div>
</div>